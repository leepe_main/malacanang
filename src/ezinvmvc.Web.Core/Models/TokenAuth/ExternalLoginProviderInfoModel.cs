﻿using Abp.AutoMapper;
using ezinvmvc.Authentication.External;

namespace ezinvmvc.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
