﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesLoans.Dto
{
    public class GetEmployeeInput
    {
        public int EmpId { get; set; }
    }
}
