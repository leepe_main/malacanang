﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.EmployeesLoans.Dto;
using ezinvmvc.App.EmployeesLoans.Models;

namespace ezinvmvc.App.EmployeesLoans
{
    public class EmployeeLoansService : ezinvmvcAppServiceBase, IEmployeeLoansService
    {
        private readonly IEmployeeLoansManager _Manager;

        public EmployeeLoansService(IEmployeeLoansManager employeeLoansManager)
        {
            _Manager = employeeLoansManager;
        }

        public async Task CreateEmployeeLoansAsync(CreateEmployeeLoansInput input)
        {
            EmployeeLoans output = Mapper.Map<EmployeeLoans>(input);

            CheckErrors(await _Manager.CreateEmployeeLoansAsync(output));

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteEmployeeLoansAsync(DeleteEmployeeLoansInput input)
        {
            CheckErrors(await _Manager.DeleteEmployeeLoansAsync(input.Id));
        }

        public async Task UpdateEmployeeLoansAsync(UpdateEmployeeLoansInput input)
        {
            EmployeeLoans output = Mapper.Map<UpdateEmployeeLoansInput, EmployeeLoans>(input);
            CheckErrors(await _Manager.UpdateEmployeeLoansAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<PagedResultDto<GetEmployeeLoansOutput>> GetAllEmployeeLoansAsync(GetEmployeeLoansListInput input)
        {
            var resultList = await _Manager.GetAllEmployeeLoansAsync(input.Filter);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetEmployeeLoansOutput>(listcount, ObjectMapper.Map<List<GetEmployeeLoansOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetEmployeeLoansOutput>> GetEmployeeLoansById(GetEmployeeLoansInput input)
        {
            var resultList = await _Manager.GetEmployeeLoansIdAsync(input.Id);
            int listcount = 0;
            var output = new PagedResultDto<GetEmployeeLoansOutput>(listcount, ObjectMapper.Map<List<GetEmployeeLoansOutput>>(resultList));
            return new PagedResultDto<GetEmployeeLoansOutput>(listcount, ObjectMapper.Map<List<GetEmployeeLoansOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetEmployeeLoansOutput>> GetEmployeeDetailsAsync(GetEmployeeLoansInput input)
        {
            var resultList = await _Manager.GetEmployeeDetailAsync(input.Id);
            int listcount = 0;
            return new PagedResultDto<GetEmployeeLoansOutput>(listcount, ObjectMapper.Map<List<GetEmployeeLoansOutput>>(resultList));
        }
    }
}
