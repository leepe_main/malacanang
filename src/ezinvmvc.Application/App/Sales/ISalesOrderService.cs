﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.Sales.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ezinvmvc.App.Sales
{
  public interface ISalesOrderService : IApplicationService
    {
        Task<int> CreateSalesOrder(CreateSalesOrderInput input);
        Task<PagedResultDto<SalesOrderOutput>> GetSalesOrders(GetSalesOrdersInput input);
        Task<GetSalesOrderOutput> GetSalesOrder(GetSalesOrderInput input);
        Task<int> UpdateSalesOrder(UpdateSalesOrderInput input);
        //Task DeleteVendor(DeleteSalesOrderInput input);

        Task<PagedResultDto<SalesOrderItemOutput>> GetSalesOrderItemsByParentId(GetSalesOrderInput input);
        Task<PagedResultDto<SalesOrderChargeOutput>> GetSalesOrderChargesByParentId(GetSalesOrderInput input);

    }
}
