﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ezinvmvc.App.Sales.DTO
{
  public  class SalesOrderOutput : FullAuditedEntity<int>
    {
        public int CompanyId { get; set; }

        public int SeriesTypeId { get; set; }

        public string Prefix { get; set; }

        public string Code { get; set; }

        public DateTime TransactionTime { get; set; }

        public DateTime DeliveryTime { get; set; }

        public int ClientId { get; set; }

        public string ClientOrderNo { get; set; }

        public int QuotationId { get; set; }

        public int OrderTypeId { get; set; }

        public int SalesAgentId { get; set; }

        public string Notes { get; set; }

        public int StatusId { get; set; }

        public int TaxTypeId { get; set; }

        public decimal SubTotal { get; set; }

        public decimal OtherDiscount { get; set; }

        public decimal NetTotal { get; set; }

        public decimal TaxRate { get; set; }

        public decimal Tax { get; set; }

        public decimal GrandTotal { get; set; }
        [NotMapped]
        public string SalesAgent { get; set; }
        [NotMapped]
        public string QuotationCode { get; set; }
        [NotMapped]
        public string Client { get; set; }
        [NotMapped]
        public string Status { get; set; }
        [NotMapped]
        public int TotalRows { get; set; }
    }
}
