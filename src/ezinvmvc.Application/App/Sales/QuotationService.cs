﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Sales.DTO;
using Abp.Authorization;
using ezinvmvc.Authorization;
using ezinvmvc.Dto;
using ezinvmvc.App.Common;
using ezinvmvc.App.Sales.Models;

namespace ezinvmvc.App.Sales
{
    //[AbpAuthorize(PermissionNames.Pages_Quotations)]
    public class QuotationService : ezinvmvcAppServiceBase, IQuotationService
    {
        private readonly IQuotationManager _quotationManager;
        private readonly IQuotationItemManager _quotationItemManager;
        private readonly IQuotationChargeManager _quotationChargeManager;
        private readonly IRFQManager _rfqManager;
        private readonly ISeriesTypeManager _seriesTypeManager;
        private readonly ITasksManager _taskManager;

        public QuotationService(IQuotationManager quotationmanager, IQuotationItemManager quotationitemmanager, IRFQManager rfqManager, ISeriesTypeManager seriestypemanager, IQuotationChargeManager quotationChargeManager, ITasksManager taskManager)
        {
            _quotationManager = quotationmanager;
            _quotationItemManager = quotationitemmanager;
            _rfqManager = rfqManager;
            _seriesTypeManager = seriestypemanager;
            _quotationChargeManager = quotationChargeManager;
            _taskManager = taskManager;
        }
        public async Task<int> CreateQuotation(CreateQuotationInput input)
        {
            var updaterfqstatus = await _rfqManager.GetByIdAsync(input.quotation.RequestId);
            updaterfqstatus.StatusId = 4;

            CheckErrors(await _rfqManager.UpdateAsync(updaterfqstatus));


            var updaterfqassigns = await _taskManager.GetAllByParentIdAsync(input.quotation.RequestId);

            foreach (Tasks task in updaterfqassigns)
            {
                task.Status = 1;
                CheckErrors(await _taskManager.UpdateAsync(task));
            }

            if (updaterfqassigns.Count() > 0)
            {
                var task = updaterfqassigns.First();
                task.StatusID = 1;
                 CheckErrors(await _taskManager.UpdateAsync(task));
            }

            CheckErrors(await _rfqManager.UpdateAsync(updaterfqstatus));

            //series
            var seriestype = await _seriesTypeManager.GetByIdAsync(input.quotation.SeriesTypeId);
            int nextseries = seriestype.LastSeries + 1;
            string seriescode = seriestype.Prefix + nextseries.ToString().PadLeft(seriestype.Padding, '0');
            seriestype.LastSeries = nextseries;
            CheckErrors(await _seriesTypeManager.UpdateAsync(seriestype));
            input.quotation.Code = seriescode;
            //series
            Quotation orderoutput = Mapper.Map<Quotation>(input.quotation);
            CheckErrors(await _quotationManager.CreateAsync(orderoutput));

            foreach (QuotationItemInput item in input.quotationitems)
            {
                item.QuotationId = orderoutput.Id;
                QuotationItem orderitemoutput = Mapper.Map<QuotationItem>(item);
                CheckErrors(await _quotationItemManager.CreateAsync(orderitemoutput));
            }

            foreach (QuotationChargeInput charge in input.quotationcharges)
            {
                charge.QuotationId = orderoutput.Id;
                QuotationCharge orderchargeoutput = Mapper.Map<QuotationCharge>(charge);
                CheckErrors(await _quotationChargeManager.CreateAsync(orderchargeoutput));
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            return orderoutput.Id;
        }

        public async Task<GetQuotationOutput> GetQuotation(GetQuotationInput input)
        {
            var getbyid = await _quotationManager.GetByIdAsync(input.Id);
            return Mapper.Map<GetQuotationOutput>(getbyid);
        }

        public async Task<PagedResultDto<QuotationItemOutput>> GetQuotationItemsByParentId(GetQuotationInput input)
        {
            var resultList = await _quotationItemManager.GetAllByParentId(input.Id);
            int listcount = 0;
            return new PagedResultDto<QuotationItemOutput>(listcount, ObjectMapper.Map<List<QuotationItemOutput>>(resultList));
        }
        public async Task<PagedResultDto<QuotationChargeOutput>> GetQuotationChargesByParentId(GetQuotationInput input)
        {
            var resultList = await _quotationChargeManager.GetAllByParentId(input.Id);
            int listcount = 0;
            return new PagedResultDto<QuotationChargeOutput>(listcount, ObjectMapper.Map<List<QuotationChargeOutput>>(resultList));
        }
        public async Task<PagedResultDto<QuotationOutput>> GetQuotations(GetQuotationsInput input)
        {
            var resultList = await _quotationManager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<QuotationOutput>(listcount, ObjectMapper.Map<List<QuotationOutput>>(resultList));
        }

        public async Task<IEnumerable<QuotationOutput>> GetQuotationRevisions(GetQuotationsInput input)
        {
            var resultList = await _quotationManager.GetAllRevisionList(input.Filter, input.Sorting);
            //int listcount = 0;
            //if (resultList.Count() > 0)
            //{
            //    listcount = resultList.First().TotalRows;
            //}
            return Mapper.Map<List<QuotationOutput>>(resultList);
        }

        public async Task<int> UpdateQuotation(UpdateQuotationInput input)
        {
            Quotation updatequotation = Mapper.Map<Quotation>(input.quotation);
            Quotation createquotation = Mapper.Map<Quotation>(input.quotation);
            var reuturnint = 0;

            if (updatequotation.StatusId == 5)
            {
                var updatequotationstatus = await _quotationManager.GetByIdAsync(updatequotation.Id);
                updatequotationstatus.StatusId = 5;

                CheckErrors(await _quotationManager.UpdateAsync(updatequotationstatus));

                createquotation.Id = 0;
                createquotation.RevisionNo = createquotation.RevisionNo + 1;
                createquotation.StatusId = 1;
              
                CheckErrors(await _quotationManager.CreateAsync(createquotation));
                reuturnint = createquotation.Id;
                foreach (QuotationItemInput item in input.quotationitems)
                {
                    item.QuotationId = createquotation.Id;
                    QuotationItem orderitemoutput = Mapper.Map<QuotationItem>(item);
                    orderitemoutput.Id = 0;
                    CheckErrors(await _quotationItemManager.CreateAsync(orderitemoutput));
                }
                foreach (QuotationChargeInput charge in input.quotationcharges)
                {
                    charge.QuotationId = createquotation.Id;
                    QuotationCharge orderchargeoutput = Mapper.Map<QuotationCharge>(charge);
                    orderchargeoutput.Id = 0;
                    CheckErrors(await _quotationChargeManager.CreateAsync(orderchargeoutput));
                }
            }
            else
            {
                CheckErrors(await _quotationManager.UpdateAsync(updatequotation));
                reuturnint = updatequotation.Id;
                foreach (QuotationItemInput item in input.quotationitems)
                {
                    item.QuotationId = updatequotation.Id;
                    QuotationItem orderitemoutput = Mapper.Map<QuotationItem>(item);
                    if (item.IsDeleted == true)
                    {
                        CheckErrors(await _quotationItemManager.DeleteAsync(orderitemoutput.Id));
                    }
                    else
                    {
                        if (item.Id > 0)
                        {
                            CheckErrors(await _quotationItemManager.UpdateAsync(orderitemoutput));
                        }
                        else
                        {
                            CheckErrors(await _quotationItemManager.CreateAsync(orderitemoutput));
                        }
                    }
                }
                
                foreach (QuotationChargeInput charge in input.quotationcharges)
                {
                    charge.QuotationId = updatequotation.Id;
                    QuotationCharge orderitemoutput = Mapper.Map<QuotationCharge>(charge);
                    if (charge.IsDeleted == true)
                    {
                        CheckErrors(await _quotationChargeManager.DeleteAsync(orderitemoutput.Id));
                    }
                    else
                    {
                        if (charge.Id > 0)
                        {
                            CheckErrors(await _quotationChargeManager.UpdateAsync(orderitemoutput));
                        }
                        else
                        {
                            CheckErrors(await _quotationChargeManager.CreateAsync(orderitemoutput));
                        }
                    }
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            return reuturnint;
        }

      
    }
}
