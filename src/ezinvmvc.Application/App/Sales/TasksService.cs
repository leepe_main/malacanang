﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Sales.DTO;
using ezinvmvc.App.Sales.Models;

namespace ezinvmvc.App.Sales
{
    public class TasksService : ezinvmvcAppServiceBase, ITasksService
    {
        private readonly ITasksManager _tasksManager;

        public TasksService(ITasksManager tasksManager)
        {
            _tasksManager = tasksManager;
        }

        public async Task<int> CreateTasks(CreateTasksInput input)
        {
            //input.ReferenceId = 3;
            //input.TransactionCode = "3";
            //input.ReferenceTransactionCode = "3";
            //input.Status = 0;
            Tasks tasksoutput = Mapper.Map<Tasks>(input.tasks);
            CheckErrors(await _tasksManager.CreateAsync(tasksoutput));
            await CurrentUnitOfWork.SaveChangesAsync();
            return tasksoutput.Id;
        }

        public async Task<PagedResultDto<GetTasksOutput>> GetTaskByParentId(GetTasksInput input)
        {
            var resultList = await _tasksManager.GetAllByParentIdAsync(input.Id);
            int listcount = 0;
            return new PagedResultDto<GetTasksOutput>(listcount, ObjectMapper.Map<List<GetTasksOutput>>(resultList));
        }

        public async Task<GetTasksOutput> GetTasks(GetTasksInput input)
        {
            var getbyid = await _tasksManager.GetByIdAsync(input.Id);
            return Mapper.Map<GetTasksOutput>(getbyid);

        }

        public async Task<PagedResultDto<TasksOutput>> GetTasksall(GetTasksListInput input)
        {
            var resultList = await _tasksManager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<TasksOutput>(listcount, ObjectMapper.Map<List<TasksOutput>>(resultList));
        }

        public async Task<int> UpdateTasks(UpdateTasksInput input)
        {
            Tasks output = Mapper.Map<Tasks>(input.tasks);
            CheckErrors(await _tasksManager.UpdateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
            return output.Id;
        }
    }
}
