﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Sales.DTO;
using Abp.Authorization;
using ezinvmvc.Authorization;
using ezinvmvc.Dto;
using ezinvmvc.App.Common;
using ezinvmvc.App.Clients;
using System;

namespace ezinvmvc.App.Sales
{
    [AbpAuthorize(PermissionNames.Pages_Sales_Orders)]
    public class SalesOrderService : ezinvmvcAppServiceBase, ISalesOrderService
    {
        private readonly ISalesOrderManager _orderManager;
        private readonly ISalesOrderItemManager _orderItemManager;
        private readonly ISalesOrderChargeManager _orderChargeManager;
        private readonly ISeriesTypeManager _seriesTypeManager;
        private readonly IQuotationManager _quotationManager;
        private readonly IClientManager _clientManager;

        public SalesOrderService(ISalesOrderManager ordermanager, ISalesOrderItemManager orderitemmanager, ISalesOrderChargeManager orderchargemanager, ISeriesTypeManager seriestypemanager, IQuotationManager quotationmanager, IClientManager clientManager)
        {
            _orderManager = ordermanager;
            _orderItemManager = orderitemmanager;
            _orderChargeManager = orderchargemanager;
            _seriesTypeManager = seriestypemanager;
            _quotationManager = quotationmanager;
            _clientManager = clientManager;
        }
        //Sales Order 
        public async Task<int> CreateSalesOrder(CreateSalesOrderInput input)
        {
            //quotation
            var quotation = await _quotationManager.GetByIdAsync(input.salesorder.QuotationId);
            if (quotation.StatusId != 6)
            {
                return 0;
            }
            quotation.StatusId = 4;
            CheckErrors(await _quotationManager.UpdateAsync(quotation));
            //quotation
            //series
            var seriestype = await _seriesTypeManager.GetByIdAsync(input.salesorder.SeriesTypeId);
            int nextseries = seriestype.LastSeries + 1;
            string seriescode = seriestype.Prefix + nextseries.ToString().PadLeft(seriestype.Padding,'0');
            seriestype.LastSeries = nextseries;
            CheckErrors(await _seriesTypeManager.UpdateAsync(seriestype));
            input.salesorder.Code = seriescode;
            //series
            //client status
            var updateclientstatus = await _clientManager.GetByIdAsync(input.salesorder.ClientId);
            if (updateclientstatus.StatusId != 4)
            {
                updateclientstatus.StatusId = 4;

                CheckErrors(await _clientManager.UpdateAsync(updateclientstatus));
            }
            SalesOrder orderoutput = Mapper.Map<SalesOrder>(input.salesorder);
            CheckErrors(await _orderManager.CreateAsync(orderoutput));
            foreach (SalesOrderItemInput item in input.salesorderitems)
            {
                item.SalesOrderId = orderoutput.Id;
                SalesOrderItem orderitemoutput = Mapper.Map<SalesOrderItem>(item);
                CheckErrors(await _orderItemManager.CreateAsync(orderitemoutput));
            }
            foreach (SalesOrderChargeInput charge in input.salesordercharges)
            {
                charge.SalesOrderId = orderoutput.Id;
                SalesOrderCharge orderchargeoutput = Mapper.Map<SalesOrderCharge>(charge);
                CheckErrors(await _orderChargeManager.CreateAsync(orderchargeoutput));
            }
            try
            {
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception ex) {
            }
            return orderoutput.Id;
        }
        public async Task<GetSalesOrderOutput> GetSalesOrder(GetSalesOrderInput input)
        {
            var getbyid = await _orderManager.GetByIdAsync(input.Id);
            return Mapper.Map<GetSalesOrderOutput>(getbyid);
        }

        public async Task<PagedResultDto<SalesOrderOutput>> GetSalesOrders(GetSalesOrdersInput input)
        {
            var resultList = await _orderManager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<SalesOrderOutput>(listcount, ObjectMapper.Map<List<SalesOrderOutput>>(resultList));
        }

        

        public async Task<int> UpdateSalesOrder(UpdateSalesOrderInput input)
        {
            SalesOrder orderoutput = Mapper.Map<SalesOrder>(input.salesorder);

            CheckErrors(await _orderManager.UpdateAsync(orderoutput));

            foreach (SalesOrderItemInput item in input.salesorderitems)
            {
                item.SalesOrderId = orderoutput.Id;
                SalesOrderItem orderitemoutput = Mapper.Map<SalesOrderItem>(item);
                if (item.IsDeleted == true)
                {
                        CheckErrors(await _orderItemManager.DeleteAsync(orderitemoutput.Id));
                }
                else
                {
                    if (item.Id > 0)
                    {
                        CheckErrors(await _orderItemManager.UpdateAsync(orderitemoutput));
                    }
                    else
                    {
                        CheckErrors(await _orderItemManager.CreateAsync(orderitemoutput));
                    }
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();

            return orderoutput.Id;
        }

        //Sales Order Items
        public async Task<PagedResultDto<SalesOrderItemOutput>> GetSalesOrderItemsByParentId(GetSalesOrderInput input)
        {
            var resultList = await _orderItemManager.GetAllByParentId(input.Id);
            int listcount = 0;
            return new PagedResultDto<SalesOrderItemOutput>(listcount, ObjectMapper.Map<List<SalesOrderItemOutput>>(resultList));
        }

        public async Task<PagedResultDto<SalesOrderChargeOutput>> GetSalesOrderChargesByParentId(GetSalesOrderInput input)
        {
            var resultList = await _orderChargeManager.GetAllByParentId(input.Id);
            int listcount = 0;
            return new PagedResultDto<SalesOrderChargeOutput>(listcount, ObjectMapper.Map<List<SalesOrderChargeOutput>>(resultList));
        }
    }
}
