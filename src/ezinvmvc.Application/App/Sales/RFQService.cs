﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using AutoMapper;
using ezinvmvc.App.Common;
using ezinvmvc.App.Sales.Dto;
using ezinvmvc.App.Sales.DTO;
using ezinvmvc.App.Sales.Models;
using ezinvmvc.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.Sales
{
    //[AbpAuthorize(PermissionNames.Pages_Rfq)]
    public class RFQService : ezinvmvcAppServiceBase, IRFQService
    {
        private readonly IRFQManager _rfqManager;
        private readonly ISeriesTypeManager _seriesTypeManager;
        private readonly IRFQDetailsManager _rfqDetailsManager;

        public RFQService(IRFQManager rfqManager, ISeriesTypeManager seriesTypeManager, IRFQDetailsManager rfqDetailsManager)
        {
            _rfqManager = rfqManager;
            _rfqDetailsManager = rfqDetailsManager;
            _seriesTypeManager = seriesTypeManager;
        }

        public async Task<int> CreateRFQ(CreateRFQInput input)
        {
            //series
            var seriestype = await _seriesTypeManager.GetByIdAsync(input.rfq.SeriesTypeId);
            int nextseries = seriestype.LastSeries + 1;
            string seriescode = seriestype.Prefix + nextseries.ToString().PadLeft(seriestype.Padding, '0');
            seriestype.LastSeries = nextseries;
            CheckErrors(await _seriesTypeManager.UpdateAsync(seriestype));
            input.rfq.Code = seriescode;
            //series
            RFQ rfqoutput = Mapper.Map<RFQ>(input.rfq);
            CheckErrors(await _rfqManager.CreateAsync(rfqoutput));

            foreach (RFQDetailsInput item in input.rfqdetails)
            {
                item.RFQId = rfqoutput.Id;
                RFQDetails rfqdetailsoutput = Mapper.Map<RFQDetails>(item);
                CheckErrors(await _rfqDetailsManager.CreateAsync(rfqdetailsoutput));
            }

            await CurrentUnitOfWork.SaveChangesAsync();

            return rfqoutput.Id;
        }

        public async Task<GetRFQOutput> GetRFQ(GetRFQInput input)
        {
            var getbyid = await _rfqManager.GetByIdAsync(input.Id);
            return Mapper.Map<GetRFQOutput>(getbyid);
        }

        public async Task<PagedResultDto<RFQDetailsOutput>> GetRfqDetailsByParentId(GetRFQInput input)
        {
            var resultList = await _rfqDetailsManager.GetAllByParentIdAsync(input.Id);
            int listcount = 0;
            return new PagedResultDto<RFQDetailsOutput>(listcount, ObjectMapper.Map<List<RFQDetailsOutput>>(resultList));
        }

        public async Task<PagedResultDto<RFQOutput>> GetRFQs(GetRFQListInput input)
        {
            var resultList = await _rfqManager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<RFQOutput>(listcount, ObjectMapper.Map<List<RFQOutput>>(resultList));
        }

        public async Task<IEnumerable<RFQOutput>> GetRFQRevisions(GetRFQListInput input)
        {
            var resultList = await _rfqManager.GetAllRevisionList(input.Filter, input.Sorting);
            //int listcount = 0;
            //if (resultList.Count() > 0)
            //{
            //    listcount = resultList.First().TotalRows;
            //}
            return Mapper.Map<List<RFQOutput>>(resultList);
        }

        public async Task<PagedResultDto<RFQOutput>> GetRFQsforQuotation(GetRFQListInput input)
        {
            var resultList = await _rfqManager.GetAllListforQuotation(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<RFQOutput>(listcount, ObjectMapper.Map<List<RFQOutput>>(resultList));
        }

        public async Task<int> UpdateRfq(UpdateRFQInput input)
        {
            RFQ updateqrfq = Mapper.Map<RFQ>(input.rfq);
            RFQ createrfq = Mapper.Map<RFQ>(input.rfq);
            var reuturnint = 0;

            if (updateqrfq.StatusId == 5)
            {
                var updaterfqstatus = await _rfqManager.GetByIdAsync(updateqrfq.Id);
                updaterfqstatus.StatusId = 5;

                CheckErrors(await _rfqManager.UpdateAsync(updaterfqstatus));
                
                createrfq.Id = 0;
                createrfq.RevisionNo = createrfq.RevisionNo + 1;
                createrfq.StatusId = 2;

                CheckErrors(await _rfqManager.CreateAsync(createrfq));
                reuturnint = createrfq.Id;
                foreach (RFQDetailsInput item in input.rfqdetails)
                {
                    item.RFQId = createrfq.Id;
                    RFQDetails orderitemoutput = Mapper.Map<RFQDetails>(item);
                    orderitemoutput.Id = 0;
                    CheckErrors(await _rfqDetailsManager.CreateAsync(orderitemoutput));
                }
            }
            else
            {
                //updateqrfq.RevisionNo = revno;
                CheckErrors(await _rfqManager.UpdateAsync(updateqrfq));
                reuturnint = updateqrfq.Id;
                foreach (RFQDetailsInput item in input.rfqdetails)
                {
                    item.RFQId = updateqrfq.Id;
                    RFQDetails rfqoutput = Mapper.Map<RFQDetails>(item);
                    if (item.IsDeleted == true)
                    {
                        CheckErrors(await _rfqDetailsManager.DeleteAsync(rfqoutput.Id));
                    }
                    else
                    {
                        if (item.Id > 0)
                        {
                            CheckErrors(await _rfqDetailsManager.UpdateAsync(rfqoutput));
                        }
                        else
                        {
                            CheckErrors(await _rfqDetailsManager.CreateAsync(rfqoutput));
                        }
                    }
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            return reuturnint;
        }
    }
}
