﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUserComment.Dto;
using ezinvmvc.App.FolderUserComment.Models;

namespace ezinvmvc.App.FolderUserComment
{
    public class AnnouncementService : ezinvmvcAppServiceBase, IAnnouncementService
    {
        private readonly IAnnouncementManager _manager;
        public AnnouncementService(IAnnouncementManager announcementManager)
        {
            _manager = announcementManager;
        }

        public async Task CreateAnnouncement(CreateAnnouncementInput input)
        {
            Announcement output = Mapper.Map<Announcement>(input);
            CheckErrors(await _manager.CreateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAnnouncement(DeleteAnnouncementtInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.Id));
        }

        public async Task<GetAnnouncementOutput> GetAnnouncementById(GetAnnouncementInput input)
        {
            var getbyid = await _manager.GetAnnouncementAsync(input.Id);
            return Mapper.Map<GetAnnouncementOutput>(getbyid);
        }

        public Task<GetAnnouncementOutput> GetAnnouncementid(GetAnnouncementInput input)
        {
            throw new NotImplementedException();
        }

        public async Task<PagedResultDto<GetAnnouncementOutput>> GetAnnouncementList(GetAnnouncementListInput input)
        {
            var resultList = await _manager.GetAllListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetAnnouncementOutput>(listcount, ObjectMapper.Map<List<GetAnnouncementOutput>>(resultList));
        }
    }
}
