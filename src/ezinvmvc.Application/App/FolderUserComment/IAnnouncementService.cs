﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUserComment.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public interface IAnnouncementService : IApplicationService
    {
        Task CreateAnnouncement(CreateAnnouncementInput input);

        Task DeleteAnnouncement(DeleteAnnouncementtInput input);

        Task<GetAnnouncementOutput> GetAnnouncementById(GetAnnouncementInput input);

        Task<PagedResultDto<GetAnnouncementOutput>> GetAnnouncementList(GetAnnouncementListInput input);

        Task<GetAnnouncementOutput> GetAnnouncementid(GetAnnouncementInput input);
    }
}
