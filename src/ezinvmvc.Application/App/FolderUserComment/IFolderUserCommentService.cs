﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment.Dto
{
    public interface IFolderUserCommentService : IApplicationService
    {
        Task CreateFolderUserCommentAsync(CreateFileCommentInput input);

        Task DeleteFolderUserCommentAsync(DeleteFileCommentInput input);

        Task<GetFileCommentOutput> GetFolderUserCommentByIdAsync(GetFileCommentInput input);

        Task<PagedResultDto<GetFileCommentOutput>> GetFolderUserCommentAsync(GetFileCommentListInput input);

        Task<GetFileCommentOutput> GetTotalCommentFileAsync(GetFileCommentInput input);

        Task<GetFileCommentOutput> GetCommentFileAsync(GetFileCommentInput input);

        Task<GetFileCommentOutput> GetCommentid(GetFileCommentInput input);

        Task<PagedResultDto<GetFileCommentOutput>> GetCommentList(GetFileCommentListInput input);

        Task<GetFileCommentOutput> UpdateCommentFile(GetFileCommentInput input);
    }
}
