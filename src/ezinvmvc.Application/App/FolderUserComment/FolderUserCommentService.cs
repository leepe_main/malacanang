﻿using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUserComment.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public class FolderUserCommentService : ezinvmvcAppServiceBase, IFolderUserCommentService
    {
        private readonly IFileCommentManager _manager;
        public FolderUserCommentService(IFileCommentManager fileCommentManager)
        {
            _manager = fileCommentManager;
        }

        public async Task CreateFolderUserCommentAsync(CreateFileCommentInput input)
        {
            FileComment output = Mapper.Map<FileComment>(input);
            CheckErrors(await _manager.CreateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteFolderUserCommentAsync(DeleteFileCommentInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.Id));
        }        

        public async Task<PagedResultDto<GetFileCommentOutput>> GetFolderUserCommentAsync(GetFileCommentListInput input)
        {
            var resultList = await _manager.GetAllListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetFileCommentOutput>(listcount, ObjectMapper.Map<List<GetFileCommentOutput>>(resultList));
        }

        public async Task<GetFileCommentOutput> GetFolderUserCommentByIdAsync(GetFileCommentInput input)
        {
            var getbyid = await _manager.GetByIdAsync(input.Id);
            return Mapper.Map<GetFileCommentOutput>(getbyid);
        }

        public async Task<GetFileCommentOutput> GetTotalCommentFileAsync(GetFileCommentInput input)
        {
            var getbyid = await _manager.GetTotalCommentAsync(input.Id);
            return Mapper.Map<GetFileCommentOutput>(getbyid);
        }

        public async Task<GetFileCommentOutput> GetCommentFileAsync(GetFileCommentInput input)
        {
            var getbyid = await _manager.GetFileCommentAsync(input.Id);
            return Mapper.Map<GetFileCommentOutput>(getbyid);
        }

        public async Task<GetFileCommentOutput> GetCommentid(GetFileCommentInput input)
        {
            var getbyid = await _manager.GetCommentAsync(input.Id);
            return Mapper.Map<GetFileCommentOutput>(getbyid);
        }

        public async Task<PagedResultDto<GetFileCommentOutput>> GetCommentList(GetFileCommentListInput input)
        {
            var resultList = await _manager.GetAllCommentListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetFileCommentOutput>(listcount, ObjectMapper.Map<List<GetFileCommentOutput>>(resultList));
        }

        public async Task<GetFileCommentOutput> UpdateCommentFile(GetFileCommentInput input)
        {
            var getbyid = await _manager.UpdateFolderIdCommentAsync(input.Id);
            return Mapper.Map<GetFileCommentOutput>(getbyid);
        }
    }
}
