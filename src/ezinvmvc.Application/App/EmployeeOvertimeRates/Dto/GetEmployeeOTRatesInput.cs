﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeeOvertimeRates.Dto
{
    public class GetEmployeeOTRatesInput
    {
        public int EmpId { get; set; }
    }
}
