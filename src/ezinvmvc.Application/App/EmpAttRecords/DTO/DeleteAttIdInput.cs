﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmpAttRecords.DTO
{
    public class DeleteAttIdInput
    {
        public int Id { get; set; }
    }
}
