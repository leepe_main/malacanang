﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Leads.Dto;
using Abp.Authorization;
using ezinvmvc.Authorization;
using ezinvmvc.Dto;
using ezinvmvc.App.Leads.Exporting;
using ezinvmvc.App.Common;
using ezinvmvc.App.Clients;
using ezinvmvc.App.Employees.Dto;
using ezinvmvc.App.Employees.Models;
using ezinvmvc.App.Employees;

namespace ezinvmvc.App.Leads
{
    public class LeadService : ezinvmvcAppServiceBase, ILeadService
    {
        private readonly ILeadManager _manager;
        private readonly ISeriesTypeManager _seriesTypeManager;
        private readonly IClientManager _clientManager;
        private readonly IAccountExecutiveManager _aeManager;
        private readonly ILeadExporter _exporter;

        public LeadService(ILeadManager manager, ISeriesTypeManager seriesTypeManager, IClientManager clientManager, IAccountExecutiveManager aeManager, ILeadExporter exporter)
        {
            _manager = manager;
            _seriesTypeManager = seriesTypeManager;
            _clientManager = clientManager;
            _aeManager = aeManager;
            _exporter = exporter;
        }

        public async Task CreateLead(CreateLeadInput input)
        {
            //series
            var seriestype = await _seriesTypeManager.GetByIdAsync(input.SeriesTypeId);
            int nextseries = seriestype.LastSeries + 1;
            string seriescode = seriestype.Prefix + nextseries.ToString().PadLeft(seriestype.Padding, '0');
            seriestype.LastSeries = nextseries;
            CheckErrors(await _seriesTypeManager.UpdateAsync(seriestype));
            input.Prefix = seriestype.Prefix;
            input.Code = seriescode;

            //Leads
            Lead output = Mapper.Map<Lead>(input);

            CheckErrors(await _manager.CreateAsync(output));
            //Leads

            //Account Executive
            CreateAccountExecutiveInput aeInput = new CreateAccountExecutiveInput();
            aeInput.Reference = "Lead";
            aeInput.ReferenceId = output.Id;
            aeInput.EmployeeId = output.AssignedToId;
            aeInput.AssignedDate = output.CreationTime;
            aeInput.IsActive = true;

            AccountExecutive ae = Mapper.Map<AccountExecutive>(aeInput);

            CheckErrors(await _aeManager.CreateAsync(ae));
            //Account Executive

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteLead(DeleteLeadInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.Id));
        }

        public async Task<GetLeadOutput> GetLead(GetLeadInput input)
        {
            var getbyid = await _manager.GetByIdAsync(input.Id);
            return Mapper.Map<GetLeadOutput>(getbyid);
        }

        public async Task<IEnumerable<GetLeadOutput>> GetLeadDetails(GetLeadInput input)
        {
            var getbyid = await _manager.GetLeadDetailsByIdAsync(input.Id);
            return Mapper.Map<List<GetLeadOutput>>(getbyid);
        }

        public async Task<PagedResultDto<GetLeadOutput>> GetLeads(GetLeadListInput input)
        {
            var resultList = await _manager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetLeadOutput>(listcount, ObjectMapper.Map<List<GetLeadOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetLeadOutput>> GetLeadsforRFQ(GetLeadListInput input)
        {
            var resultList = await _manager.GetAllListforRFQ(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetLeadOutput>(listcount, ObjectMapper.Map<List<GetLeadOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetLeadOutput>> GetLeadsforRFQforEdit(GetLeadListInput input)
        {
            var resultList = await _manager.GetAllListforRFQforEdit(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetLeadOutput>(listcount, ObjectMapper.Map<List<GetLeadOutput>>(resultList));
        }

        public async Task<FileDto> GetLeadsToExcel(GetLeadListInput input)
        {
            var resultList = await _manager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, true);
            var resultToExport = ObjectMapper.Map<List<GetLeadOutput>>(resultList);
            var result = _exporter.ExportToFile(resultToExport);
            return result;
        }

        public async Task UpdateLead(UpdateLeadInput input)
        {
            Lead output = Mapper.Map<UpdateLeadInput, Lead>(input);
            if(input.StatusId == 2)
            {
                var updateclientstatus = await _clientManager.GetByIdAsync(int.Parse(input.ClientId));
                if (updateclientstatus.StatusId == 1)
                {
                    updateclientstatus.StatusId = 3;

                    CheckErrors(await _clientManager.UpdateAsync(updateclientstatus));
                }
            }
            CheckErrors(await _manager.UpdateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
    }
}
