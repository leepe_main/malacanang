﻿using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Employees;
using ezinvmvc.App.Employees.Dto;
using ezinvmvc.App.Employees.Models;
using ezinvmvc.App.Leads.Dto;
using ezinvmvc.App.Leads.Exporter;
using ezinvmvc.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.Leads
{
    public class LeadUpdateService : ezinvmvcAppServiceBase, ILeadUpdateService
    {
        private readonly ILeadUpdateManager _manager;
        private readonly IAccountExecutiveManager _aeManager;
        private readonly ILeadUpdateExporter _exporter;

        public LeadUpdateService(ILeadUpdateManager manager, IAccountExecutiveManager aeManager, ILeadUpdateExporter exporter)
        {
            _manager = manager;
            _aeManager = aeManager;
            _exporter = exporter;
        }

        public async Task CreateLeadUpdate(CreateLeadUpdateInput input)
        {
            //input.LeadUpdateDate = Convert.ToDateTime(Convert.ToDateTime(input.LeadUpdateDate.ToString()).ToString("MM/dd/yyyy") + " " + System.DateTime.Now.ToString("hh:mm tt"));
            LeadUpdate output = Mapper.Map<LeadUpdate>(input);

            CheckErrors(await _manager.CreateAsync(output));

            //AccountExecutive
            var oldAE = await _aeManager.GetByReferenceIdAsync(output.LeadId, "Lead");
            if (oldAE == null)
            {
                CreateAccountExecutiveInput aeInput = new CreateAccountExecutiveInput();
                aeInput.Reference = "Lead";
                aeInput.ReferenceId = output.LeadId;
                aeInput.EmployeeId = output.AssignedToId;
                aeInput.AssignedDate = System.DateTime.Now;
                aeInput.IsActive = true;

                AccountExecutive ae = Mapper.Map<AccountExecutive>(aeInput);

                CheckErrors(await _aeManager.CreateAsync(ae));
            }
            else if (oldAE.EmployeeId != output.AssignedToId)
            {
                oldAE.IsActive = false;

                AccountExecutive updateoldae = Mapper.Map<AccountExecutive>(oldAE);

                CheckErrors(await _aeManager.UpdateAsync(updateoldae));

                CreateAccountExecutiveInput aeInput = new CreateAccountExecutiveInput();
                aeInput.Reference = "Lead";
                aeInput.ReferenceId = output.LeadId;
                aeInput.EmployeeId = output.AssignedToId;
                aeInput.AssignedDate = output.CreationTime;
                aeInput.IsActive = true;

                AccountExecutive ae = Mapper.Map<AccountExecutive>(aeInput);

                CheckErrors(await _aeManager.CreateAsync(ae));
            }
            else
            {
                //oldAddress.AddressDescription = input.Address;

                AccountExecutive updateae = Mapper.Map<AccountExecutive>(oldAE);

                CheckErrors(await _aeManager.UpdateAsync(updateae));
            }
            //AccountExecutive

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteLeadUpdate(DeleteLeadUpdateInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.Id));
        }

        public async Task<GetLeadUpdateOutput> GetLeadUpdate(GetLeadUpdateInput input)
        {
            var getbyid = await _manager.GetByIdAsync(input.Id);
            return Mapper.Map<GetLeadUpdateOutput>(getbyid);
        }

        public async Task<PagedResultDto<GetLeadUpdateOutput>> GetLeadUpdates(GetLeadUpdateListInput input)
        {
            var resultList = await _manager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetLeadUpdateOutput>(listcount, ObjectMapper.Map<List<GetLeadUpdateOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetLeadUpdateOutput>> GetLeadUpdatesByLeadId(GetLeadUpdateListByLeadIdInput input)
        {
            var resultList = await _manager.GetAllListByLeadId(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, false);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetLeadUpdateOutput>(listcount, ObjectMapper.Map<List<GetLeadUpdateOutput>>(resultList));
        }

        public async Task<FileDto> GetLeadUpdatesToExcel(GetLeadUpdateListInput input)
        {
            var resultList = await _manager.GetAllList(input.Filter, input.Sorting, input.SkipCount, input.MaxResultCount, true);
            var resultToExport = ObjectMapper.Map<List<GetLeadUpdateOutput>>(resultList);
            var result = _exporter.ExportToFile(resultToExport);
            return result;
        }

        public async Task UpdateLeadUpdate(UpdateLeadUpdateInput input)
        {
            LeadUpdate output = Mapper.Map<UpdateLeadUpdateInput, LeadUpdate>(input);
            CheckErrors(await _manager.UpdateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
    }
}
