﻿
namespace ezinvmvc.App.Products.Dto
{
    public class GetProductPricesInput
    {
        public int ProductId { get; set; }
        public int PricingTypeId { get; set; }
        public int UnitId { get; set; }
    }
}
