﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.Jobs.Dto;
using System.Threading.Tasks;

namespace ezinvmvc.App.Jobs
{
    public interface IJobsService : IApplicationService
    {
        Task CreateJobs(CreateJobsInput input);

        Task UpdateJobsAsync(UpdateJobsInput input);

        Task DeleteJobsAsync(DeleteJobsInput input);

        Task<PagedResultDto<GetJobsOutput>> GetJobsAsync (GetJobListInput input);

        Task<PagedResultDto<GetJobsOutput>> UpdateJobsStatusAsync(UpdateJobsInput input);

    }
}
