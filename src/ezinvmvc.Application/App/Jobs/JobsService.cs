﻿using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.Jobs
{
    public class JobsService : ezinvmvcAppServiceBase, IJobsService
    {
        private readonly IJobsManager _Manager;

        public JobsService(IJobsManager jobsManager)
        {
            _Manager = jobsManager;
        }

        public async Task CreateJobs(CreateJobsInput input)
        {
            Jobs output = Mapper.Map<Jobs>(input);

            CheckErrors(await _Manager.CreateJobsAsync(output));

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteJobsAsync(DeleteJobsInput input)
        {
            CheckErrors(await _Manager.DeleteJobsAsync(input.Id));
        }

        public async Task<PagedResultDto<GetJobsOutput>> GetJobsAsync(GetJobListInput input)
        {
            var resultList = await _Manager.GetJobsAsync(input.Filter);
            int listcount = 0;
            if (resultList.Count() > 0)
            {
                listcount = resultList.First().TotalRows;
            }
            return new PagedResultDto<GetJobsOutput>(listcount, ObjectMapper.Map<List<GetJobsOutput>>(resultList));
        }

        public async Task UpdateJobsAsync(UpdateJobsInput input)
        {
            Jobs output = Mapper.Map<UpdateJobsInput, Jobs>(input);
            CheckErrors(await _Manager.UpdateJobsAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public Task<PagedResultDto<GetJobsOutput>> UpdateJobsStatusAsync(UpdateJobsInput input)
        {
            throw new NotImplementedException();
        }
    }
}
