﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesAccountsAbility.Dto
{
    public class GetAccountAbilityInput
    {
        public int EmpId { get; set; }
    }
}
