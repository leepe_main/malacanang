﻿using Abp.Runtime.Validation;
using ezinvmvc.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.OvertimeRates.Dto
{
    public class GetOvertimeRateInput
    {
        public int Id { get; set; }
    }
}
