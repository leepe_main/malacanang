﻿namespace ezinvmvc.App.GroupTypes.Dto
{
    public class GetGroupTypeGroupInput
    {
        public string GroupType { get; set; }

        public string Group { get; set; }
    }
}
