﻿namespace ezinvmvc.App.GroupTypes.Dto
{
    class GetGroupTypeGroup
    {
        public string GroupTypes { get; set; }

        public string Group { get; set; }
    }
}
