﻿namespace ezinvmvc.App.GroupTypes.Dto
{
    public class GetGroupTypeInput
    {
        public int Id { get; set; }
    }
}
