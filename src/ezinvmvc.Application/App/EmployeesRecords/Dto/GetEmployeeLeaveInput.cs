﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesRecords.Dto
{
    public class GetEmployeeLeaveInput
    {
        public int EmpId { get; set; }
    }
}
