﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesRecords.Dto
{
    public class GetEmployeeAttendanceInput
    {
        public int EmpId { get; set; }
    }
}
