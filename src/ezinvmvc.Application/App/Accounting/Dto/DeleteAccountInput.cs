﻿
namespace ezinvmvc.App.Accounting.Dto
{
   public class DeleteAccountInput
    {
        public int Id { get; set; }
    }
}
