﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.Accounting.Dto
{
    public class UpdateJournalEntryInput : FullAuditedEntity<int>
    {
        public JournalEntryInput JournalEntry { get; set; }
        public List<JournalEntryItemInput> JournalEntryItems { get; set; }
    }
}
