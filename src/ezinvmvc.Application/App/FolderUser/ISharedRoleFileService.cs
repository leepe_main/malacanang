﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface ISharedRoleFileService : IApplicationService
    {
        Task CreateSharedRoleAsync(CreateSharedRoleFileInput input);

        Task DeleteSharedRoleAsync(DeleteSharedRoleFileInput input);

        Task<PagedResultDto<GetSharedRoleFileOutput>> GetSharedRoleListAsync(GetSharedRoleFileListInput input);

        Task<GetSharedRoleFileOutput> GetUserRoleAsync(GetFolderUsersInput input);

        Task<GetSharedRoleFileOutput> GetTotalSharedRoleFileAsync(GetFolderUsersInput input);

        Task<PagedResultDto<GetSharedRoleFileOutput>> GetShareROleFileList(GetSharedRoleFileListInput input);

        Task<GetSharedRoleFileOutput> GetUserAsync(GetFolderUsersInput input);
    }
}
