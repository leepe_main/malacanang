﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUser.Dto;

namespace ezinvmvc.App.FolderUser
{
    public class NewFolderService : ezinvmvcAppServiceBase, INewFolderService
    {
        private readonly INewFolderManager _manager;

        public NewFolderService(INewFolderManager newFolderManager)
        {
            _manager = newFolderManager;
        }

        public async Task CreateNewFolderAsync(CreateNewFolderInput input)
        {
            NewFolder output = Mapper.Map<NewFolder>(input);
            CheckErrors(await _manager.Create(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteNewFolderAsync(DeleteNewFolderInput input)
        {
            CheckErrors(await _manager.Delete(input.Id));
        }

        public async Task<GetNewFolderOutput> GetNewFolderbyIdAsync(GetNewFolderInput input)
        {
            var getbyid = await _manager.GetByIdAsync(input.Id);
            return Mapper.Map<GetNewFolderOutput>(getbyid);
        }

        public async Task<PagedResultDto<GetNewFolderOutput>> GetNewFolderListAsync(GetNewFolderListInput input)
        {
            var resultList = await _manager.GetAllListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetNewFolderOutput>(listcount, ObjectMapper.Map<List<GetNewFolderOutput>>(resultList));
        }
    }
}
