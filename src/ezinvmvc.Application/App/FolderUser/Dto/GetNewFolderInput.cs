﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.FolderUser.Dto
{
    public class GetNewFolderInput
    {
        public int Id { get; set; }
    }
}
