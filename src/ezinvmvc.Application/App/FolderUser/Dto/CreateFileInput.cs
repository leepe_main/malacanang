﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.FolderUser.Dto
{
    public class CreateFileInput
    {
        public List<CreateFolderUsersInput> foldersusersinput { get; set; }
    }
}
