﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class NotificationService : ezinvmvcAppServiceBase, INotificationService
    {
        private readonly INotificationManager _manager;

        public NotificationService(INotificationManager notificationManager)
        {
            _manager = notificationManager;
        }

        public async Task CreateNotificationUserAsync(CreateNotificationUsersInput input)
        {
            Notification output = Mapper.Map<Notification>(input);
            CheckErrors(await _manager.CreateShareUserAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task<PagedResultDto<GetNotificationCountUserOutput>> GetNotificationCountListAsync(GetFolderUsersListInput input)
        {
            var resultList = await _manager.GetShareUSerNotificationListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetNotificationCountUserOutput>(listcount, ObjectMapper.Map<List<GetNotificationCountUserOutput>>(resultList));
        }

        public async Task<GetNotificationCountUserOutput> GetNotificationCountUserAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetShareUserCountNotificationAsync(input.Id);
            return Mapper.Map<GetNotificationCountUserOutput>(getbyid);
        }

        public async Task<GetNotificationCountUserOutput> GetNotificationCountRoleAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetShareRoleCountNotificationAsync(input.Id);
            return Mapper.Map<GetNotificationCountUserOutput>(getbyid);
        }
        
        public async Task<PagedResultDto<GetNotificationCountUserOutput>> GetNotificationCountRoleListAsync(GetFolderUsersListInput input)
        {
            var resultList = await _manager.GetShareRoleNotificationListAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetNotificationCountUserOutput>(listcount, ObjectMapper.Map<List<GetNotificationCountUserOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetNotificationCountUserOutput>> GetTop5DownloadListAsync()
        {
            var resultList = await _manager.GetTopDownloadListAsync();
            int listcount = 0;
            return new PagedResultDto<GetNotificationCountUserOutput>(listcount, ObjectMapper.Map<List<GetNotificationCountUserOutput>>(resultList));
        }
    }
}
