﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUser.Dto;

namespace ezinvmvc.App.FolderUser
{
    public class SharedRoleFileService : ezinvmvcAppServiceBase, ISharedRoleFileService
    {
        private readonly ISharedRoleFileManager _manager;

        public SharedRoleFileService(ISharedRoleFileManager sharedrolefilemanager)
        {
            _manager = sharedrolefilemanager;
        }

        public async Task CreateSharedRoleAsync(CreateSharedRoleFileInput input)
        {
            SharedRoleFile output = Mapper.Map<SharedRoleFile>(input);
            CheckErrors(await _manager.Create(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteSharedRoleAsync(DeleteSharedRoleFileInput input)
        {
            CheckErrors(await _manager.Delete(input.RoleId, input.FileId));
        }

        public async Task<PagedResultDto<GetSharedRoleFileOutput>> GetSharedRoleListAsync(GetSharedRoleFileListInput input)
        {
            var resultList = await _manager.GetAllShareRole(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetSharedRoleFileOutput>(listcount, ObjectMapper.Map<List<GetSharedRoleFileOutput>>(resultList));
        }

        public async Task<GetSharedRoleFileOutput> GetUserRoleAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetUserRole(input.Id);
            return Mapper.Map<GetSharedRoleFileOutput>(getbyid);
        }

        public async Task<GetSharedRoleFileOutput> GetTotalSharedRoleFileAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetTotalSharedRole(input.Id);
            return Mapper.Map<GetSharedRoleFileOutput>(getbyid);
        }

        public async Task<PagedResultDto<GetSharedRoleFileOutput>> GetShareROleFileList(GetSharedRoleFileListInput input)
        {
            var resultList = await _manager.GetShareRoleAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetSharedRoleFileOutput>(listcount, ObjectMapper.Map<List<GetSharedRoleFileOutput>>(resultList));
        }

        public async Task<GetSharedRoleFileOutput> GetUserAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetUsersAsync(input.Id);
            return Mapper.Map<GetSharedRoleFileOutput>(getbyid);
        }
    }
}
