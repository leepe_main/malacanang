﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface INotificationService : IApplicationService
    {
        Task CreateNotificationUserAsync(CreateNotificationUsersInput input);

        Task<GetNotificationCountUserOutput> GetNotificationCountUserAsync(GetFolderUsersInput input);
        
        Task<PagedResultDto<GetNotificationCountUserOutput>> GetNotificationCountListAsync(GetFolderUsersListInput input);

        Task<GetNotificationCountUserOutput> GetNotificationCountRoleAsync(GetFolderUsersInput input);
        
        Task<PagedResultDto<GetNotificationCountUserOutput>> GetNotificationCountRoleListAsync(GetFolderUsersListInput input);

        Task<PagedResultDto<GetNotificationCountUserOutput>> GetTop5DownloadListAsync();
    }
}
