﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUser.Dto;

namespace ezinvmvc.App.FolderUser
{
    public class SharedUserFileService : ezinvmvcAppServiceBase, ISharedUserFileService
    {
        private readonly ISharedUserFileManager _manager;

        public SharedUserFileService(ISharedUserFileManager shareduserfilemanager)
        {
            _manager = shareduserfilemanager;
        }

        public async Task CreateSharedUserAsync(CreateSharedUserFileInput input)
        {
            SharedUserFile output = Mapper.Map<SharedUserFile>(input);
            CheckErrors(await _manager.CreateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteSharedUserceAsync(DeleteSharedUserFileInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.UserId, input.FileId));
        }

        public async Task<PagedResultDto<GetSharedUserFileOutput>> GetFolderUsersListAsync(GetSharedUserFileListInput input)
        {
            var resultList = await _manager.GetAllShareUsersAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetSharedUserFileOutput>(listcount, ObjectMapper.Map<List<GetSharedUserFileOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetSharedUserFileOutput>> GetShareFileList(GetSharedUserFileListInput input)
        {
            var resultList = await _manager.GetShareAsync(input.Filter);
            int listcount = 0;
            return new PagedResultDto<GetSharedUserFileOutput>(listcount, ObjectMapper.Map<List<GetSharedUserFileOutput>>(resultList));
        }

        public async Task<GetSharedUserFileOutput> GetTotalSharedFile(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetTotalSharedAsync(input.Id);
            return Mapper.Map<GetSharedUserFileOutput>(getbyid);
        }
    }
}
