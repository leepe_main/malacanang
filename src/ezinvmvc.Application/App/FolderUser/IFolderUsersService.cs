﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface IFolderUsersService : IApplicationService
    {
        Task CreateFolderUserAsync(CreateFolderUsersInput input);

        Task DeleteAttendanceAsync(DeleteFolderUsersInput input);

        Task<GetFolderUsersOutput> GetDataByIdAsync(GetFolderUsersInput input);

        Task<PagedResultDto<GetFolderUsersOutput>> GetFolderUsersIdAsync(GetFolderUsersInput input);

        Task<PagedResultDto<GetFolderUsersOutput>> GetFolderUsersListAsync(GetFolderUsersListInput input);

        Task<PagedResultDto<GetFolderUsersOutput>> GetAllUsersListAsync();

        Task<PagedResultDto<GetFolderUsersOutput>> GetAllRolesListAsync();

        Task<GetFolderUsersOutput> GetTotalUploadFileAsync(GetFolderUsersInput input);

        Task<GetFolderUsersOutput> GetDataFileById(GetFolderUsersInput input);

        Task<GetFolderUsersOutput> GetDataFileByRoleId(GetFolderUsersInput input);

        //for Upload
        Task CreateFileUsersInputAsync(CreateFileInput input);
    }
}
