﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.FolderUser.Dto;

namespace ezinvmvc.App.FolderUser
{
    public class FolderUsersService : ezinvmvcAppServiceBase, IFolderUsersService
    {
        private readonly IFolderUserManager _manager;

        public FolderUsersService(IFolderUserManager folderUserManager)
        {
            _manager = folderUserManager;
        }

        public async Task CreateFolderUserAsync(CreateFolderUsersInput input)
        {
            FolderUsers output = Mapper.Map<FolderUsers>(input);
            CheckErrors(await _manager.CreateAsync(output));
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAttendanceAsync(DeleteFolderUsersInput input)
        {
            CheckErrors(await _manager.DeleteAsync(input.Id));
        }

        public async Task<PagedResultDto<GetFolderUsersOutput>> GetAllUsersListAsync()
        {
            var resultList = await _manager.GetAllUsers();
            int listcount = 0;
            return new PagedResultDto<GetFolderUsersOutput>(listcount, ObjectMapper.Map<List<GetFolderUsersOutput>>(resultList));
        }

        public async Task<GetFolderUsersOutput> GetDataByIdAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetByIdAsync(input.Id);
            return Mapper.Map<GetFolderUsersOutput>(getbyid);
        }

        public async Task<PagedResultDto<GetFolderUsersOutput>> GetFolderUsersIdAsync(GetFolderUsersInput input)
        {
            var resultList = await _manager.GetByIdAsync(input.Id);
            int listcount = 0;
            var output = new PagedResultDto<GetFolderUsersOutput>(listcount, ObjectMapper.Map<List<GetFolderUsersOutput>>(resultList));
            return new PagedResultDto<GetFolderUsersOutput>(listcount, ObjectMapper.Map<List<GetFolderUsersOutput>>(resultList));  
        }

        public async Task<PagedResultDto<GetFolderUsersOutput>> GetFolderUsersListAsync(GetFolderUsersListInput input)
        {
            var resultList = await _manager.GetAllListAsync(input.Filter);
            int listcount = 0;           
            return new PagedResultDto<GetFolderUsersOutput>(listcount, ObjectMapper.Map<List<GetFolderUsersOutput>>(resultList));
        }

        public async Task<PagedResultDto<GetFolderUsersOutput>> GetAllRolesListAsync()
        {
            var resultList = await _manager.GetAllRoles();
            int listcount = 0;
            return new PagedResultDto<GetFolderUsersOutput>(listcount, ObjectMapper.Map<List<GetFolderUsersOutput>>(resultList));
        }

        public async Task<GetFolderUsersOutput>GetTotalUploadFileAsync(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetTotalUploadAsync(input.Id);
            return Mapper.Map<GetFolderUsersOutput>(getbyid);
        }

        public async Task<GetFolderUsersOutput> GetDataFileById(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetDataFileByIdAsync(input.Id);
            return Mapper.Map<GetFolderUsersOutput>(getbyid);
        }

        public async Task<GetFolderUsersOutput> GetDataFileByRoleId(GetFolderUsersInput input)
        {
            var getbyid = await _manager.GetDataFileRoleByIdAsync(input.Id);
            return Mapper.Map<GetFolderUsersOutput>(getbyid);
        }

        public async Task CreateFileUsersInputAsync(CreateFileInput input)
        {
            foreach (CreateFolderUsersInput item in input.foldersusersinput)
            {
                FolderUsers output = Mapper.Map<FolderUsers>(item);
                CheckErrors(await _manager.CreateAsync(output));
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        ////for Upload
        //public async Task CreateFileUsersInput(CreateFileInput input)
        //{
        //    foreach (CreateFolderUsersInput item in input.FoldersUsersInput)
        //    {
        //        FolderUsers output = Mapper.Map<FolderUsers>(item);
        //        CheckErrors(await _manager.CreateAsync(output));
        //    }
        //    await CurrentUnitOfWork.SaveChangesAsync();
        //    //return 1;
        //}
    }
}
