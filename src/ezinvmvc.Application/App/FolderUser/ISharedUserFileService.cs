﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface ISharedUserFileService : IApplicationService
    {
        Task CreateSharedUserAsync(CreateSharedUserFileInput input);

        Task DeleteSharedUserceAsync(DeleteSharedUserFileInput input);

        Task<PagedResultDto<GetSharedUserFileOutput>> GetFolderUsersListAsync(GetSharedUserFileListInput input);

        Task<GetSharedUserFileOutput> GetTotalSharedFile(GetFolderUsersInput input);

        Task<PagedResultDto<GetSharedUserFileOutput>> GetShareFileList(GetSharedUserFileListInput input);
    }
}
