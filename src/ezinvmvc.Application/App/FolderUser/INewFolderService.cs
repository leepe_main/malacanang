﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface INewFolderService : IApplicationService
    {
        Task CreateNewFolderAsync(CreateNewFolderInput input);

        Task DeleteNewFolderAsync(DeleteNewFolderInput input);

        Task<GetNewFolderOutput> GetNewFolderbyIdAsync(GetNewFolderInput input);

        Task<PagedResultDto<GetNewFolderOutput>> GetNewFolderListAsync(GetNewFolderListInput input);
    }
}
