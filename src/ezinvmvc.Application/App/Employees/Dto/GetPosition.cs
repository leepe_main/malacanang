﻿namespace ezinvmvc.App.Employees.Dto
{
    public class GetPosition
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int DepartmentId { get; set; }

        public int SectorId { get; set; }
    }
}
