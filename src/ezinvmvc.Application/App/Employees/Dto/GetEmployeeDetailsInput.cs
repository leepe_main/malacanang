﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.Employees.Dto
{
    public class GetEmployeeDetailsInput
    {
        public int EmployeeId { get; set; }
    }
}
