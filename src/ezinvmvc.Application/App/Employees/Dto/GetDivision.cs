﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.Employees.Dto
{
    public class GetDivision
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
