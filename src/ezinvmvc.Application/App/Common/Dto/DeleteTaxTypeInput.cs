﻿namespace ezinvmvc.App.Common.Dto
{
    public class DeleteTaxTypeInput
    {
        public int Id { get; set; }
    }
}
