﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace ezinvmvc.App.Common.Dto
{
   public class CreateSeriesTypeInput: Entity<int>
    {
        [Required]
        [StringLength(ezinvmvcConsts.MaxLenght8, ErrorMessage = ezinvmvcConsts.ErrorMessage8)]
        public string Prefix { get; set; }

        [Required]
        public int LastSeries { get; set; }

        [Required]
        public int Padding { get; set; }

        [Required]
        public int TransactionId { get; set; }

        [Required]
        public int CompanyId { get; set; }
    }
}
