﻿using Abp.Domain.Entities;

namespace ezinvmvc.App.Common.Dto
{
   public class GetTaxTypeOutput: Entity<int>
    {
        public int Code { get; set; }
        
        public string Name { get; set; }
        
        public decimal Rate { get; set; }

        public int LiabilityAccountId { get; set; }
    }
}
