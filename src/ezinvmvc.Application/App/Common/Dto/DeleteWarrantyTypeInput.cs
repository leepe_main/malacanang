﻿namespace ezinvmvc.App.Common.Dto
{
   public class DeleteWarrantyTypeInput
    {
        public int Id { get; set; }
    }
}
