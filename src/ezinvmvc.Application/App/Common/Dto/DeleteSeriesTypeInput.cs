﻿using Abp.Domain.Entities;

namespace ezinvmvc.App.Common.Dto
{
    public class DeleteSeriesTypeInput: Entity<int>
    {
        public string Prefix { get; set; }

        public int LastSeries { get; set; }

        public int Padding { get; set; }

        public int TransactionId { get; set; }

        public int CompanyId { get; set; }
    }
}
