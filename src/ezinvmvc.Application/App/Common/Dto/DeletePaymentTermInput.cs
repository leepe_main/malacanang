﻿

namespace ezinvmvc.App.Common.Dto
{
   public class DeletePaymentTermInput
    {
        public int Id { get; set; }
    }
}
