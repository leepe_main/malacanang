﻿namespace ezinvmvc.App.Common.Dto
{
    public class DeleteDeliveryTypeInput
    {
        public int Id { get; set; }
    }
}
