﻿using Abp.Domain.Entities;

namespace ezinvmvc.App.Common.Dto
{
    public class GetOrderTypeOutput : Entity<int>
    {
        public string Name { get; set; }
    }
}
