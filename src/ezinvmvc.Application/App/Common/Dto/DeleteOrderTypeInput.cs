﻿namespace ezinvmvc.App.Common.Dto
{
   public class DeleteOrderTypeInput
    {
        public int Id { get; set; }
    }
}
