﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesSalaryRate.Dto
{
    public class GetEmployeeSalariesInput
    {
        public int EmpId { get; set; }
    }
}
