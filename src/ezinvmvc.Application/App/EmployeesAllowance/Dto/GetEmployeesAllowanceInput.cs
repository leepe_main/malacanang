﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ezinvmvc.App.EmployeesAllowance.Dto
{
    public class GetEmployeesAllowanceInput
    {
        public int EmpId { get; set; }
    }
}
