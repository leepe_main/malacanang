﻿using Abp.Application.Services.Dto;

namespace ezinvmvc.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

