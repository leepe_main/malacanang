﻿using Abp.MultiTenancy;
using ezinvmvc.Authorization.Users;

namespace ezinvmvc.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
