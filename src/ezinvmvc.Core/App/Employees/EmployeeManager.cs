﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;

namespace ezinvmvc.App.Employees
{
    public class EmployeeManager : DomainService, IEmployeeManager
    {
        private readonly IRepository<Employee> _repositoryEmployee;
        private readonly IDapperRepository<Employee> _repositoryEmployeeDapper;

        public EmployeeManager(IRepository<Employee> repository, IDapperRepository<Employee> repositoryDapper)
        {
            _repositoryEmployee = repository;
            _repositoryEmployeeDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(Employee entity)
        {
            var result = _repositoryEmployee.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repositoryEmployee.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repositoryEmployee.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repositoryEmployee.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IEnumerable<Employee>> GetAgents(string name)
        {
            string wc = " Where a.isdeleted = '0' and isagent = 1 and (a.Firstname like @name or a.MiddleName like @name or a.LastName like @name) ";
            string sort = " order by CompleteName asc ";
            var dp = new DynamicParameters();
            dp.Add("@name", "%" + name + "%");
            try
            {
                IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select a.*,a.FirstName+' '+ a.LastName as CompleteName from appemployee as a " + wc + sort, dp);
                return getAll;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Employee>> GetAllEmployeeAsync()
        {
            string wc = " Where isdeleted = 0 ";
            string sort = " order by Id asc ";
            var dp = new DynamicParameters();
            try
            {
                IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select *,FirstName+' '+ MiddleName+' '+ LastName as UserFullName from appemployee " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Employee>> GetAllSalescordinator(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            //start//
            string[] tokens = filter.Split('|');

            string ddFilter = "";
            string TxtFilter = "";

            if (tokens[0].ToString() != "null")
            {
                ddFilter = tokens[0].ToString();
            }
            if (tokens[1].ToString() != "null")
            {
                TxtFilter = tokens[1].ToString();
            }
            //end//

            //string wc = " Where a.isdeleted = '0' ";
            //if (filter != "")
            //{
            //    wc = wc + " And  ( a.FirstName+' '+ a.MiddleName+' '+ a.LastName like @Filter ) ";
            //}

            //start//


            var dp = new DynamicParameters();

            string wc = " Where a.isdeleted = '0' and f.TransactionCode = '108' and  ur.rolaname = 'Sales Coordinator'";
            if (ddFilter == "CompleteName")
            {
                wc = wc + " And  ( a.FirstName+' '+ a.MiddleName+' '+ a.LastName like @Filter2 ) ";
            }
            if (ddFilter == "Department")
            {
                wc = wc + " And ( b.Description like @Filter2 ) ";
            }
            if (ddFilter == "Division")
            {
                wc = wc + " And ( c.Description like @Filter2 ) ";
            }
            if (ddFilter == "Sectors")
            {
                wc = wc + " And ( d.Description like @Filter2 ) ";
            }
            if (ddFilter == "Position")
            {
                wc = wc + " And ( e.Description like @Filter2 ) ";
            }
            if (ddFilter == "UserId")
            {
                wc = wc + " And a.UserId = @Filter3 ";
            }
            //end
            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " asc ";
            }
            else
            {
                sort = " order by CompleteName asc ";
            }
            dp.Add("@Filter", "%" + filter + "%");
            dp.Add("@Filter2", "%" + TxtFilter + "%");
            dp.Add("@Filter3", TxtFilter);
            //dp.Add("@Filter2", FullNameFilter);
            try
            {
                if (!forexport)
                {
                    //var getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select * from AppEmployee " + wc + sort, dp);
                    //IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) inner join AbpUserRoles as ur on ur.UserId = a.UserId Left outer join AppDepartment as b on a.DepartmentId = b.Id Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " Rows Only", dp);
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) inner join (select aur.*, ar.Name rolaname from AbpUserRoles aur inner join AbpRoles ar on ar.Id = aur.RoleId) ur on ur.UserId = a.UserId  Left outer join AppDepartment as b on a.DepartmentId = b.Id  Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code  " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " Rows Only", dp);
                    return getAll;
                }
                else
                {
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) inner join (select aur.*, ar.Name rolaname from AbpUserRoles aur inner join AbpRoles ar on ar.Id = aur.RoleId) ur on ur.UserId = a.UserId  Left outer join AppDepartment as b on a.DepartmentId = b.Id  Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code " + wc + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Employee>> GetAllList(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            //start//
            string[] tokens = filter.Split('|');

            string ddFilter = "";
            string TxtFilter = "";

            if (tokens[0].ToString() != "null")
            {
                ddFilter = tokens[0].ToString();
            }
            if (tokens[1].ToString() != "null")
            {
                TxtFilter = tokens[1].ToString();
            }
            //end//

            //string wc = " Where a.isdeleted = '0' ";
            //if (filter != "")
            //{
            //    wc = wc + " And  ( a.FirstName+' '+ a.MiddleName+' '+ a.LastName like @Filter ) ";
            //}

            //start//


            var dp = new DynamicParameters();

            string wc = " Where a.isdeleted = '0' and f.TransactionCode = '108'";
            if (ddFilter == "CompleteName")
            {
                wc = wc + " And  ( a.FirstName+' '+ a.MiddleName+' '+ a.LastName like @Filter2 ) ";
            }
            if (ddFilter == "Department")
            {
                wc = wc + " And ( b.Description like @Filter2 ) ";
            }
            if (ddFilter == "Division")
            {
                wc = wc + " And ( c.Description like @Filter2 ) ";
            }
            if (ddFilter == "Sectors")
            {
                wc = wc + " And ( d.Description like @Filter2 ) ";
            }
            if (ddFilter == "Position")
            {
                wc = wc + " And ( e.Description like @Filter2 ) ";
            }
            if (ddFilter == "UserId")
            {
                wc = wc + " And a.UserId = @Filter3 ";
            }
            //end
            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " asc ";
            }
            else
            {
                sort = " order by CompleteName asc ";
            }
            dp.Add("@Filter", "%" + filter + "%");
            dp.Add("@Filter2", "%" + TxtFilter + "%");
            dp.Add("@Filter3", TxtFilter);
            //dp.Add("@Filter2", FullNameFilter);
            try
            {
                if (!forexport)
                {
                    //var getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select * from AppEmployee " + wc + sort, dp);
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) Left outer join AppDepartment as b on a.DepartmentId = b.Id Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " Rows Only", dp);
                    return getAll;
                }
                else
                {
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) Left outer join AppDepartment as b on a.DepartmentId = b.Id Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code" + wc + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Employee>> GetAllUserList(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            //start//
            string[] tokens = filter.Split('|');

            string username = "", empid = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null" && tokens[0].Trim() != "")
                {
                    username = tokens[0].Trim();
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    empid = tokens[1].Trim();
                }
            }

            string wc = " Where b.isdeleted = 0 and isnull(a.isdeleted,0) = 0 and b.TenantId is null ";
            string wc2 = " Where b.isdeleted = 0 and isnull(a.isdeleted,0) = 0 and b.TenantId is null and a.UserId is null ";
            if (empid != "" && empid != null)
            {
                wc = wc + " AND a.Id = @empid ";
            }
            if (username != "" && username != null)
            {
                wc2 = wc2 + " AND b.UserName like @username ";
            }
            //end
            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " " + lastWord;
            }
            else
            {
                sort = " order by UserName asc ";
            }
            var dp = new DynamicParameters();
            dp.Add("@username", "%" + username + "%");
            dp.Add("@empid", empid);
            //dp.Add("@Filter2", FullNameFilter);
            try
            {
                if (!forexport)
                {
                    //var getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select * from AppEmployee " + wc + sort, dp);
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select a.*, b.Id as UserUserId, b.UserName, b.Name + ' ' + b.Surname AS UserFullName, b.EmailAddress as UserEmailAddress from Appemployee as a right outer join AbpUsers as b on b.Id=a.UserId " + wc + "UNION " +
                        "Select a.*, b.Id as UserUserId, b.UserName, b.Name + ' ' + b.Surname AS UserFullName, b.EmailAddress as UserEmailAddress from Appemployee as a right outer join AbpUsers as b on b.Id=a.UserId " + wc2 + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " Rows Only", dp);
                    return getAll;
                }
                else
                {
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("Select a.*, b.Id as UserUserId, b.UserName, b.Name + ' ' + b.Surname AS UserFullName, b.EmailAddress as UserEmailAddress from Appemployee as a right outer join AbpUsers as b on b.Id=a.UserId " + wc + "UNION " +
                        "Select a.*, b.Id as UserUserId, b.UserName, b.Name + ' ' + b.Surname AS UserFullName, b.EmailAddress as UserEmailAddress from Appemployee as a right outer join AbpUsers as b on b.Id=a.UserId " + wc2 + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Employee> GetByIdAsync(int id)
        {
            var result = _repositoryEmployee.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                return await _repositoryEmployee.GetAsync(id);
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IdentityResult> UpdateAsync(Employee entity)
        {
            try
            {
                await _repositoryEmployee.UpdateAsync(entity);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Updating: " + ex.ToString());
            }
        }


        public async Task<IEnumerable<Employee>> GetAllAttendanceList(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            //start//
            string[] tokens = filter.Split('|');

            string ddFilter = "";
            string TxtFilter = "";
            string e = "";

            if (tokens[0].ToString() != "null")
            {
                ddFilter = tokens[0].ToString();
            }
            if (tokens[1].ToString() != "null")
            {
                TxtFilter = tokens[1].ToString();
            }
            
            if (tokens[2].ToString() != "null")
            {
                e = tokens[2].ToString();
            }

            var dp = new DynamicParameters();

            string wc = " Where a.isdeleted = '0' and f.TransactionCode = '108' and a.Id not in (select EmpId as Id from AppEmployeeAttRecord where Attid = @e  and IsDeleted = '0') ";
            if (ddFilter == "CompleteName")
            {
                wc = wc + " And  ( a.FirstName+' '+ a.MiddleName+' '+ a.LastName like @Filter2 ) ";
            }
            if (ddFilter == "Department")
            {
                wc = wc + " And ( b.Description like @Filter2 ) ";
            }
            if (ddFilter == "Division")
            {
                wc = wc + " And ( c.Description like @Filter2 ) ";
            }
            if (ddFilter == "Sectors")
            {
                wc = wc + " And ( d.Description like @Filter2 ) ";
            }
            if (ddFilter == "Position")
            {
                wc = wc + " And ( e.Description like @Filter2 ) ";
            }
            if (ddFilter == "UserId")
            {
                wc = wc + " And a.UserId = @Filter3 ";
            }
            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " asc ";
            }
            else
            {
                sort = " order by CompleteName asc ";
            }
            dp.Add("@e",e);
            dp.Add("@Filter", "%" + filter + "%");
            dp.Add("@Filter2", "%" + TxtFilter + "%");
            dp.Add("@Filter3", TxtFilter);
            try
            {
                if (!forexport)
                {
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) Left outer join AppDepartment as b on a.DepartmentId = b.Id Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " Rows Only", dp);
                    return getAll;
                }
                else
                {
                    IEnumerable<Employee> getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>("select count(*) Over() AS TotalRows, a.*,a.FirstName+' '+ a.MiddleName+' '+ a.LastName as CompleteName,b.Name as Dept,c.Name as Div,d.Name as Sect,e.Name as Post,f.Status from appemployee as a with (nolock) Left outer join AppDepartment as b on a.DepartmentId = b.Id Left outer join AppDivEmployee as c on a.DivisionId = c.Id Left outer join AppSectors as d on a.sectorsid = d.Id Left outer join AppPosition as e on a.PositionID = e.Id Left outer join AppStatusTypes as f on a.StatusId = f.code" + wc + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Employee> GetEmployeeAsync(int id)
        {
            string wc = " where UserId = @id ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryEmployeeDapper.QueryAsync<Employee>(" select * from  AppEmployee " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

    }
}
