﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface INewFolderManager : IDomainService
    {
        Task<IdentityResult> Create(NewFolder entity);

        Task<NewFolder> GetByIdAsync(int id);

        Task<IdentityResult> Delete(int id);

        Task<IEnumerable<NewFolder>> GetAllListAsync(string filter);
    }
}
