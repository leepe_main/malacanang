﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface IFolderUserManager : IDomainService
    {
        Task<IdentityResult> CreateAsync(FolderUsers entity);

        Task<FolderUsers> GetByIdAsync(int id);

        Task<IdentityResult> DeleteAsync(int id);

        Task<IEnumerable<FolderUsers>> GetAllListAsync(string filter);

        Task<IEnumerable<FolderUsers>> GetAllUsers();

        Task<IEnumerable<FolderUsers>> GetAllRoles();

        Task<FolderUsers> GetTotalUploadAsync(int CreatorUserId);
        
        Task<FolderUsers> GetDataFileByIdAsync(int id);

        Task<FolderUsers> GetDataFileRoleByIdAsync(int id);
    }
}
