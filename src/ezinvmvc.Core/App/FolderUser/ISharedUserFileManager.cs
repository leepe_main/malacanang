﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface ISharedUserFileManager : IDomainService
    {
        Task<IdentityResult> CreateAsync(SharedUserFile entity);

        Task<IdentityResult> DeleteAsync(int UserId, int FileId);

        Task<IEnumerable<SharedUserFile>> GetAllShareUsersAsync(string filter);

        Task<SharedUserFile> GetTotalSharedAsync(int UserId);

        Task<IEnumerable<SharedUserFile>> GetShareAsync(string filter);
    }
}
