﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface INotificationManager : IDomainService
    {
        Task<IdentityResult> CreateShareUserAsync(Notification entity);

        Task<Notification> UpdateShareUserAsync(int id);

        Task<Notification> GetShareUserCountNotificationAsync(int id);

        Task<IEnumerable<Notification>> GetShareUSerNotificationListAsync(string filter);

        Task<Notification> GetShareRoleCountNotificationAsync(int id);

        Task<IEnumerable<Notification>> GetShareRoleNotificationListAsync(string filter);

        Task<IEnumerable<Notification>> GetTopDownloadListAsync();
    }
}
