﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public interface ISharedRoleFileManager : IDomainService
    {
        Task<IdentityResult> Create(SharedRoleFile entity);

        Task<IdentityResult> Delete(int RoleId, int FileId);

        Task<SharedRoleFile> GetUserRole(int Id);

        Task<IEnumerable<SharedRoleFile>> GetAllShareRole(string filter);

        Task<SharedRoleFile> GetTotalSharedRole(int Roleid);

        Task<IEnumerable<SharedRoleFile>> GetShareRoleAsync(string filter);

        Task<SharedRoleFile> GetUsersAsync(int Id);
    }
}
