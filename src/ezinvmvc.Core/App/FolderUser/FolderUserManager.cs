﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class FolderUserManager : DomainService, IFolderUserManager
    {
        private readonly IRepository<FolderUsers> _repository;
        private readonly IDapperRepository<FolderUsers> _repositoryDapper;

        public FolderUserManager(IRepository<FolderUsers> repository, IDapperRepository<FolderUsers> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(FolderUsers entity)
        {
            //var result = _repository.FirstOrDefault(x => x.FileName == entity.FileName && x.CreatorUserId == entity.CreatorUserId);

            var result = _repository.FirstOrDefault(x => x.FileName == entity.FileName && x.Path == entity.Path);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<FolderUsers> GetByIdAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                return await _repository.GetAsync(id);
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<FolderUsers>> GetAllListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string datefrom = "";
            string dateTo = "";
            string CreatorUserId = "";
            string filename = "";
            string cat1 = "";
            string status = "";
            string folderid = "";


            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        dateTo = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            CreatorUserId = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                filename = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    cat1 = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        status = tokens[5];
                                    }
                                    if (tokens.Length > 6)
                                    {
                                        if (tokens[6].ToString() != "null")
                                        {
                                            folderid = tokens[6];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = "";
            if (CreatorUserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where CreatorUserId =  @EnpId ";
                }
                else
                {
                    wc = wc + " and CreatorUserId =  @EnpId ";
                }

                dp.Add("@EnpId", CreatorUserId);
                //wc = wc + " where CreatorUserId =  @EnpId ";

            }
            if (filename != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where filename like  @filename ";
                }
                else
                {
                    wc = wc + " and filename like  @filename ";
                }
                dp.Add("@filename", "%" + filename + "%");

            }
            if (cat1 != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where cat1 =  @cat1 ";
                }
                else
                {
                    wc = wc + " and cat1 =  @cat1 ";
                }
                dp.Add("@cat1", cat1);

            }
            if (status != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where status =  @status ";
                }
                else
                {
                    wc = wc + " and status =  @status ";
                }
                dp.Add("@status", status);
            }
            if (folderid != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where folderid =  @folderid ";
                }
                else
                {
                    wc = wc + " and folderid =  @folderid ";
                }
                dp.Add("@folderid", folderid);
            }
            if ((datefrom != "" && datefrom != "null") && (dateTo != "" && dateTo != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(dateTo).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by datetime asc  ";
            try
            {
                //var getAll = await _repositoryDapper.QueryAsync<FolderUsers>("select count(*) Over() AS TotalRows, * from AppFolderUser " + wc + sort, dp);

                var getAll = await _repositoryDapper.QueryAsync<FolderUsers>("select count(*) Over() AS TotalRows, * from AppFolderUser " + wc + " union select count(*) Over() AS TotalRows, * from appNewfolder " + wc + "" + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<IEnumerable<FolderUsers>> GetAllUsers()
        {
            string wc = " where a.tenantId is null and a.IsDeleted = 0 and a.IsActive = 1 ";
            string sort = " order by a.NormalizedUserName asc ";
            var dp = new DynamicParameters();
            try
            {
                IEnumerable<FolderUsers> getAll = await _repositoryDapper.QueryAsync<FolderUsers>("select count(*) Over() AS TotalRows,  a.Id,a.Name as Cat2,c.Name as Cat3,b.RoleId as Cat4 from Abpusers as a inner join AbpUserRoles as b on a.id=b.userid inner join AbpRoles as c on b.RoleId = c.id  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
        public async Task<IEnumerable<FolderUsers>> GetAllRoles()
        {
            string wc = " where tenantId is null and IsDeleted = 0 ";
            string sort = " order by Name asc ";
            var dp = new DynamicParameters();
            try
            {
                IEnumerable<FolderUsers> getAll = await _repositoryDapper.QueryAsync<FolderUsers>("select count(*) Over() AS TotalRows,Id,Name as FileName from AbpRoles " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<FolderUsers> GetTotalUploadAsync(int CreatorUserId)
        {
            string wc = " where CreatorUserId = @CreatorUserId and IsDeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@CreatorUserId", CreatorUserId);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FolderUsers>(" select count(*) as TotalRows from AppFolderUser " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<FolderUsers> GetDataFileByIdAsync(int id)
        {
            string wc = " where b.Id = @id ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FolderUsers>(" select b.Id,b.Datetime,a.Path,a.FileName,b.description,a.MB,a.Extention,a.Icon,c.Name as cat1,e.Name as cat2 from AppFolderUser as a inner join AppSharedUserFile as b on a.id = b.FileId inner join AbpUsers as c on a.CreatorUserId = c.Id inner join AbpUserRoles as d on a.CreatorUserId = d.UserId inner join AbpRoles as e on d.RoleId = e.Id " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<FolderUsers> GetDataFileRoleByIdAsync(int id)
        {
            string wc = " where b.Id = @id ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FolderUsers>(" select b.Id,b.Datetime,a.Path,a.FileName,b.description,a.MB,a.Extention,a.Icon,c.Name as cat1,e.Name as cat2 from AppFolderUser as a inner join AppSharedRoleFile as b on a.id = b.FileId inner join AbpUsers as c on a.CreatorUserId = c.Id inner join AbpUserRoles as d on a.CreatorUserId = d.UserId inner join AbpRoles as e on d.RoleId = e.Id  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
    }
}
