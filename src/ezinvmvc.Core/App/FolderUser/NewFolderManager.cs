﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class NewFolderManager : DomainService, INewFolderManager
    {
        private readonly IRepository<NewFolder> _repository;
        private readonly IDapperRepository<NewFolder> _repositoryDapper;

        public NewFolderManager(IRepository<NewFolder> repository, IDapperRepository<NewFolder> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> Create(NewFolder entity)
        {
            var result = _repository.FirstOrDefault(x => x.FileName == entity.FileName && x.CreatorUserId == entity.CreatorUserId);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> Delete(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<NewFolder>> GetAllListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string datefrom = "";
            string dateTo = "";
            string CreatorUserId = "";
            string filename = "";
            string cat1 = "";
            string status = "";


            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        dateTo = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            CreatorUserId = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                filename = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    cat1 = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        status = tokens[5];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = "";
            if (CreatorUserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where CreatorUserId =  @EnpId ";
                }
                else
                {
                    wc = wc + " and CreatorUserId =  @EnpId ";
                }

                dp.Add("@EnpId", CreatorUserId);
                //wc = wc + " where CreatorUserId =  @EnpId ";

            }
            if (filename != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where filename =  @filename ";
                }
                else
                {
                    wc = wc + " and filename =  @filename ";
                }

                dp.Add("@filename", filename);

            }
            if (cat1 != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where cat1 =  @cat1 ";
                }
                else
                {
                    wc = wc + " and cat1 =  @cat1 ";
                }
                dp.Add("@cat1", cat1);

            }
            if (status != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where status =  @status ";
                }
                else
                {
                    wc = wc + " and status =  @status ";
                }
                dp.Add("@status", status);
            }
            if ((datefrom != "" && datefrom != "null") && (dateTo != "" && dateTo != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(dateTo).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by datetime asc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<NewFolder>("select count(*) Over() AS TotalRows, * from AppNewFolder " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<NewFolder> GetByIdAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                return await _repository.GetAsync(id);
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

    }
}
