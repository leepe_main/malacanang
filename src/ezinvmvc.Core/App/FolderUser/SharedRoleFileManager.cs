﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class SharedRoleFileManager : DomainService, ISharedRoleFileManager
    {
        private readonly IRepository<SharedRoleFile> _repository;
        private readonly IDapperRepository<SharedRoleFile> _repositoryDapper;

        public SharedRoleFileManager(IRepository<SharedRoleFile> repository, IDapperRepository<SharedRoleFile> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> Create(SharedRoleFile entity)
        {
            var result = _repository.FirstOrDefault(x => x.FileId == entity.FileId && x.RoleId == entity.RoleId);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> Delete(int RoleId, int FileId)
        {
            var result = _repository.FirstOrDefault(x => x.RoleId == RoleId && x.FileId == FileId);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<SharedRoleFile>> GetAllShareRole(string filter)
        {
            string[] tokens = filter.Split('|');

            string FileId = "";
            string RoleId = "";
            string FileName = "";
            string UserName = "";


            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    FileId = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        RoleId = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            FileName = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                UserName = tokens[3];
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " Where b.IsDeleted = 0 ";
            if (FileId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where b.FileId =  @FileId ";
                }
                else
                {
                    wc = wc + " and b.FileId =  @FileId ";
                }

                dp.Add("@FileId", FileId);
            }
            
            if (RoleId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.RoleId =  @RoleId ";
                }
                else
                {
                    wc = wc + " and b.RoleId =  @RoleId ";
                }
                dp.Add("@RoleId", RoleId);

            }
            if (FileName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where d.FileName like @FileName ";
                }
                else
                {
                    wc = wc + " and d.FileName like @FileName ";
                }
                dp.Add("@FileName", "%" + FileName + "%");

            }
            if (UserName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where e.Name like @UserName ";
                }
                else
                {
                    wc = wc + " and e.Name like @UserName ";
                }
                dp.Add("@UserName", "%" + UserName + "%");

            }
            string sort = " order by c.Name asc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedRoleFile>(" select distinct b.Id,b.FileId,b.RoleId,b.dateTime,c.Name as Cat1,b.RoleId,e.Name as Cat2,d.FileName as Cat3,d.Icon as Cat4,d.Path as Cat5,d.MB as Field1,b.description from AbpUserRoles as a inner join  AppSharedRoleFile as b on a.RoleId = b.RoleId inner join  AbpUsers as c on b.CreatorUserId = c.Id inner join AppFolderUser as d on d.Id = b.FileId inner join AbpRoles as e on a.RoleId = e.id " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<SharedRoleFile> GetUserRole(int id)
        {
            string wc = " where UserId =  @Id ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedRoleFile>(" select CreationTime as Datetime,RoleId from AbpUserRoles " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<SharedRoleFile> GetTotalSharedRole(int Roleid)
        {
            string wc = " where RoleId = @Roleid and isdeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@Roleid", Roleid);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedRoleFile>(" select count (*) as TotalRows from AppSharedRoleFile " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<IEnumerable<SharedRoleFile>> GetShareRoleAsync(string filter)
        {
            string[] tokens = filter.Split('|');

            string Datefrom = "";
            string Dateto = "";
            string FileId = "";
            string UserId = "";
            string RoleId = "";
            string FileName = "";
            string ShareName = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    Datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        Dateto = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            FileId = tokens[2];
                        }
                        if (tokens.Length > 2)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                UserId = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    RoleId = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        FileName = tokens[5];
                                    }
                                    if (tokens.Length > 6)
                                    {
                                        if (tokens[6].ToString() != "null")
                                        {
                                            ShareName = tokens[6];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " Where b.IsDeleted = 0 ";
            if (FileId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where b.FileId =  @FileId ";
                }
                else
                {
                    wc = wc + " and b.FileId =  @FileId ";
                }

                dp.Add("@FileId", FileId);
            }
            if (UserId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.Userid =  @UserId ";
                }
                else
                {
                    wc = wc + " and a.Userid =  @UserId ";
                }

                dp.Add("@UserId", UserId);

            }
            if (RoleId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.RoleId =  @RoleId ";
                }
                else
                {
                    wc = wc + " and b.RoleId =  @RoleId ";
                }
                dp.Add("@RoleId", RoleId);

            }
            if (FileName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where d.FileName like  @FileName ";
                }
                else
                {
                    wc = wc + " and d.FileName like  @FileName ";
                }
                dp.Add("@FileName", "%" + FileName + "%");

            }
            if (ShareName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where c.Name like  @ShareName ";
                }
                else
                {
                    wc = wc + " and c.Name like  @ShareName ";
                }
                dp.Add("@ShareName", "%" + ShareName + "%");

            }
            if ((Datefrom != "" && Datefrom != "null") && (Dateto != "" && Dateto != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and b.datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(Datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(Dateto).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by b.datetime desc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedRoleFile>(" select distinct count(*) Over() AS TotalRows,  b.Id,b.FileId,b.dateTime,c.Name as Cat1,b.RoleId,e.Name as Cat2,d.FileName as Cat3,d.Icon as Cat4,d.Path as Cat5,d.MB as Field1,b.description from AbpUserRoles as a inner join  AppSharedRoleFile as b on a.RoleId = b.RoleId inner join  AbpUsers as c on b.CreatorUserId = c.Id inner join AppFolderUser as d on d.Id = b.FileId inner join AbpRoles as e on a.RoleId = e.id  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<SharedRoleFile> GetUsersAsync(int id)
        {
            string wc = " where a.Id =  @Id and a.Isdeleted = 0";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedRoleFile>(" select a.id,NORMALIZEdUserName as Description,a.Name as Cat1,Surname as Cat2,Password as Cat3,NormalizedEMailAddress as Cat4,b.UserImagePath as Cat5, IsActive as Field1 from abpusers as a left outer join AppEmployee as b on a.id = b.UserId " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
    }
}
