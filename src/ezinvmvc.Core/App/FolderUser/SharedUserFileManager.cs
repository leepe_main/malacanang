﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class SharedUserFileManager : DomainService, ISharedUserFileManager
    {
        private readonly IRepository<SharedUserFile> _repository;
        private readonly IDapperRepository<SharedUserFile> _repositoryDapper;

        public SharedUserFileManager(IRepository<SharedUserFile> repository, IDapperRepository<SharedUserFile> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(SharedUserFile entity)
        {
            var result = _repository.FirstOrDefault(x => x.FileId == entity.FileId && x.UserId == entity.UserId);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int UserId, int FileId)
        {
            var result = _repository.FirstOrDefault(x => x.UserId == UserId && x.FileId == FileId);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<SharedUserFile>> GetAllShareUsersAsync(string filter)
        {
            string[] tokens = filter.Split('|');

            string FileId = "";
            string UserId = "";
            string RoleId = "";
            string FileName = "";
            string ShareName = "";


            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    FileId = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        UserId = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            RoleId = tokens[2];
                        }
                        if (tokens.Length > 2)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                FileName = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    ShareName = tokens[4];
                                }

                            }

                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " Where a.IsDeleted = 0 ";
            if (FileId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.FileId =  @FileId ";
                }
                else
                {
                    wc = wc + " and a.FileId =  @FileId ";
                }

                dp.Add("@FileId", FileId);
            }
            if (UserId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.Userid =  @UserId ";
                }
                else
                {
                    wc = wc + " and a.Userid =  @UserId ";
                }

                dp.Add("@UserId", UserId);

            }
            if (RoleId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.RoleId =  @RoleId ";
                }
                else
                {
                    wc = wc + " and a.RoleId =  @RoleId ";
                }
                dp.Add("@RoleId", RoleId);

            }
            if (FileName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where d.FileName like  @FileName ";
                }
                else
                {
                    wc = wc + " and d.FileName like  @FileName ";
                }
                dp.Add("@FileName", "%" + FileName + "%");

            }
            if (ShareName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where c.Name like  @ShareName ";
                }
                else
                {
                    wc = wc + " and c.Name like  @ShareName ";
                }
                dp.Add("@ShareName", "%" + ShareName + "%");

            }
            string sort = " order by b.Name asc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedUserFile>("select count(*) Over() AS TotalRows, a.id,a.FileId,a.Userid,a.RoleId,a.datetime,b.Name as Cat1,a.RoleId,c.Name as Cat2,d.FileName as Cat3,d.Icon as Cat4,d.Path as Cat5,d.mb as Field1,a.description from AppSharedUserFile as a inner join AbpUsers as b on a.UserId = b.id inner join AbpRoles as c on a.RoleId = c.id inner join AppFolderUser as d on d.Id = a.FileId  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<SharedUserFile> GetTotalSharedAsync(int UserId)
        {
            string wc = " where UserId = @UserId and isdeleted = 0 and FolderId = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@UserId", UserId);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedUserFile>(" select count (*) as TotalRows from AppSharedUserFile " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<IEnumerable<SharedUserFile>> GetShareAsync(string filter)
        {
           
            string[] tokens = filter.Split('|');

            string Datefrom = "";
            string Dateto = "";
            string FileId = "";
            string UserId = "";
            string RoleId = "";
            string FileName = "";
            string ShareName = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    Datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        Dateto = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            FileId = tokens[2];
                        }
                        if (tokens.Length > 2)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                UserId = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    RoleId = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        FileName = tokens[5];
                                    }
                                    if (tokens.Length > 6)
                                    {
                                        if (tokens[6].ToString() != "null")
                                        {
                                            ShareName = tokens[6];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " Where a.IsDeleted = 0 ";
            if (FileId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.FileId =  @FileId ";
                }
                else
                {
                    wc = wc + " and a.FileId =  @FileId ";
                }

                dp.Add("@FileId", FileId);
            }
            if (UserId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.Userid =  @UserId ";
                }
                else
                {
                    wc = wc + " and a.Userid =  @UserId ";
                }

                dp.Add("@UserId", UserId);

            }
            if (RoleId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.RoleId =  @RoleId ";
                }
                else
                {
                    wc = wc + " and a.RoleId =  @RoleId ";
                }
                dp.Add("@RoleId", RoleId);

            }
            if (FileName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where d.FileName like  @FileName ";
                }
                else
                {
                    wc = wc + " and d.FileName like  @FileName ";
                }
                dp.Add("@FileName", "%" + FileName + "%");

            }
            if (ShareName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where c.Name like  @ShareName ";
                }
                else
                {
                    wc = wc + " and c.Name like  @ShareName ";
                }
                dp.Add("@ShareName", "%" + ShareName + "%");

            }
            if ((Datefrom != "" && Datefrom != "null") && (Dateto != "" && Dateto != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and a.datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(Datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(Dateto).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by a.datetime desc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SharedUserFile>("select count(*) Over() AS TotalRows, a.id,a.FileId,a.Userid,a.RoleId,a.datetime,b.Name as Cat1,a.RoleId,c.Name as Cat2,d.FileName as Cat3,d.Icon as Cat4,d.Path as Cat5,d.mb as Field1,a.description from AppSharedUserFile as a inner join AbpUsers as b on a.UserId = b.id inner join AbpRoles as c on a.RoleId = c.id inner join AppFolderUser as d on d.Id = a.FileId  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
            
        }
    }
}
