﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using ezinvmvc.App.FolderUser;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUser
{
    public class NotificationManager : DomainService, INotificationManager
    {
        private readonly IRepository<Notification> _repository;
        private readonly IDapperRepository<Notification> _repositoryDapper;

        public NotificationManager(IRepository<Notification> repository, IDapperRepository<Notification> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateShareUserAsync(Notification entity)
        {
            //var result = _repository.FirstOrDefault(x => x.FileId == entity.FileId && x.UserId == entity.UserId);
            var result = _repository.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<Notification> GetShareUserCountNotificationAsync(int id)
        {
            string wc = " where a.userid = @id and c.field1 is null and a.isdeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Notification>(" SELECT COUNT(*) as Field1 FROM (select Top 5 a.id,a.FileId,a.UserId,a.RoleId,a.DateTime,b.path as Cat1,b.Filename as cat2,b.Icon cat3,c.field1 as cat4,d.NormalizedUserName as cat5 from AppSharedUserFile as a right outer join AppFolderUser as b on a.FileId = b.Id left outer join AbpUsers as d on a.CreatorUserId = d.Id left outer join Appnotification as c on a.Fileid = c.Fileid and a.UserId = c.UserId " + wc + ") as Counts  " + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Notification>> GetShareUSerNotificationListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string UserId = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    UserId = tokens[0];
                }
            }

            var dp = new DynamicParameters();
            string wc = "";
            if (UserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.userid = @UserId and c.field1 is null and a.isdeleted = 0 ";
                }
                else
                {
                    wc = wc + "  and a.userid = @UserId and c.field1 is null and a.isdeleted = 0 ";
                }

                dp.Add("@UserId", UserId);
            }
            string sort = " order by a.datetime asc ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Notification>(" select Top 5 a.id,a.FileId,a.UserId,a.RoleId,a.DateTime,b.path as Cat1,b.Filename as cat2,b.Icon cat3,c.field1 as cat4,d.NormalizedUserName as cat5 from AppSharedUserFile as a right outer join AppFolderUser as b on a.FileId = b.Id left outer join AbpUsers as d on a.CreatorUserId = d.Id left outer join Appnotification as c on a.Fileid = c.Fileid and a.UserId = c.UserId " + wc + sort, dp);

                //var getAll = await _repositoryDapper.QueryAsync<Notification>(" select Top 5 a.id,a.FileId,a.UserId as Field1,a.RoleId as Field2,a.DateTime,b.path as Cat1,b.Filename as cat2,b.Icon cat3,c.Status1 cat4,d.NormalizedUserName as cat5 from AppSharedUserFile as a inner join AppFolderUser as b on a.FileId = b.Id left outer join AbpUsers as d on a.CreatorUserId = d.Id left outer join Appnotification as c on a.id = c.Fileid " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public Task<Notification> UpdateShareUserAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Notification> GetShareRoleCountNotificationAsync(int id)
        {
            string wc = " where a.RoleId = @id and c.field1 is null and c.Userid is null and a.isdeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Notification>(" SELECT COUNT(*) as Field1 FROM (select Top 5 a.id,a.FileId,c.Userid,a.RoleId,a.DateTime,b.path as Cat1,b.Filename as cat2,b.Icon cat3,c.field1 as cat4,d.NormalizedUserName as cat5 from AppSharedRoleFile as a right outer join AppFolderUser as b on a.FileId = b.Id left outer join AbpUsers as d on a.CreatorUserId = d.Id left outer join Appnotification as c on a.Fileid = c.Fileid and a.RoleId = c.RoleId  " + wc + " ) as RoleCounts" + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Notification>> GetShareRoleNotificationListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string UserId = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    UserId = tokens[0];
                }
            }

            var dp = new DynamicParameters();
            string wc = "";
            if (UserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.RoleId = @UserId and c.field1 is null and a.isdeleted = 0 ";
                }
                else
                {
                    wc = wc + "  and a.RoleId = @UserId and c.field1 is null and a.isdeleted = 0 ";
                }

                dp.Add("@UserId", UserId);
            }
            string sort = " order by a.datetime asc ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Notification>(" select Top 5 a.id,a.FileId,c.UserId,a.RoleId,a.DateTime,b.path as Cat1,b.Filename as cat2,b.Icon cat3,c.field1 as cat4,d.NormalizedUserName as cat5 from AppSharedRoleFile as a right outer join AppFolderUser as b on a.FileId = b.Id left outer join AbpUsers as d on a.CreatorUserId = d.Id left outer join Appnotification as c on a.Fileid = c.Fileid and a.RoleId = c.RoleId " + wc + sort, dp);

                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }


        public async Task<IEnumerable<Notification>> GetTopDownloadListAsync()
        {
            var dp = new DynamicParameters();
            string wc = " where a.isdeleted = 0 ";
            string sort = " group by a.FileId,b.filename,c.Name  order by count(*) asc ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Notification>("  select top 10 a.FileId, count(*) as TotalRows, b.filename as Description,c.Name as cat1 from appnotification as a inner join AppFolderUser as b on a.fileid = b.id inner join abpusers as c on b.CreatorUserId = c.Id " + wc + sort, dp);

                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }
    }
}
