﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.EmployeeAttendance
{
    public class EmpAttRecordDetailManager : DomainService, IEmpAttRecordDetailManager
    {
        private readonly IRepository<EmpAttRecordDetail> _repository;
        private readonly IDapperRepository<EmpAttRecordDetail> _repositoryDapper;

        public EmpAttRecordDetailManager(IRepository<EmpAttRecordDetail> repository, IDapperRepository<EmpAttRecordDetail> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(EmpAttRecordDetail entity)
        {
            var result = _repository.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteEmpAttDetailAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetEmpAttDetailAsync(string filter)
        {

            string[] tokens = filter.Split('|');
            string EmpId = "0";
            string AttRecId = "0";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "0")
                {
                    EmpId = tokens[0];
                }
                if (tokens[1].ToString() != "0")
                {
                    AttRecId = tokens[1];
                }
            }


            var dp = new DynamicParameters();
            string wc = " Where IsDeleted = '0' ";

            if (EmpId != "0")
            {
                wc = wc + " And (EmpId = @EmpId) ";
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                wc = wc + " And (EmpId = @EmpId) ";
                dp.Add("@EmpId", "0");
            }

            if (AttRecId != "0")
            {
                wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", "0");
            }

            string sort = " order by date asc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select * from AppEmployeeAttRecordDetails " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }            
        }

        public async Task<IdentityResult> UpdateEmpAttDetailsAsync(EmpAttRecordDetail entity)
        {
            try
            {
                await _repository.UpdateAsync(entity);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Updating: " + ex.ToString());
            }
        }


        public async Task<IEnumerable<EmpAttRecordDetail>> GetBasicSalaryCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                //wc = wc + " EmpId = @EmpId";
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                //wc = wc + " And (EmpId = @EmpId) ";
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select count(AttrecId) as BasicSalaryCurrent from AppEmployeeAttRecordDetails  where EmpId = @EmpId and AttRecId = @AttRecId and days not in (select days from appEmployeeRestday where Empid in (@EmpId,0) )  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetBasicSalaryAdjustmentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                //wc = wc + " EmpId = @EmpId";
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                //wc = wc + " And (EmpId = @EmpId) ";
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                //IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select COUNT (SalaryAdjustment) as BasicSalaryAdjustment from (select Holiday as SalaryAdjustment from AppEmployeeAttRecordDetails where isdeleted = 0 and EmpId = @EmpId and AttRecId = @AttRecId and Holiday !='' and Days not in (select days from AppEmployeeRestday where EmpId in (@EmpId,'0')) union select EnTitlement as SalaryAdjustment from AppEmployeeAttRecordDetails where isdeleted = 0 and EmpId = @EmpId and AttRecId = @AttRecId and EnTitlement != '' and Days not in (select Days from AppEmployeeRestday where EmpId in (@EmpId,'0'))) as AD " + wc + sort, dp);
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select sum(a.Counts) as BasicSalaryAdjustment,sum(a.BasicSalaryAdjustmentAmount2) as BasicSalaryAdjustmentAmount from (select id,count(AttendanceId) as Counts, sum((HolRates* PayrollRatePerHour) * HrsPerDay) as BasicSalaryAdjustmentAmount2 from AppEmployeeAttRecordDetails  as A where EmpId = @EmpId and AttRecId =  @AttRecId and AttendanceId = 'Restday' and timein > '' group by id union select id,count(AttendanceId) as Counts, sum(((HolRates * PayrollRatePerHour)* HrsPerDay)-PayrollRatePerDay) as BasicSalaryAdjustmentAmount2  from AppEmployeeAttRecordDetails  as b where EmpId = @EmpId and AttRecId =  @AttRecId and  timein > '' and holiday != 'Regular' group by id union select id,count(AttendanceId) as Counts, sum((LRates * PayrollRatePerHour)* HrsPerDay) as BasicSalaryAdjustmentAmount2 from AppEmployeeAttRecordDetails  as C where EmpId = @EmpId and AttRecId =  @AttRecId and AttendanceId = 'Leave' and timein > '' and Entitlement > '' group by id ) A " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetAbsensesCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select COUNT (AttendanceId) as AbsensesCurrent from AppEmployeeAttRecordDetails where isdeleted = 0 and EmpId = @EmpId and AttRecId = @AttRecId and AttendanceId = 'Absent' and Days not in (select days from AppEmployeeRestday where EmpId in (@EmpId, 0)) and DateIn is null and Status in ('Active', 'Disapproved') " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetTardinessCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" SELECT sum(convert(int,MinLates)) as TardinessCurrent FROM AppEmployeeAttRecordDetails  where isdeleted = 0 and EmpId = @EmpId and AttRecId = @AttRecId and Days not in (select days from AppEmployeeRestday where EmpId in (@EmpId,0) ) and MinLates > 0 and Status in ('Active')  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetUndertimeCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" SELECT sum(convert(int,UnderTimeHour)) as UndertimeCurrent FROM AppEmployeeAttRecordDetails where  EmpId = @EmpId and AttRecId = @AttRecId and Days not in (select days from AppEmployeeRestday where EmpId in (@EmpId,0)) and UnderTimeHour > '0' and Status  = 'Active' " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetRGOTCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select sum(OtHour) as RGOTCurrent from AppEmployeeAttRecordDetails where EmpId = @EmpId and AttRecId = @AttRecId  and timein > '' and OtHour > 0 " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetRGOTAmountCurrentAsync(int EmpId, string AttRecId)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                dp.Add("@EmpId", '0');
            }

            if (AttRecId != "0")
            {
                dp.Add("@AttRecId", AttRecId);
            }
            else
            {
                dp.Add("@AttRecId", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select sum((OtPerHourPercent * PayrollRatePerHour)* OtHour) as RGOTAmount from AppEmployeeAttRecordDetails where EmpId = @EmpId and AttRecId = @AttRecId and timein > '' and OtHour > 0 " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetSSSCurrentAsync(decimal SSSAmount)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (SSSAmount != '0')
            {
                dp.Add("@SSSAmount", SSSAmount);
            }
            else
            {
                dp.Add("@SSSAmount", '0');
            }

            string sort = " order by Id desc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select Top 1 * from AppSSS where Start < @SSSAmount and Isdeleted = 0 " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetPhilhealthCurrentAsync()
        {
            var dp = new DynamicParameters();
            string wc = " ";

            string sort = " order by Id desc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select * from AppPhilHealth where  Year = datepart(YEAR, getdate()) and IsDeleted = 0 " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetPagIbigCurrentAsync()
        {
            var dp = new DynamicParameters();
            string wc = " ";            

            string sort = " order by Id desc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select * from AppPagIbig where  Year = datepart(YEAR, getdate())  and IsDeleted = 0 " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetTaxAmountAsync(string Compensation, decimal SSSAmount)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (SSSAmount != '0')
            {
                dp.Add("@SSSAmount", SSSAmount);
            }
            else
            {
                dp.Add("@SSSAmount", '0');
            }

            if (Compensation != "")
            {
                dp.Add("@Compensation", Compensation);
            }
            else
            {
                dp.Add("@Compensation", "");
            }

            string sort = " order by id asc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select top 1 * from AppTax where Compensation = @Compensation and Startamount < @SSSAmount and Isdeleted = '0' " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetLoanAsync(int EmpId, int LoanTitle)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (EmpId != '0')
            {
                //wc = wc + " EmpId = @EmpId";
                dp.Add("@EmpId", EmpId);
            }
            else
            {
                //wc = wc + " And (EmpId = @EmpId) ";
                dp.Add("@EmpId", '0');
            }

            if (LoanTitle != '0')
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@LoanTitle", LoanTitle);
            }
            else
            {
                //wc = wc + " And (AttRecId = @AttRecId) ";
                dp.Add("@LoanTitle", "0");
            }

            string sort = " ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>("select sum(LoanAmount) as SSSLoanCurrent,sum(MonthlyAmortization) as SSSLoanAdjustment,sum(EmpLoans) as SSSLoanAmount from (select CASE WHEN DeductionType = 1 THEN MonthlyAmortization/1 WHEN DeductionType = 2 THEN MonthlyAmortization/3  WHEN DeductionType = 3 THEN MonthlyAmortization/2 WHEN DeductionType = 4 THEN MonthlyAmortization/1 END as EmpLoans,* from appEmployeeLoans Where IsDeleted = 0 And(EmpId = @EmpId) and status = 'Active' and LoanTitle = @LoanTitle and SYSDATETIME() > DateStart) as LOANS " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmpAttRecordDetail>> GetTaxAmountDailyAsync(string compensation, decimal SSSAmount)
        {
            var dp = new DynamicParameters();
            string wc = " ";

            if (SSSAmount != '0')
            {
                dp.Add("@SSSAmount", SSSAmount);
            }
            else
            {
                dp.Add("@SSSAmount", '0');
            }

            if (compensation != "")
            {
                dp.Add("@Compensation", compensation);
            }
            else
            {
                dp.Add("@Compensation", "");
            }

            string sort = " order by id desc ";
            try
            {
                IEnumerable<EmpAttRecordDetail> getAll = await _repositoryDapper.QueryAsync<EmpAttRecordDetail>(" select top 1 * from AppTax where Compensation = @Compensation and Startamount < @SSSAmount and Isdeleted = '0' " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
    }
}
