﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.BioAttendance.Models
{
    public class AttendanceManager : DomainService, IAttendanceManager
    {
        private readonly IRepository<Attendance> _repositoryAttendance;
        private readonly IDapperRepository<Attendance> _repositoryAttendanceDapper;

        public AttendanceManager(IRepository<Attendance> repository, IDapperRepository<Attendance> repositoryDapper)
        {
            _repositoryAttendance = repository;
            _repositoryAttendanceDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAttendanceAsync(Attendance entity)
        {
            var result = _repositoryAttendance.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repositoryAttendance.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IEnumerable<Attendance>> GetAttendanceAsync()
        {
            string wc = " Where IsDeleted = 0";
            string sort = " order by DateT desc";
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select distinct CAST(CreationTime AS DATE) as DateT,AttendanceId,Company from AppAttendance  " + wc + sort);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }        

        public async Task<IEnumerable<Attendance>> GetAttendanceByIdAsync(string filter)
        {
            string wc = " Where (AttendanceId = @AttendanceId)";
            string sort = " order by NO,Name,Date asc ";
            var dp = new DynamicParameters();
            dp.Add("@AttendanceId", filter);
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select * from AppAttendance " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Attendance>> GetAttendanceByCodeAsync(string filter)
        {
            //start//
            string[] tokens = filter.Split('|');

            string EmpCode = "";
            string AttId = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    EmpCode = tokens[0];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    AttId = tokens[1];
                }
            }

            //string wc = " Where IsDeleted = 0 ";
            string wc = "  ";
            var dp = new DynamicParameters();

            if (EmpCode != "null")
            {
                wc = wc + " Where (No = @no) ";
                dp.Add("@no", EmpCode);
            }

            if (AttId != "null")
            {
                wc = wc + " And (AttendanceId = @AttId) ";
                dp.Add("@AttId", AttId);
            }
            string sort = " order by NO,Name,Date asc ";
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select * from AppAttendance " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Attendance>> GetAttendance(string filter)
        {
            //start//
            string[] tokens = filter.Split('|');

            string EmpCode = "";
            string AttId = "";
            string DateFrom = "";
            string DateTo = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    EmpCode = tokens[0];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    AttId = tokens[1];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[2].ToString() != "null")
                {
                    DateFrom = tokens[2];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[3].ToString() != "null")
                {
                    DateTo = tokens[3];
                }
            }

            string wc = " ";
            string wc2 = " ";
            string wc3 = " ";
            string wc4 = " ";
            var dp = new DynamicParameters();


            if (DateFrom != "null")
            {
                wc2 =  " @DateFrom ";
                dp.Add("@DateFrom", DateFrom);
            }
            if (DateTo != "null")
            {
                wc3 = " @DateTo ";
                dp.Add("@DateTo", DateTo);
            }
            if (EmpCode != "null")
            {
                wc4 = " @no ";
                dp.Add("@no", EmpCode);
            }
            if (EmpCode != "null")
            {
                wc = wc + " where No = @no ";
                dp.Add("@no", EmpCode);
            }
            
            if (AttId != "null")
            {
                if(EmpCode != "null")
                { 
                    wc = wc + " And (AttendanceId = @AttId) ";                    
                }
                else
                {
                    wc = wc + " where (AttendanceId = @AttId) ";
                }
                dp.Add("@no", EmpCode);
                dp.Add("@AttId", AttId);
            }
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("   ;DECLARE @MinDate DATE = @DateFrom, @MaxDate DATE = @DateTo "
                            + " ; WITH dates_CTE(date) as (select @MinDate Union ALL select DATEADD(day, 1, date) from dates_CTE where date < @MaxDate),  "
                            + " TimeIn AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeIn asc)  As Row1, V1.*from(Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeIn, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V1), "
                            + " TimesOut AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeOut desc) As Row2, V2.TimeOut, V2.DateIn from(Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeOut, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V2), "
                            + " cte4 AS(Select Description,'Holiday' as EmpHoliday, DateFrom from APpHolidays where DateFrom > @MinDate and DateTo < @MaxDate ),cte5 AS(Select Description,'Leave' as EmpLeave, DateFrom, Le.EmployeeCode from appEmployeeLeaves as L inner join AppEmployee as LE on l.EmpId = LE.Id where l.Status = 'Approved' and DateFrom > @DateFrom and DateTo < @DateTo and LE.EmployeeCode = @No) "
                            + " SELECT c1.No ,isnull(c1.AttendanceId, IsNull(c5.Description, 'Absent/RestDay')) as AttendanceId,c3.Date,c1.DateIn,c1.TimeIn,c2.TimeOut,IsNull(c4.Description, c5.Description) as Holiday,IsNull(c4.EmpHoliday, c5.EmpLeave) as EnTitlement FROM TimeIn as c1 "
                            + " full outer join TimesOut as c2 on c1.DateIn = c2.DateIn right outer join dates_CTE as c3 on c1.DateIn = c3.Date left outer join cte4 as c4 on c3.Date = c4.DateFrom left outer join cte5 as c5 on c3.Date = c5.DateFrom WHERE c1.row1 is null or c1.Row1 = '1' and c2.row2 = '1' or c2.row2 is null order by date asc  ", dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Attendance> GetTop1AttendanceAscAsync(string AttId)
        {
            string wc = " Where AttendanceId = @Filter ";
            string sort = " order by Date asc ";
            var dp = new DynamicParameters();
            dp.Add("@Filter", AttId);
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select top 1 AttendanceId,Date from appAttendance  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Attendance> GetTop1AttendanceDescAsync(string AttId)
        {
            string wc = " Where AttendanceId = @Filter  ";
            string sort = " order by Date desc ";
            var dp = new DynamicParameters();
            dp.Add("@Filter", AttId);
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select top 1 AttendanceId,Date from appAttendance  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Attendance> GetRestdaycAsync(int EmpId, string Day)
        {
            string wc = " Where EmpId in (0, @EmpId) and days = @Day and IsDeleted = 0";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@EmpId", EmpId);
            dp.Add("@Day", Day);
            try
            {
                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("select * from appEmployeeRestday  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }


        public async Task<IEnumerable<Attendance>> GetAttendanceVersion2(string filter)
        {
            //start//
            string[] tokens = filter.Split('|');

            string EmpCode = "";
            string AttId = "";
            string DateFrom = "";
            string DateTo = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    EmpCode = tokens[0];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    AttId = tokens[1];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[2].ToString() != "null")
                {
                    DateFrom = tokens[2];
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[3].ToString() != "null")
                {
                    DateTo = tokens[3];
                }
            }

            string wc = " ";
            string wc2 = " ";
            string wc3 = " ";
            string wc4 = " ";
            var dp = new DynamicParameters();


            if (DateFrom != "null")
            {
                wc2 = " @DateFrom ";
                dp.Add("@DateFrom", DateFrom);
            }
            if (DateTo != "null")
            {
                wc3 = " @DateTo ";
                dp.Add("@DateTo", DateTo);
            }
            if (EmpCode != "null")
            {
                wc4 = " @no ";
                dp.Add("@no", EmpCode);
            }
            if (EmpCode != "null")
            {
                wc = wc + " where No = @no ";
                dp.Add("@no", EmpCode);
            }

            if (AttId != "null")
            {
                if (EmpCode != "null")
                {
                    wc = wc + " And (AttendanceId = @AttId) ";
                }
                else
                {
                    wc = wc + " where (AttendanceId = @AttId) ";
                }
                dp.Add("@no", EmpCode);
                dp.Add("@AttId", AttId);
            }
            try
            {
                //IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("  ;DECLARE @MinDate DATE = @DateFrom, @MaxDate DATE = @DateTo "
                //                        + "  ; WITH dates_CTE(date) as (select @MinDate Union ALL select DATEADD(day, 1, date) from dates_CTE where date < @MaxDate),  "

                //                        + "  TimeIn AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeIn asc)  As Row1, V1.* from (Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeIn, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V1), "
                //                        + "  TimesOut AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeOut desc) As Row2, V2.TimeOut, V2.DateIn from(Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeOut, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V2), "
                //                        + "  cte4 AS(Select Description as EmpHoliday,Rates as HolRates,OTRateDescription, DateFrom from APpHolidays where DateFrom > @MinDate and DateTo < @MaxDate ), "
                //                        + "  cte5 AS(Select Description as EmpLeave, DateFrom,Rates as LRates, Le.EmployeeCode from appEmployeeLeaves as L inner join AppEmployee as LE on l.EmpId = LE.Id where l.Status = 'Approved' and DateFrom > @MinDate and DateTo < @MaxDate and LE.EmployeeCode = @No), "
                //                        + "  cte6 AS(Select Id, EmployeeCode from AppEMployee where isdeleted = 0 and StatusId = 1),"
                //                        + "  cte7 AS(Select top 1 AppSal.* from AppEMployeeSalaries as AppSal inner join AppEmployee as Emp on AppSal.EmpId = Emp.Id where AppSal.isdeleted = 0 and Status = 'Active' order by AppSal.Id desc), "
                //                        + "  cte8 AS(select FORMAT(effectivetime, 'HH:mm') as OTEffectiveTime,FORMAT(EffectiveTime2, 'HH:mm') as OTEffectiveTime2,OTDescription,PercentPerOTHour,PercentPerOTHour2 from AppOTTables) "

                //                        + "  SELECT c6.Id,c1.No ,isnull(c1.AttendanceId, IsNull(c4.EmpHoliday, IsNull(c5.EmpLeave, 'Absent/RestDay'))) as AttendanceId,'' as Days,c3.Date as Date,c1.DateIn,FORMAT(c7.TimeIn, 'HH:mm') as WorkIn,c1.TimeIn,FORMAT(c7.MinLate, 'HH:mm') as LateIn, "
                //                        + "  CASE WHEN cast(c7.MinLate as time) > cast(c1.TimeIn as time) THEN '0' ELSE DATEDIFF(MINUTE, cast(c7.TimeIn as time),cast(c1.TimeIn as time)) END AS MinLates, "
                //                        + "  FORMAT(c7.TimeIOut, 'HH:mm') as WorkOut,c2.TimeOut,CONVERT(NUMERIC(18, 0), DATEDIFF(minute, cast(c1.TimeIn as time), cast(c2.TimeOut as time)) / 60) as HrsWorked, "
                //                        + "  CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c7.TimeIOut as time), cast(c2.TimeOut as time))) < 0 THEN '0' ELSE CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) / 60 ) END AS OtHour, "
                //                        + "  CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c2.TimeOut as time), cast(c7.TimeIOut as time))) < 0 THEN '0' ELSE CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c2.TimeOut as time), cast(c7.TimeIOut as time)) / 60 ) END AS UnderTimeHour, "
                //                        + "  isnull(CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c7.TimeIOut as time), cast(c2.TimeOut as time))) < 0 THEN '0' ELSE CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) / 60) > 0 THEN isnull(c4.OTRateDescription, 'Regular') ELSE  c4.OTRateDescription END END, 0) as HolidayDescription, "
                //                        + "  CASE WHEN cast(c2.TimeOut as time) > c8.OTEffectiveTime  THEN c8.PercentPerOTHour ELSE c8.PercentPerOTHour2 END as OTPerHourPercent, "
                //                        + "  c4.EmpHoliday as Holiday,c4.HolRates, c5.EmpLeave as EnTitlement,c5.LRates,c7.DayCount as HrsPerDay,c7.PayrollRatePerHour,c7.DeductionLate,c7.PayrollRatePerDay "

                //                        + "  FROM TimeIn as c1 "
                //                        + "  full outer join TimesOut as c2 on c1.DateIn = c2.DateIn "
                //                        + "  right outer join dates_CTE as c3 on c1.DateIn = c3.Date "
                //                        + "  left outer join cte4 as c4 on c3.Date = c4.DateFrom "
                //                        + "  left outer join cte5 as c5 on c3.Date = c5.DateFrom "
                //                        + "  left outer join cte6 as c6 on c6.EmployeeCode = c1.No "
                //                        + "  left outer join cte7 as c7 on c7.EmpId = c6.Id "
                //                        + "  left outer join cte8 as c8 on c8.OTDescription = CASE WHEN CONVERT(NUMERIC(18, 0),DATEDIFF(MINUTE, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) ) < 0 THEN '0' ELSE CASE WHEN CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) / 60 ) > 0 THEN isnull(c4.OTRateDescription,'Regular') ELSE c4.OTRateDescription END END "

                //                        + "  WHERE c1.row1 is null or c1.Row1 = '1' and c2.row2 = '1' or c2.row2 is null order by date asc  ", dp);

                IEnumerable<Attendance> getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>("  ;DECLARE @MinDate DATE = @DateFrom, @MaxDate DATE = @DateTo  "
                + " ; WITH dates_CTE(date) as (select @MinDate Union ALL select DATEADD(day, 1, date) from dates_CTE where date < @MaxDate),  "
                + " TimeIn AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeIn asc)  As Row1, V1.*from(Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeIn, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V1),  "
                + " TimesOut AS(select Row_Number() Over(Partition by DateIn Order By DateIn desc, TimeOut desc) As Row2, V2.TimeOut, V2.DateIn from(Select distinct CAST(date AS DATE) as DateIn, FORMAT(date, 'HH:mm') as TimeOut, No, AttendanceId from AppAttendance " + wc + " and IsDeleted = 0) as V2),  "
                + " cte4 AS(Select Description as EmpHoliday,Rates as HolRates,OTRateDescription, DateFrom,'Holiday' as Holiday from APpHolidays where DateFrom > @MinDate and DateTo < @MaxDate ), "
                + " cte5 AS(Select L.Leave,Description as EmpLeave, DateFrom,Rates as LRates, Le.EmployeeCode from appEmployeeLeaves as L inner join AppEmployee as LE on l.EmpId = LE.Id where l.Status = 'Approved' and DateFrom > @MinDate and DateTo < @MaxDate and LE.EmployeeCode = @No),  "
                + " cte6 AS(Select Id, EmployeeCode from AppEMployee where isdeleted = 0 and StatusId = 1), "
                + " cte7 AS(Select AppSal.* from AppEMployeeSalaries as AppSal inner join AppEmployee as Emp on AppSal.EmpId = Emp.Id where AppSal.isdeleted = 0 and Status = 'Active'), "
                + " cte8 AS(select FORMAT(effectivetime, 'HH:mm') as OTEffectiveTime,FORMAT(EffectiveTime2, 'HH:mm') as OTEffectiveTime2,OTDescription,PercentPerOTHour,PercentPerOTHour2 from AppOTTables), "
                + " cte9 AS(select EmpId, Description, Days,DescRates,Rates from appEmployeeRestday where isdeleted = 0) "

                + " SELECT c6.Id,c1.No,'' as AttRecId, isnull(c9.Description, IsNull(c5.Leave, IsNull(c4.Holiday, IsNull(c1.AttendanceId,'Absent')))) as AttendanceId,FORMAT(c3.Date, 'ddd') as Days,c3.Date as Date,c1.DateIn,FORMAT(c7.TimeIn, 'HH:mm') as WorkIn,c1.TimeIn,FORMAT(c7.MinLate, 'HH:mm') as LateIn,  "
                + " CASE WHEN cast(c7.MinLate as time) > cast(c1.TimeIn as time) THEN '0' ELSE DATEDIFF(MINUTE, cast(c7.TimeIn as time),cast(c1.TimeIn as time)) END AS MinLates, "
                + " FORMAT(c7.TimeIOut, 'HH:mm') as WorkOut,c2.TimeOut,CONVERT(NUMERIC(18, 0), DATEDIFF(minute, cast(c1.TimeIn as time), cast(c2.TimeOut as time)) / 60) as HrsWorked, "
                + " CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c7.TimeIOut as time), cast(c2.TimeOut as time))) < 0 THEN '0' ELSE CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) / 60 ) END AS OtHour, "
                + " CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c2.TimeOut as time), cast(c7.TimeIOut as time))) < 0 THEN '0' ELSE CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c2.TimeOut as time), cast(c7.TimeIOut as time)) / 60 ) END AS UnderTimeHour, "
                + " isnull(CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(MINUTE, cast(c7.TimeIOut as time), cast(c2.TimeOut as time))) < 0 THEN '0' ELSE CASE WHEN CONVERT(NUMERIC(18, 0), DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time)) / 60) > 0 THEN c4.OTRateDescription ELSE  c4.OTRateDescription END END, 0) as HolidayDescription, "
                + " CASE WHEN cast(c2.TimeOut as time) > c8.OTEffectiveTime  THEN c8.PercentPerOTHour ELSE c8.PercentPerOTHour2 END as OTPerHourPercent, "
                + " isnull(EmpHoliday,c9.DescRates) as Holiday,isnull(c4.HolRates,c9.Rates) as HolRates, c5.EmpLeave as EnTitlement,c5.LRates,c7.DayCount as HrsPerDay,c7.PayrollRatePerHour,c7.DeductionLate,c7.PayrollRatePerDay "

                + " FROM TimeIn as c1 "
                + " full outer join TimesOut as c2 on c1.DateIn = c2.DateIn "
                + " right outer join dates_CTE as c3 on c1.DateIn = c3.Date "
                + " left outer join cte4 as c4 on c3.Date = c4.DateFrom "
                + " left outer join cte5 as c5 on c3.Date = c5.DateFrom "
                + " left outer join cte6 as c6 on c6.EmployeeCode = c1.No "
                + " left outer join cte7 as c7 on c7.EmpId = c6.Id "
                + " left outer join cte8 as c8 on  c8.OTDescription = CASE WHEN cast(c2.TimeOut as time) > cast(c8.OTEffectiveTime as time) THEN isnull(c4.OTRateDescription,'Regular') ELSE CASE WHEN CONVERT(NUMERIC(18, 0),DATEDIFF(minute, cast(c7.TimeIOut as time), cast(c2.TimeOut as time))/ 60 ) > 0 THEN isnull(c4.OTRateDescription,'Regular') ELSE  c4.OTRateDescription END END "
                + " left outer join cte9 as c9 on c9.Days = FORMAT(c3.Date, 'ddd') "
                + " WHERE c1.row1 is null or c1.Row1 = '1' and c2.row2 = '1' or c2.row2 is null order by date asc ", dp);


                return getAll;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<Attendance> GetOtRates(string TimeOut, string HolidayDescription)
        {
            string wc2 = " ";
            string wc3 = " ";
            var dp = new DynamicParameters();

            if (HolidayDescription != null)
            {
                wc2 = "@OTDescription";
                dp.Add("@OTDescription", HolidayDescription);
            }
            else
            {
                wc2 = "@OTDescription";
                dp.Add("@OTDescription", "Regular");
            }
            if (TimeOut != null)
            {
                wc3 = "@TimeOut";
                dp.Add("@TimeOut", TimeOut);
            }
            else
            {
                wc3 = "@TimeOut";
                dp.Add("@TimeOut", "");
            }

            try
            {
                var getAll = await _repositoryAttendanceDapper.QueryAsync<Attendance>(" select Top 1 FORMAT(effectivetime, 'HH:mm') as OTEffectiveTime from AppOTTables where cast(effectivetime as time) < @TimeOut and OTDescription = @OTDescription order by effectivetime desc   ", dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
    }
}
