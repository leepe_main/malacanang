﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ezinvmvc.App.BioAttendance.Models
{
    [Table("AppAttendance")]
    public class Attendance : FullAuditedEntity<int>
    {
        [Required]
        public string AttendanceId { get; set; }

        public string Company { get; set; }

        public string Name { get; set; }

        public int No { get; set; }

        public DateTime? Date { get; set; }

        public int LocId { get; set; }

        public int IdNumber { get; set; }

        public string VerifyCode { get; set; }

        public int CardNo { get; set; }

        public string Status { get; set; }

        [NotMapped]
        public int TotalRows { get; set; }

        [NotMapped]
        public DateTime DateT { get; set; }

        [NotMapped]
        public string DateIn { get; set; }

        [NotMapped]
        public string TimeIn { get; set; }

        [NotMapped]
        public string TimeOut { get; set; }

        [NotMapped]
        public int Hours { get; set; }

        [NotMapped]
        public string Late { get; set; }

        [NotMapped]
        public string UTime { get; set; }

        [NotMapped]
        public int OT { get; set; }

        [NotMapped]
        public string Days { get; set; }

        [NotMapped]
        public string Holiday { get; set; }

        [NotMapped]
        public string EnTitlement { get; set; }

        //New v2
        [NotMapped]
        public int EmpId { get; set; }
        [NotMapped]
        public string WorkIn { get; set; }
        [NotMapped]
        public string LateIn { get; set; }
        [NotMapped]
        public string MinLates { get; set; }
        [NotMapped]
        public string WorkOut { get; set; }
        [NotMapped]
        public string HrsWorked { get; set; }
        [NotMapped]
        public int? OtHour { get; set; }
        [NotMapped]
        public int? UnderTimeHour { get; set; }
        [NotMapped]
        public string HolidayDescription { get; set; }
        [NotMapped]
        public decimal? OTPerHourPercent { get; set; }
        [NotMapped]
        public decimal? HolRates { get; set; }
        [NotMapped]
        public decimal? LRates { get; set; }
        [NotMapped]
        public int? HrsPerDay { get; set; }
        [NotMapped]
        public decimal? PayrollRatePerHour { get; set; }
        [NotMapped]
        public decimal? DeductionLate { get; set; }
        [NotMapped]
        public decimal? PayrollRatePerDay { get; set; }
        [NotMapped]
        public string AttRecId { get; set; }

    }
}
