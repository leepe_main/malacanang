﻿using Abp.Domain.Services;
using ezinvmvc.App.Employees;
using ezinvmvc.App.EmployeesLoans.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ezinvmvc.App.EmployeesLoans
{
    public interface IEmployeeLoansManager : IDomainService
    {
        Task<IdentityResult> CreateEmployeeLoansAsync(EmployeeLoans entity);
        Task<IdentityResult> UpdateEmployeeLoansAsync(EmployeeLoans entity);
        Task<IdentityResult> DeleteEmployeeLoansAsync(int id);

        Task<IEnumerable<EmployeeLoans>> GetEmployeeDetailAsync(int empId);
        Task<IEnumerable<EmployeeLoans>> GetAllEmployeeLoansAsync(string filter);
        Task<IEnumerable<EmployeeLoans>> GetEmployeeLoansIdAsync(int Id);

    }
}
