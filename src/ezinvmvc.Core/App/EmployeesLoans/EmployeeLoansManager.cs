﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using ezinvmvc.App.EmployeesLoans.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.EmployeesLoans
{
    public class EmployeeLoansManager : DomainService, IEmployeeLoansManager
    {
        private readonly IRepository<EmployeeLoans> _repositoryEmployeeLoans;
        private readonly IDapperRepository<EmployeeLoans> _repositoryEmployeeLoanssDapper;


        public EmployeeLoansManager(IRepository<EmployeeLoans> repository, IDapperRepository<EmployeeLoans> repositoryDapper)
        {
            _repositoryEmployeeLoans = repository;
            _repositoryEmployeeLoanssDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateEmployeeLoansAsync(EmployeeLoans entity)
        {
            var result = _repositoryEmployeeLoans.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repositoryEmployeeLoans.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteEmployeeLoansAsync(int id)
        {
            var result = _repositoryEmployeeLoans.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repositoryEmployeeLoans.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IdentityResult> UpdateEmployeeLoansAsync(EmployeeLoans entity)
        {
            try
            {
                await _repositoryEmployeeLoans.UpdateAsync(entity);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Updating: " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmployeeLoans>> GetAllEmployeeLoansAsync(string filter)
        {
            string wc = " Where a.IsDeleted = 0 And (a.EmpId = @empId) and a.status = 'Active'";
            string sort = " order by a.Id desc";
            var dp = new DynamicParameters();
            dp.Add("@empId", filter);
            try
            {
                IEnumerable<EmployeeLoans> getAll = await _repositoryEmployeeLoanssDapper.QueryAsync<EmployeeLoans>("select a.*,b.FirstName+ ' ' +b.MiddleName +' '+ b.LastName as FullName,b.EmployeeCode,c.LoanTitleName,d.LoanTypeName from AppEmployeeLoans as a with (nolock) inner join AppEmployee as b with (nolock) on a.EmpId = b.Id inner join appLoanTitle as c with(nolock) on a.LoanTitle = c.Id inner join appLoanType as d with(nolock) on a.LoanType = d.Id " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<EmployeeLoans>> GetEmployeeDetailAsync(int empId)
        {
            string wc = " Where IsDeleted = 0 And (Id = @empId) and statusId = 1";
            string sort = " order by Id desc";
            var dp = new DynamicParameters();
            dp.Add("@empId", empId);
            try
            {
                IEnumerable<EmployeeLoans> getAll = await _repositoryEmployeeLoanssDapper.QueryAsync<EmployeeLoans>("select * from AppEmployee " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<IEnumerable<EmployeeLoans>> GetEmployeeLoansIdAsync(int Id)
        {
            string wc = " Where a.IsDeleted = 0 And (a.Id = @Id) and a.status = 'Active'";
            string sort = " order by a.Id desc";
            var dp = new DynamicParameters();
            dp.Add("@Id", Id);
            try
            {
                IEnumerable<EmployeeLoans> getAll = await _repositoryEmployeeLoanssDapper.QueryAsync<EmployeeLoans>("select a.*,b.FirstName,b.MiddleName,b.LastName,b.Address from AppEmployeeLoans as a with (nolock) inner join AppEmployee as b with (nolock) on a.EmpId = b.Id " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

    }
}
