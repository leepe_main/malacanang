﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Microsoft.AspNetCore.Identity;
using Dapper;
using Abp.Dapper.Repositories;
using System;
using System.Linq;

namespace ezinvmvc.App.Clients
{
    public class ClientManager : DomainService, IClientManager
    {
        private readonly IRepository<Client> _repository;
        private readonly IDapperRepository<Client> _repositoryDapper;

        public ClientManager(IRepository<Client> repository, IDapperRepository<Client> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }
        public async Task<IdentityResult> CreateAsync(Client entity)
        {
            var result = _repository.FirstOrDefault(x => x.Name == entity.Name);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAndGetIdAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<IEnumerable<Client>> GetAllList(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            string[] tokens = filter.Split('|');
            string clientfilter = "", statusfilter = "", accountexecutive = "";
            if(tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    clientfilter = tokens[0];
                }
            }
            if(tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    statusfilter = tokens[1];
                }
            }
            if (tokens.Length > 2)
            {
                if (tokens[2].ToString() != "null")
                {
                    accountexecutive = tokens[2].ToString();
                }
            }

            string wc = " Where isdeleted = 0 ";
            var dp = new DynamicParameters();
            //if (filter != null && filter.Trim() != "")
            //{
            //    wc = wc + " And (v.name like @Filter) ";
            //}
            if (clientfilter != "")
            {
                wc = wc + " And (v.name like @Filter) ";
                dp.Add("@Filter", "%" + clientfilter + "%");
            }
            if(statusfilter != "")
            {
                wc = wc + " And v.StatusId in (" + statusfilter + ")";
            }
            if (accountexecutive != "")
            {
                wc = wc + " And v.AssignedToId = @empid ";
                dp.Add("@empid", Convert.ToInt32(accountexecutive));
            }
            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " " + lastWord;
            }
            else
            {
                sort = " order by id asc ";
            }
            try
            {
                if (!forexport)
                {
                    //var getAll = await _repositoryDapper.QueryAsync<Client>("select count(*) Over() TotalRows,v.* from appclients v " + wc + sort + " Limit " + offset + "," + fetch, dp);
                    var getAll = await _repositoryDapper.QueryAsync<Client>("select count(*) Over() TotalRows, FORMAT(v.Id, '000000000#') AS refNo, v.* from (SELECT c.*, c.Address + ', ' + cty.Name + ', ' + prv.Name + ', ' + ctr.Name AS CompleteAddress, i.name as industry, vat.name as VatType, ctr.name as Country, prv.Name as Province, cty.Name as City, cmp.Name as Company, st.Status, e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName AS AssignedTo from appclients as c inner join appindustries as i on c.IndustryId=i.Id inner join apptaxtypes as vat on c.VATtypeId=vat.id inner join appcountries as ctr on c.countryid=ctr.id inner join appprovinces as prv on c.provinceid=prv.id inner join appcities as cty on c.cityid=cty.id inner join appcompany as cmp on c.companyid=cmp.id INNER JOIN AppStatusTypes st ON c.StatusId=st.Code AND st.TransactionCode = 105 LEFT OUTER JOIN AppEmployee AS e ON c.AssignedToId=e.Id) v " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " ROWS ONLY ", dp);
                    return getAll;
                }
                else
                {
                    var getAll = await _repositoryDapper.QueryAsync<Client>("select count(*) Over() TotalRows, FORMAT(v.Id, '000000000#') AS refNo, v.* from (SELECT c.*, c.Address + ', ' + cty.Name + ', ' + prv.Name + ', ' + ctr.Name AS CompleteAddress, i.name as industry, vat.name as VatType, ctr.name as Country, prv.Name as Province, cty.Name as City, cmp.Name as Company, st.Status, e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName AS AssignedTo from appclients as c inner join appindustries as i on c.IndustryId=i.Id inner join apptaxtypes as vat on c.VATtypeId=vat.id inner join appcountries as ctr on c.countryid=ctr.id inner join appprovinces as prv on c.provinceid=prv.id inner join appcities as cty on c.cityid=cty.id inner join appcompany as cmp on c.companyid=cmp.id INNER JOIN AppStatusTypes st ON c.StatusId=st.Code AND st.TransactionCode = 105 LEFT OUTER JOIN AppEmployee AS e ON c.AssignedToId=e.Id) v " + wc + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Client>> GetAllClientList()
        {
            return await _repository.GetAllListAsync();
        }

        public async Task<Client> GetByIdAsync(int id)
        {
            //var result = _repository.FirstOrDefault(x => x.Id == id);
            //if (result != null)
            //{
            //    return await _repository.GetAsync(id);
            //}
            //else
            //{
            //    throw new UserFriendlyException("No Data Found!");
            //}
            string wc = " Where c.Id = @Id ";

            var dp = new DynamicParameters();
            dp.Add("@Id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Client>("SELECT c.*, c.Address + ', ' + cty.Name + ', ' + prv.Name + ', ' + ctr.Name AS CompleteAddress, i.name as industry, vat.name as VatType, ctr.name as Country, prv.Name as Province, cty.Name as City, cmp.Name as Company, st.Status, e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName AS AssignedTo from appclients as c inner join appindustries as i on c.IndustryId=i.Id inner join apptaxtypes as vat on c.VATtypeId=vat.id inner join appcountries as ctr on c.countryid=ctr.id inner join appprovinces as prv on c.provinceid=prv.id inner join appcities as cty on c.cityid=cty.id inner join appcompany as cmp on c.companyid=cmp.id INNER JOIN AppStatusTypes st ON c.StatusId=st.Code AND st.TransactionCode = 105 LEFT OUTER JOIN AppEmployee AS e ON c.AssignedToId=e.Id " + wc, dp);

                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<Client>> GetDetailsByIdAsync(int id)
        {
            string wc = " Where isdeleted = 0 ";
            var dp = new DynamicParameters();
            if (id > 0)
            {
                wc = wc + " And (v.id = @Filter) ";
                dp.Add("@Filter", id);
            }

            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Client>("select count(*) Over() TotalRows, FORMAT(v.Id, '000000000#') AS refNo, v.* from (SELECT c.*, c.Address + ', ' + cty.Name + ', ' + prv.Name + ', ' + ctr.Name AS CompleteAddress, i.name as industry, vat.name as VatType, ctr.name as Country, prv.Name as Province, cty.Name as City, cmp.Name as Company, st.Status, e.FirstName + ' ' + e.MiddleName + ' ' + e.LastName AS AssignedTo from appclients as c inner join appindustries as i on c.IndustryId=i.Id inner join apptaxtypes as vat on c.VATtypeId=vat.id inner join appcountries as ctr on c.countryid=ctr.id inner join appprovinces as prv on c.provinceid=prv.id inner join appcities as cty on c.cityid=cty.id inner join appcompany as cmp on c.companyid=cmp.id INNER JOIN AppStatusTypes st ON c.StatusId=st.Code AND st.TransactionCode = 105 LEFT OUTER JOIN AppEmployee AS e ON c.AssignedToId=e.Id) v " + wc, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IdentityResult> UpdateAsync(Client entity)
        {
            try
            {
                await _repository.UpdateAsync(entity);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Updating: " + ex.ToString());
            }
        }
    }
}
