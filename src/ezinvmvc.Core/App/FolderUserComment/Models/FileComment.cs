﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ezinvmvc.App.FolderUserComment
{
    [Table("AppFileComment")]
    public class FileComment : FullAuditedEntity<int>
    {
        public int UserId { get; set; }

        public int FileId { get; set; }

        public DateTime DateTime { get; set; }

        public string Comments { get; set; }

        //Not Use Extra Field//

        public string Cat1 { get; set; }

        public string Cat2 { get; set; }

        public string Cat3 { get; set; }

        public string Cat4 { get; set; }

        public string Cat5 { get; set; }

        public string Field1 { get; set; }

        public string Field2 { get; set; }

        public string Field3 { get; set; }

        public string Field4 { get; set; }

        public string Field5 { get; set; }

        public string FolderId { get; set; }

        //Not Use Extra Field//

        [NotMapped]
        public string UserImage { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string FileName { get; set; }

        [NotMapped]
        public string TotalRows { get; set; }

    }
}
