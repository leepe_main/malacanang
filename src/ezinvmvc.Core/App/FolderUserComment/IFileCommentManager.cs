﻿using Abp.Domain.Services;
using ezinvmvc.App.FolderUser;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public interface IFileCommentManager : IDomainService
    {
        Task<IdentityResult> CreateAsync(FileComment entity);

        Task<FileComment> GetByIdAsync(int id);

        Task<IdentityResult> DeleteAsync(int id);

        Task<IEnumerable<FileComment>> GetAllListAsync(string filter);

        Task<FileComment> GetTotalCommentAsync(int CreatorUserId);

        Task<FileComment> GetFileCommentAsync(int CreatorUserId);

        Task<FileComment> GetCommentAsync(int id);

        Task<IEnumerable<FileComment>> GetAllCommentListAsync(string filter);

        Task<FileComment> UpdateFolderIdCommentAsync(int id);
    }
}
