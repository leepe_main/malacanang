﻿using Abp.Domain.Services;
using ezinvmvc.App.FolderUserComment.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public interface IAnnouncementManager : IDomainService
    {
        Task<IdentityResult> CreateAsync(Announcement entity);

        Task<Announcement> GetByIdAsync(int id);

        Task<IdentityResult> DeleteAsync(int id);

        Task<IEnumerable<Announcement>> GetAllListAsync(string filter);

        Task<Announcement> GetAnnouncementAsync(int id);

    }
}
