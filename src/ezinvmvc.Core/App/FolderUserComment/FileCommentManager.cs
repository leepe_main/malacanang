﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using ezinvmvc.App.FolderUser;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public class FileCommentManager : DomainService, IFileCommentManager
    {
        private readonly IRepository<FileComment> _repository;
        private readonly IDapperRepository<FileComment> _repositoryDapper;

        public FileCommentManager(IRepository<FileComment> repository, IDapperRepository<FileComment> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(FileComment entity)
        {
            var result = _repository.FirstOrDefault(x => x.Id == entity.Id );
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<FileComment>> GetAllListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string datefrom = "";
            string dateTo = "";
            string CreatorUserId = "";
            string fileId = "";
            string status = "";
            string UserId = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        dateTo = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            CreatorUserId = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                fileId = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    status = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        UserId = tokens[5];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " where a.IsDeleted = 0 ";
            if (CreatorUserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where c.CreatorUserId =  @CreatorUserId ";
                }
                else
                {
                    wc = wc + " and c.CreatorUserId =  @CreatorUserId ";
                }

                dp.Add("@CreatorUserId", CreatorUserId);
                //wc = wc + " where CreatorUserId =  @EnpId ";

            }
            if (fileId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.fileId =  @fileId ";
                }
                else
                {
                    wc = wc + " and a.fileId =  @fileId ";
                }
                dp.Add("@fileId", fileId);

            }
            if (status != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where status =  @status ";
                }
                else
                {
                    wc = wc + " and status =  @status ";
                }
                dp.Add("@status", status);
            }
            if (UserId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.UserId =  @UserId ";
                }
                else
                {
                    wc = wc + " and a.UserId =  @UserId ";
                }
                dp.Add("@UserId", UserId);
            }
            if ((datefrom != "" && datefrom != "null") && (dateTo != "" && dateTo != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and a.datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(dateTo).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by a.datetime asc  ";
            try
            {
                //var getAll = await _repositoryDapper.QueryAsync<FolderUsers>("select count(*) Over() AS TotalRows, * from AppFolderUser " + wc + sort, dp);

                var getAll = await _repositoryDapper.QueryAsync<FileComment>("select count(*) Over() AS TotalRows, a.*,b.Name as UserName,c.CreatorUserid as FolderId,c.FileName from AppFileComment as a inner join AbpUsers as b on a.UserId = b.Id inner join AppFolderUser as c on a.FileId = c.id " + wc + "" + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<FileComment> GetByIdAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                return await _repository.GetAsync(id);
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }

        public async Task<FileComment> GetTotalCommentAsync(int CreatorUserId)
        {
            string wc = " where fileid in (select id as FiledId from AppFolderUser where CreatoruserId = @CreatorUserId) and folderId = 0 and IsDeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@CreatorUserId", CreatorUserId);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FileComment>(" select count (*) as TotalRows from appFileComment  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<FileComment> GetFileCommentAsync(int fileId)
        {
            string wc = "  where fileId = @fileId and FolderId = 0 and IsDeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@fileId", fileId);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FileComment>(" select count (*) as TotalRows from appFileComment  " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }

        }

        public async Task<FileComment> GetCommentAsync(int id)
        {
            string wc = "  where a.Id = @Id and a.IsDeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@Id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FileComment>(" select a.Id,b.CreatorUserId,a.UserId,c.Name,a.FileId,b.FileName as cat1,b.Icon as cat2,b.Extention as cat3,a.comments,a.DateTime,a.folderId from AppFileComment as a inner join AppFolderUser as b on a.FileId = b.id inner join AbpUsers as c on a.UserId = c.Id " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IEnumerable<FileComment>> GetAllCommentListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string datefrom = "";
            string dateTo = "";
            string id = "";
            string creatorUserId = "";
            string commentUserName = "";
            string fileId = "";
            string fileName = "";
            string folderId = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        dateTo = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            id = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                creatorUserId = tokens[3];
                            }
                            if (tokens.Length > 4)
                            {
                                if (tokens[4].ToString() != "null")
                                {
                                    commentUserName = tokens[4];
                                }
                                if (tokens.Length > 5)
                                {
                                    if (tokens[5].ToString() != "null")
                                    {
                                        fileId = tokens[5];
                                    }
                                    if (tokens.Length > 6)
                                    {
                                        if (tokens[6].ToString() != "null")
                                        {
                                            fileName = tokens[6];
                                        }
                                        if (tokens.Length > 7)
                                        {
                                            if (tokens[7].ToString() != "null")
                                            {
                                                folderId = tokens[7];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " where a.IsDeleted = 0 ";
            if ((datefrom != "" && datefrom != "null") && (dateTo != "" && dateTo != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and a.datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(dateTo).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            if (id != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.id =  @id ";
                }
                else
                {
                    wc = wc + " and a.id =  @id ";
                }

                dp.Add("@id", id);

            }
            if (creatorUserId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.creatorUserId =  @creatorUserId ";
                }
                else
                {
                    wc = wc + " and b.creatorUserId =  @creatorUserId ";
                }
                dp.Add("@creatorUserId", creatorUserId);

            }
            if (commentUserName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where c.name like  @commentUserName ";
                }
                else
                {
                    wc = wc + " and c.name like  @commentUserName ";
                }
                dp.Add("@commentUserName", "%" + commentUserName + "%");
            }
            if (fileId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.fileId =  @fileId ";
                }
                else
                {
                    wc = wc + " and a.fileId =  @fileId ";
                }
                dp.Add("@fileId", fileId);
            }
            if (fileName != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.fileName like  @fileName ";
                }
                else
                {
                    wc = wc + " and b.fileName like  @fileName ";
                }
                dp.Add("@fileName", "%" + fileName + "%");
            }
            if (folderId != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.folderId =  @folderId ";
                }
                else
                {
                    wc = wc + " and a.folderId = @folderId ";
                }
                dp.Add("@folderId", folderId);
            }

            string sort = " order by a.Folderid,a.datetime asc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FileComment>(" select top 50 a.Id,b.CreatorUserId,a.UserId,c.Name as cat5,a.FileId,b.path as cat1,b.FileName as cat2,'fa '+b.Icon as cat3,b.Extention as cat4,a.comments,a.DateTime,a.folderId,isnull(d.UserImagePath,'images/avatar/user.png') as UserImage from AppFileComment as a inner join AppFolderUser as b on a.FileId = b.id inner join AbpUsers as c on a.UserId = c.Id left join appemployee as d on a.CreatorUserId = d.CreatorUserId  " + wc + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }
        }

        public async Task<FileComment> UpdateFolderIdCommentAsync(int id)
        {
            string wc = " where Id = @Id and IsDeleted = 0 ";
            string sort = "";
            var dp = new DynamicParameters();
            dp.Add("@Id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<FileComment>(" update AppFileComment set Folderid = 1 " + wc + sort, dp);
                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }
    }
}
