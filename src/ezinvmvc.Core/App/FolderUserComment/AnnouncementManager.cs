﻿using Abp.Dapper.Repositories;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Dapper;
using ezinvmvc.App.FolderUserComment.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ezinvmvc.App.FolderUserComment
{
    public class AnnouncementManager : DomainService, IAnnouncementManager
    {
        private readonly IRepository<Announcement> _repository;
        private readonly IDapperRepository<Announcement> _repositoryDapper;

        public AnnouncementManager(IRepository<Announcement> repository, IDapperRepository<Announcement> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(Announcement entity)
        {
            var result = _repository.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<Announcement>> GetAllListAsync(string filter)
        {
            string[] tokens = filter.Split('|');
            string datefrom = "";
            string dateTo = "";
            string creatorUserId = "";
            string username = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    datefrom = tokens[0];
                }
                if (tokens.Length > 1)
                {
                    if (tokens[1].ToString() != "null")
                    {
                        dateTo = tokens[1];
                    }
                    if (tokens.Length > 2)
                    {
                        if (tokens[2].ToString() != "null")
                        {
                            creatorUserId = tokens[2];
                        }
                        if (tokens.Length > 3)
                        {
                            if (tokens[3].ToString() != "null")
                            {
                                username = tokens[3];
                            }
                        }
                    }
                }
            }
            var dp = new DynamicParameters();
            string wc = " where a.IsDeleted = 0 ";
            if (creatorUserId != "")
            {
                if (wc == "")
                {
                    wc = wc + " where a.CreatorUserId =  @CreatorUserId ";
                }
                else
                {
                    wc = wc + " and a.CreatorUserId =  @CreatorUserId ";
                }

                dp.Add("@CreatorUserId", creatorUserId);
            }
            if (username != "")
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where b.username like @username ";
                }
                else
                {
                    wc = wc + " and b.username like  @username ";
                }
                dp.Add("@username", "%" + username + "%");

            }
            if ((datefrom != "" && datefrom != "null") && (dateTo != "" && dateTo != "null"))
            {
                if (string.IsNullOrEmpty(wc))
                {
                    wc = wc + " where a.datetime between @StartDate and @EndDate ";
                }
                else
                {
                    wc = wc + " and a.datetime between @StartDate and @EndDate ";
                }
                dp.Add("@StartDate", Convert.ToDateTime(datefrom).ToString("MM/dd/yyyy") + " 00:00:00");
                dp.Add("@EndDate", Convert.ToDateTime(dateTo).ToString("MM/dd/yyyy") + " 23:59:59");
            }
            string sort = " order by a.datetime asc  ";
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<Announcement>("select count(*) Over() AS TotalRows, a.*,b.UserName from AppAnnouncement as a inner join AbpUsers as b on a.CreatorUserId = b.Id " + wc + "" + sort, dp);
                return getAll;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());

            }

        }

        public Task<Announcement> GetAnnouncementAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Announcement> GetByIdAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                return await _repository.GetAsync(id);
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");
            }
        }
    }
}
