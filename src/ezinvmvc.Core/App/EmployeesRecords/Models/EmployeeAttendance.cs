﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ezinvmvc.App.EmployeesRecords
{
    [Table("appEmployeeAttendance")]
    public class EmployeeAttendance : FullAuditedEntity<int>
    {
        //public int EmpId { get; set; }

        //public DateTime? Date { get; set; }

        //public DateTime? time { get; set; }

        //public string Status { get; set; }

        //[NotMapped]
        //public int TotalRows { get; set; }

        public int EmpId { get; set; }

        public string AttRecId { get; set; }

        public string AttendanceId { get; set; }

        public string Days { get; set; }

        public DateTime? Date { get; set; }

        public DateTime? DateIn { get; set; }

        public string TimeIn { get; set; }

        public string TimeOut { get; set; }

        [Required]
        [StringLength(ezinvmvcConsts.MaxLenght328, ErrorMessage = ezinvmvcConsts.ErrorMessage328)]
        public string Description { get; set; }

        public string Status { get; set; }

        [NotMapped]
        public int TotalRows { get; set; }
    }
}
