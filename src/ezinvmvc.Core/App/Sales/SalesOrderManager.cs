﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using Microsoft.AspNetCore.Identity;
using Dapper;
using Abp.Dapper.Repositories;
using System;
using System.Linq;

namespace ezinvmvc.App.Sales
{
    public class SalesOrderManager : DomainService, ISalesOrderManager
    {
        private readonly IRepository<SalesOrder> _repository;
        private readonly IDapperRepository<SalesOrder> _repositoryDapper;
        public SalesOrderManager(IRepository<SalesOrder> repository, IDapperRepository<SalesOrder> repositoryDapper)
        {
            _repository = repository;
            _repositoryDapper = repositoryDapper;
        }

        public async Task<IdentityResult> CreateAsync(SalesOrder entity)
        {
            var result = _repository.FirstOrDefault(x => x.Id == entity.Id);
            if (result != null)
            {
                throw new UserFriendlyException("Already exist!");
            }
            else
            {
                await _repository.InsertAndGetIdAsync(entity);
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var result = _repository.FirstOrDefault(x => x.Id == id);
            if (result != null)
            {
                await _repository.DeleteAsync(result);
                return IdentityResult.Success;
            }
            else
            {
                throw new UserFriendlyException("No Data Found!");

            }
        }

        public async Task<IEnumerable<SalesOrder>> GetAllList(string filter, string sorting, int offset, int fetch, bool forexport)
        {
            string[] tokens = filter.Split('|');

            string idfilter = "";
            string clientfilter = "";
            string statusfilter = "";
            string startdatefilter = "";
            string enddatefilter = "";
            string clientidfilter = "";

            if (tokens.Length > 0)
            {
                if (tokens[0].ToString() != "null")
                {
                    idfilter = tokens[0].ToString();
                }
            }
            if (tokens.Length > 1)
            {
                if (tokens[1].ToString() != "null")
                {
                    clientfilter = tokens[1].ToString();
                }
            }
            if (tokens.Length > 2)
            {
                if (tokens[2].ToString() != "null")
                {
                    statusfilter = tokens[2].ToString();
                }
            }
            if (tokens.Length > 4)
            {
                if (tokens[3].ToString() != "null" && tokens[4].ToString() != "null")
                {
                    startdatefilter = tokens[3].ToString();
                    enddatefilter = tokens[4].ToString();
                }
            }
            if (tokens.Length > 5)
            {
                if (tokens[5].ToString() != "null")
                {
                    clientidfilter = tokens[5].ToString();
                }
            }

            string wc = " Where so.isdeleted = 0 ";
            var dp = new DynamicParameters();

            if (idfilter != "")
            {
                wc = wc + " And so.code = @Id ";
                dp.Add("@Id", idfilter);
            }
            if (clientfilter != "")
            {
                wc = wc + " And c.name like @Client ";
                dp.Add("@Client", "%" + clientfilter + "%");
            }
            if (statusfilter != "")
            {
                statusfilter = "'" + statusfilter.Replace(",", "','") + "'";
                wc = wc + " And so.statusid  in (" + statusfilter + ") ";
            }

            if (startdatefilter != "" && enddatefilter != "")
            {
                wc = wc + " And so.transactiontime  between @StartDate and @EndDate ";
                dp.Add("@StartDate", startdatefilter);
                dp.Add("@EndDate", enddatefilter);
            }
            if(clientidfilter != "")
            {
                wc = wc + " And so.ClientId = @ClientId ";
                dp.Add("@ClientId", clientidfilter);
            }

            string sort = "";
            if (sorting.Trim().Length > 0)
            {
                var firstWord = sorting.Split(' ').First();
                var lastWord = sorting.Split(' ').Last();
                var firstlupper = firstWord.First().ToString().ToUpper();
                var finalfield = firstlupper + firstWord.Substring(1);
                sort = " order by " + finalfield + " " + lastWord;
            }
            else
            {
                sort = " order by Id asc";
            }
            try
            {
                if (!forexport)
                {
                    var getAll = await _repositoryDapper.QueryAsync<SalesOrder>("select count(*) Over() TotalRows,so.*,c.Name Client,ss.Status, 	CONVERT(VARCHAR(10), TransactionTime, 101) TransactionTimeF, 	CONVERT(VARCHAR(10), DeliveryTime, 101) DeliveryTimeF,e.FirstName + ' ' + e.LastName SalesAgent  from appsalesorders so inner join appclients c on c.id = so.clientid inner join appseriestype st on st.id = so.SeriesTypeId inner join appstatustypes ss on ss.code = so.StatusId and ss.transactioncode = 101 inner join AppEmployee e on e.Id = so.SalesAgentId " + wc + sort + " OFFSET " + offset + " ROWS FETCH NEXT " + fetch + " ROWS ONLY ", dp);
                    return getAll;
                }
                else
                {
                    var getAll = await _repositoryDapper.QueryAsync<SalesOrder>("select count(*) Over() TotalRows,so.*,c.Name Client,ss.Status, 	CONVERT(VARCHAR(10), TransactionTime, 101) TransactionTimeF, 	CONVERT(VARCHAR(10), DeliveryTime, 101) DeliveryTimeF,e.FirstName + ' ' + e.LastName SalesAgent  from appsalesorders so inner join appclients c on c.id = so.clientid inner join appseriestype st on st.id = so.SeriesTypeId inner join appstatustypes ss on ss.code = so.StatusId and ss.transactioncode = 101 inner join AppEmployee e on e.Id = so.SalesAgentId " + wc + sort, dp);
                    return getAll;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<SalesOrder> GetByIdAsync(int id)
        {
            string wc = " Where so.Id = @Id ";

            var dp = new DynamicParameters();
            dp.Add("@Id", id);
            try
            {
                var getAll = await _repositoryDapper.QueryAsync<SalesOrder>("select count(*) Over() TotalRows,so.*,c.Name Client,ss.Status, 	CONVERT(VARCHAR(10), TransactionTime, 101) TransactionTimeF, 	CONVERT(VARCHAR(10), DeliveryTime, 101) DeliveryTimeF,e.FirstName + ' ' + e.LastName SalesAgent  from appsalesorders so inner join appclients c on c.id = so.clientid inner join appseriestype st on st.id = so.SeriesTypeId inner join appstatustypes ss on ss.code = so.StatusId and ss.transactioncode = 101 inner join AppEmployee e on e.Id = so.SalesAgentId " + wc, dp);

                return getAll.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Internal Error, " + ex.ToString());
            }
        }

        public async Task<IdentityResult> UpdateAsync(SalesOrder entity)
        {
            try
            {
                await _repository.UpdateAsync(entity);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Updating: " + ex.ToString());
            }
        }
    }
}
