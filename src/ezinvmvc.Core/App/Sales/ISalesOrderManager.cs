﻿using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ezinvmvc.App.Sales
{
   public interface ISalesOrderManager : IDomainService
    {
        Task<IEnumerable<SalesOrder>> GetAllList(string filter, string sorting, int offset, int fetch, bool forexport);
        Task<SalesOrder> GetByIdAsync(int id);
        Task<IdentityResult> CreateAsync(SalesOrder entity);
        Task<IdentityResult> UpdateAsync(SalesOrder entity);
        Task<IdentityResult> DeleteAsync(int id);
    }
}
