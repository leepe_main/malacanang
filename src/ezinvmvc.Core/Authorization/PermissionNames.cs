﻿namespace ezinvmvc.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Purchase_Orders = "Pages.Purchase.Orders";
        public const string Pages_Purchase_Orders_Create = "Pages.Purchase.Orders.Create";
        public const string Pages_Purchase_Orders_Edit = "Pages.Purchase.Orders.Edit";
        public const string Pages_Purchase_Orders_Delete = "Pages.Purchase.Orders.Delete";
        public const string Pages_Purchase_Orders_Export = "Pages.Purchase.Orders.Export";

        public const string Pages_Purchase_Receipts = "Pages.Purchase.Receipts";

        public const string Pages_Invoices = "Pages.Invoices";

        public const string Pages_StockTransfers = "Pages.StockTransfers";

        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Master_Data  = "Master.Data";

        public const string Master_Roles = "Master.Roles";

        public const string Master_Warehouse = "Master.Warehouse";
        public const string Master_Buyers = "Master.Buyers";

        public const string Master_Vendors = "Master.Vendors";
        public const string Master_Vendors_Create = "Master.Vendors.Create";
        public const string Master_Vendors_Edit = "Master.Vendors.Edit";
        public const string Master_Vendors_Delete = "Master.Vendors.Delete";
        public const string Master_Vendors_Export = "Master.Vendors.Export";

        public const string Master_Clients = "Master.Clients";
        public const string Master_Clients_AllAccounts = "Master.Clients.AllAccounts";
        public const string Master_Clients_Create = "Master.Clients.Create";
        public const string Master_Clients_Edit = "Master.Clients.Edit";
        public const string Master_Clients_Approve = "Master.Clients.Approve";
        public const string Master_Clients_Delete = "Master.Clients.Delete";
        public const string Master_Clients_Export = "Master.Clients.Export";

        public const string Master_Products  = "Master.Products";
        public const string Master_Products_Create  = "Master.Products.Create";
        public const string Master_Products_Edit  = "Master.Products.Edit";
        public const string Master_Products_Delete = "Master.Products.Delete";
        public const string Master_Products_Export = "Master.Products.Export";

        public const string Pages_Rfq = "Pages.Rfq";
        public const string Pages_Rfq_AllAccounts = "Pages.Rfq.AllAccounts";
        public const string Pages_Rfq_Create = "Pages.Rfq.Create";
        public const string Pages_Rfq_Edit = "Pages.Rfq.Edit";
        public const string Pages_Rfq_Delete = "Pages.Rfq.Delete";
        public const string Pages_Rfq_Export = "Pages.Rfq.Export";
        public const string Pages_Rfq_Assign = "Pages.Rfq.Assign";

        public const string Pages_Assignment = "Pages.Assignment";
        public const string Pages_Assignment_AllAccounts = "Pages.Assignment.AllAccounts";
        public const string Pages_Assignment_Create = "Pages.Assignment.Create";
        public const string Pages_Assignment_Edit = "Pages.Assignment.Edit";
        public const string Pages_Assignment_Delete = "Pages.Assignment.Delete";
        public const string Pages_Assignment_Export = "Pages.Assignment.Export";
        
        public const string Pages_Sales_Orders = "Pages.Sales.Orders";
        public const string Pages_Sales_Orders_Create = "Pages.Sales.Orders.Create";
        public const string Pages_Sales_Orders_Edit = "Pages.Sales.Orders.Edit";
        public const string Pages_Sales_Orders_Delete = "Pages.Sales.Orders.Delete";
        public const string Pages_Sales_Orders_Export = "Pages.Sales.Orders.Export";

        public const string Pages_Sales_Invoice = "Pages.Sales.Invoice";
        public const string Pages_Sales_Invoice_Create = "Pages.Sales.Invoice.Create";
        public const string Pages_Sales_Invoice_Edit = "Pages.Sales.Invoice.Edit";
        public const string Pages_Sales_Invoice_Delete = "Pages.Sales.Invoice.Delete";
        public const string Pages_Sales_Invoice_Export = "Pages.Sales.Invoice.Export";

        public const string Pages_Journal_Entry = "Pages.Journal.Entry";
        public const string Pages_Journal_Entry_Create = "Pages.Journal.Entry.Create";
        public const string Pages_Journal_Entry_Edit = "Pages.Journal.Entry.Edit";
        public const string Pages_Journal_Entry_Delete = "Pages.Journal.Entry.Delete";
        public const string Pages_Journal_Entry_Export = "Pages.Journal.Entry.Export";
        public const string Pages_Journal_Entry_Details = "Pages.Journal.Entry.Export";
        
        public const string Pages_Rfp = "Pages.Rfp";
        public const string Pages_Rfp_AllAccounts = "Pages.Rfp.AllAccounts";
        public const string Pages_Rfp_Create = "Pages.Rfp.Create";
        public const string Pages_Rfp_Edit = "Pages.Rfp.Edit";
        public const string Pages_Rfp_Delete = "Pages.Rfp.Delete";
        public const string Pages_Rfp_Export = "Pages.Rfp.Export";
        public const string Pages_Rfp_Assign = "Pages.Rfp.Assign";

        public const string Pages_Collections = "Pages.Collections";
        public const string Pages_Collections_Create = "Pages.Collections.Create";
        public const string Pages_Collections_Edit = "Pages.Collections.Edit";
        public const string Pages_Collections_Delete = "Pages.Collections.Delete";
        public const string Pages_Collections_Export = "Pages.Collections.Export";

        public const string CRM_Leads = "CRM.Leads";
        public const string CRM_Leads_AllAccounts = "CRM.Leads.AllAccounts";
        public const string CRM_Leads_Create = "CRM.Leads.Create";
        public const string CRM_Leads_Edit = "CRM.Leads.Edit";
        public const string CRM_Leads_Approve = "CRM.Leads.Approve";
        public const string CRM_Leads_Delete = "CRM.Leads.Delete";
        public const string CRM_Leads_Updates = "CRM.Leads.Updates";
        public const string CRM_Leads_Export = "CRM.Leads.Export";

        /* Employee */
        public const string Pages_JobDescription = "JobDescription";

        public const string Pages_EmployeeApp = "EmployeeApp";

        public const string Master_Employees = "Master.Employees";
        public const string Master_Employees_Export = "Master.Employees.Export";
        public const string Master_Employees_Create = "Master.Employees.Create";
        public const string Master_Employees_Edit = "Master.Employees.Edit";
        public const string Master_Users = "Master.Users";

        public const string Pages_Accounts_Ability = "Pages.Accounts.Ability";
        public const string Pages_Employee_Records = "Pages.Employees.Records";
        public const string Pages_Employee_Salary_Rates = "Pages.Employee.Salary.Rates";
        public const string Pages_Employee_Allowance = "Pages.Employee.Allowance";
        public const string Pages_Employee_OTRates = "Pages.Employee.OTRates";
        public const string Pages_Employee_Loan = "Pages.Employee.Loan";

        /* Attendance */
        public const string Pages_Attendance = "Attendance";
        public const string Pages_EmpAttendance = "Employee.Attendance";
        public const string Pages_AttendanceApprover = "AttendanceApprover";

        /* Payroll */
        public const string Pages_Payroll = "Payroll";

        public const string Pages_Overtime_Rates = "Pages.Overtime.Rates";
        /* GroupType */
        public const string Master_GroupTypes = "Master.GroupTypes";

        public const string Pages_Quotations = "Pages.Quotations";
        public const string Pages_Quotations_AllAccounts = "Pages.Quotations.AllAccounts";
        public const string Pages_Quotations_Create = "Pages.Quotations.Create";
        public const string Pages_Quotations_Edit = "Pages.Quotations.Edit";
        public const string Pages_Quotations_Delete = "Pages.Quotations.Delete";
        public const string Pages_Quotations_Export = "Pages.Quotations.Export";
        public const string Pages_Quotations_Submit = "Pages.Quotations.Submit";

        public const string Pages_Stock_Entry = "Pages.Stock.Entry";
        public const string Pages_Stock_Entry_Create = "Pages.Stock.Entry.Create";
        public const string Pages_Stock_Entry_Edit = "Pages.Stock.Entry.Edit";
        public const string Pages_Stock_Entry_Delete = "Pages.Stock.Entry.Delete";
        public const string Pages_Stock_Entry_Export = "Pages.Stock.Entry.Export";
        public const string Pages_Stock_Entry_Submit = "Pages.Stock.Entry.Submit";
        public const string Pages_Stock_Card = "Pages.Stock.Card";
        public const string Pages_Stock_Summary = "Pages.Stock.Summary";

        public const string Master_Accounts = "Master.Accounts";
        public const string Master_Accounts_Create = "Master.Accounts.Create";
        public const string Master_Accounts_Edit = "Master.Accounts.Edit";
        public const string Master_Accounts_Delete = "Master.Accounts.Delete";
        public const string Master_Accounts_Export = "Master.Accounts.Export";

        /* Attendance */
        public const string Pages_General_Ledger = "Pages.General.Ledger";
        public const string Pages_Accounts_Receivable = "Pages.Accounts.Receivable";

        /* Folders*/

        public const string Pages_Folder_Users = "FolderUsers";
        public const string Pages_Shared_Users = "SharedUser";
        public const string Pages_Shared_Roles = "SharedRole";

    }
}