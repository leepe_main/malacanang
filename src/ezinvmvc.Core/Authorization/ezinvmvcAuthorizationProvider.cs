﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace ezinvmvc.Authorization
{
    public class ezinvmvcAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {

            ////context.CreatePermission(PermissionNames.Master_Accounts, L("Accounts"));
            ////context.CreatePermission(PermissionNames.Master_Accounts_Create, L("AccountsCreate"));
            ////context.CreatePermission(PermissionNames.Master_Accounts_Edit, L("AccountsEdit"));
            ////context.CreatePermission(PermissionNames.Master_Accounts_Delete, L("AccountsDelete"));
            ////context.CreatePermission(PermissionNames.Master_Accounts_Export, L("AccountsExport"));

            ////context.CreatePermission(PermissionNames.Pages_General_Ledger, L("GeneralLedger"));
            ////context.CreatePermission(PermissionNames.Pages_Accounts_Receivable, L("AccountsReceivable"));


            ////context.CreatePermission(PermissionNames.Pages_Stock_Summary, L("StockSummary"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Card, L("StockCard"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry, L("StockEntry"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry_Create, L("StockEntryCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry_Edit, L("StockEntryEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry_Delete, L("StockEntryDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry_Export, L("StockEntryExport"));
            ////context.CreatePermission(PermissionNames.Pages_Stock_Entry_Submit, L("StockEntrySubmit"));

            ////context.CreatePermission(PermissionNames.Pages_Quotations, L("Quotations"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_AllAccounts, L("QuotationsAllAccounts"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_Create, L("QuotationsCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_Edit, L("QuotationsEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_Delete, L("QuotationsDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_Export, L("QuotationsExport"));
            ////context.CreatePermission(PermissionNames.Pages_Quotations_Submit, L("QuotationsSubmit"));

            ////context.CreatePermission(PermissionNames.Pages_Sales_Orders, L("SalesOrders"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Orders_Create, L("SalesOrdersCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Orders_Edit, L("SalesOrdersEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Orders_Delete, L("SalesOrdersDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Orders_Export, L("SalesOrdersExport"));

            ////context.CreatePermission(PermissionNames.Pages_Sales_Invoice, L("SalesInvoice"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Invoice_Create, L("SalesInvoiceCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Invoice_Edit, L("SalesInvoiceEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Invoice_Delete, L("SalesInvoiceDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Sales_Invoice_Export, L("SalesInvoiceExport"));

            ////context.CreatePermission(PermissionNames.Pages_Journal_Entry, L("JournalEntry"));
            ////context.CreatePermission(PermissionNames.Pages_Journal_Entry_Create, L("JournalEntryCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Journal_Entry_Edit, L("JournalEntryEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Journal_Entry_Delete, L("JournalEntryDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Journal_Entry_Export, L("JournalEntryxport"));

            ////context.CreatePermission(PermissionNames.Pages_Collections, L("Collections"));
            ////context.CreatePermission(PermissionNames.Pages_Collections_Create, L("CollectionsCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Collections_Edit, L("CollectionsEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Collections_Delete, L("CollectionsDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Collections_Export, L("CollectionsExport"));

            ////context.CreatePermission(PermissionNames.Pages_Rfp, L("RFP"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_AllAccounts, L("RFPAllAccounts"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_Create, L("RFPCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_Edit, L("RFPEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_Delete, L("RFPDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_Export, L("RFPExport"));
            ////context.CreatePermission(PermissionNames.Pages_Rfp_Assign, L("RFPAssign"));
            
            ////context.CreatePermission(PermissionNames.Pages_Purchase_Orders, L("PurchaseOrders"));
            ////context.CreatePermission(PermissionNames.Pages_Invoices, L("Invoices"));

            ////context.CreatePermission(PermissionNames.Pages_Rfq, L("RFQ"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_AllAccounts, L("RFQAllAccounts"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_Create, L("RFQCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_Edit, L("RFQEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_Delete, L("RFQDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_Export, L("RFQExport"));
            ////context.CreatePermission(PermissionNames.Pages_Rfq_Assign, L("RFQAssign"));

            ////context.CreatePermission(PermissionNames.Pages_Assignment, L("Assignment"));
            ////context.CreatePermission(PermissionNames.Pages_Assignment_AllAccounts, L("AssignAllAccounts"));
            ////context.CreatePermission(PermissionNames.Pages_Assignment_Create, L("AssignmentCreate"));
            ////context.CreatePermission(PermissionNames.Pages_Assignment_Edit, L("AssignmentEdit"));
            ////context.CreatePermission(PermissionNames.Pages_Assignment_Delete, L("AssignmentDelete"));
            ////context.CreatePermission(PermissionNames.Pages_Assignment_Export, L("AssignmentExport"));

            ////context.CreatePermission(PermissionNames.Pages_StockTransfers, L("StockTransfers"));
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Master_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            ////context.CreatePermission(PermissionNames.Master_Warehouse, L("Warehouse"));
            ////context.CreatePermission(PermissionNames.Master_Products, L("Products"));
            //context.CreatePermission(PermissionNames.Master_Buyers, L("Buyers"));

            ////context.CreatePermission(PermissionNames.Master_Clients, L("Clients"));
            ////context.CreatePermission(PermissionNames.Master_Clients_AllAccounts, L("ClientsAllAccounts"));
            ////context.CreatePermission(PermissionNames.Master_Clients_Create, L("ClientCreate"));
            ////context.CreatePermission(PermissionNames.Master_Clients_Edit, L("ClientEdit"));
            ////context.CreatePermission(PermissionNames.Master_Clients_Approve, L("ClientApprove"));
            ////context.CreatePermission(PermissionNames.Master_Clients_Delete, L("ClientDelete"));
            ////context.CreatePermission(PermissionNames.Master_Clients_Export, L("ClientExport"));

            ////context.CreatePermission(PermissionNames.CRM_Leads, L("Leads"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_AllAccounts, L("LeadsAllAccounts"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Create, L("LeadCreate"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Edit, L("LeadEdit"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Approve, L("LeadApprove"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Delete, L("LeadDelete"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Updates, L("LeadUpdates"));
            ////context.CreatePermission(PermissionNames.CRM_Leads_Export, L("LeadExport"));

            ////context.CreatePermission(PermissionNames.Master_Vendors, L("Vendors"));
            
            ////context.CreatePermission(PermissionNames.Master_Vendors_Create, L("VendorCreate"));
            ////context.CreatePermission(PermissionNames.Master_Vendors_Edit, L("VendorEdit"));
            ////context.CreatePermission(PermissionNames.Master_Vendors_Delete, L("VendorDelete"));
            ////context.CreatePermission(PermissionNames.Master_Vendors_Export, L("VendorExport"));

            ////context.CreatePermission(PermissionNames.Master_Products_Create, L("ProductCreate"));
            ////context.CreatePermission(PermissionNames.Master_Products_Edit, L("ProductEdit"));
            ////context.CreatePermission(PermissionNames.Master_Products_Delete, L("ProductDelete"));
            ////context.CreatePermission(PermissionNames.Master_Products_Export, L("ProductExport"));

            ////context.CreatePermission(PermissionNames.Pages_JobDescription, L("JobDescription"));

            ////context.CreatePermission(PermissionNames.Pages_EmployeeApp, L("EmployeeApp"));
            ////context.CreatePermission(PermissionNames.Master_Employees, L("Employees"));
            ////context.CreatePermission(PermissionNames.Master_Employees_Create, L("EmployeesCreate"));
            ////context.CreatePermission(PermissionNames.Master_Employees_Edit, L("EmployeesEdit"));
            ////context.CreatePermission(PermissionNames.Master_Employees_Export, L("EmployeesExport"));
            ////context.CreatePermission(PermissionNames.Pages_Accounts_Ability, L("AccountsAbility"));
            ////context.CreatePermission(PermissionNames.Pages_Employee_Records, L("EmployeesRecords"));
            ////context.CreatePermission(PermissionNames.Pages_Employee_Salary_Rates, L("EmployeePayrollRate"));
            ////context.CreatePermission(PermissionNames.Pages_Employee_Allowance, L("EmployeeAllowance"));
            ////context.CreatePermission(PermissionNames.Pages_Employee_OTRates, L("EmployeeOTRates"));
            ////context.CreatePermission(PermissionNames.Pages_Overtime_Rates, L("OTRate"));
            ////context.CreatePermission(PermissionNames.Pages_Employee_Loan, L("EmployeeLoan"));

            ////context.CreatePermission(PermissionNames.Pages_Attendance, L("Attendance"));
            ////context.CreatePermission(PermissionNames.Pages_EmpAttendance, L("EmployeeAttendace"));
            ////context.CreatePermission(PermissionNames.Pages_AttendanceApprover, L("AttendanceApprover"));
            ////context.CreatePermission(PermissionNames.Pages_Payroll, L("Payroll"));
            ////context.CreatePermission(PermissionNames.Master_GroupTypes, L("GroupTypes"));

            context.CreatePermission(PermissionNames.Pages_Folder_Users, L("FolderUsers"));
            context.CreatePermission(PermissionNames.Pages_Shared_Users, L("SharedUser"));
            context.CreatePermission(PermissionNames.Pages_Shared_Roles, L("SharedRole"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ezinvmvcConsts.LocalizationSourceName);
        }
    }
}
