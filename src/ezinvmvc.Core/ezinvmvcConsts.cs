﻿namespace ezinvmvc
{
    public class ezinvmvcConsts
    {
        public const int MaxLenght1024 = 1024;
        public const string ErrorMessage1024 = "Maximum Length 1024";
        public const int MaxLenght512 = 512;
        public const string ErrorMessage512 = "Maximum Length 512";
        public const int MaxLenght328 = 328;
        public const string ErrorMessage328 = "Maximum Length 328";
        public const int MaxLenght256 = 256;
        public const string ErrorMessage256 = "Maximum Length 256";
        public const int MaxLenght128 = 128;
        public const string ErrorMessage128 = "Maximum Length 128";
        public const int MaxLenght64 = 64;
        public const string ErrorMessage64 = "Maximum Length 64";
        public const int MaxLenght32 = 32;
        public const string ErrorMessage32 = "Maximum Length 32";
        public const int MaxLenght16 = 16;
        public const string ErrorMessage16 = "Maximum Length 16";
        public const int MaxLenght8 = 8;
        public const string ErrorMessage8 = "Maximum Length 8";

        public const string LocalizationSourceName = "ezinvmvc";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
