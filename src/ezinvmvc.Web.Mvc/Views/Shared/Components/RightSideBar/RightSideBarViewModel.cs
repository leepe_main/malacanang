﻿using ezinvmvc.Configuration.Ui;

namespace ezinvmvc.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
