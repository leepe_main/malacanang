﻿using ezinvmvc.App.Addresses.Dto;
using System.Collections.Generic;

namespace ezinvmvc.Web.Models.Addresses
{
    public class CountryCreateModel
    {
        public CreateCountryInput Country { get; set; }
    }
}
