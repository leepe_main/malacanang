﻿using ezinvmvc.App.Employees.Dto;

namespace ezinvmvc.Web.Models.Employees
{
    public class DepartmentCreateModel
    {
        public CreateDepartmentInput Name { get; set; }

    }
}
