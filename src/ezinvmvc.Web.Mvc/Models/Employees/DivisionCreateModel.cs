﻿using ezinvmvc.App.Employees.Dto;

namespace ezinvmvc.Web.Models.Employees
{
    public class DivisionCreateModel
    {
        public CreateDivEmployeeInput Name { get; set; }
    }
}
