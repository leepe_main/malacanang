﻿using ezinvmvc.App.Employees.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.Employees
{
    public class PositionCreateModel
    {
        public CreatePositionInput Name { get; set; }
    }
}
