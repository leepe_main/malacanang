﻿using ezinvmvc.App.FolderUser.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.FolderUser
{
    public class FolderUserViewModel
    {
        public CreateFolderUsersInput FolderUsers { get; set; }
    }
}
