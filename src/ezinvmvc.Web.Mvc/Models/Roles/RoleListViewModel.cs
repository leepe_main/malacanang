﻿using System.Collections.Generic;
using ezinvmvc.Roles.Dto;

namespace ezinvmvc.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
