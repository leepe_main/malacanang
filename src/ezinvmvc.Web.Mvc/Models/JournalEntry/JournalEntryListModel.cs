﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.JournalEntry
{
    public class JournalEntryListModel
    {
        public string FilterText { get; set; }

        public int EmpId { get; set; }
    }
}
