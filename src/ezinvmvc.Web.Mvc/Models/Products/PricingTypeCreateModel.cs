﻿using ezinvmvc.App.Products.Dto;

namespace ezinvmvc.Web.Models.Products
{
    public class PricingTypeCreateModel
    {
        public CreatePricingTypeInput PricingType { get; set; }
    }
}
