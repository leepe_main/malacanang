﻿using ezinvmvc.App.Products.Dto;
using System.Collections.Generic;

namespace ezinvmvc.Web.Models.Products
{
    public class UnitCreateModel
    {
        public CreateUnitInput Unit { get; set; }
    }
}
