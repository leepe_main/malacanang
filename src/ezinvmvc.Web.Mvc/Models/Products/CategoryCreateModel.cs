﻿using ezinvmvc.App.Products.Dto;
using System.Collections.Generic;

namespace ezinvmvc.Web.Models.Products
{
    public class CategoryCreateModel
    {
        public CreateCategoryInput Category { get; set; }
    }
}
