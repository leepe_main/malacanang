﻿using Abp.Auditing;
using Abp.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.Account
{
    public class ResetPasswordViewModel
    {
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}", ErrorMessage = "...")]
        [DisableAuditing]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string EmailAddress { get; set; }
        public string Token { get; set; }
    }
}
