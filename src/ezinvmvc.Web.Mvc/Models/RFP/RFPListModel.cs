﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.RFP
{
    public class RFPListModel
    {
        public string FilterText { get; set; }

        public int EmpId { get; set; }
    }
}
