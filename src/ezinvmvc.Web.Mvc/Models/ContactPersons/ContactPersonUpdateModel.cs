﻿using ezinvmvc.App.ContactPersons.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.ContactPersons
{
    public class ContactPersonUpdateModel
    {
        public UpdateContactPersonInput ContactPerson { get; set; }
    }
}
