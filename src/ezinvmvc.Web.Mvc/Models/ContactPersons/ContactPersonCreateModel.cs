﻿using ezinvmvc.App.ContactPersons.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezinvmvc.Web.Models.ContactPersons
{
    public class ContactPersonCreateModel
    {
        public CreateContactPersonInput ContactPerson { get; set; }
    }
}
