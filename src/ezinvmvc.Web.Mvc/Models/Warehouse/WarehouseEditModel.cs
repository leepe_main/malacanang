﻿using ezinvmvc.App.Common.Dto;

namespace ezinvmvc.Web.Models.Warehouse
{
    public class WarehouseEditModel
    {
        public UpdateWarehouseInput Warehouse { get; set; }
    }
}
