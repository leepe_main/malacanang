﻿using ezinvmvc.App.Common.Dto;

namespace ezinvmvc.Web.Models.Warehouse
{
    public class WarehouseCreateModel
    {
        public CreateWarehouseInput Warehouse { get; set; }
    }
}
