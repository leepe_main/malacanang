﻿
using ezinvmvc.App.Vendors.Dto;

namespace ezinvmvc.Web.Models.Vendors
{
    public class VendorEditModel
    {
        public UpdateVendorInput Vendor { get; set; }
    }
}
