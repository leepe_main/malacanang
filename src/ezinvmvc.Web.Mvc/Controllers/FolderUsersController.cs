﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ezinvmvc.App.FolderUser;
using ezinvmvc.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ezinvmvc.Web.Mvc.Controllers
{
    public class FolderUsersController : ezinvmvcControllerBase
    {
        private readonly IFolderUsersService _folderUsersService;
        public FolderUsersController(IFolderUsersService folderUsersService)
        {
            _folderUsersService = folderUsersService;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UploadReports()
        {
            return View();
        }
        public IActionResult CommentsReports()
        {
            return View();
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 909715200)]

        public async Task<IActionResult> UploadFileUser(List<IFormFile> file, string path, string name)
        {
            var serverpath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", path);
            var fullpath = Path.Combine(serverpath);

            if (!Directory.Exists(serverpath))
            {
                Directory.CreateDirectory(serverpath);
            }

            foreach (var formFile in file)
            {
                var filePath = Path.Combine(serverpath, formFile.FileName);
                if (formFile.Length > 0)
                {
                    using (var stream = System.IO.File.Create(filePath))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void NewFolder(string path)
        {
            var serverpath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", path);
            var fullpath = Path.Combine(serverpath);

            if (!Directory.Exists(serverpath))
            {
                Directory.CreateDirectory(serverpath);
            }
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public void DeleteFile(string path)
        {
            var serverpath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", path);
            var fullpath = Path.Combine(serverpath);           

            if (!Directory.Exists(serverpath))
            {
                Directory.Delete(serverpath);
            }
            //return RedirectToAction("Index");
        }
    }
}