﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using AutoMapper;
using ezinvmvc.App.Vendors;
using ezinvmvc.App.Vendors.Dto;
using ezinvmvc.Controllers;
using ezinvmvc.Web.Models.Vendors;
using Microsoft.AspNetCore.Mvc;

namespace ezinvmvc.Web.Mvc.Controllers
{
    public class SalesOrdersController : ezinvmvcControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> Create()
        {
            return View();
        }
        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}