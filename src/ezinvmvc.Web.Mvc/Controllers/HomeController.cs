﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ezinvmvc.Controllers;

namespace ezinvmvc.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : ezinvmvcControllerBase
    {
        public ActionResult Index()
        {
            if (!AbpSession.UserId.HasValue)
            {
                RedirectToAction("Login", "Account");
            }
            return View();
        }
	}
}
