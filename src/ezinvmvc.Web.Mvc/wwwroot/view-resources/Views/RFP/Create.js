﻿//abp.ui.block();

function decimalOnly(txt) {
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode === 46) {
        var txtbx = document.getElementById(txt);
        var amount = document.getElementById(txt).value;
        var present = 0;
        var count = 0;

        do {
            present = amount.indexOf(".", present);
            if (present !== -1) {
                count++;
                present++;
            }
        }
        while (present !== -1);
        if (present === -1 && amount.length === 0 && event.keyCode === 46) {
            event.keyCode = 0;
            return false;
        }

        if (count >= 1 && event.keyCode === 46) {

            event.keyCode = 0;
            return false;
        }
        if (count === 1) {
            var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
            if (lastdigits.length >= 2) {
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    else {
        event.keyCode = 0;
        return false;
    }
}

$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});
//abp.ui.block();

(function ($) {
    var _pricingTypeService = abp.services.app.pricingTypeService;
    var _productPriceService = abp.services.app.productPriceService;
    var _productService = abp.services.app.productService;
    var _companyService = abp.services.app.companyService;
    var _commonService = abp.services.app.commonService;
    var _clientService = abp.services.app.clientService;
    var _accountService = abp.services.app.accountService;
    var _salesOrderService = abp.services.app.salesOrderService;
    var _rfqService = abp.services.app.rFQService;
    var _cpersonService = abp.services.app.contactPersonService;
    var _leadService = abp.services.app.leadService;
    var _journalEntryservice = abp.services.app.journalEntryService
    var _employeeService = abp.services.app.employeeService;
    var _quotationService = abp.services.app.quotationService;
    var _rfpService = abp.services.app.rFPService;

    var _$form = $('form[name=RfpForm]');
    var _$itemsTable = $('#ItemsTable');

    function getcompanies() {
        var companies = $('#Companies');
        abp.ui.block($('#Companies'));
        companies.empty();
        _companyService.getCompanies().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (result.items[i].isDefault === true) {
                    companies.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                    getseriestype(result.items[i].id);
                }
                else {
                    companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            companies.selectpicker('refresh');
        });
        abp.ui.unblock('#Companies');
    }
    getcompanies();
    $('#Companies').on('change', function (e) {
        getseriestype($('#Companies').val());
    });

    function getseriestype(companyid) {
        var series = $('#Series');
        series.empty();
        _commonService.getSeriesTypesFiltered({ id: 0, transactionCode: 110, companyId: companyid }).done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                series.append('<option value=' + result.items[i].id + '>' + result.items[i].prefix + '</option>');
            }
            series.selectpicker('refresh');
        });
    }

    //Client Autocomplete
    //var getclients = function (request, response) {
    //    _clientService.getClients({ filter: request.term }).done(function (result) {
    //        response($.map(result.items, function (el) {
    //            return {
    //                label: el.name,
    //                value: el.id
    //            };
    //        }));
    //    });
    //};

    //function getclient() {
    //    var $clientid = $('#ClientId').val();
    //    _clientService.getClientDetails({ id: $clientid }).done(function (result) {
    //        $('#Telno').val(result[0].telNo);
    //        $('#Phone').val(result[0].mobileNo);
    //        $('#Taxno').val(result[0].taxNo);
    //        $('#Email').val(result[0].email);
    //        $('#Address').val(result[0].completeAddress);
    //    });
    //};

    //var selectclient = function (event, ui) {
    //    event.preventDefault();
    //    $("#ClientId").val(ui.item ? ui.item.value : "");
    //    $("#ClientName").val(ui.item ? ui.item.label : "");
    //    getclient();
    //    return false;
    //};
    //var focusclient = function (event, ui) {
    //    event.preventDefault();
    //    $("#ClientId").val(ui.item.value);
    //    $("#ClientName").val(ui.item.label);
    //};
    //var changeclient = function (event, ui) {
    //    event.preventDefault();
    //    $("#ClientId").val(ui.item ? ui.item.value : "");
    //    $("#ClientName").val(ui.item ? ui.item.label : "");
    //    if (ui.item === null) {
    //        $('#Telno').val('');
    //        $('#Phone').val('');
    //        $('#Taxno').val('');
    //        $('#Email').val('');
    //        $('#Address').val('');
    //    }
    //};
    //$("#ClientName").autocomplete({
    //    source: getclients,
    //    select: selectclient,
    //    focus: focusclient,
    //    minLength: 2,
    //    delay: 100,
    //    change: changeclient
    //});
    //Client Autocomplete

    function getclientbyid(id) {
        _clientService.getClientDetails({ id: id }).done(function (result) {
            $('#ClientId').val(id);
            $('#ClientName').val(result[0].name);
        });
    };


    //Quotation Autocomplete
    var getQuotations = function (request, response) {
        _quotationService.getQuotations({ filter: request.term }).done(function (result) {
            response($.map(result.items, function (el) {
                return {
                    label: el.code,
                    value: el.id
                };
            }));
        });
    };

    function getQuotation() {
        var $quotationid = $('#RequestId').val();
        _quotationService.getQuotation({ id: $quotationid }).done(function (result) {
            //$('#ClientId').val(result.clientId);
            //$('#ClientName').val(result[0].mobileNo);
            //$('#Taxno').val(result[0].taxNo);
            //$('#Email').val(result[0].email);
            //$('#Address').val(result[0].completeAddress);

            var sonettotal = currencyFormat(result.netTotal);
            var sotax = currencyFormat(result.tax);
            var sototal = currencyFormat(result.grandTotal);
            $('#SubTotal').val(sonettotal);
            $('#Tax').val(sotax);
            $('#Total').val(sototal);
            $('#StatusBadge').text(result.status);
            getclientbyid(result.clientId);
            getquotationitems(result.id);
            gettaxtype(result.taxTypeId);
            getpricingtype(result.pricingTypeId);

        });
    };

    var selectQuotations = function (event, ui) {
        event.preventDefault();
        $("#RequestId").val(ui.item ? ui.item.value : "");
        $("#RequestCode").val(ui.item ? ui.item.label : "");
        getQuotation();
        return false;
    };

    var focusQuotations = function (event, ui) {
        event.preventDefault();
        $("#RequestId").val(ui.item.value);
        $("#RequestCode").val(ui.item.label);
    };

    var changQuotations = function (event, ui) {
        event.preventDefault();
        $("#RequestId").val(ui.item ? ui.item.value : "");
        $("#RequestCode").val(ui.item ? ui.item.label : "");
        if (ui.item === null) {
            $('#Telno').val('');
            $('#Phone').val('');
            $('#Taxno').val('');
            $('#Email').val('');
            $('#Address').val('');
        }
    };

    $("#RequestCode").autocomplete({
        source: getQuotations,
        select: selectQuotations,
        focus: focusQuotations,
        minLength: 2,
        delay: 100,
        change: changQuotations
    });
    //Client Autocomplete



    function getquotationitems(id) {
        _$itemsTable.DataTable().rows().remove().draw(false);
        _quotationService.getQuotationItemsByParentId({ id: id }).done(function (result) {

            for (var i = 0; i < result.items.length; i++) {
                var $sqiid = result.items[i].id;
                var $sqiproductid = result.items[i].productId;
                var $sqiproductcode = result.items[i].productCode;
                var $sqiproductname = result.items[i].productName;
                var $sqiproductdescription = result.items[i].productDescription;
                var $sqiunitid = result.items[i].unitId;
                var $sqiunit = result.items[i].unit;
                var $sqiquantity = result.items[i].orderQty;
                var $sqiprice = result.items[i].unitPrice;
                var $sqiimagename = result.items[i].imageName;

                var $sqidisc1 = result.items[i].disc1;
                var $sqidisc2 = result.items[i].disc2;
                var $sqidisc3 = result.items[i].disc3;
                var $sqidtype1 = result.items[i].discType1;
                var $sqidtype2 = result.items[i].discType2;
                var $sqidtype3 = result.items[i].discType3;
                var $sqiperdescription = result.items[i].description;

                var sqiprice = parseFloat($sqiprice);
                var sqiquantity = parseFloat($sqiquantity);

                var sqidisc1 = 0;
                var sqidisc2 = 0;
                var sqidisc3 = 0;
                if ($sqidisc1 !== "") {
                    sqidisc1 = parseFloat($sqidisc1);
                }
                if ($sqidisc2 !== "") {
                    sqidisc2 = parseFloat($sqidisc2);
                }
                if ($sqidisc3 !== "") {
                    sqidisc3 = parseFloat($sqidisc3);
                }

                var sqidiscount = priceDiscount(sqiprice, sqidisc1, parseInt($sqidtype1), sqidisc2, parseInt($sqidtype2), sqidisc3, parseInt($sqidtype3));
                var sqitotaldiscount = sqidiscount * sqiquantity;
                var sqilessprice = sqiprice - sqidiscount;
                var sqitotal = sqilessprice * sqiquantity;
                var sqidatacount = dataTable.rows().count();
                var sqiitemno = sqidatacount + 1;

                dataTable.row.add([sqiitemno,
                    '<a href="#" class="btn-link">' + $sqiproductcode + '</a><br /><small><label class="text-muted">' + $sqiproductname + '</label></small>',
                    '<label class="text-muted">' + $sqiquantity + '</label>|<label class="text-muted">' + $sqiunit + '</label>',
                    sqiprice,
                    sqitotaldiscount,
                    sqitotal,
                    '',
                    $sqiproductid, $sqiproductname, $sqiperdescription, $sqiquantity, $sqiunitid, sqiprice, sqidisc1, parseInt($sqidtype1), sqidisc2, parseInt($sqidtype2), sqidisc3, parseInt($sqidtype3), sqitotaldiscount, sqitotal, , $sqiid
                ]).draw();

                //dataTablePrint.row.add(['<label class="font-weight-bold">' + $sqiproductcode + '</label><br/><img src="' + abp.appPath + 'products/' + $sqiproductcode + '/' + $sqiimagename + '" style="height: 150px; width: 150px;"/>',
                //'<label class="font-weight-bold">' + $sqiproductname + '</label><br/><label class="text-muted" style="white-space: pre-wrap;">' + $sqiproductdescription + '</label>',
                //'<label class="text-muted">' + $sqiquantity + '</label>',
                //    sqiprice,
                //    sqitotaldiscount,
                //    sqitotal]).draw();
            }
        });
    };

    //gettaxtype(1);
    function gettaxtype(id) {
        var taxtypes = $('#TaxTypes');
        taxtypes.empty();
        _commonService.getTaxTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                }

            }
            taxtypes.selectpicker('refresh');
        });
    }
    function getpricingtype(id) {

        var pricingtypes = $('#PricingTypes');
        pricingtypes.empty();
        _pricingTypeService.getPricingTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    pricingtypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    pricingtypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            pricingtypes.selectpicker('refresh');
        });
    }

    function getAll() {
        dataTable.ajax.reload();
    }

    var dataTable = _$itemsTable.DataTable({
        responsive: true,
        paging: false,
        "bInfo": false,
        searching: false,
        columnDefs: [{
            "visible": false,
            targets: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        },
        {
            orderable: false,
            targets: [0, 1, 2, 3, 4, 5, 6]
        },
        {
            render: $.fn.dataTable.render.number(',', '.', 2),
            className: 'text-right',
            targets: [3, 4, 5]
        },
        {
            className: 'text-center',
            targets: [2]
        },
        {
            visible: false,
            data: null,
            className: "text-center",
            "render": function () {
                return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
            },
            targets: [6]
        }
        ]
    });



    function save() {
        if (!_$form.valid()) {
            return;
        }
        var disabled = _$form.find(':input:disabled').removeAttr('disabled');
        var formdata = _$form.serializeFormToObject();

        if (formdata.DebitTotal !== formdata.CreditTotal) {
            abp.notify.warn('Entry type not balance.', 'Warning');
            return;
        }

        //alert(formdata.ClientId);
        //alert(formdata.RequestId);
        //alert(formdata.RequestCode);

        //$("#PartyClient").removeAttr("required");
        //remove.removeAttr('required', 'required');

        var viewData = {
            rfp: {
                "companyId": formdata.CompanyId,
                "seriesTypeId": formdata.SeriesTypeId,
                "prefix": $("#Series option:selected").html(),
                "code": "0",
                "transactionTime": formdata.TransactionTime,
                "clientId": formdata.ClientId,
                "requestId": formdata.RequestId,
                "requestCode": formdata.RequestCode,
                //"clientId": "1",
                //"requestId": "1",
                //"requestCode": "1",
                "notes": formdata.Notes,
                "termsAndConditions": "",
                "statusId": "1",
                "taxTypeId": formdata.TaxTypeId,
                "paymentTermId": "1",
                "subTotal": formdata.SubTotal,
                "netTotal": "0",
                "taxRate": "0",
                "tax": formdata.Tax,
                "grandTotal": formdata.Total,
            },
            rfpitems: []
        };
        disabled.attr('disabled', 'disabled');

        //sales order items
        var tableitem = _$itemsTable.DataTable();
        var form_data = tableitem.rows().data();
        var f = form_data;

        //jsonObj = [];
        for (var i = 0; f.length > i; i++) {
            item = {};
            item["requestId"] = formdata.SeriesTypeId;
            item["productId"] = f[i][7];
            item["productName"] = f[i][8];
            item["description"] = f[i][9];
            item["qty"] = f[i][10];
            item["unitId"] = f[i][11];
            item["unitPrice"] = f[i][12];
            item["disc1"] = f[i][13];
            item["discType1"] = f[i][14];
            item["disc2"] = f[i][15];
            item["discType2"] = f[i][16];
            item["disc3"] = f[i][17];
            item["discType3"] = f[i][18];
            item["discTotal"] = f[i][19];
            item["total"] = f[i][20];
            item["groupName"] = "";
            item["inventoryAccountId"] = "1";


            viewData.rfpitems.push(item);
            //jsonObj.push(item);
        }

        //var salesorderinput = JSON.stringify(viewData);
        var rfqinput = JSON.stringify(viewData);
        abp.message.confirm(
            'New RFP will be created.',
            'Are you sure?',
            function (isConfirmed) {
                if (isConfirmed) {
                    abp.ui.setBusy(_$form);
                    //_salesOrderService.createSalesOrder(viewData).done(function () {
                    _rfpService.createRFP(viewData).done(function () {
                        abp.notify.success('Request For Payment created', 'Success');

                        var url = 'Index';
                        setTimeout(function () {
                            window.location.href = url; //will redirect to your blog page (an ex: blog.html)
                        }, 2000);
                    }).always(function () {
                        abp.ui.clearBusy(_$form);
                    });
                }
            }
        );
    }

    $('#SaveButton').click(function (e) {
        e.preventDefault();
        save();
    });

})(jQuery);

