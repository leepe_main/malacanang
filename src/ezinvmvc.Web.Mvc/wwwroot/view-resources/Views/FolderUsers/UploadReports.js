﻿
$(".date-picker").datepicker();
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

(function () {
    $(function () {
        var _folderUsersService = abp.services.app.folderUsersService;
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _newFolderService = abp.services.app.newFolderService;
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _$fileTable = $('#fileTable');

        $(document).ready(function () {
            GetRoles(abp.session.userId);            
        });

        function GetRoles($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
                $('#roleId').val(result.roleId);
                $('#userid').val(abp.session.userId);
                GetUploadFiles()
            });
        }

        $('#btnSearch').click(function (e) {
            e.preventDefault();

            if ($('#SearchOption').val() == 0) {
                $('#userid').val(abp.session.userId);
                GetUploadFiles();
            }
            if ($('#SearchOption').val() == 1) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                GetUploadFiles();
            }
            if ($('#SearchOption').val() == 2) {
                $('#userid').val(abp.session.userId);
                $('#Inputpath').val("");
                GetUploadFiles();
            }
            if ($('#SearchOption').val() == 3) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val();
                $('#dateto').val();
                $('#Inputpath').val();
                GetUploadFiles();
            }
            
        })

        function GetUploadFiles() {
            dataTable.ajax.reload();
        }

        var dataTable = _$fileTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _folderUsersService.getFolderUsersList,
                inputFilter: function () {
                    var $a = $("#datefrom").val();
                    var $b = $("#dateto").val();
                    var $c = $('#userid').val();
                    var $d = $("#Inputpath").val();
                    var $e = '';
                    var $f = '';
                    var $g = '';
                    return {
                        filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f + '|' + $g
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 1,
                    data: { icon: "icon" },
                    render: function (data) {
                        var icons = '<i class="fa ' + data.icon + ' fa-2x flat-color-6"></i> '
                        return icons;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 2,
                    data: "fileName"
                },
                {
                    visible: false,
                    orderable: false,
                    targets: 3,
                    data: "description"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 4,
                    data: "path"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 5,
                    data: "dateTime",
                    "render": function (data) {

                        var options = {
                            //weekday: "short",
                            year: "numeric",
                            month: "short",
                            day: "numeric"
                        };

                        var newDateFormat = new Date(data).toLocaleDateString("en-US", options);
                        var newTimeFormat = new Date(data).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;

                        return dateAndTime;
                    }
                },
                
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { Id: "id", extention: "extention", fileName: "fileName", path:"path"},
                    "render": function (data) {
                        var $extention = data.extention;
                        var $Fileid = data.id;
                        var $icon = "";
                        if ($('#roleId').val() == 1) {
                            if ($extention != "Folder") {
                                $icon = '<a id="data-file" title="Download" href="#" class="data-file btn btn-outline-primary btn-sm flat-color-5" data-file-id=' + $Fileid + '><i class="fa fa-download flat-color-6"></i></a>'; /*| <a id="data-delete" title="delete" href="#" class="data-delete btn btn-outline-danger btn-sm flat-color-6 " data-delete-id=' + $Fileid + '><i class="fa fa-times flat-color-6"></i></a>';*/
                            }
                            else {
                                $icon = '<a title="Not Downloadable" href="#" class="btn btn-outline-primary btn-sm flat-color-5"><i class="fa fa-download flat-color-9"></i></a>'; /*| <a id="data-delete" title="delete" href="#" class="data-delete btn btn-outline-danger btn-sm flat-color-6 " data-delete-id=' + $Fileid + '><i class="fa fa-times flat-color-6"></i></a>';*/
                            }
                        }
                        else {
                            if ($extention != "Folder") {
                                $icon = '<a id="data-file" title="Download" href="#" class="data-file btn btn-outline-primary btn-sm flat-color-5" data-file-id=' + $Fileid + '><i class="fa fa-download flat-color-6"></i></a>';
                            }
                            else {
                                $icon = '<a title="Not Downloadable" href="#" class="btn btn-outline-primary btn-sm flat-color-5"><i class="fa fa-download flat-color-9"></i></a>';
                            }
                        }
                        return $icon;
                    }
                }
            ]
        });

        $('#fileTable').on('click', 'a.data-file', function (e) {
            e.preventDefault();
            var $Id = $(this).attr("data-file-id");
            _folderUsersService.getDataById({ id: $Id }).done(function (result) {
                var filepath = '../' + result.path + result.fileName;
                //$("<a download/>").attr("href", apiUrl).get(0).click();
                var link = document.createElement('a');
                link.href = filepath;
                link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
                link.click();
            })
            
        });

        $('#fileTable').on('click', 'a.data-delete', function (e) {
            e.preventDefault();
            var $Id = $(this).attr("data-delete-id");
            _folderUsersService.getDataById({ id: $Id }).done(function (result) {
                var apiUrl = '/' + result.path + result.fileName;
                //$("<a download/>").attr("href", apiUrl).get(0).click();
                var $a = apiUrl;

                $.ajax({
                    url: abp.appPath + 'FolderUsers/DeleteFile?path=' + $a,
                    type: 'POST',
                    //data: formData,
                    //processData: false,
                    //contentType: false,
                    xhr: function () {  },
                    success: function () {   
                        _folderUsersService.deleteAttendanceAsync({
                            id: $Id
                        }).always(function () {
                            abp.ui.clearBusy(_$newfolder);
                            GetRoles(abp.session.userId); 
                        });
                    },
                    error: function (e) { }
                });
            })

        });
    })
})(jQuery);
