﻿
$(".date-picker").datepicker();
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

(function () {
    $(function () {
        var _folderUsersService = abp.services.app.folderUsersService;
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _announcementService = abp.services.app.announcementService;
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _$commentsTable = $('#commentsTable');

        

        $(document).ready(function () {
            $('#InputUser').hide();
            $('#comments').show();
            $('#delete').hide();
            GetTotalComment(abp.session.userId);
            GetUsersRole(abp.session.userId);
            GetAllComments();

        });

        $("#SearchOption").change(function () {
            var selectedOption = $("#SearchOption").val()
            if (selectedOption == 0) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            }
            if (selectedOption == 1) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            }
            if (selectedOption == 2) {
                $('#InputUser').show();
                $('#Inputpath').hide();
            }
            if (selectedOption == 3) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            } if (selectedOption == 4) {
                $('#InputUser').show();
                $('#Inputpath').show();
            }
        });

        function GetTotalComment($a) {
            //$('#Commentcount').empty();
            _folderUserCommentService.getTotalCommentFile({ id: $a }).done(function (result) {
                //$('#Commentcount').append(result.totalRows);
            });
        }
        
        function GetUsersRole($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
                //GetTotalSharedRole(result.roleId)
            });
        }

        $('#btnSearch').click(function (e) {
            e.preventDefault();

            if ($('#SearchOption').val() == 0) {
                $('#userid').val(abp.session.userId);
                GetAllComments();
            }
            if ($('#SearchOption').val() == 1) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                $('#InputUser').val("");
                $('#Inputpath').val();
                GetAllComments();
            }
            if ($('#SearchOption').val() == 2) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                $('#InputUser').val();
                $('#Inputpath').val("");
                GetAllComments();
            }
            if ($('#SearchOption').val() == 3) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val();
                $('#dateto').val();
                $('#InputUser').val("");
                $('#Inputpath').val("");
                GetAllComments();
            }
            if ($('#SearchOption').val() == 4) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val();
                $('#dateto').val();
                $('#InputUser').val();
                $('#Inputpath').val();
                GetAllComments();
            }

        })

        function GetAllComments() {
            dataTable.ajax.reload();
        }

        var dataTable = _$commentsTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _folderUserCommentService.getCommentList,
                inputFilter: function () {
                    var $a = $("#datefrom").val();
                    var $b = $("#dateto").val();
                    var $c = 'null';
                    var $d = abp.session.userId;
                    var $e = $('#InputUser').val();
                    var $f = 'null';
                    var $g = $('#Inputpath').val();
                    var $h = 'null';
                    return {
                        filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f + '|' + $g + '|' + $h
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderable: true,
                    targets: 1,
                    data: "cat5"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 2,
                    data: { cat3: "cat3" },
                    render: function (data) {
                        var icons = '<i class="' + data.cat3 + ' fa-2x flat-color-6"></i> '
                        return icons;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 3,
                    data: "cat2"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 4,
                    data: "dateTime",
                    "render": function (data) {

                        var options = {
                            //weekday: "short",
                            year: "numeric",
                            month: "short",
                            day: "numeric"
                        };

                        var newDateFormat = new Date(data).toLocaleDateString("en-US", options);
                        var newTimeFormat = new Date(data).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;

                        return dateAndTime;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 5,
                    data: "comments"
                },
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { id: "id", folderId: "folderId", comments: "comments", cat2: "cat2", cat3: "cat3" },
                    "render": function (data) {
                        var $folderId = data.folderId;
                        var $checkicon = "";
                        if ($folderId == 0) {
                            $checkicon = '<a id="read-comment" title="Read comment" href="#" class="read-comment" data-comment-id="' + data.id + '" data-comment-comments="' + data.comments + '" data-comment-cat2="' + data.cat2 + '" data-comment-cat3="' + data.cat3 + '" data-toggle="modal" data-target="#ModalComment"><i class="fa fa-square-o fa-2x flat-color-8"></i></a>';
                        }
                        else {
                            $checkicon = '<a id="read-comment" title="Read comment" href="#" class="read-comment" data-comment-id="' + data.id + '" data-comment-comments="' + data.comments + '" data-comment-cat2="' + data.cat2 + '" data-comment-cat3="' + data.cat3 + '" data-toggle="modal" data-target="#ModalComment"><i class="fa fa-check-square fa-2x flat-color-6"></i></a>';
                        }
                        return $checkicon;
                    }
                }
            ]
        });

        $('#commentsTable').on('click', 'a.read-comment', function (e) {
            e.preventDefault();
            $("#commentfilename").empty();
            $("#comment").empty();
            $("#commentIcon").empty();
            var $id = $(this).attr("data-comment-id");
            var $cat3 = $(this).attr("data-comment-cat3");
            var $cat2 = $(this).attr("data-comment-cat2");
            var $comment = $(this).attr("data-comment-comments");
            var $icons = '<i class="' + $cat3 + ' fa-1x flat-color-6"></i> '
            $("#commentIcon").append($icons);
            $("#commentfilename").append($cat2);
            $("#comment").append($comment);

            _folderUserCommentService.updateCommentFile({ id: $id }).done(function (result) {
                GetAllComments();
                GetTotalComment(abp.session.userId);
            });
        });
    })
})(jQuery);