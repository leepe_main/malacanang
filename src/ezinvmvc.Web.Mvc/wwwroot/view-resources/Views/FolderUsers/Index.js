﻿//Image Upload
//$('.custom-file-input').on('change', function () {

//    let fileName = $(this).val().split('\\').pop();
//    $(this).next('.custom-file-label').addClass("selected").html(fileName);
//});

//function readURL(input) {
//    if (input.files && input.files[0])
//    {
//        var _fileName = input.files[0].name;
//        var _size = input.files[0].size;

//        var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
//            i = 0; while (_size > 900) { _size /= 1024; i++; }
//        var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
//        var fileExtension = _fileName.split('.').pop();

//        var today = new Date();
//        var dd = today.getDate();
//        var mm = today.getMonth() + 1; //January is 0!
//        var yyyy = today.getFullYear();
//        var time = today.getHours() + ":" + today.getMinutes();
//        if (dd < 10) { dd = '0' + dd }
//        if (mm < 10) { mm = '0' + mm }
//        today = yyyy + '-' + mm + '-' + dd + ' ' + time;

//        $('#mb').val(exactSize);
//        $('#imageName').val(_fileName);
//        $('#extentionType').val(fileExtension);
//        $('#dateTime').val(today);
//        $('#path').val('~/' + abp.session.userId + '/' + _fileName);
//        $('#cat1').val(input.files[0].type);
//        var reader = new FileReader();
//        reader.onload = function (e)
//        {
//            $('#filepreview').attr('src', e.target.result);           
//        }
//        reader.readAsDataURL(input.files[0]);
//    }
//}

//$("#fileinput").change(function () {
//    readURL(this);
//});
//Image Upload


$(document).ready(function () {
    $("#Userids").hide();
    $("#Userids2").hide();
    $("#Roles").hide();
    $("#Roles2").hide();
    $("#Inputpath").val('Folders/' + abp.session.userId + '/');
    $('#FolderId').val('0');
});
function myFunction() {
    var x = document.getElementById("fileinput");
    $("#btnStartUploadfile").show();
    $("#btnclose").show();
    var txt = "";
    if ('files' in x) {
        if (x.files.length <= 0) {
            txt = "Select one or more files.";
        } else {
            for (var i = 0; i < x.files.length; i++) {
                txt += "<br><strong>" + (i + 1) + ". file</strong><br>";
                var file = x.files[i];
                if ('name' in file) {
                    txt += "name: " + file.name + " / size: " + file.size + " bytes <br>";
                }
            }
        }
    }
    else {
        if (x.value == "") {
            txt += "Select one or more files.";
        } else {
            txt += "The files property is not supported by your browser!";
            txt += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead.
        }
    }
    document.getElementById("demo").innerHTML = txt;
}

(function () {
    $(function () {
        var _folderUsersService = abp.services.app.folderUsersService;
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _newFolderService = abp.services.app.newFolderService;
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _$commentform = $('form[name=commentform]');
        var _$usersTable = $('#UsersTable');
        var _$shareduser = $('#shareduserTable');
        var _$sharedroleTable = $('#sharedroleTable');
        var _$sharedroleTable2 = $('#sharedroleTable2');
        var _$newfolder = $('form[name=newfolder]');
        var _$form1 = $('form[name=fileupload]');
        var _$shareuserfile = $('form[name=shareuserfile]');
        var _$sharerolefile = $('form[name=sharerolefile]');
        var _$formfile = $('form[name=formfile]');
        //Upload1
        $('#btnStartUpload').click(function (e) {

            e.preventDefault();
            if (!_$form1.valid()) {
                return;
            }
            abp.ui.setBusy(_$form1);
            var formData = new FormData();
            formData.append('file', $('#fileinput')[0].files[0]);

            var items = _$form1.serializeFormToObject();

            if ($('#fileinput')[0].files.length !== 0) {
                items.imageName = $('#fileinput')[0].files[0].name;

                var $a = items.imageName;
                var $b = abp.session.userId;

                _folderUsersService.createFolderUser(items).done(function () {
                    $.ajax({
                        url: abp.appPath + 'FolderUsers/UploadFileUser?userid=' + $b + '&name = ' + $a,
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function () {
                            abp.message.success('Upload', 'Success');
                        },
                        error: function (e) { }
                    }).always(function () {
                        abp.ui.clearBusy(_$form1);
                    });
                });
            }
            abp.ui.clearBusy(_$form1);
        });
        //Upload2
        //$('#btnStartUploadfile').click(function (e) {

        //    e.preventDefault();
        //    if (!_$form1.valid()) {
        //        return;
        //    }
        //    abp.ui.setBusy(_$form1);

        //    $("#btnStartUploadfile").hide();
        //    $("#btnclose").hide();
        //    var formData = new FormData();

        //    var progressEle = $(this).siblings(".progressBar");
        //    progressEle.css("background", "#796fcc");

        //    $.each($("input[type='file']")[0].files, function (i, file) {
        //        formData.append('file', file);
        //        var items = _$form1.serializeFormToObject();
        //        //name
        //        var $a = file.name;
        //        //size
        //        var _size = file.size;
        //        var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        //            i = 0; while (_size > 900) { _size /= 1024; i++; }
        //        var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
        //        //extention
        //        var fileExtension = $a.split('.').pop();
        //        //date
        //        var today = new Date();
        //        var dd = today.getDate();
        //        var mm = today.getMonth() + 1; //January is 0!
        //        var yyyy = today.getFullYear();
        //        var time = today.getHours() + ":" + today.getMinutes();
        //        if (dd < 10) { dd = '0' + dd }
        //        if (mm < 10) { mm = '0' + mm }
        //        today = yyyy + '-' + mm + '-' + dd + ' ' + time;
        //        //date


        //        //--type trim--//        
        //        var icons = "fa-cogs"
        //        if (fileExtension == "aac") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "abw") {
        //            icons = "fa-file-word-o"
        //        };
        //        if (fileExtension == "arc") {
        //            icons = "fa-compress"
        //        };
        //        if (fileExtension == "avi") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "azw") {
        //            icons = "fa-book"
        //        };
        //        if (fileExtension == "bin") {
        //            icons = "fa-file-code-o"
        //        };
        //        if (fileExtension == "bmp") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "bz") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "bz2") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "cda") {
        //            icons = "fa-line-chart"
        //        };
        //        if (fileExtension == "csh") {
        //            icons = "fa-book"
        //        };
        //        if (fileExtension == "css") {
        //            icons = "fa-css3"
        //        };
        //        if (fileExtension == "csv") {
        //            icons = "fa-file-excel-o"
        //        };
        //        if (fileExtension == "doc") {
        //            icons = "fa-file-word-o"
        //        };
        //        if (fileExtension == "docx") {
        //            icons = "fa-file-word-o"
        //        };
        //        if (fileExtension == "eot") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "epub") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "gz") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "gif") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "ico") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "ics") {
        //            icons = "fa-calendar"
        //        };
        //        if (fileExtension == "jar") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "json") {
        //            icons = "fa-jsfiddle"
        //        };
        //        if (fileExtension == "jsonld") {
        //            icons = "fa-jsfiddle"
        //        };
        //        if (fileExtension == "mjs") {
        //            icons = "fa-jsfiddle"
        //        };
        //        if (fileExtension == "mp3") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "mp4") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "mpeg") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "mpkg") {
        //            icons = "fa-apple"
        //        };
        //        if (fileExtension == "odp") {
        //            icons = "fa-file-powerpoint-o"
        //        };
        //        if (fileExtension == "ods") {
        //            icons = "fa-file-excel-o"
        //        };
        //        if (fileExtension == "odt") {
        //            icons = "fa-file-word-o"
        //        };
        //        if (fileExtension == "oga") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "ogv") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "ogx") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "opus") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "otf") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "png") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "pdf") {
        //            icons = "fa-file-pdf-o"
        //        };
        //        if (fileExtension == "php") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "ppt") {
        //            icons = "fa-file-powerpoint-o"
        //        };
        //        if (fileExtension == "pptx") {
        //            icons = "fa-file-powerpoint-o"
        //        };
        //        if (fileExtension == "rar") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "rtf") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "sh") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "svg") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "swf") {
        //            icons = "fa-bolt"
        //        };
        //        if (fileExtension == "tar") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "ts") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "wmv") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "ttf") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "txt") {
        //            icons = "fa-file-text-o"
        //        };
        //        if (fileExtension == "vsd") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "wav") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "weba") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "webm") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "webp") {
        //            icons = "fa-camera-retro"
        //        };
        //        if (fileExtension == "woff") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "woff2") {
        //            icons = "fa-font"
        //        };
        //        if (fileExtension == "xhtml") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "xls") {
        //            icons = "fa-file-excel-o"
        //        };
        //        if (fileExtension == "xlsx") {
        //            icons = "fa-file-excel-o"
        //        };
        //        if (fileExtension == "xul") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "zip") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "7z") {
        //            icons = "fa-file-archive-o"
        //        };
        //        if (fileExtension == "js") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "jpeg") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "jpg") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "htm") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "html") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "mid") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "midi") {
        //            icons = "fa-file-audio-o"
        //        };
        //        if (fileExtension == "tif") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "tiff") {
        //            icons = "fa-file-image-o"
        //        };
        //        if (fileExtension == "xml") {
        //            icons = "fa-code"
        //        };
        //        if (fileExtension == "3gp") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "3g2") {
        //            icons = "fa-file-video-o"
        //        };
        //        if (fileExtension == "jfif") {
        //            icons = "fa-file-image-o"
        //        };
        //        //--end trim--//


        //        var $a = file.name;
        //        var $e = $('#Inputpath').val();

        //        items.filename = file.name;
        //        items.mb = exactSize;
        //        items.extention = fileExtension;
        //        items.dateTime = today;
        //        items.path = $('#Inputpath').val();
        //        items.fileType = file.type;
        //        items.icon = icons;
        //        items.status = 'Active';
        //        items.field5 = $('#Ipaddress').val();
        //        items.folderId = $('#FolderId').val();
        //        $.ajax({
        //            url: abp.appPath + 'FolderUsers/UploadFileUser?path=' + $e + '&name = ' + $a,
        //            type: 'POST',
        //            data: formData,
        //            processData: false,
        //            contentType: false,
        //            xhr: function () {
        //                var xhr = new window.XMLHttpRequest();
        //                xhr.upload.addEventListener("progress", function (evt) {
        //                    if (evt.lengthComputable) {
        //                        var progress = Math.round((evt.loaded / evt.total) * 100);
        //                        progressEle.width(progress + "%");
        //                    }
        //                }, false);
        //                return xhr;
        //            },
        //            success: function (data) {
        //                if (data.state == 0) {

        //                }
        //                progressEle.width(0);
        //                $("#progress").hide();
        //                $("#fileinput").val('');
        //                $("#btnStartUploadfile").show();
        //                $("#btnclose").show();
        //                myFunction();

        //                _folderUsersService.createFolderUser(items).done(function () {
        //                    abp.message.success('Upload', 'Success');
        //                }).always(function () {
        //                    abp.ui.clearBusy(_$form1);

        //                    loadfile("", "", abp.session.userId, "", "", "", $("#FolderId").val());
        //                });
        //            },
        //            error: function (e) { }
        //        });
        //    });

        //    abp.ui.clearBusy(_$form1);
        //});
        //end Upload2

        //Start User
        var dataTable = _$usersTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _folderUsersService.getAllUsersList,
                inputFilter: function () {
                    var $r = $('#lbRoles').val();
                    var $u = $('#UserTableFilter').val();
                    return {
                        filter: $u
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderable: true,
                    targets: 1,
                    data: "cat2"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 2,
                    data: "cat3"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 3,
                    data: "cat4"
                },
                {
                    orderable: false,
                    targets: 4,
                    class: "text-center",
                    data: { id: "id", cat4: "cat4" },
                    "render": function (data) {
                        return '<a id="add-user" title="add" href="#" class="add-user btn btn-primary btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '"><i class="fa fa-check"></i></a>&nbsp;|&nbsp;<a id="delete-user" title="delete" href="#" class="delete-user btn btn-danger btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '" data-user-role="' + data.cat4 + '"><i class="fa fa-times"></i></a>';
                    }
                }
            ]
        });
        $('#UsersTable').on('click', 'a.add-user', function (e) {
            e.preventDefault();
            //date
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;
            //date
            if (!_$shareuserfile.valid()) {
                return;
            }
            var items = _$shareuserfile.serializeFormToObject();
            items.dateTime = today;
            items.userId = $(this).attr("data-user-id");
            items.roleId = $(this).attr("data-user-role");
            items.fileId = $('#fileId').val();
            items.description = $('#description2').val();
            items.folderId = 0;
            _sharedUserFileService.createSharedUser(items).done(function () {
                abp.message.success('Shared created', 'Success');
                GetSharedusers();
            });
        });
        $('#UsersTable').on('click', 'a.delete-user', function (e) {
            e.preventDefault();
            var $fileId = $('#fileId').val();
            var $userId = $(this).attr("data-user-id");

            _sharedUserFileService.deleteSharedUserce({ userId: $userId, fileId: $fileId }).done(function () {

            });
            GetSharedusers();
        });
        function Getusers() {
            dataTable.ajax.reload();
        }
        function GetSharedusers() {
            dataTable2.ajax.reload();
        }
        $(document).ready(function () {
            Getusers();
        });
        var dataTable2 = _$shareduser.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _sharedUserFileService.getFolderUsersList,
                inputFilter: function () {
                    var $a = $('#fileId').val();
                    var $b = '';
                    var $c = '';
                    var $d = '';
                    if ($b === '') {
                        $b = 'null';
                    }
                    if ($c === '') {
                        $c = 'null';
                    }
                    return {
                        filter: $a + '|' + $b + '|' + $c + '|' + $d
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 1,
                    data: "fileId"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 2,
                    data: "userId"
                },
                {
                    visible: true,
                    orderable: true,
                    targets: 3,
                    data: "cat1"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 4,
                    data: "roleId"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 5,
                    data: "cat2"
                },
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { id: "id", cat4: "cat4" },
                    "render": function (data) {
                        return '<a id="delete-user" title="delete" href="#" class="delete-user btn btn-danger btn-sm" data-user-id="' + data.userId + '" data-user-role="' + data.cat4 + '" data-user-role="' + data.cat4 + '"><i class="fa fa-times"></i></a>';
                        //return '<a id="add-user" title="add" href="#" class="add-user btn btn-primary btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '"><i class="fa fa-check"></i></a>&nbsp;|&nbsp;<a id="delete-user" title="add" href="#" class="delete-user btn btn-danger btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '" data-user-role="' + data.cat4 + '"><i class="fa fa-times"></i></a>';
                    }
                }
            ]
        });
        $('#shareduserTable').on('click', 'a.delete-user', function (e) {
            e.preventDefault();
            var $fileId = $('#fileId').val();
            var $userId = $(this).attr("data-user-id");

            _sharedUserFileService.deleteSharedUserce({ userId: $userId, fileId: $fileId }).done(function () {
                GetSharedusers();
            });

        });
        $(document).ready(function () {
            $("#Logos3").show();
            $("#Userids2").hide();
            $("#Roles2").hide();
            $("#row-list").empty();
            //$.getJSON("https://api.ipify.org/?format=json", function (e) {
            //    //console.log(e.ip);
            //    //alert(e.ip);
            //    $("#Ipaddress").val(e.ip);
            //});
            $.getJSON('https://json.geoiplookup.io/?callback=?', function (data) {
                console.log(JSON.stringify(data, null, 2));
                $("#Ipaddress").val('Country :' + data.country_name + ' / Latitude :' + data.latitude + ' / Longitude :' + data.longitude + ' / Region :' + data.region + ' / City :' + data.city + ' / Ip :' + data.ip + ' / Isp :' +  data.isp);
            });
            loadfile("", "", abp.session.userId, "", "", "", "0");
            setInterval(function () { loadComment("", "", "", $('#fileId').val(), "", ""); }, 30000);            
        });

        function loadfile($a, $b, $c, $d, $e, $f, $g) {
            
            $("#row-list").empty();
            _folderUsersService.getFolderUsersList({ filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f + '|' + $g }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $id = result.items[i].id;
                    var $imageName = result.items[i].fileName;
                    var $dateTime = result.items[i].dateTime;
                    var $cat1 = result.items[i].fileType;
                    var $cat2 = result.items[i].icon;
                    var $mb = result.items[i].mb;
                    var $path = result.items[i].path + result.items[i].fileName;

                    var $pathfolder = result.items[i].path;
                    var $status = result.items[i].status;
                    var $folderId = result.items[i].folderId;
                    //--trim--//
                    var file_name = $imageName;
                    if (file_name.length > 10) {
                        file_name = file_name.substr(0, 7) + '..' + file_name.substr(-4);
                    }

                    //--end trim--//
                    abp.ui.setBusy(_$formfile);
                    $("#row-list").prepend('<a href="javascript:void(0)" class="teacher-link" title=' + $imageName + ' data-val-id=' + $id + ' data-val-name=' + $imageName + ' data-val-date=' + $dateTime + ' data-val-mb=' + $mb + ' data-val-cat1=' + $cat1 + ' data-val-status=' + $status + ' data-val-path=' + $path + ' data-val-pathfolder=' + $pathfolder + ' data-val-folderid=' + $folderId + ' style="width: 120px;"><div class="parent-icon flat-color-6"><i class="fa ' + $cat2 + ' fa-2x "></i></div>' + file_name + '</a>');
                    abp.ui.clearBusy(_$formfile);
                }
                $('.teacher-link').on("click", function (e) {
                    var $FileFolder = $(this).attr("data-val-cat1");
                    if ($FileFolder == "Folder")
                    {
                        _newFolderService.getNewFolderbyId({ id: $(this).attr("data-val-id") }).done(function (result) {
                            $('#Inputpath').val(result.path);
                        }) 
                        $('#FolderId').val($(this).attr("data-val-id"));     
                        $('#LastFolderId').val($(this).attr("data-val-folderid"));
                        loadfile("", "", abp.session.userId, "", "", "", $("#FolderId").val());
                    }
                    else
                    {
                        $('#mymodal').modal('show');

                        $("#Logos").show();
                        $("#Logos3").show();
                        $("#Userids").hide();
                        $("#Userids2").hide();
                        $("#Roles").hide();
                        $("#Roles2").hide();
                        if ($('#FolderId').val() != "0") {
                            $("#Inputpath").val();
                            $('#FolderId').val();
                        }
                        else {
                            $("#Inputpath").val('Folders/' + abp.session.userId + '/');
                            $('#FolderId').val('0');
                        }
                        var myValid = $(this).attr("data-val-id");
                        var myValdate = $(this).attr("data-val-date");
                        var myValmb = $(this).attr("data-val-mb");
                        var myValcat1 = $(this).attr("data-val-cat1");
                        var myValstatus = $(this).attr("data-val-status");
                        var myValpath = $(this).attr("data-val-path");

                        var options = {
                            weekday: "long",
                            year: "numeric",
                            month: "short",
                            day: "numeric"
                        };

                        var newDateFormat = new Date(myValdate).toLocaleDateString("en-US", options);
                        var newTimeFormat = new Date(myValdate).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;
                        $('#fileId').val(myValid);
                        _folderUsersService.getDataById({ id: myValid }).done(function (result) {

                            var myValname2 = result.fileName;
                            $('#filepath').val(result.path);
                            $('#filepath2').val(result.path);
                            $('#filename').val(result.fileName);
                            $('#filename2').val(result.fileName);
                            GetSharedusers();
                            GetAllSharedRoles();
                            loadComment("", "", "", $('#fileId').val(), "", "");
                            $("#details").html('<div class= "col-sm-12"><table class="table" style="border="1"><tbody><tr><td style="text-align: right;">File&nbsp;Id :</td><td style="text-align: left;">&nbsp;' + myValid + '</td></tr><tr><td style="text-align: right;">File&nbsp;Name :</td><td style="text-align: left;">&nbsp;' + myValname2 + '</td></tr><tr><td style="text-align: right;">Date :</td><td style="text-align: left;">&nbsp;' + dateAndTime + '</td></tr><tr><td style="text-align: right;">File&nbsp;Size :</td><td style="text-align: left;">&nbsp;' + myValmb + '&nbsp;mb </td></tr><tr><td style="text-align: right;">File&nbsp;Type :</td><td style="text-align: left;">' + myValcat1 + '</td></tr><tr><td style="text-align: right;">Status :</td><td style="50%; text-align: left;">&nbsp;' + myValstatus + '</td></tr><tr><td style="text-align: right;">Path :</td><td style="text-align: left;">&nbsp;' + myValpath + '</td></tr><tr><td style = "text-align: right;">&nbsp;</td><td style="text-align: left;"><a href="' + myValpath + '" target="_blank"  title="Download"></td></tr></tbody></table></div>');

                        }) 


                    }                   
                })
            });
           
        }       

        $('.file-name').on("click", function (e) {
            e.preventDefault();
            var $name = $(this).attr("val-name");
            $('#filename').val($name);
            $('#filename2').val($name);
            $('#fileId').val($id);
        })
        
        $('#btnshare').click(function (e) {
            e.preventDefault();
            $("#Logos").hide();
            $("#Logos3").hide();
            $("#Userids").show();
            $("#Userids2").show();
            $("#Roles").hide();
            $("#Roles2").hide();
        });

        $('#btndownload').click(function () {
            $("#Logos").show();
            $("#Logos3").show();
            $("#Userids").hide();
            $("#Userids2").hide();
            $("#Roles").hide();
            $("#Roles2").hide();
            var filepath = $('#filepath').val() + $('#filename').val();
            //location.href = filepath;

            var link = document.createElement('a');
            link.href = filepath;
            link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
            link.click();
        });
        
        $('#btnshareroles').click(function (e) {
            e.preventDefault();
            $("#Logos").hide();
            $("#Logos3").hide();
            $("#Userids").hide();
            $("#Userids2").hide();
            $("#Roles").show();
            $("#Roles2").show();
        });
        //end Users

        //Start Roles
        function GetAllRoles() {
            dataTable3.ajax.reload();
        }
        var dataTable3 = _$sharedroleTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _folderUsersService.getAllRolesList,
                inputFilter: function () {
                    return {
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: true,
                    orderable: true,
                    targets: 1,
                    data: "fileName"
                },

                {
                    orderable: false,
                    targets: 2,
                    class: "text-center",
                    data: { id: "id", fileName: "fileName" },
                    "render": function (data) {
                        return '<a id="add-role" title="add" href="#" class="add-role btn btn-primary btn-sm" data-role-id="' + data.id + '" data-role-role="' + data.fileName + '"><i class="fa fa-check"></i></a>&nbsp;|&nbsp;<a id="delete-user" title="delete" href="#" class="delete-user btn btn-danger btn-sm" data-role-id="' + data.id + '" data-role-role="' + data.fileName + '"><i class="fa fa-times"></i></a>';
                    }
                }
            ]
        });
        $('#sharedroleTable').on('click', 'a.add-role', function (e) {
            e.preventDefault();
            //date
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;
            //date
            if (!_$sharerolefile.valid()) {
                return;
            }
            var items = _$sharerolefile.serializeFormToObject();
            items.dateTime = today;
            items.roleId = $(this).attr("data-role-id");
            items.fileId = $('#fileId').val();
            items.description = $('#descriptionroles').val();
            _sharedRoleFileService.createSharedRole(items).done(function () {
                abp.message.success('Shared created', 'Success');
                GetAllSharedRoles();
            });
        });
        $('#sharedroleTable').on('click', 'a.delete-user', function (e) {
            e.preventDefault();
            var $fileId = $('#fileId').val();
            var $roleId = $(this).attr("data-role-id");

            _sharedRoleFileService.deleteSharedRole({ roleId: $roleId, fileId: $fileId }).done(function () {
                abp.message.success('Shared Deleted', 'Success');
                GetAllSharedRoles();
            });

        });

        function GetAllSharedRoles() {
            dataTable4.ajax.reload();
        }
        var dataTable4 = _$sharedroleTable2.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _sharedRoleFileService.getSharedRoleList,
                inputFilter: function () {
                    var $a = $('#fileId').val();
                    var $b = '';
                    var $c = '';
                    if ($b === '') {
                        $b = 'null';
                    }
                    if ($c === '') {
                        $c = 'null';
                    }
                    return {
                        filter: $a + '|' + $b + '|' + $c
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 1,
                    data: "fileId"
                },
                {
                    visible: false,
                    orderable: true,
                    targets: 2,
                    data: "roleId"
                },
                {
                    visible: true,
                    orderable: true,
                    targets: 3,
                    data: "cat2"
                },
                {
                    orderable: false,
                    targets: 4,
                    class: "text-center",
                    data: { id: "id", cat4: "cat4" },
                    "render": function (data) {
                        return '<a id="delete-user" title="delete" href="#" class="delete-user btn btn-danger btn-sm" data-user-id="' + data.roleId + '" data-user-role="' + data.cat4 + '" data-user-role="' + data.cat4 + '"><i class="fa fa-times"></i></a>';
                        //return '<a id="add-user" title="add" href="#" class="add-user btn btn-primary btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '"><i class="fa fa-check"></i></a>&nbsp;|&nbsp;<a id="delete-user" title="add" href="#" class="delete-user btn btn-danger btn-sm" data-user-id="' + data.id + '" data-user-role="' + data.cat4 + '" data-user-role="' + data.cat4 + '"><i class="fa fa-times"></i></a>';
                    }
                }
            ]
        });
        $('#sharedroleTable2').on('click', 'a.delete-user', function (e) {
            e.preventDefault();
            var $fileId = $('#fileId').val();
            var $roleId = $(this).attr("data-user-id");

            _sharedRoleFileService.deleteSharedRole({ roleId: $roleId, fileId: $fileId }).done(function () {
                abp.message.success('Shared Deleted', 'Success');
                GetAllSharedRoles();
            });

        });
        //End Roles    

        //start new folder
        $('#btncreatenewfolder').click(function (e) {
            e.preventDefault();
            if (!_$newfolder.valid()) {
                return;
            }
            var items2 = _$newfolder.serializeFormToObject();
            abp.ui.setBusy(_$newfolder);

            $("#btncreatenewfolder").hide();
            $("#btnclosenewfolder").hide();

            var formData = new FormData();

            var progressEle = $(this).siblings(".progressBar");
            progressEle.css("background", "#796fcc");

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            //items.foldername = $('#foldername').val();
            //items.folderpath = $('#Inputpath').val();
            //items.fileType = 'File FOlder';
            //items.status = 'Active';

            var $a = $('#Inputpath').val() + $('#foldername').val() + '/';

            items2.fileName = $('#foldername').val();
            items2.mb = '0';
            items2.extention = 'Folder';
            items2.dateTime = today;
            items2.path = $a;
            items2.fileType = 'Folder';
            items2.icon = 'fa-folder-o';
            items2.status = 'Active';
            items2.folderId = $("#FolderId").val();

            $.ajax({
                url: abp.appPath + 'FolderUsers/NewFolder?path=' + $a,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var progress = Math.round((evt.loaded / evt.total) * 100);
                            progressEle.width(progress + "%");
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    if (data.state == 0) {

                    }
                    progressEle.width(0);
                    $("#progress").hide();
                    $("#foldername").val('');
                    $("#btncreatenewfolder").show();
                    $("#btnclosenewfolder").show();
                    myFunction();

                    _newFolderService.createNewFolder(items2).done(function () {
                        abp.message.success('Create new folder', 'Success');
                        
                    }).always(function () {
                        abp.ui.clearBusy(_$newfolder);

                        loadfile("", "", abp.session.userId, "", "", "", $("#FolderId").val());
                        
                    });
                },
                error: function (e) { }
            });
            //$("#newfolder").hide();
        });

        $('#btnback').click(function (e) {
            e.preventDefault();
            if ($("#LastFolderId").val() == 0) {
               $("#FolderId").val(0);
            }
            var id1 = $("#LastFolderId").val();
            var id2 = $("#FolderId").val();
            //getfolderid(id1);
            if ($("#LastFolderId").val() == 0) {
                $("#Inputpath").val('Folders/' + abp.session.userId + '/');
                loadfile("", "", abp.session.userId, "", "", "", id1);
            }
            else
            {
                getfolderid(id1);
            }
            //loadfile("", "", abp.session.userId, "", "", "", $("#LastFolderId").val());
        });

        function getfolderid(id1) {
            _newFolderService.getNewFolderbyId({ id: id1 }).done(function (result) {
                $("#FolderId").val(result.id);
                $("#LastFolderId").val(result.folderId);
                $("#Inputpath").val(result.path);
                loadfile("", "", abp.session.userId, "", "", "", result.id);
            })   
        }
        //End new folder

        //start comment//
        $('#btnComments').click(function (e) {
            e.preventDefault();
            loadComment("", "", "", $('#fileId').val(), "", "");
        });

        $('#btnaddcomment').click(function (e) {
            e.preventDefault();
            AddComment();
        });

        function AddComment() {
            if (!_$commentform.valid()) {
                return;
            }
            var items = _$commentform.serializeFormToObject();
            abp.ui.setBusy(_$commentform);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            items.userId = abp.session.userId;
            items.fileId = $('#fileId').val();
            items.dateTime = today;
            items.comments = $('#comments').val();
            items.folderId = 1;
            _folderUserCommentService.createFolderUserComment(items).done(function () {
                abp.message.success('Comment Created', 'Success');
                $('#comments').val("");
                abp.ui.clearBusy(_$commentform);
                loadComment("", "", "", $('#fileId').val(), "", "");
            });
        }

        function loadComment($a, $b, $c, $d, $e, $f) {

            $("#comment-list").empty();
            _folderUserCommentService.getFolderUserComment({ filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $fileId = result.items[i].fileId;
                    var $userImage = result.items[i].userImage;
                    var $dateTime = result.items[i].dateTime;
                    var $comments = result.items[i].comments;
                    var $UserName = result.items[i].userName;
                    var $userId = result.items[i].userId;
                    var $folderId = result.items[i].folderId;
                    var $dateTime2 = $dateTime.replace(/T/g, " ");
                    abp.ui.setBusy(_$commentform);
                    if ($userId == $folderId) {
                        $("#comment-list").prepend('<div class="d-flex flex-row p-3"><img src="images/avatar/64-2.jpg" width="50" height="50" style="border-radius:50px"><div class="chat ml-2 p-3"> <span style="color:blue">' + $UserName + ' </span><span style="color:gray;font-size:9px">' + $dateTime2 + ' </span><br>' + $comments + '</div></div>');
                    }
                    else {
                        $("#comment-list").prepend('<div class="d-flex flex-row p-3"><div class="mr-2 p-2" style="width:100%; text-align:end;"><span style="color:red">' + $UserName + '</span> <span style="color:gray;font-size:9px">' + $dateTime2 + ' </span><br><span class="text-muted">' + $comments + '</span></div> <img src="images/avatar/1.jpg" width="50" height="50" style="border-radius:50px"></div>');

                    }
                    abp.ui.clearBusy(_$commentform);
                }
            });
        }
        //end comment//

        //Upload3
        $('#btnUploadfile').click(function (e) {
            e.preventDefault();
            saveinput();                  
        });
        function saveinput() {
            abp.ui.setBusy(_$form1);

            $("#btnStartUploadfile").hide();
            $("#btnclose").hide();
            $("#btnUploadfile").hide();
            

            if (!_$form1.valid()) {
                return;
            }
            //var disabled = _$form.find(':input:disabled').removeAttr('disabled');
            //var formdata = _$form.serializeFormToObject();
            var $e = $('#Inputpath').val();

            var progressEle = $(this).siblings(".progressBar");
            progressEle.css("background", "#796fcc");

            var viewData = {
                foldersusersinput: []
            };

            var formData = new FormData();
            $.each($("input[type='file']")[0].files, function (i, file) {
                formData.append('file', file);
                //var viewData = _$form1.serializeFormToObject();
                //name
                var $a = file.name;
                //size
                var _size = file.size;
                var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
                    i = 0; while (_size > 900) { _size /= 1024; i++; }
                var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
                //extention
                var fileExtension = $a.split('.').pop();
                //date
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                var time = today.getHours() + ":" + today.getMinutes();
                if (dd < 10) { dd = '0' + dd }
                if (mm < 10) { mm = '0' + mm }
                today = yyyy + '-' + mm + '-' + dd + ' ' + time;
                //date


                //--type trim--//        
                var icons = "fa-cogs";

                icons = functionIconSelector(fileExtension);

                var $a = file.name;
                var $e = $('#Inputpath').val();

                //items.filename = file.name;
                //items.mb = exactSize;
                //items.extention = fileExtension;
                //items.dateTime = today;
                //items.path = $('#Inputpath').val();
                //items.fileType = file.type;
                //items.icon = icons;
                //items.status = 'Active';
                //items.field5 = $('#Ipaddress').val();
                //items.folderId = $('#FolderId').val();

                //folderusersinput: []

                obj = {};
                obj["FileName"] = file.name;
                obj["MB"] = exactSize;
                obj["Extention"] = fileExtension;
                obj["DateTime"] = today;
                obj["Path"] = $('#Inputpath').val();
                obj["FileType"] = file.type;
                obj["Icon"] = icons;
                obj["Status"] = 'Active';
                obj["Field5"] = $('#Ipaddress').val();
                obj["FolderId"] = $('#FolderId').val();
                viewData.foldersusersinput.push(obj);
            });

            //console.log(viewData);

            $.ajax({
                url: abp.appPath + 'FolderUsers/UploadFileUser?path=' + $e + '&name = ' + 'b',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                xhr: function () {

                    $("#fileProgress").hide();

                    var fileXhr = $.ajaxSettings.xhr();
                    if (fileXhr.upload) {
                        $("progress").show();
                        fileXhr.upload.addEventListener("progress", function (e) {
                            if (e.lengthComputable) {
                                $("#fileProgress").attr({
                                    value: e.loaded,
                                    max: e.total
                                });
                            }
                        }, false);
                    }
                    return fileXhr;
                },
                success: function (data) {
                    if (data.state == 0) {

                    }
                    progressEle.width(0);
                    $("#progress").hide();
                    $("#fileinput").val('');
                    $("#btnStartUploadfile").show();
                    $("#btnclose").show();
                    $("#btnUploadfile").show();
                    myFunction();

                    _folderUsersService.createFileUsersInput(viewData).done(function () {
                        abp.message.success('Upload', 'Success');
                    }).always(function () {
                        abp.ui.clearBusy(_$form1);


                        loadfile("", "", abp.session.userId, "", "", "", $("#FolderId").val());
                    });
                },
                error: function (e) { }
            });

            abp.ui.clearBusy(_$form1);
        }

        function functionIconSelector(fileExtension) {
            var icons = "fa-cogs";
            if (fileExtension == "aac") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "abw") {
                icons = "fa-file-word-o";
            };
            if (fileExtension == "arc") {
                icons = "fa-compress";
            };
            if (fileExtension == "avi") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "azw") {
                icons = "fa-book";
            };
            if (fileExtension == "bin") {
                icons = "fa-file-code-o";
            };
            if (fileExtension == "bmp") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "bz") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "bz2") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "cda") {
                icons = "fa-line-chart";
            };
            if (fileExtension == "csh") {
                icons = "fa-book";
            };
            if (fileExtension == "css") {
                icons = "fa-css3";
            };
            if (fileExtension == "csv") {
                icons = "fa-file-excel-o";
            };
            if (fileExtension == "doc") {
                icons = "fa-file-word-o";
            };
            if (fileExtension == "docx") {
                icons = "fa-file-word-o";
            };
            if (fileExtension == "eot") {
                icons = "fa-font";
            };
            if (fileExtension == "epub") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "gz") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "gif") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "ico") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "ics") {
                icons = "fa-calendar";
            };
            if (fileExtension == "jar") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "json") {
                icons = "fa-jsfiddle";
            };
            if (fileExtension == "jsonld") {
                icons = "fa-jsfiddle";
            };
            if (fileExtension == "mjs") {
                icons = "fa-jsfiddle";
            };
            if (fileExtension == "mp3") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "mp4") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "mpeg") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "mpkg") {
                icons = "fa-apple";
            };
            if (fileExtension == "odp") {
                icons = "fa-file-powerpoint-o";
            };
            if (fileExtension == "ods") {
                icons = "fa-file-excel-o";
            };
            if (fileExtension == "odt") {
                icons = "fa-file-word-o";
            };
            if (fileExtension == "oga") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "ogv") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "ogx") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "opus") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "otf") {
                icons = "fa-font";
            };
            if (fileExtension == "png") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "pdf") {
                icons = "fa-file-pdf-o";
            };
            if (fileExtension == "php") {
                icons = "fa-code";
            };
            if (fileExtension == "ppt") {
                icons = "fa-file-powerpoint-o";
            };
            if (fileExtension == "pptx") {
                icons = "fa-file-powerpoint-o";
            };
            if (fileExtension == "rar") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "rtf") {
                icons = "fa-font";
            };
            if (fileExtension == "sh") {
                icons = "fa-code";
            };
            if (fileExtension == "svg") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "swf") {
                icons = "fa-bolt";
            };
            if (fileExtension == "tar") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "ts") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "wmv") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "ttf") {
                icons = "fa-font";
            };
            if (fileExtension == "txt") {
                icons = "fa-file-text-o";
            };
            if (fileExtension == "vsd") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "wav") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "weba") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "webm") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "webp") {
                icons = "fa-camera-retro";
            };
            if (fileExtension == "woff") {
                icons = "fa-font";
            };
            if (fileExtension == "woff2") {
                icons = "fa-font";
            };
            if (fileExtension == "xhtml") {
                icons = "fa-code";
            };
            if (fileExtension == "xls") {
                icons = "fa-file-excel-o";
            };
            if (fileExtension == "xlsx") {
                icons = "fa-file-excel-o";
            };
            if (fileExtension == "xul") {
                icons = "fa-code";
            };
            if (fileExtension == "zip") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "7z") {
                icons = "fa-file-archive-o";
            };
            if (fileExtension == "js") {
                icons = "fa-code";
            };
            if (fileExtension == "jpeg") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "jpg") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "htm") {
                icons = "fa-code";
            };
            if (fileExtension == "html") {
                icons = "fa-code";
            };
            if (fileExtension == "mid") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "midi") {
                icons = "fa-file-audio-o";
            };
            if (fileExtension == "tif") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "tiff") {
                icons = "fa-file-image-o";
            };
            if (fileExtension == "xml") {
                icons = "fa-code";
            };
            if (fileExtension == "3gp") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "3g2") {
                icons = "fa-file-video-o";
            };
            if (fileExtension == "jfif") {
                icons = "fa-file-image-o";
            };

            return icons;
        }
    })

})(jQuery);