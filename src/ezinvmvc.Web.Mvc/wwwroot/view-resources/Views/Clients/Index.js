﻿// Clients
(function () {
    $(function () {

        var _$table = $('#ClientsTable');
        var _service = abp.services.app.clientService;
        var _$modal = $('#ClientCreateModal');
        var _$form = _$modal.find('form');

        var _permissions = {
            create: abp.auth.hasPermission('Master.Clients.Create'),
            edit: abp.auth.hasPermission('Master.Clients.Edit'),
            'delete': abp.auth.hasPermission('Master.Clients.Delete')
        };

        var dataTable = _$table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _service.getClients,
                inputFilter: function () {
                    var $s = $('#SearchFilter').val();
                    var $statusid = $('#StatusTypes').val();
                    var $accountexecutive = 'null';
                    if (!abp.auth.isGranted("Master.Clients.AllAccounts")) {
                        var empid = $('#h1').val(); //getUserEmployee(abp.session.userId);
                        console.log(empid);
                        $accountexecutive = empid;
                    }
                    return {
                        filter: $s + '|' + $statusid + '|' + $accountexecutive
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderData: 7,
                    targets: 1,
                    data: { id: "id", code: "code" },
                    render: function (data) {
                        return '<a id="view-client" title="view" href="#" class="view-client" data-client-id="' + data.id + '" data-client-code="' + data.code + '">' + data.code + '</i></a>';
                    }
                },
                {
                    targets: 2,
                    data: "name"
                },
                {
                    targets: 3,
                    data: "industry"
                },
                {
                    targets: 4,
                    data: "completeAddress"
                },
                {
                    orderData: 8,
                    targets: 5,
                    data: { status: "status", statusid: "statusId" },
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 4) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 5) {
                            return '<span class="badge badge-danger">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                },
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { id: "id", name: "name"},
                    "render": function (data) {
                        return '<a id="edit-client" title="edit" href="#" class="edit-client" data-client-id="' + data.id + '"><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-client" title="delete" href="#" class="delete-client" data-client-id="' + data.id + '" data-client-name="' + data.name + '"><i class="fa fa-trash"></i></a>';
                    }
                },
                {
                    visible: false,
                    targets: 7,
                    data: "code"
                },
                {
                    visible: false,
                    targets: 8,
                    data: "statusId"
                }
            ]
        });
       
        function getAll() {
            dataTable.ajax.reload();
        }

        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getAll();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getAll();
        });

        $("#StatusTypes").change(function () {
            getAll();
        });

        $('#SearchFilter').focus();

        // Save record
        function save() {
            if (!_$form.valid()) {
                return;
            }
            var client = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
            abp.ui.setBusy(_$modal);
            _service.createClient(client).done(function () {
                _$form.trigger("reset");
                _$modal.modal('hide');
                getAll();
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        }

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();
            save();
        });

        _$form.find('input').on('keypress', function (e) {
            if (e.which === 13) {
                e.preventDefault();
                save();
            }
        });

        _$form.find('select').on('keypress', function (e) {
            if (e.which === 13) {
                e.preventDefault();
                save();
            }
        });
        _$modal.on('shown.bs.modal', function () {
            _$form.find('input[type=text]:first').focus();
        });

        // View record
        _$table.on('click', 'a.view-client', function (e) {
            e.preventDefault();
            var clientId = $(this).attr("data-client-id");
            window.location.href = abp.appPath + 'Clients/Details?id=' + clientId;
        });

        // Edit record
        _$table.on('click', 'a.edit-client', function (e) {
            e.preventDefault();
            var clientId = $(this).attr("data-client-id");
            window.location.href = abp.appPath + 'Clients/Edit?id=' + clientId;
        });

        // Delete record
        _$table.on('click', 'a.delete-client', function (e) {
            var id = $(this).attr("data-client-id");
            var name = $(this).attr("data-client-name");

            e.preventDefault();
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('DeleteClientConfirmation', 'ezinvmvc'), name),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _service.deleteClient({
                            id: id
                        }).done(function () {
                            getAll();
                        });
                    }
                }
            );
        });


        $('#ExportToExcelButton').click(function (e) {
            e.preventDefault();

            _service.getClientsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });


        $('#ExportButton').click(function () {
            _service
                .getClientsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

    });
})();