﻿(function () {
    $(function () {
        var _employeesservice = abp.services.app.employeeService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _$form1 = $('form[name=form1]');

        $(document).ready(function () {
            loadRegProfile(abp.session.userId);
        });

        function loadRegProfile($a) {
            _sharedRoleFileService.getUser({ id: $a }).done(function (result) {
                $("#UserId").val(result.id);
                $("#UserName").val(result.description);
                $("#EmailAdd").val(result.cat4);
                if (result.field1 = 1) {
                    $("#Status").val("Active");
                }
                else {
                    $("#Status").val("In-Active");
                }    
                $("#NameFile").val(result.cat1);
                $("#SureName").val(result.cat2);
                
                //$("#profileimage").empty();
                if (result.cat5 == null || result.cat5 == "") {
                    $("#profile").attr({ "src": "../images/avatar/user.png" });
                    $("#profileimage").attr({ "src": "../images/avatar/user.png" });
                }
                else {
                    $("#profile").attr({ "src": "../" + result.cat5 });
                    $("#profileimage").attr({ "src": "../" + result.cat5 });
                }   
                loadEmpProfile(result.id);
            })
        }

        function loadEmpProfile($a) {
            _employeesservice.getEmployeeProfile({ id: $a }).done(function (result) {
                if (result === null) {
                    abp.notify.warn('Please Update Details', 'No Profile');
                    $("#btnUpdate").hide();
                    $("#btnEdit").hide();
                    $("#btnSave").show();
                    $("#btnCancelled").show();
                }
                else {
                    $("#EmpId").val(result.id);
                    $("#firstName").val(result.firstName);
                    $("#middleName").val(result.middleName);
                    $("#lastName").val(result.lastName);
                    $("#telNo").val(result.telNo);
                    $("#cellNo").val(result.cellNo);
                    $("#remarks").val(result.remarks);
                    $("#address").val(result.address);
                    $("#OldImage").val(result.userImagePath);

                    if (result.userImagePath == null || result.userImagePath == "") {
                        $("#profile").attr({ "src": "../images/avatar/user.png" });
                        $("#profileimage").attr({ "src": "../images/avatar/user.png" });
                    }
                    else {
                        $("#profile").attr({ "src": "../" + result.userImagePath });
                        $("#profileimage").attr({ "src": "../" + result.userImagePath });
                    }   

                    if (result.id != null || result.id != "") {
                        $("#firstName").prop("disabled", true);
                        $("#middleName").prop("disabled", true);
                        $("#lastName").prop("disabled", true);
                        $("#telNo").prop("disabled", true);
                        $("#cellNo").prop("disabled", true);
                        $("#remarks").prop("disabled", true);
                        $("#address").prop("disabled", true);
                        $('input#fileinput').prop('disabled', true); 
                        $("#btnSave").hide();
                        $("#btnUpdate").hide();
                        $("#btnCancelled").hide();
                    }
                    else {
                        $("#btnSave").show();
                    }
                }
            })
        }


        $('#btnEdit').click(function (e) {
            e.preventDefault();
            $("#firstName").prop("disabled", false);
            $("#middleName").prop("disabled", false);
            $("#lastName").prop("disabled", false);
            $("#telNo").prop("disabled", false);
            $("#cellNo").prop("disabled", false);
            $("#remarks").prop("disabled", false);
            $("#address").prop("disabled", false);
            $('input#fileinput').prop('disabled', false); 
            $("#btnUpdate").show();
            $("#btnCancelled").show();
            $("#btnSave").hide();
        });
        $('#btnCancelled').click(function (e) {
            e.preventDefault();
            $("#firstName").prop("disabled", true);
            $("#middleName").prop("disabled", true);
            $("#lastName").prop("disabled", true);
            $("#telNo").prop("disabled", true);
            $("#cellNo").prop("disabled", true);
            $("#remarks").prop("disabled", true);
            $("#address").prop("disabled", true);
            $('input#fileinput').prop('disabled', true);
            $("#btnUpdate").hide();
            $("#btnSave").hide();
        });

        $('#btnSave').click(function (e) {
            e.preventDefault();
            if (!_$form1.valid()) {
                return;
            }
            abp.ui.setBusy(_$form1);

            //$("#btnSave").hide();
            var formData = new FormData();

            $.each($("input[type='file']")[0].files, function (i, file) {
                formData.append('file', file);
                var items = _$form1.serializeFormToObject();
                //name
                var $a = file.name;
                var $e = "images/avatar/";
                items.userId = $("#UserId").val();
                items.firstName = $("#firstName").val();
                items.middleName = $("#middleName").val();
                items.lastName = $("#lastName").val();
                items.telNo = $("#telNo").val();
                items.userImagePath = $e + $a;
                items.cellNo = $("#cellNo").val();
                items.remarks = $("#remarks").val();
                items.address = $("#address").val();
                abp.message.confirm(
                    'New Profile will be added.',
                    'Are you sure?',
                    function (isConfirmed) {
                        if (isConfirmed) {
                            abp.ui.setBusy(_$form1);
                            _employeesservice.createEmployee(items).done(function () {
                                $.ajax({
                                    url: abp.appPath + 'Employees/UploadFileUser?path=' + $e,
                                    type: 'POST',
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function () {
                                        $("#btnSave").hide();
                                        $("#btnUpdate").hide();
                                        abp.message.success('New Profile added successfully', 'Success');
                                    },
                                    error: function (e) { }
                                });
                            }).always(function () {
                                abp.ui.clearBusy(_$form1);
                            });
                        }
                    }
                );
            });
        });

        _$form1.find('input').on('keypress', function (e) {
            if (e.which === 13) {
                e.preventDefault();
                updateProfile();
            }
        });
        $('#btnUpdate').click(function (e) {
            e.preventDefault();
            updateProfile();
        });

        function updateProfile() {
            if (!_$form1.valid()) {
                return;
            }
            var $e = "images/avatar/";
            var formData = new FormData();
            formData.append('file', $('#fileinput')[0].files[0]);

            var $oimgn = $('#OldImage').val();
            var items = _$form1.serializeFormToObject(); //serializeFormToObject is defined in main.js

            if ($('#fileinput')[0].files.length !== 0) {
               var $a = $e + $('#fileinput')[0].files[0].name;
            }
            else {
                var $a = $oimgn;
            }
            items.id = $("#EmpId").val();
            items.firstName = $("#firstName").val();
            items.middleName = $("#middleName").val();
            items.lastName = $("#lastName").val();
            items.telNo = $("#telNo").val();
            items.userImagePath = $a;
            items.cellNo = $("#cellNo").val();
            items.remarks = $("#remarks").val();
            items.address = $("#address").val();
            items.userId = $("#UserId").val();

            abp.message.confirm(
                'New Profile will be Update.',
                'Are you sure?',
                function (isConfirmed) {
                    if (isConfirmed) {
                        abp.ui.setBusy(_$form1);
                        _employeesservice.updateEmployee(items).done(function () {
                            $.ajax({
                                url: abp.appPath + 'Employees/UploadFileUser?path=' + $e,
                                type: 'POST',
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function () {
                                    abp.message.success('Profile info updated', 'Success');
                                    location.reload(true);
                                },
                                error: function (e) { }
                            });
                        }).always(function () {
                            abp.ui.clearBusy(_$form1);
                        });
                    }
                }
            );
        }




        //$('#btnUpdate').click(function (e) {
        //    e.preventDefault();
        //    if (!_$form1.valid()) {
        //        return;
        //    }
        //    abp.ui.setBusy(_$form1);

        //    //$("#btnSave").hide();
        //    var formData = new FormData();

        //    $.each($("input[type='file']")[0].files, function (i, file) {
        //        formData.append('file', file);
        //        var items = _$form1.serializeFormToObject();
        //        //name
        //        var $a = file.name;
        //        var $e = "images/avatar/";
        //        items.firstName = $("#firstName").val();
        //        items.middleName = $("#middleName").val();
        //        items.lastName = $("#lastName").val();
        //        items.telNo = $("#telNo").val();
        //        items.userImagePath = $e + $a;
        //        items.cellNo = $("#cellNo").val();
        //        items.remarks = $("#remarks").val();
        //        items.address = $("#address").val();
        //        items.userId = $("#UserId").val();
        //        abp.message.confirm(
        //            'New Profile will be added.',
        //            'Are you sure?',
        //            function (isConfirmed) {
        //                if (isConfirmed) {
        //                    abp.ui.setBusy(_$form1);
        //                    _employeesservice.updateEmployee(items).done(function () {
        //                        $.ajax({
        //                            url: abp.appPath + 'Employees/UploadFileUser?path=' + $e + '&name = ' + $a,
        //                            type: 'POST',
        //                            data: formData,
        //                            processData: false,
        //                            contentType: false,
        //                            success: function () {
        //                                abp.message.success('New Profile added successfully', 'Success');
        //                            },
        //                            error: function (e) { }
        //                        });
        //                    }).always(function () {
        //                        abp.ui.clearBusy(_$form1);
        //                    });
        //                }
        //            }
        //        );
        //    });
        //});
    });
})(jQuery);