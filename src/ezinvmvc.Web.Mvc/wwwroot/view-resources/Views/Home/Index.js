﻿//$(function () {
//    //Widgets count
//    $('.count-to').countTo();

//    //Sales count to
//    $('.sales-count-to').countTo({
//        formatter: function (value, options) {
//            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
//        }
//    });

//    initRealTimeChart();
//    initDonutChart();
//    initSparkline();
//});

//var realtime = 'on';
//function initRealTimeChart() {
//    //Real time ==========================================================================================
//    var plot = $.plot('#real_time_chart', [getRandomData()], {
//        series: {
//            shadowSize: 0,
//            color: 'rgb(0, 188, 212)'
//        },
//        grid: {
//            borderColor: '#f3f3f3',
//            borderWidth: 1,
//            tickColor: '#f3f3f3'
//        },
//        lines: {
//            fill: true
//        },
//        yaxis: {
//            min: 0,
//            max: 100
//        },
//        xaxis: {
//            min: 0,
//            max: 100
//        }
//    });

//    function updateRealTime() {
//        plot.setData([getRandomData()]);
//        plot.draw();

//        var timeout;
//        if (realtime === 'on') {
//            timeout = setTimeout(updateRealTime, 320);
//        } else {
//            clearTimeout(timeout);
//        }
//    }

//    updateRealTime();

//    $('#realtime').on('change', function () {
//        realtime = this.checked ? 'on' : 'off';
//        updateRealTime();
//    });
//    //====================================================================================================
//}

//function initSparkline() {
//    $(".sparkline").each(function () {
//        var $this = $(this);
//        $this.sparkline('html', $this.data());
//    });
//}

//function initDonutChart() {
//    Morris.Donut({
//        element: 'donut_chart',
//        data: [{
//                label: 'Chrome',
//                value: 37
//            }, {
//                label: 'Firefox',
//                value: 30
//            }, {
//                label: 'Safari',
//                value: 18
//            }, {
//                label: 'Opera',
//                value: 12
//            },
//            {
//                label: 'Other',
//                value: 3
//            }],
//        colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
//        formatter: function (y) {
//            return y + '%'
//        }
//    });
//}

//var data = [], totalPoints = 110;
//function getRandomData() {
//    if (data.length > 0) data = data.slice(1);

//    while (data.length < totalPoints) {
//        var prev = data.length > 0 ? data[data.length - 1] : 50, y = prev + Math.random() * 10 - 5;
//        if (y < 0) { y = 0; } else if (y > 100) { y = 100; }

//        data.push(y);
//    }

//    var res = [];
//    for (var i = 0; i < data.length; ++i) {
//        res.push([i, data[i]]);
//    }

//    return res;
//}


(function () {
    $(function () {
        var _folderUsersService = abp.services.app.folderUsersService;
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _announcementService = abp.services.app.announcementService;
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _notificationService = abp.services.app.notificationService;

        var _$announcementform = $('form[name=announcementform]');
        var _$commentsTable = $('#commentsTable');

        $('.popover-dismiss').popover({
            trigger: 'focus'
        })
       
        $(document).ready(function () {
            $('#comments').show();
            $('#delete').hide();
            GetRoles(abp.session.userId);
            GetTotalUpload(abp.session.userId);
            GetTotalComment(abp.session.userId);
            GetTotalShares(abp.session.userId);
            GetUsersRole(abp.session.userId);
            GetAllComments();
            Top5Download();
            
        });
        function GetRoles($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {    
                $('#roleid').val(result.roleId)
                if (result.roleId == 1 || result.roleId == 3) { $('#comments').show(); }
                else { $('#comments').hide(); }
                GetUser(abp.session.userId)
                loadannouncement("", "", "", "");
            });
        }

        function GetUser($b) {
            _sharedRoleFileService.getUserRole({ id: $b }).done(function (result) {
                //$('#roleid').val(result.description)
                
            });
        }

        function GetTotalUpload($a) {
            $('#count').empty();
            _folderUsersService.getTotalUploadFile({ id: $a }).done(function (result) {
                $('#count').append(result.totalRows);
            });
        }

        function GetTotalComment($a) {
            $('#Commentcount').empty();
            _folderUserCommentService.getTotalCommentFile({ id: $a }).done(function (result) {
                $('#Commentcount').append(result.totalRows);
            });
        }

        function GetTotalShares($a) {
            $('#Sharedcount').empty();
            _sharedUserFileService.getTotalSharedFile({ id: $a }).done(function (result) {
                $('#Sharedcount').append(result.totalRows);
            });
        }

        function GetUsersRole($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
                GetTotalSharedRole(result.roleId)
            });
        }

        function GetTotalSharedRole($a) {
            $('#SharedRolecount').empty();
            _sharedRoleFileService.getTotalSharedRoleFile({ id: $a }).done(function (result) {
                $('#SharedRolecount').append(result.totalRows);
            });
        }

        function GetAllComments() {
            dataTable.ajax.reload();
        }

        var dataTable = _$commentsTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _folderUserCommentService.getCommentList,
                inputFilter: function () {
                    var $a = 'null';
                    var $b = 'null';
                    var $c = 'null';
                    var $d = abp.session.userId;
                    var $e = 'null';
                    var $f = 'null';
                    var $g = 'null';
                    var $h = 'null';
                    return {
                        filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f + '|' + $g + '|' + $h
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderable: true,
                    targets: 1,
                    data: { userImage: "userImage" },
                    render: function (data) {
                        var icons = '<img src="' + data.userImage + '" width=30px height=30px /> '
                        return icons;
                    }
                },
                {
                    orderable: true,
                    targets: 2,
                    data: "cat5"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 3,
                    data: { cat3: "cat3" },
                    render: function (data)
                    {
                        var icons = '<i class="' + data.cat3 +' fa-2x flat-color-6"></i> '
                        return icons;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 4,
                    data: "cat2"
                },
                {
                    orderable: false,
                    targets: 5,
                    class: "text-center",
                    data: { id: "id", folderId: "folderId", comments: "comments", cat2: "cat2", cat3: "cat3"},
                    "render": function (data) {
                        var $folderId = data.folderId;
                        var $checkicon = "";
                        if ($folderId == 0) {
                            $checkicon = '<a id="read-comment" title="Read comment" href="#" class="read-comment" data-comment-id="' + data.id + '" data-comment-comments="' + data.comments + '" data-comment-cat2="' + data.cat2 + '" data-comment-cat3="' + data.cat3 + '" data-toggle="modal" data-target="#ModalComment"><i class="fa fa-square-o fa-2x flat-color-8"></i></a>';
                        }
                        else {
                            $checkicon = '<a id="read-comment" title="Read comment" href="#" class="read-comment" data-comment-id="' + data.id + '" data-comment-comments="' + data.comments + '" data-comment-cat2="' + data.cat2 + '" data-comment-cat3="' + data.cat3 + '" data-toggle="modal" data-target="#ModalComment"><i class="fa fa-check-square fa-2x flat-color-6"></i></a>';
                        }
                        return $checkicon;
                    }
                }
            ]
        });

        $('#commentsTable').on('click', 'a.read-comment', function (e) {
            e.preventDefault();
            $("#commentfilename").empty();
            $("#comment").empty();
            $("#commentIcon").empty();
            var $id = $(this).attr("data-comment-id");
            var $cat3 = $(this).attr("data-comment-cat3");
            var $cat2 = $(this).attr("data-comment-cat2");
            var $comment = $(this).attr("data-comment-comments");
            var $icons = '<i class="' + $cat3 + ' fa-1x flat-color-6"></i> '
            $("#commentIcon").append($icons);
            $("#commentfilename").append($cat2);
            $("#comment").append($comment);

            _folderUserCommentService.updateCommentFile({ id: $id }).done(function (result) {
                GetAllComments();
                GetTotalComment(abp.session.userId);
            });
        });

        $('#btnaddannouncement').click(function (e) {
            e.preventDefault();
            Addannouncement();
        });

        function Addannouncement() {
            if (!_$announcementform.valid()) {
                return;
            }
            var items = _$announcementform.serializeFormToObject();
            abp.ui.setBusy(_$announcementform);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time; 0

            items.dateTime = today;
            items.Announce = $('#Announce').val();
            _announcementService.createAnnouncement(items).done(function () {
                abp.message.success('Announcement Created', 'Success');
                $('#Announce').val("");
                abp.ui.clearBusy(_$announcementform);
                loadannouncement("", "", "", "");
            });
        }

        function loadannouncement($a, $b, $c, $d) {
            $("#announcement").empty();
            _announcementService.getAnnouncementList({ filter: $a + '|' + $b + '|' + $c + '|' + $d }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {

                    var $id = result.items[i].id;
                    var $announce = result.items[i].announce;
                    var $dateTime = result.items[i].dateTime;
                    var $UserName = result.items[i].userName;
                    var $dateTime2 = $dateTime.replace(/T/g, " ");
                    abp.ui.setBusy(_$announcementform);

                    if ($('#roleid').val() == "1" || $('#roleid').val() == "3") {
                        $("#announcement").prepend('<div class="inner-box" style="background-color: white; padding: 10px; border-radius: 3px; margin: 10px 10px 10px 0px;"><div class="comment" style="font-size: 11px;text-align: right;color: darkgray;"><a href="javascript:void(0)" class="data-val" title=' + $id + ' data-val-id=' + $id + '><div class="parent-icon flat-color-6"><i class="fa fa-lg fa-times"></i></div></a></div><div class="name" style="color: blueviolet;">' + $UserName + '</div><div class="send-time" style="font-size: 11px;">' + $dateTime2 + '</div><div class="meg" style="padding-top: 10px;">' + $announce + '</div></div>');
                    }
                    else {
                        $("#announcement").prepend('<div class="inner-box" style="background-color: white; padding: 10px; border-radius: 3px; margin: 10px 10px 10px 0px;"><div class="comment" style="font-size: 11px;text-align: right;color: darkgray;"></div><div class="name" style="color: blueviolet;">' + $UserName + '</div><div class="send-time" style="font-size: 11px;">' + $dateTime2 + '</div><div class="meg" style="padding-top: 10px;">' + $announce + '</div></div>');

                    }
                    abp.ui.clearBusy(_$announcementform);
                }
                $('.data-val').on("click", function (e) {
                    var $id = $(this).attr("data-val-id");
                    $("#announcement").empty();
                    _announcementService.deleteAnnouncement({ id: $id }).done(function () { abp.message.success('Announcement Deleted', 'Success'); loadannouncement("", "", "", ""); })
                    
                })
            });
        }

        function Top5Download() {
            $("#downloadCount").empty();
            _notificationService.getTop5DownloadList().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $fileid = result.items[i].fileId;
                    var $downloadCount = result.items[i].totalRows;
                    var $filename = result.items[i].description;
                    var $cat1 = result.items[i].cat1;

                    $("#downloadCount").prepend('<div class="progress-box progress-1" style="margin-bottom:10px!important;"><div class="por-txt" style="color: red;">'  + $filename +" / " + $cat1 + '</div><div class="progress mb-2" style="height: 5px;"><div class="progress-bar bg-flat-color-1" role="progressbar" style="width: ' + $downloadCount + '%;" aria-valuenow="' + $downloadCount + '" aria-valuemin="0" aria-valuemax="100"></div></div></div>');

                }
            });
        }
    })
})(jQuery);