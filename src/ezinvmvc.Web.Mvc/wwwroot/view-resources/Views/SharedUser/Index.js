﻿

(function () {
    $(function () {
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _folderUsersService = abp.services.app.folderUsersService;
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _notificationService = abp.services.app.notificationService;

        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;

        var _$commentform = $('form[name=commentform]');
        var _$formfile = $('form[name=formfile]');   
        var _$Formshareduser = $('form[name=Formshareduser]');

        $(document).ready(function () {
            $('#usersid').val(abp.session.userId);
            //$("#Userids2").hide();
            //$("#Roles").hide();
            //$("#Roles2").hide();
            //$("#Inputpath").val('Folders/' + abp.session.userId + '/');
            GetRoles(abp.session.userId);
            $("#details").hide();
        });

        function GetRoles($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
                $('#roleId').val(result.roleId);
                loadfile("", abp.session.userId, "", "", "", "");
            });
        }

        function loadfile($a, $b, $c, $d, $e) {

            $("#row-list").empty();
            //var $a = 'null';
            //var $b = 'null';
            //var $c = abp.session.userId;
            //var $d = 'null';
            //var $e = 'null';
            //var $f = 'null';
            //var $g = $("#FolderId").val();
            _sharedUserFileService.getFolderUsersList({ filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $shareid = result.items[i].id;
                    var $id = result.items[i].fileId;
                    var $imageName = result.items[i].cat3;
                    //var $dateTime = result.items[i].dateTime;
                    var $cat1 = result.items[i].cat2;
                    var $cat2 = result.items[i].cat4;
                    //var $mb = result.items[i].field1;
                    //var $path = result.items[i].cat5;
                    var $description5 = result.items[i].description;

                    //--trim--//
                    var file_name = $imageName;
                    if (file_name.length > 10) {
                        file_name = file_name.substr(0, 7) + '..' + file_name.substr(-4);
                    }
                    //--end trim--//
                    abp.ui.setBusy(_$formfile);
                    $("#row-list").prepend('<a href="javascript:void(0)" class="data-val" title=' + $imageName + ' data-val-id=' + $id + '   data-val-cat1=' + $cat1 + ' data-val-shareId=' + $shareid + '><div class="parent-icon flat-color-6"><i class="fa ' + $cat2 + ' fa-2x "></i></div>' + file_name + '</a>');
                    abp.ui.clearBusy(_$formfile);
                }
                $('.data-val').on("click", function (e) {
                    $("#Logos3").hide();
                    $("#details").show();
                    var myValid = $(this).attr("data-val-id");
                    var mycat1 = $(this).attr("data-val-cat1");
                    var shareId = $(this).attr("data-val-shareId");

                    _folderUsersService.getDataFileById({ id: shareId }).done(function (result) {

                        var myValdate = result.dateTime;
                        var newDateFormat = new Date(myValdate).toLocaleDateString("en-US");
                        var newTimeFormat = new Date(myValdate).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;

                        var $extention = result.extention;

                        $('#fileId').val(myValid);
                        $('#filename').val(result.fileName);
                        $('#icon').val(result.icon);
                        $('#cat1').val(result.cat1);
                        $('#userroleid').val(result.cat2);
                        $('#date').val(dateAndTime);
                        $('#mb').val(result.mb);
                        $('#filepath').val(result.path);
                        $('#description').val(result.description);
                        if ($extention == "jpg") {
                            $("#img1").attr({ "src": result.path + result.fileName });
                        }
                        else {
                            $("img").append('<i class="fa ' + result.icon + ' fa-1x flat-color-6"></i> ');
                        }
                        

                        loadComment("", "", "", $('#fileId').val(), "", "");
                    })
                })
            });
        }

        $('#btnsearch').click(function (e) {
            e.preventDefault();
            loadfile("", abp.session.userId, "", $("#Inputpath").val(),"")
        });

        $("#btndownload").click(function (e) {
            e.preventDefault();
            var filepath = $('#filepath').val() + $('#filename').val();
            //$("<a download/>").attr("href", apiUrl).get(0).click();
            var link = document.createElement('a');
            link.href = filepath;
            link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
            link.click();
            Notified();
        });

        $('#btnaddcomment').click(function (e) {
            e.preventDefault();
            AddComment();
        });

        function AddComment() {
            if (!_$commentform.valid()) {
                return;
            }
            var items = _$commentform.serializeFormToObject();
            abp.ui.setBusy(_$commentform);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            items.userId = abp.session.userId;
            items.fileId = $('#fileId').val();
            items.dateTime = today;
            items.comments = $('#comments').val();
            items.folderId = 0;
            _folderUserCommentService.createFolderUserComment(items).done(function () {
                abp.message.success('Comment Created', 'Success');
                $('#comments').val("");
                abp.ui.clearBusy(_$commentform);
                loadComment("", "", "", $('#fileId').val(), "", "");
            });
        }

        function loadComment($a, $b, $c, $d, $e, $f) {

            $("#comment-list").empty();
            //var $a = 'null';
            //var $b = 'null';
            //var $c = abp.session.userId;
            //var $d = 'null';
            //var $e = 'null';
            //var $f = 'null';
            //var $g = $("#FolderId").val();
            _folderUserCommentService.getFolderUserComment({ filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $fileId = result.items[i].fileId;
                    var $userImage = result.items[i].userImage;
                    var $dateTime = result.items[i].dateTime;
                    var $comments = result.items[i].comments;
                    var $UserName = result.items[i].userName;
                    var $userId = result.items[i].userId;
                    var $folderId = result.items[i].folderId;
                    var $dateTime2 = $dateTime.replace(/T/g, " ");
                    abp.ui.setBusy(_$commentform);
                    if ($userId == $folderId) {
                        $("#comment-list").prepend('<div class="d-flex flex-row p-3"><img src="images/avatar/64-2.jpg" width="50" height="50" style="border-radius:50px"><div class="chat ml-2 p-3"> <span style="color:blue">' + $UserName + ' </span><span style="color:gray;font-size:9px">' + $dateTime2 + ' </span><br>' + $comments + '</div></div>');
                    }
                    else {
                        $("#comment-list").prepend('<div class="d-flex flex-row p-3"><div class="mr-2 p-2" style="width:100%; text-align:end;"><span style="color:red">' + $UserName + '</span> <span style="color:gray;font-size:9px">' + $dateTime2 + ' </span><br><span class="text-muted">' + $comments + '</span></div> <img src="images/avatar/1.jpg" width="50" height="50" style="border-radius:50px"></div>');
                        //$("#comment-list").prepend('<div class="d-flex flex-row p-3"><div class="bg-white mr-2 p-2"><span style="color:red">' + $UserName + '</span> <span style="color:gray;font-size:9px">' + $dateTime2 + ' </span><br><span class="text-muted">' + $comments + '</span></div> <img src="images/avatar/1.jpg" width="50" height="50" style="border-radius:50px"></div>');
                    }
                    abp.ui.clearBusy(_$commentform);
                }
            });
        }

        function Notified() {
            if (!_$Formshareduser.valid()) {
                return;
            }
            var items2 = _$Formshareduser.serializeFormToObject();
            abp.ui.setBusy(_$Formshareduser);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            items2.fileId = $('#fileId').val();
            items2.dateTime = today;
            items2.userId = $('#usersid').val();
            items2.roleId = $('#roleId').val();
            items2.field1 = 1;

            _notificationService.createNotificationUser(items2).done(function () {
                GetCountShareUser($('#usersid').val())
                abp.ui.clearBusy(_$Formshareduser);
            });
        }

        function GetCountShareUser($b) {
            _notificationService.getNotificationCountUser({ id: $b }).done(function (result) {
                var countshared = 0;
                countshared = result.field1;
                //resultcount.empty().append();
                $("#red").empty();
                $("#resultcount").append('<span id="resultcount" class="count bg-danger">' + countshared + '</span>');
                $("#red").prepend('<p class="red" style="color: white; font-size: smaller; width: max-content;">You have ' + countshared + ' Notification</p>');
            });
        }

    })    

})(jQuery);