﻿(function () {
    $(function () {
        var _folderUserCommentService = abp.services.app.folderUserCommentService;
        var _$commentform = $('form[name=commentform]');

        $(document).ready(function () {
            loadComment("", "", abp.session.userId, $('#fileId').val(), "", "");
        });

        $('#btnaddcomment').click(function (e) {
            e.preventDefault();
            AddComment();
        });

        function AddComment() {
            if (!_$commentform.valid()) {
                return;
            }
            var items = _$commentform.serializeFormToObject();
            abp.ui.setBusy(_$commentform);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            items.userId = abp.session.userId;
            items.fileId = $('#fileId').val();
            items.dateTime = today;
            items.comments = $('#comments').val();

            _folderUserCommentService.createFolderUserComment(items).done(function () {
                abp.message.success('Comment Created', 'Success');
                abp.ui.clearBusy(_$commentform);
                loadComment("", "", abp.session.userId, $('#fileId').val(), "","");
            });
        }

        function loadComment($a, $b, $c, $d, $e, $f) {

            $("#comment-list").empty();
            //var $a = 'null';
            //var $b = 'null';
            //var $c = abp.session.userId;
            //var $d = 'null';
            //var $e = 'null';
            //var $f = 'null';
            //var $g = $("#FolderId").val();
            _folderUserCommentService.getFolderUserComment({ filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $fileId = result.items[i].fileId;
                    var $userImage = result.items[i].userImage;
                    var $dateTime = result.items[i].dateTime;
                    var $comments = result.items[i].comments;
                    var $UserName = result.items[i].UserName;
                    
                    abp.ui.setBusy(_$commentform);
                    $("#comment-list").prepend('<div class="d-flex flex-row p-3"><img src="https://img.icons8.com/color/48/000000/circled-user-female-skin-type-7.png" width="60" height="60" style="padding-top:10px"><div class="chat ml-2 p-3"><span style="color:blue">' + $UserName + ' </span><br>' + $comments + '</div></div>');
                    abp.ui.clearBusy(_$commentform);
                }
                $('.teacher-link').on("click", function (e) {
                    $("#Logos3").hide();
                    $("#details").show();
                    var myValid = $(this).attr("data-val-id");
                    var myValpath = $(this).attr("data-val-path");
                    var mycat1 = $(this).attr("data-val-cat1");

                    _folderUsersService.getDataById({ id: myValid }).done(function (result) {

                        var myValdate = result.dateTime;
                        var newDateFormat = new Date(myValdate).toLocaleDateString("en-US");
                        var newTimeFormat = new Date(myValdate).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;

                        $('#fileId').val(result.id);
                        $('#filename').val(result.fileName);
                        $('#icon').val(result.icon);
                        $('#cat1').val(mycat1);
                        $('#date').val(dateAndTime);
                        $('#mb').val(result.mb);
                        $('#filepath').val(result.path);
                        $('#description').val(result.description);
                        $("#img1").attr({ "src": result.path + result.fileName });
                    })
                })
            });
        }
    })
})(jQuery);