﻿
$(".date-picker").datepicker();
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

(function () {
    $(function () {
        var _folderUsersService = abp.services.app.folderUsersService;
        var _sharedUserFileService = abp.services.app.sharedUserFileService;
        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
        var _announcementService = abp.services.app.announcementService;
        var _notificationService = abp.services.app.notificationService;
        var _$shareTable = $('#shareTable');
        var _$notificationform2 = $('form[name=notiform2]');

        $(document).ready(function () {
            $('#InputUser').hide();
            $('#comments').show();
            $('#delete').hide();
            $('#usersid').val(abp.session.userId);
            GetRoles(abp.session.userId);
            Getdata();

        });

        function GetRoles($a) {
            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
                $('#roleId').val(result.roleId);
            });
        }

        $("#SearchOption").change(function () {
            var selectedOption = $("#SearchOption").val()
            if (selectedOption == 0) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            }
            if (selectedOption == 1) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            }
            if (selectedOption == 2) {
                $('#InputUser').show();
                $('#Inputpath').hide();
            }
            if (selectedOption == 3) {
                $('#InputUser').hide();
                $('#Inputpath').show();
            }
            if (selectedOption == 4) {
                $('#InputUser').show();
                $('#Inputpath').show();
            }
        });

        $('#btnSearch').click(function (e) {
            e.preventDefault();

            if ($('#SearchOption').val() == 0) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                $('#InputUser').val("");
                $('#Inputpath').val("");
                Getdata();
            }
            if ($('#SearchOption').val() == 1) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                $('#InputUser').val("");
                $('#Inputpath').val();
                Getdata();
            }
            if ($('#SearchOption').val() == 2) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val("");
                $('#dateto').val("");
                $('#InputUser').val();
                $('#Inputpath').val("");
                Getdata();
            }
            if ($('#SearchOption').val() == 3) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val();
                $('#dateto').val();
                $('#InputUser').val("");
                $('#Inputpath').val("");
                Getdata();
            }
            if ($('#SearchOption').val() == 4) {
                $('#userid').val(abp.session.userId);
                $('#datefrom').val();
                $('#dateto').val();
                $('#InputUser').val();
                $('#Inputpath').val();
                Getdata();
            }

        })

        function Getdata() {
            dataTable.ajax.reload();
        }

        var dataTable = _$shareTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            searching: false,
            "bInfo": false,
            listAction: {
                ajaxFunction: _sharedUserFileService.getShareFileList,
                inputFilter: function () {
                    var $a = $("#datefrom").val();
                    var $b = $("#dateto").val();
                    var $c = 'null';
                    var $d = abp.session.userId;
                    var $e = 'null';
                    var $f = $('#Inputpath').val();
                    var $g = $('#InputUser').val();  
                    return {
                        filter: $a + '|' + $b + '|' + $c + '|' + $d + '|' + $e + '|' + $f + '|' + $g
                    };
                }
            },
            columnDefs: [
                {
                    //className: 'control responsive',
                    orderable: false,
                    visible: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderable: false,
                    targets: 1,
                    data: "cat1"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 2,
                    data: { cat4: "cat4" },
                    render: function (data) {
                        var icons = '<i class="fa ' + data.cat4 + ' fa-2x flat-color-6"></i> '
                        return icons;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 3,
                    data: "cat3"
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 4,
                    data: "dateTime",
                    "render": function (data) {

                        var options = {
                            //weekday: "short",
                            year: "numeric",
                            month: "short",
                            day: "numeric"
                        };

                        var newDateFormat = new Date(data).toLocaleDateString("en-US", options);
                        var newTimeFormat = new Date(data).toLocaleTimeString();
                        var dateAndTime = newDateFormat + ' ' + newTimeFormat;

                        return dateAndTime;
                    }
                },
                {
                    visible: true,
                    orderable: false,
                    targets: 5,
                    data: "cat5"
                },
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { fileId: "fileId"},
                    "render": function (data) {
                        var $Fileid = data.fileId;
                        $dlicon = '<a id="data-download" title="Download File" href="#" class="data-download btn btn-outline-primary btn-sm flat-color-5" data-download-id=' + $Fileid + '><i class="fa fa-download flat-color-6"></i></a>';

                        return $dlicon;
                    }
                }
            ]
        });

        $('#shareTable').on('click', 'a.data-download', function (e) {
            e.preventDefault();
            var $Id = $(this).attr("data-download-id");
            $('#fid').val($Id);
            _folderUsersService.getDataById({ id: $Id }).done(function (result) {
                var filepath = '../' + result.path + result.fileName;
                //$("<a download/>").attr("href", apiUrl).get(0).click();
                var link = document.createElement('a');
                link.href = filepath;
                link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
                link.click();
            })
            Notified();

        });

        function Notified() {
            if (!_$notificationform2.valid()) {
                return;
            }
            var items2 = _$notificationform2.serializeFormToObject();
            abp.ui.setBusy(_$notificationform2);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var time = today.getHours() + ":" + today.getMinutes();
            if (dd < 10) { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = yyyy + '-' + mm + '-' + dd + ' ' + time;

            items2.fileId = $('#fid').val();
            items2.dateTime = today;
            items2.userId = $('#usersid').val();
            items2.roleId = $('#roleId').val();
            items2.field1 = 1;

            _notificationService.createNotificationUser(items2).done(function () {
                GetCountShareUser($('#usersid').val())
                abp.ui.clearBusy(_$notificationform2);
            });
        }

        function GetCountShareUser($b) {
            _notificationService.getNotificationCountUser({ id: $b }).done(function (result) {
                var countshared = 0;
                countshared = result.field1;
                //resultcount.empty().append();
                $("#red").empty();
                $("#resultcount").append('<span id="resultcount" class="count bg-danger">' + countshared + '</span>');
                $("#red").prepend('<p class="red" style="color: white; font-size: smaller; width: max-content;">You have ' + countshared + ' Notification</p>');
            });
        }

    })
})(jQuery);