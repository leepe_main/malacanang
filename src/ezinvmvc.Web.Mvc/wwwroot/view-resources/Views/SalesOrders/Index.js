﻿$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
var $month = (new Date().getMonth() + 1);
var mdayone = ($month.toString().length > 1 ? $month : "0" + $month) + "/01/" + new Date().getFullYear();
$('#DateFrom').val(mdayone);

$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

$('select').selectpicker();

(function () {
    $(function () {

        var _$salesOrdersTable = $('#SalesOrdersTable');
        var _salesOrderService = abp.services.app.salesOrderService;

        var _permissions = {
            create: abp.auth.hasPermission('Page.Sales.Orders.Create'),
            edit: abp.auth.hasPermission('Page.Sales.Orders.Edit'),
            'delete': abp.auth.hasPermission('Page.Sales.Orders.Delete')
        };

        var dataTable = _$salesOrdersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _salesOrderService.getSalesOrders,
                inputFilter: function () {
                    var $id = $('#SearchFilter').val();
                    var $client = 'null';
                    var $statusid = $('#StatusTypes').val();
                    var $datefrom = $('#DateFrom').val();
                    var $dateto = $('#DateTo').val();
                    if ($id === '') {
                        $id = 'null';
                    }
                    return {
                        filter: $id + '|' + $client + '|' + $statusid + '|' + $datefrom + '|' + $dateto
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    targets: 1,
                    data: "code"
                },
                {
                    targets: 2,
                    data: "client"
                }
                ,
                {
                    targets: 3,
                    "data": "transactionTime",
                    "render": function (data) {
                        var tt = new Date(data);
                        return getFormattedDate(tt);
                    }
                },
                {
                    targets: 4,
                    "data": "deliveryTime",
                    "render": function (data) {
                        var dt = new Date(data);
                        return getFormattedDate(dt);
                    }
                },
                {
                    targets: 5,
                    data: { status: "status", statusId: "statusId" },
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-primary">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                },
                {
                    targets: 6,
                    data: "grandTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    orderable: false,
                    targets: 7,
                    class: "text-center",
                    data: { id: "id", code: "code" },
                    "render": function (data) {
                        return '<a id="edit-order" title="edit" href="#" class="edit-order" data-order-id="' + data.id + '"><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-order" title="delete" href="#" class="delete-order" data-order-id="' + data.id + '" data-order-code="' + data.code + '"><i class="fa fa-trash"></i></a>';
                    }
                }
            ]
        });

        // Edit record
        _$salesOrdersTable.on('click', 'a.edit-order', function (e) {
            e.preventDefault();
            var orderId = $(this).attr("data-order-id");
            window.location.href = abp.appPath + 'SalesOrders/Edit?id=' + orderId;
        });

        // Delete record
        $('#ProductsTable').on('click', 'a.delete-product', function (e) {
            var productId = $(this).attr("data-product-id");
            var productName = $(this).attr("data-product-name");
            var productCode = $(this).attr("data-product-code");

            e.preventDefault();
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('DeleteProductConfirmation', 'ezinvmvc'), productName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _productService.deleteProduct({
                            id: productId
                        }).done(function () {

                            $.ajax({
                                url: abp.appPath + 'Products/RemoveFile?code=' + productCode,
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                success: function () { },
                                error: function (e) { }
                            });

                            getSalesOrders();
                        });
                    }
                }
            );
        });

        function getSalesOrders() {
            dataTable.ajax.reload();
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#ExportCompanyToExcelButton').click(function () {
            _companyService
                .getProductsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });



        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getSalesOrders();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getSalesOrders();
        });

        abp.event.on('app.createOrEditProductModalSaved', function () {
            getSalesOrders();
        });

        $('#ProductTableFilter').focus();

        $("#lbBrands").change(function () {
            getSalesOrders();
        });

        $("#lbCategories").change(function () {
            getSalesOrders();
        });

    });
})();
