﻿// Vendors
(function ($) {

    var _$table = $('#VendorsTable');
    var _service = abp.services.app.vendorService;
    var _$modal = $('#VendorCreateModal');
    var _$form = _$modal.find('form');

    var _permissions = {
        create: abp.auth.hasPermission('Master.Vendors.Create'),
        edit: abp.auth.hasPermission('Master.Vendors.Edit'),
        'delete': abp.auth.hasPermission('Master.Vendors.Delete')
    };

    var dataTable = _$table.DataTable({
        paging: true,
        serverSide: true,
        processing: true,
        searching: false,
        listAction: {
            ajaxFunction: _service.getVendors,
            inputFilter: function () {
                var $s = $('#SearchFilter').val();
                return {
                    filter: $s
                };
            }
        },
        columnDefs: [
            {
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                targets: 1,
                data: "name"
            },
            {
                targets: 2,
                data: "email"
            }
            ,
            {
                targets: 3,
                data: "phone"
            },
            {
                targets: 4,
                data: "city"
            },
            {
                targets: 5,
                data: "country"
            },
            {
                orderable: false,
                targets: 6,
                class: "text-center",
                data: { id: "id", name: "name" },
                "render": function (data) {
                    return '<a id="edit-vendor" title="edit" href="#" class="edit-vendor" data-vendor-id="' + data.id + '" data-toggle="modal" data-target="#VendorEditModal" ><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-vendor" title="delete" href="#" class="delete-vendor" data-vendor-id="' + data.id + '" data-vendor-name="' + data.name + '"><i class="fa fa-trash"></i></a>';
                }
            }
        ]
    });

    function getAll() {
        dataTable.ajax.reload();
    }

    $('#SearchButton').click(function (e) {
        e.preventDefault();
        getAll();
    });

    $('#SearchFilter').on('keydown', function (e) {
        if (e.keyCode !== 13) {
            return;
        }
        e.preventDefault();
        getAll();
    });

    $('#SearchFilter').focus();

    // Save record
    function save() {
        if (!_$form.valid()) {
            return;
        }
        var vendor = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        abp.ui.setBusy(_$modal);
        _service.createVendor(vendor).done(function () {
            _$form.trigger("reset");
            _$modal.modal('hide');
            getAll();
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    _$form.find('button[type="submit"]').click(function (e) {
        e.preventDefault();
        save();
    });

    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    _$form.find('select').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });
    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });

    // Edit record
    _$table.on('click', 'a.edit-vendor', function (e) {
        var id = $(this).attr("data-vendor-id");
        alert("a");
        e.preventDefault();
        $.ajax({
            url: abp.appPath + 'Vendors/EditModal?id=' + id,
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#VendorEditModal div.modal-content').html(content);
                $('select').selectpicker();
            },
            error: function (e) { }
        });
    });

    // Delete record
    _$table.on('click', 'a.delete-vendor', function (e) {
        var id = $(this).attr("data-vendor-id");
        var name = $(this).attr("data-vendor-name");

        e.preventDefault();
        abp.message.confirm(
            abp.utils.formatString(abp.localization.localize('DeleteVendorConfirmation', 'ezinvmvc'), name),
            function (isConfirmed) {
                if (isConfirmed) {
                    _service.deleteVendor({
                        id: id
                    }).done(function () {
                        getAll();
                    });
                }
            }
        );
    });

    $('#ExportToExcelButton').click(function (e) {
        e.preventDefault();

        _service.getVendorsToExcel({})
            .done(function (result) {
                app.downloadTempFile(result);
            });
    });

    $('#ExportButton').click(function () {
        _service
            .getVendorsToExcel({})
            .done(function (result) {
                app.downloadTempFile(result);
            });
    });
}) (jQuery);

