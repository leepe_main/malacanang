﻿////Skin changer
//function skinChanger() {
//    $('.right-sidebar .demo-choose-skin li').on('click', function () {
//        var currentTheme = $('.right-sidebar .demo-choose-skin li.active').data('theme');
//        $('.right-sidebar .demo-choose-skin li').removeClass('active');

//        var $selected = $(this);
//        $selected.addClass('active');
//        var selectedTheme = $selected.data('theme');

//        $('body')
//            .removeClass('theme-' + currentTheme)
//            .addClass('theme-' + selectedTheme);

//        //Change theme settings on the server
//        abp.services.app.configuration.changeUiTheme({
//            theme: selectedTheme
//        });
//    });
//}

////Skin tab content set height and show scroll
//function setSkinListHeightAndScroll() {
//    var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
//    var $el = $('.demo-choose-skin');

//    $el.slimScroll({ destroy: true }).height('auto');
//    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();

//    $el.slimscroll({
//        height: height + 'px',
//        color: 'rgba(0,0,0,0.5)',
//        size: '4px',
//        alwaysVisible: false,
//        borderRadius: '0',
//        railBorderRadius: '0'
//    });
//}

////Setting tab content set height and show scroll
//function setSettingListHeightAndScroll() {
//    var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
//    var $el = $('.right-sidebar .demo-settings');

//    $el.slimScroll({ destroy: true }).height('auto');
//    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();

//    $el.slimscroll({
//        height: height + 'px',
//        color: 'rgba(0,0,0,0.5)',
//        size: '4px',
//        alwaysVisible: false,
//        borderRadius: '0',
//        railBorderRadius: '0'
//    });
//}

////Activate notification and task dropdown on top right menu
//function activateNotificationAndTasksScroll() {
//    $('.navbar-right .dropdown-menu .body .menu').slimscroll({
//        height: '254px',
//        color: 'rgba(0,0,0,0.5)',
//        size: '4px',
//        alwaysVisible: false,
//        borderRadius: '0',
//        railBorderRadius: '0'
//    });
//}

//(function ($) {

//    //Initialize BSB admin features
//    $(function () {
//        skinChanger();
//        activateNotificationAndTasksScroll();

//        setSkinListHeightAndScroll();
//        setSettingListHeightAndScroll();
//        $(window).resize(function () {
//            setSkinListHeightAndScroll();
//            setSettingListHeightAndScroll();
//        });
//    });

//})(jQuery);



//(function ($) {

//    //Initialize BSB admin features
//    $(function () {
//        var _sharedRoleFileService = abp.services.app.sharedRoleFileService;

//        $(document).ready(function () {
//            GetRoles(abp.session.userId);
//        });
//        function GetRoles($a) {
//            _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
//                GetUser(abp.session.userId)
//            });
//        }

//        function GetUser($b) {
//            _sharedRoleFileService.getUser({ id: $b }).done(function (result) {
//                $('#UserName').val(result.description)

//            });
//        }
//    });

//})(jQuery);
$(function () {

    var _sharedRoleFileService = abp.services.app.sharedRoleFileService;
    var _notificationService = abp.services.app.notificationService;
    var _folderUsersService = abp.services.app.folderUsersService;

    var _$notificationform = $('form[name=notiform]');

    $(document).ready(function () {
        GetUser(abp.session.userId);
        GetCountShareUser(abp.session.userId);
        GetListCountShareUser(abp.session.userId);
        GetUsersRole(abp.session.userId);
        $('#Uid').val(abp.session.userId);
    });
    function GetUsersRole($a) {
        _sharedRoleFileService.getUserRole({ id: $a }).done(function (result) {
            $('#Rid').val(result.roleId);
            GetCountShareRole(result.roleId)
            GetListCountShareRole(result.roleId)
        });
    }

    function GetUser($b) {
        _sharedRoleFileService.getUser({ id: $b }).done(function (result) {
            $('#Name').val(result.description);
            if (result.cat5 == null || result.cat5 == "") {
                $("#profileimage").attr({ "src": "images/avatar/user.png" });
            }
            else {
                $("#profileimage").attr({ "src": result.cat5 });
            }
        });
    }
    
    function GetCountShareUser($b) {
        _notificationService.getNotificationCountUser({ id: $b }).done(function (result) {
            var countshared = 0;
            countshared = result.field1;
            //resultcount.empty().append();
            $("#resultcount").append('<span id="resultcount" class="count bg-danger">' + countshared + '</span>');
            $("#red").prepend('<p class="red" style="color: white; font-size: smaller; width: max-content;">You have ' + countshared + ' Notification</p>');
        });
    }
    function GetListCountShareUser($b) {
        _notificationService.getNotificationCountList({ filter:  $b }).done(function (result) {
            for (var i = 0; i < result.items.length; i++) {

                //var $listcount = "";
                $Iconlistcount = result.items[i].cat3;
                $User = result.items[i].cat5;
                $date = result.items[i].dateTime;
                $FileName = result.items[i].cat2;

                $fileId2 = result.items[i].fileId;
                $userId = result.items[i].userId;
                $RoleId = result.items[i].roleId;
                $("#listcount2").prepend('<a href="javascript:void(0)" class="dropdown-item media teacher-link2" title=' + $FileName + ' data-val-id=' + $fileId2 + ' data-val-name=' + $FileName + ' data-val-userId=' + $userId + ' data-val-RoleId=' + $RoleId + '><i class="fa ' + $Iconlistcount + ' fa-2x "></i><p>' + $FileName + '</p></a >');
            }
            $('.teacher-link2').on("click", function (e) {
                $('#fid').val("");
                $('#Name2').val("");
                //$('#Uid').val("");
                $('#Rid').val("");
                $('#r1c').val(1);
                var $fId = $(this).attr("data-val-id");
                var $userId = $('#Uid').val();
                var $RoleId = $(this).attr("data-val-RoleId");
                var $name = $(this).attr("data-val-name");
                $('#fid').val($fId);
                $('#Name2').val($name);
                $('#Uid').val($userId);
                $('#Rid').val($RoleId);
                $('#r1c').val(1);
                Notified();
                _folderUsersService.getDataById({ id: $fId }).done(function (result) {
                    var filepath = '../' + result.path + result.fileName;
                    var link = document.createElement('a');
                    link.href = filepath;
                    link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
                    link.click();
                })

                

                

            })
        });
    }
    function Notified() {
        if (!_$notificationform.valid()) {
            return;
        }
        var items = _$notificationform.serializeFormToObject();
        abp.ui.setBusy(_$notificationform);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var time = today.getHours() + ":" + today.getMinutes();
        if (dd < 10) { dd = '0' + dd }
        if (mm < 10) { mm = '0' + mm }
        today = yyyy + '-' + mm + '-' + dd + ' ' + time;

        items.fileId = $('#fid').val();
        items.dateTime = today;
        items.userId = $('#Uid').val();
        items.roleId = $('#Rid').val();
        items.field1 = $('#r1c').val();

        _notificationService.createNotificationUser(items).done(function () {
            $('#fid').val("");
            $('#Name2').val("");
            //$('#Uid').val("");
            $('#Rid').val("");
            $("#listcount2").empty();
            $("#red").empty();
            $("#red2").empty();
            $("#resultcount").empty();
            $("#resultcount2").empty();
            $("#listcount3").empty();
            $('#r1c').val(0);
            GetCountShareUser(abp.session.userId);
            GetListCountShareUser(abp.session.userId);
            GetUsersRole(abp.session.userId)
            //Top5Download();
            abp.ui.clearBusy(_$notificationform);
        });
    }
    
    function GetCountShareRole($b) {
        _notificationService.getNotificationCountRole({ id: $b }).done(function (result) {
            var countshared2 = 0;
            countshared2 = result.field1;
            //resultcount.empty().append();
            $("#resultcount2").append('<span id="resultcount2" class="count bg-danger">' + countshared2 + '</span>');
            $("#red2").prepend('<p class="red" style="color: white; font-size: smaller; width: max-content;">You have ' + countshared2 + ' Notification</p>');
        });
    }

    function GetListCountShareRole($b) {
        _notificationService.getNotificationCountRoleList({ filter: $b }).done(function (result) {
            for (var i = 0; i < result.items.length; i++) {

                //var $listcount = "";
                $Iconlistcount = result.items[i].cat3;
                $User = result.items[i].cat5;
                $date = result.items[i].dateTime;
                $FileName = result.items[i].cat2;

                $fileId2 = result.items[i].fileId;
                $userId = result.items[i].userId;
                $RoleId = result.items[i].roleId;
                $("#listcount3").prepend('<a href="javascript:void(0)" class="dropdown-item media teacher-link3" title=' + $FileName + ' data-val-id=' + $fileId2 + ' data-val-name=' + $FileName + ' data-val-userId=' + $userId + ' data-val-RoleId=' + $RoleId + '><i class="fa ' + $Iconlistcount + ' fa-2x "></i><p>' + $FileName + '</p></a >');
            }
            $('.teacher-link3').on("click", function (e) {
                $('#fid').val("");
                $('#Name2').val("");
                //$('#Uid').val();
                $('#Rid').val("");
                $('#r1c').val(2);
                var $fId = $(this).attr("data-val-id");
                var $userId = $('#Uid').val();
                var $RoleId = $(this).attr("data-val-RoleId");
                var $name = $(this).attr("data-val-name");
                $('#fid').val($fId);
                $('#Name2').val($name);
                $('#Uid').val($userId);
                $('#Rid').val($RoleId);
                $('#r1c').val(2);

                Notified();
                _folderUsersService.getDataById({ id: $fId }).done(function (result) {
                    var filepath = '../' + result.path + result.fileName;
                    var link = document.createElement('a');
                    link.href = filepath;
                    link.download = filepath.substr(filepath.lastIndexOf('/') + 1);
                    link.click();
                })
            })
        });
    }

    function Top5Download() {
        $("#downloadCount").empty;
        _notificationService.getTop5DownloadList().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                var $fileid = result.items[i].fileId;
                var $downloadCount = result.items[i].totalRows;
                var $filename = result.items[i].description;
                var $cat1 = result.items[i].cat1;

                $("#downloadCount").prepend('<div class="progress-box progress-1" style="margin-bottom:10px!important;"><h4 class="por-title" style="color: red;">' + $filename + '</h4><div class="por-txt">' + $cat1 + '</div><div class="progress mb-2" style="height: 5px;"><div class="progress-bar bg-flat-color-1" role="progressbar" style="width: ' + $downloadCount + '%;" aria-valuenow="' + $downloadCount + '" aria-valuemin="0" aria-valuemax="100"></div></div></div>');
            }
        });
    }
})