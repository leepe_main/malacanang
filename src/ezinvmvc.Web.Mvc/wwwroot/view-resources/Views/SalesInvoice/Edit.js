﻿function decimalOnly(txt) {
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode === 46) {
        var txtbx = document.getElementById(txt);
        var amount = document.getElementById(txt).value;
        var present = 0;
        var count = 0;

        do {
            present = amount.indexOf(".", present);
            if (present !== -1) {
                count++;
                present++;
            }
        }
        while (present !== -1);
        if (present === -1 && amount.length === 0 && event.keyCode === 46) {
            event.keyCode = 0;
            return false;
        }

        if (count >= 1 && event.keyCode === 46) {

            event.keyCode = 0;
            return false;
        }
        if (count === 1) {
            var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
            if (lastdigits.length >= 2) {
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    else {
        event.keyCode = 0;
        return false;
    }
}

$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

(function () {
    $(function () {

        var _pricingTypeService = abp.services.app.pricingTypeService;
        var _productPriceService = abp.services.app.productPriceService;
        var _productService = abp.services.app.productService;
        var _companyService = abp.services.app.companyService;
        var _commonService = abp.services.app.commonService;
        var _clientService = abp.services.app.clientService;
        var _salesOrderService = abp.services.app.salesOrderService;
        var _contactPersonService = abp.services.app.contactPersonService;
        var _salesInvoiceService = abp.services.app.salesInvoiceService;

        var _$form = $('form[name=SalesOrderForm]');
        var _$chargesTable = $('#ChargesTable');
        var _$itemsTable = $('#ItemsTable');
        var _$ledgerTable = $('#LedgerTable');

        getinvoice();
        function getinvoice() {
            $("#SubmitButton").attr("hidden", true);
            abp.ui.setBusy(_$form);
            var $id = $('#Id').val();
            _salesInvoiceService.getSalesInvoice({ id: $id }).done(function (result) {
                $('#Code').val(result.code);
                $('#Prefix').val(result.prefix);
                $('#SeriesTypeId').val(result.seriesTypeId);
                $('#Companies').val(result.companyId);
                $('#ClientId').val(result.clientId);
                $('#ClientName').val(result.client);
                $('#SalesAgentId').val(result.salesAgentId);
                $('#SalesAgent').val(result.salesAgent);
                $('#SalesAgentId').val(result.salesAgentId);
                $('#ClientOrderNo').val(result.clientOrderNo);
                var sonettotal = currencyFormat(result.netTotal);
                var sootherdiscount = currencyFormat(result.otherDiscount);
                var soothercharges = currencyFormat(result.otherCharges);
                var sosubtotal = currencyFormat(result.subTotal);
                var sotax = currencyFormat(result.tax);
                var sograndtotal = currencyFormat(result.grandTotal);
                $('#DiscountTotal').val(sootherdiscount);
                $('#NetTotal').val(sonettotal);
                $('#Tax').val(sotax);
                $('#Total').val(sosubtotal);
                $('#ChargesTotal').val(soothercharges);
                $('#GrandTotal').val(sograndtotal);
                $('#StatusBadge').text(result.status);
                $('#CashAccountId').text(result.cashAccountId);
                $('#TaxAccountId').text(result.taxAccountId);
                $('#ReceivableAccountId').text(result.receivableAccountId);

                switch (result.statusId) {
                    case 1:
                        $('#StatusBadge').addClass('badge badge-secondary');
                        if ($('#SaveButton').length) {
                            $('#SaveButton').removeAttr('hidden');
                        }
                        if ($('#SubmitButton').length) {
                            $('#SubmitButton').removeAttr('hidden');
                        }
                        break;
                    case 2:
                        $('#StatusBadge').addClass('badge badge-success');
                        if ($('#ActionButton').length) {
                            $('#ActionButton').removeAttr('hidden');
                        }
                        break;
                    case 3:
                        $('#StatusBadge').addClass('badge badge-danger');
                        if ($('#SubmitButton').length) {
                            $('#SubmitButton').removeAttr('hidden');
                        }
                        break;
                    case 4:
                        $('#StatusBadge').addClass('badge badge-primary');
                        break;
                    case 5:
                        $('#StatusBadge').addClass('badge badge-info');
                        break;
                    case 6:
                        $('#StatusBadge').addClass('badge badge-warning');
                        break;
                    default:
                        $('#StatusBadge').addClass('badge badge-secondary');
                }

                getsalesorder(result.salesOrderId);
                getqcompanies(result.companyId);
                getqordertype(result.orderTypeId);
                getqtaxtype(result.taxTypeId);
                getqpricingtype(result.pricingTypeId);
                getqdeliverytype(result.deliveryTypeId);
                getqpaymentterm(result.paymentTermId);
                getqwarrantytype(result.warrantyTypeId);
                getclient();
                dataTable.clear().draw();
                getinvoiceitems($id);
                getinvoicecharges($id);
                $('#PricingTypes').prop('disabled', 'disabled');
                $('#OrderTypes').prop('disabled', 'disabled');
                $('#Companies').prop('disabled', 'disabled');
                $('#Series').prop('disabled', 'disabled');
            });
        };
        function getinvoiceitems(id) {
            _salesInvoiceService.getSalesInvoiceItemsByParentId({ id: id }).done(function (result) {

                for (var i = 0; i < result.items.length; i++) {
                    var $sqiid = result.items[i].id;
                    var $sqiproductid = result.items[i].productId;
                    var $sqiproductcode = result.items[i].productCode;
                    var $sqiproductname = result.items[i].productName;
                    var $sqiproductdescription = result.items[i].productDescription;
                    var $sqiunitid = result.items[i].unitId;
                    var $sqiunit = result.items[i].unit;
                    var $sqiquantity = result.items[i].orderQty;
                    var $sqiprice = result.items[i].unitPrice;
                    var $sqiimagename = result.items[i].imageName;

                    var $sqidisc1 = result.items[i].disc1;
                    var $sqidisc2 = result.items[i].disc2;
                    var $sqidisc3 = result.items[i].disc3;
                    var $sqidtype1 = result.items[i].discType1;
                    var $sqidtype2 = result.items[i].discType2;
                    var $sqidtype3 = result.items[i].discType3;
                    var $sqiperdescription = result.items[i].description;
                    var $sqidisctotal = result.items[i].discTotal;
                    var $sqitotal = result.items[i].total;
                    var sqiprice = parseFloat($sqiprice);
                    var sqiquantity = parseFloat($sqiquantity);
                    var sqitotaldiscount = parseFloat($sqidisctotal);
                    var sqitotal = parseFloat($sqitotal);

                    var sqidisc1 = 0;
                    var sqidisc2 = 0;
                    var sqidisc3 = 0;
                    if ($sqidisc1 !== "") {
                        sqidisc1 = parseFloat($sqidisc1);
                    }
                    if ($sqidisc2 !== "") {
                        sqidisc2 = parseFloat($sqidisc2);
                    }
                    if ($sqidisc3 !== "") {
                        sqidisc3 = parseFloat($sqidisc3);
                    }
                    var $expenseid = result.items[i].expenseAccountId;
                    var $inventoryid = result.items[i].inventoryAccountId;
                    var $incomeid = result.items[i].incomeAccountId;

                    var sqidatacount = dataTable.rows().count();
                    var sqiitemno = sqidatacount + 1;

                    dataTable.row.add([sqiitemno,
                        '<a href="#" class="btn-link">' + $sqiproductcode + '</a><br /><small><label class="text-muted">' + $sqiproductname + '</label></small>',
                        '<label class="text-muted">' + $sqiquantity + '</label>|<label class="text-muted">' + $sqiunit + '</label>',
                        sqiprice,
                        sqitotaldiscount,
                        sqitotal,
                        '',
                        $sqiproductid, $sqiperdescription, $sqiquantity, $sqiunitid, sqidisc1, parseInt($sqidtype1), sqidisc2, parseInt($sqidtype2), sqidisc3, parseInt($sqidtype3), $expenseid, $inventoryid, $incomeid, $sqiid
                    ]).draw();
                }
            });
        }
        function getinvoicecharges(id) {
            _salesInvoiceService.getSalesInvoiceChargesByParentId({ id: id }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var $sqccid = result.items[i].id;
                    var $sqcchargetypeid = result.items[i].chargeTypeId;
                    var $sqcchargetype = result.items[i].chargeType;
                    var $sqcrate = result.items[i].rate;
                    var $sqcamount = result.items[i].amount;
                    var $sqctotal = result.items[i].total;
                    var $sqcrevenueaccountid = result.items[i].revenueAccountId;
                    var sqcdatacount = dataTableCharges.rows().count();
                    var sqcitemno = sqcdatacount + 1;
                    dataTableCharges.row.add([sqcitemno,
                        $sqcchargetype,
                        $sqcrate,
                        $sqcamount, $sqctotal, '', $sqcchargetypeid, $sqcrevenueaccountid, $sqccid]).draw();
                }
                abp.ui.clearBusy(_$form);
            });
        };
        function getsalesorder(id) {
            _salesOrderService.getSalesOrder({ id: id }).done(function (result) {
                $('#SalesOrderId').val(id);
                $('#SalesOrderCode').val(result.code);
                $('#SalesAgentId').val(result.salesAgentId);
                $('#SalesAgent').val(result.salesAgent);
            });
        };
        function getcompanies() {
            var companies = $('#Companies');
            companies.empty();
            _companyService.getCompanies().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (result.items[i].isDefault === true) {
                        companies.append('<option value=' + result.items[i].id + ' data-bankaccountid=' + result.items[i].bankAccountId + ' data-cashaccountid=' + result.items[i].cashAccountId + ' data-payableaccountid=' + result.items[i].payableAccountId + ' data-receivableaccountid=' + result.items[i].receivableAccountId + ' data-taxaccountid=' + result.items[i].taxAccountId + ' selected>' + result.items[i].name + '</option>');
                        $('#CashAccountId').val(result.items[i].cashAccountId);
                        $('#BankAccountId').val(result.items[i].bankAccountId);
                        $('#TaxAccountId').val(result.items[i].taxAccountId);
                        $('#ReceivableAccountId').val(result.items[i].receivableAccountId);
                        getseriestype(result.items[i].id);
                    }
                    else {
                        companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    }
                }
                companies.selectpicker('refresh');
            });
        }
        getcompanies();
        $('#Companies').on('change', function (e) {
            var bankaccountid = $("#Companies option:selected").data('bankaccountid');
            var cashaccountid = $("#Companies option:selected").data('cashaccountid');
            var payableaccountid = $("#Companies option:selected").data('payableaccountid');
            var receivableaccountid = $("#Companies option:selected").data('receivableaccountid');
            var taxaccountid = $("#Companies option:selected").data('taxaccountid');
            $('#CashAccountId').val(cashaccountid);
            $('#BankAccountId').val(bankaccountid);
            $('#TaxAccountId').val(taxaccountid);
            $('#ReceivableAccountId').val(receivableaccountid);
            getseriestype($('#Companies').val());
        });
        $('#Series').on('change', function (e) {
            getnextseries($('#Series').val());
        });
        function getseriestype(companyid) {
            var series = $('#Series');
            series.empty();
            _commonService.getSeriesTypesByTransId({ id: 0, transactionCode: 103, companyId: companyid }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    series.append('<option value=' + result.items[i].id + '>' + result.items[i].prefix + '</option>');
                    if (i === 0) {
                        getnextseries(result.items[i].id);
                    }
                }
                series.selectpicker('refresh');
            });
        }
        function getnextseries(seriesid) {
            _commonService.getNextSeriesCode({ id: seriesid, transactionCode: 0, companyId: 0 }).done(function (result) {
                $('#SeriesCode').val(result);
            });
        }
        function getordertype() {

            var ordertypes = $('#OrderTypes');
            ordertypes.empty();
            _commonService.getOrderTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    ordertypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                ordertypes.selectpicker('refresh');
            });
        }
        function getpricingtype() {
            var pricingtypes = $('#PricingTypes');
            pricingtypes.empty();
            _pricingTypeService.getPricingTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    pricingtypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                pricingtypes.selectpicker('refresh');
            });
        }
        function getpaymentterm() {
            var paymentterms = $('#PaymentTerms');
            paymentterms.empty();
            _commonService.getPaymentTerms().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    paymentterms.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                paymentterms.selectpicker('refresh');
            });
        }
        function gettaxtype() {
            var taxtypes = $('#TaxTypes');
            taxtypes.empty();
            _commonService.getTaxTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                }
                taxtypes.selectpicker('refresh');
            });
        }
        function getdeliverytype() {
            var deliverytypes = $('#DeliveryTypes');
            deliverytypes.empty();
            _commonService.getDeliveryTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    deliverytypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                deliverytypes.selectpicker('refresh');
            });
        }
        function getwarrantytype() {
            var warrantytypes = $('#WarrantyTypes');
            warrantytypes.empty();
            _commonService.getWarrantyTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    warrantytypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                warrantytypes.selectpicker('refresh');
            });
        }
        getordertype();
        getpricingtype();
        getpaymentterm();
        gettaxtype();
        getwarrantytype();
        getdeliverytype();

        //Client Autocomplete
        var getclients = function (request, response) {
            _clientService.getClients({ filter: request.term }).done(function (result) {
                response($.map(result.items, function (el) {
                    return {
                        label: el.name,
                        value: el.id
                    };
                }));
            });
        };
        function getclient() {
            var $clientid = $('#ClientId').val();
            _clientService.getClient({ id: $clientid }).done(function (result) {
                $('#ClientAddress').val(result.address);
                $('#DeliveryAddress').val(result.address);
                $('#ClientEmail').val(result.email);
            });
        };
        var selectclient = function (event, ui) {
            event.preventDefault();
            $("#ClientId").val(ui.item ? ui.item.value : "");
            $("#ClientName").val(ui.item ? ui.item.label : "");
            getclient();
            return false;
        };
        var focusclient = function (event, ui) {
            event.preventDefault();
            $("#ClientId").val(ui.item.value);
            $("#ClientName").val(ui.item.label);
        };
        var changeclient = function (event, ui) {
            event.preventDefault();
            $("#ClientId").val(ui.item ? ui.item.value : "");
            $("#ClientName").val(ui.item ? ui.item.label : "");
            if (ui.item === null) {
                $('#ClientAddress').val("");
                $('#ClientEmail').val("");
            }
        };
        $("#ClientName").autocomplete({
            source: getclients,
            select: selectclient,
            focus: focusclient,
            minLength: 2,
            delay: 100,
            change: changeclient
        });
        //Client Autocomplete

        //Item Autocomplete
        var getproducts = function (request, response) {
            _productService.getProductByName({ filter: request.term }).done(function (result) {
                response($.map(result.items, function (el) {
                    return {
                        label: el.name,
                        value: el.id
                    };
                }));
            });
        };
        function getproduct() {
            var $productid = $('#ProductId').val();
            _productService.getProduct({ id: $productid }).done(function (result) {
                $('#ProductCode').val(result.code);
            });
        }
        function getproductunits() {
            var units = $('#Units');
            var $productid = $('#ProductId').val();
            units.empty();
            _productService.getProductUnits({
                id: $productid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    units.append('<option value=' + result.items[i].unitId + '>' + result.items[i].unit + '</option>');
                }
                units.selectpicker('refresh');
            });
        }
        function getproductprice() {
            var $unitid = $('#Units').val();
            var $pricingtypeid = $('#PricingTypes').val();
            var $productid = $('#ProductId').val();
            if ($unitid === null) {
                $unitid = 0;
            }
            if ($pricingtypeid === null) {
                $pricingtypeid = 0;
            }
            _productPriceService.getProductPrices({
                productId: $productid, pricingTypeId: $pricingtypeid, unitId: $unitid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var price = currencyFormat(result.items[i].unitPrice);
                    $("#Price").val(result.items[i].unitPrice ? price : "");
                }
            });
        }
        var selectproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item ? ui.item.value : "");
            $("#ProductName").val(ui.item ? ui.item.label : "");
            $("#PerDescription").val(ui.item ? ui.item.label : "");
            getproduct();
            getproductunits();
            getproductprice();
            return false;
        };
        var focusproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item.value);
            $("#ProductName").val(ui.item.label);
            $("#PerDescription").val(ui.item.label);
        };
        var changeproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item ? ui.item.value : "");
            $("#ProductName").val(ui.item ? ui.item.label : "");
            if (ui.item === null) {
                $("#ProductCode").val("");
                $("#Quantity").val("");
                $("#Price").val("");
                $("#PerDescription").val("");
                var units = $('#Units');
                units.empty();
                units.selectpicker('refresh');
            }
        };
        $("#ProductName").autocomplete({
            source: getproducts,
            select: selectproduct,
            focus: focusproduct,
            minLength: 2,
            delay: 100,
            change: changeproduct
        });
        $('#Units').on('change', function (e) {
            getproductprice();
        });
        //Item Autocomplete

        //Quotation Autocomplete
        var dataTableLedger = _$ledgerTable.DataTable({
            responsive: true,
            paging: false,
            "bInfo": false,
            searching: false,
            columnDefs: [{
                "visible": false,
                targets: [5, 6]
            },
            {
                orderable: false,
                targets: [0, 1, 2, 3, 4, 5]
            },
            {
                render: $.fn.dataTable.render.number(',', '.', 2),
                className: 'text-right',
                targets: [2, 3]
            }
            ]
        });
        var dataTableCharges = _$chargesTable.DataTable({
            responsive: true,
            paging: false,
            "bInfo": false,
            searching: false,
            columnDefs: [{
                "visible": false,
                targets: [6, 7, 8]
            },
            {
                orderable: false,
                targets: [0, 1, 2, 3, 4, 5]
            },
            {
                render: $.fn.dataTable.render.number(',', '.', 2),
                className: 'text-right',
                targets: [2, 3, 4]
            },
            {
                data: null,
                className: "text-center",
                "render": function () {
                    return '';
                    //return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
                },
                targets: [5]
            }
            ]
        });
        var dataTable = _$itemsTable.DataTable({
            responsive: true,
            paging: false,
            "bInfo": false,
            searching: false,
            columnDefs: [{
                "visible": false,
                targets: [4, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
            },
            {
                orderable: false,
                targets: [0, 1, 2, 3, 4, 5, 6]
            },
            {
                render: $.fn.dataTable.render.number(',', '.', 2),
                className: 'text-right',
                targets: [3, 4, 5]
            },
            {
                className: 'text-center',
                targets: [2]
            },
            {
                data: null,
                className: "text-center",
                "render": function () {
                    return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
                },
                targets: [6]
            }
            ]
        });

        function getqpricingtype(id) {

            var pricingtypes = $('#PricingTypes');
            pricingtypes.empty();
            _pricingTypeService.getPricingTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        pricingtypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        pricingtypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    }
                }
                pricingtypes.selectpicker('refresh');
            });
        }
        function getqtaxtype(id) {
            var taxtypes = $('#TaxTypes');
            taxtypes.empty();
            _commonService.getTaxTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                    }

                }
                taxtypes.selectpicker('refresh');
            });
        }
        function getqcompanies(id) {

            var companies = $('#Companies');
            companies.empty();
            _companyService.getCompanies().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        companies.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    }
                }
                companies.selectpicker('refresh');
            });
        }
        function getqordertype(id) {

            var ordertypes = $('#OrderTypes');
            ordertypes.empty();
            _commonService.getOrderTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        ordertypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        ordertypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    }
                }
                ordertypes.selectpicker('refresh');
            });
        }
        function getqdeliverytype(id) {
            var deliverytypes = $('#DeliveryTypes');
            deliverytypes.empty();
            _commonService.getDeliveryTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        deliverytypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        deliverytypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                    }

                }
                deliverytypes.selectpicker('refresh');
            });
        }
        function getqpaymentterm(id) {
            var paymentterms = $('#PaymentTerms');
            paymentterms.empty();
            _commonService.getPaymentTerms().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        paymentterms.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        paymentterms.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                    }

                }
                paymentterms.selectpicker('refresh');
            });
        }
        function getqwarrantytype(id) {
            var warrantytypes = $('#WarrantyTypes');
            warrantytypes.empty();
            _commonService.getWarrantyTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (id === result.items[i].id) {
                        warrantytypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                    }
                    else {
                        warrantytypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                    }
                }
                warrantytypes.selectpicker('refresh');
            });
        }
        var getquotationcodes = function (request, response) {
            _salesOrderService.getSalesOrders({
                filter: null + '|' + request + '|' + '2,3' + '|' + null + '|' + null
            }).done(function (result) {
                response($.map(result.items, function (el) {
                    return {
                        label: el.code,
                        value: el.id
                    };
                }));
            });
        };
        var selectquotation = function (event, ui) {
            event.preventDefault();
            $("#SalesOrderId").val(ui.item ? ui.item.value : "");
            $("#SalesOrderCode").val(ui.item ? ui.item.label : "");
            getquotation();
            return false;
        };
        var focusquotation = function (event, ui) {
            event.preventDefault();
            $("#SalesOrderId").val(ui.item ? ui.item.value : "");
            $("#SalesOrderCode").val(ui.item ? ui.item.label : "");
        };
        var changequotation = function (event, ui) {
            event.preventDefault();
            $("#SalesOrderId").val(ui.item ? ui.item.value : "");
            $("#SalesOrderCode").val(ui.item ? ui.item.label : "");
            //if (ui.item === null) {
            //}
        };
        $("#SalesOrderCode").autocomplete({
            source: getquotationcodes,
            select: selectquotation,
            focus: focusquotation,
            minLength: 2,
            delay: 100,
            change: changequotation
        });
        //Quotation Autocomplete

        //Datatable Add


        function addnewitem() {

            var $productid = $('#ProductId').val();
            var $productcode = $('#ProductCode').val();
            var $productname = $('#ProductName').val();
            var $unitid = $('#Units').val();
            var $unit = $("#Units option:selected").html();
            var $quantity = $('#Quantity').val();
            var $price = $('#Price').val();

            var $disc1 = $('#Discount1').val();
            var $disc2 = $('#Discount2').val();
            var $disc3 = $('#Discount3').val();
            var $dtype1 = $('#DiscountType1').val();
            var $dtype2 = $('#DiscountType2').val();
            var $dtype3 = $('#DiscountType3').val();
            var $perdescription = $('#PerDescription').val();

            if ($productid === '' || $productcode === '' || $productname === '' || $quantity === '' || $price === '') { return; }

            var price = parseFloat($price.replace(',', ''));
            var quantity = parseFloat($quantity);

            var disc1 = 0;
            var disc2 = 0;
            var disc3 = 0;
            if ($disc1 !== "") {
                disc1 = parseFloat($disc1);
            }
            if ($disc2 !== "") {
                disc2 = parseFloat($disc2);
            }
            if ($disc3 !== "") {
                disc3 = parseFloat($disc3);
            }

            var discount = priceDiscount(price, disc1, parseInt($dtype1), disc2, parseInt($dtype2), disc3, parseInt($dtype3));
            var totaldiscount = discount * quantity;
            var lessprice = price - discount;
            var total = lessprice * quantity;
            var datacount = dataTable.rows().count();
            var itemno = datacount + 1;
            var $rowid = "id_row_" + itemno;
            var $rowcode = "code_row_" + itemno;
            var $rowname = "name_row_" + itemno;
            var $rowquantity = "quantity_row_" + itemno;
            var $rowunit = "unit_row_" + itemno;
            dataTable.row.add([itemno,
                '<a href="#" name="' + $rowcode + '" class="btn-link">' + $productcode + '</a><br /><small><label name="' + $rowname + '" class="text-muted">' + $perdescription + '</label></small>',
                '<label name="' + $rowquantity + '" class="text-muted">' + $quantity + '</label>|<label name="' + $rowunit + '" class="text-muted">' + $unit + '</label>',
                lessprice,
                totaldiscount,
                total,
                '',
                $productid, $perdescription, $quantity, $unitid, disc1, parseInt($dtype1), disc2, parseInt($dtype2), disc3, parseInt($dtype3)
            ]).draw();

            computeTotal();

            $('#Discount1').val("");
            $('#Discount2').val("");
            $('#Discount3').val("");
            $('#Quantity').val("");
        }

        function computeTotal() {
            var grandtotal = 0;
            var discounttotal = 0;
            var chargestotal = $('#ChargesTotal').val();
            var taxrate = 0;
            var tax = 0;
            var taxcode = 101;
            var nettotal = 0;
            dataTable.column(5).data()
                .each(function (value, index) {
                    var $grandtotal = parseFloat(value);
                    grandtotal = grandtotal + $grandtotal;
                });
            dataTable.column(4).data()
                .each(function (value, index) {
                    var $discounttotal = parseFloat(value);
                    discounttotal = discounttotal + $discounttotal;
                });

            var $taxtypeid = $('#TaxTypes').val();

            taxcode = $("#TaxTypes option:selected").data('code');
            taxrate = $("#TaxTypes option:selected").data('rate');

            if (taxcode === 101) {
                nettotal = grandtotal / taxrate;
                tax = nettotal * (taxrate - 1);
            }
            else if (taxcode === 104) {
                nettotal = grandtotal;
                tax = nettotal * (taxrate - 1);
                grandtotal = nettotal * taxrate;
            }
            else {
                nettotal = grandtotal;
                tax = 0;
            }
            var newgrandtotal = grandtotal + parseFloat(chargestotal);

            $('#DiscountTotal').val(currencyFormat(discounttotal));
            $('#NetTotal').val(currencyFormat(nettotal));
            $('#Tax').val(currencyFormat(tax));
            $('#Total').val(currencyFormat(grandtotal));
            //$('#ChargesTotal').val(currencyFormat(chargestotal));
            $('#GrandTotal').val(currencyFormat(newgrandtotal));
        }

        function generateLedger() {

            var otherdiscount = parseFloat($('#DiscountTotal').val().replace(',', ''));
            var nettotal = parseFloat($('#NetTotal').val().replace(',', ''));
            var tax = parseFloat($('#Tax').val().replace(',', ''));
            var total = parseFloat($('#Total').val().replace(',', ''));
            var chargetotal = parseFloat($('#ChargesTotal').val().replace(',', ''));
            var grandtotal = parseFloat($('#GrandTotal').val().replace(',', ''));

            var cashid = $('#CashAccountId').val();
            var taxid = $('#TaxAccountId').val();
            var receivableid = $('#ReceivableAccountId').val();
            var clientid = $('#ClientId').val();
            var client = $('#ClientName').val();

            dataTableLedger.clear().draw();

            var ctr = 1;

            dataTableLedger.row.add([ctr,
                receivableid,
                grandtotal,
                0,
                client,
                receivableid,
                clientid]).draw();//AR
            ctr++;
            dataTableLedger.row.add([ctr,
                taxid,
                0,
                tax,
                '',
                taxid,
                0]).draw();//Tax
            ctr++;

            var table = _$itemsTable.DataTable();
            var form_data = table.rows().data();
            var f = form_data;

            for (var i = 0; f.length > i; i++) {
                var snettotal = 0;
                var stax = 0;
                var subtotal = f[i][5];
                var expenseid = f[i][17];
                var inventoryid = f[i][18];
                var incomeid = f[i][19];

                var taxcode = $("#TaxTypes option:selected").data('code');
                var taxrate = $("#TaxTypes option:selected").data('rate');

                if (taxcode === 101) {
                    snettotal = subtotal / taxrate;
                    stax = nettotal * (taxrate - 1);
                }
                else if (taxcode === 104) {
                    snettotal = subtotal;
                    stax = snettotal * (taxrate - 1);
                    subtotal = snettotal * taxrate;
                }
                else {
                    snettotal = subtotal;
                    stax = 0;
                }

                if (incomeid > 0 && subtotal > 0) {

                    var retindex = getledgerdup(incomeid);
                    if (retindex === 0) {
                        dataTableLedger.row.add([ctr,
                            incomeid,
                            0,
                            snettotal,
                            '',
                            incomeid,
                            0]).draw();//Income
                        ctr++;
                    }
                    else {
                        var table2 = _$ledgerTable.DataTable();
                        var temp2 = table2.row(retindex).data();
                        var temp2value = temp2[3];
                        var ccredit = parseFloat(temp2value);
                        var newcredit = ccredit + snettotal;
                        temp2[3] = newcredit;
                        $('#LedgerTable').dataTable().fnUpdate(temp2, retindex, undefined, false);
                   }
                }
            }

            var tablecharges = _$chargesTable.DataTable();
            var form_datacharges = tablecharges.rows().data();
            var h = form_datacharges;

            for (var k = 0; h.length > k; k++) {
                var revenueid = h[k][7];
                var totalamount = h[k][4];

                if (revenueid > 0 && totalamount > 0) {
                    dataTableLedger.row.add([ctr,
                        revenueid,
                        0,
                        totalamount,
                        '',
                        revenueid,
                        0]).draw();//Income
                    ctr++;
                }
            }
        }
        function getledgerdup(accountid) {
            var tableledger = _$ledgerTable.DataTable();
            var form_dataledgers = tableledger.rows().data();
            var h = form_dataledgers;
            for (var k = 0; h.length > k; k++) {
                var revenueid = h[k][5];
                if (revenueid === accountid) {
                    return k;
                }
            }
            return 0;
        }

        function save() {
            if (!_$form.valid()) {
                return;
            }

            generateLedger();

            var disabled = _$form.find(':input:disabled').removeAttr('disabled');
            var formdata = _$form.serializeFormToObject();

            var viewData = {
                salesinvoice: {
                    "id": formdata.Id,
                    "code": formdata.Code,
                    "companyId": formdata.CompanyId,
                    "seriesTypeId": formdata.SeriesTypeId,
                    "prefix": formdata.Prefix,
                    "transactionTime": formdata.TransactionTime,
                    "clientId": formdata.ClientId,
                    "clientOrderNo": formdata.ClientOrderNo,
                    "salesOrderId": formdata.SalesOrderId,
                    "orderTypeId": formdata.OrderTypeId,
                    "salesAgentId": formdata.SalesAgentId,
                    "notes": formdata.Notes,
                    "statusId": 2,
                    "taxTypeId": formdata.TaxTypeId,
                    "paymentTermId": formdata.PaymentTermId,
                    "deliveryTypeId": formdata.DeliveryTypeId,
                    "warrantyTypeId": formdata.WarrantyTypeId,
                    "subTotal": formdata.Total,
                    "otherDiscount": formdata.DiscountTotal,
                    "otherCharges": formdata.ChargesTotal,
                    "netTotal": formdata.NetTotal,
                    "taxRate": $("#TaxTypes option:selected").data('rate'),
                    "tax": formdata.Tax,
                    "grandTotal": formdata.GrandTotal,
                    "taxAccountId": formdata.TaxAccountId,
                    "receivableAccountId": formdata.ReceivableAccountId,
                    "cashAccountId": formdata.CashAccountId
                },
                salesinvoiceitems: [],
                salesinvoicecharges: [],
                generalledger: []
            };
            disabled.attr('disabled', 'disabled');

            //sales order items
            var table = _$itemsTable.DataTable();
            var form_data = table.rows().data();
            var f = form_data;

            //jsonObj = [];
            for (var i = 0; f.length > i; i++) {

                item = {};
                item["Id"] = f[i][20];
                item["SalesInvoiceId"] = "0";
                item["ProductId"] = f[i][7];
                item["Description"] = f[i][8];
                item["OrderQty"] = f[i][9];
                item["UnitId"] = f[i][10];
                item["UnitPrice"] = f[i][3];
                item["Disc1"] = f[i][11];
                item["DiscType1"] = f[i][12];
                item["Disc2"] = f[i][13];
                item["DiscType2"] = f[i][14];
                item["Disc3"] = f[i][15];
                item["DiscType3"] = f[i][16];
                item["DiscTotal"] = f[i][4];
                item["Total"] = f[i][5];
                item["ExpenseAccountId"] = f[i][17];
                item["InventoryAccountId"] = f[i][18];
                item["IncomeAccountId"] = f[i][19];
                viewData.salesinvoiceitems.push(item);
            }
            //charges
            var tablecharges = _$chargesTable.DataTable();
            var form_datacharges = tablecharges.rows().data();
            var h = form_datacharges;

            for (var k = 0; h.length > k; k++) {

                charge = {};
                charge["SalesInvoiceId"] = "0";
                charge["ChargeTypeId"] = h[k][6];
                charge["Rate"] = h[k][2];
                charge["Amount"] = h[k][3];
                charge["Total"] = h[k][4];
                charge["RevenueAccountId"] = h[k][7];
                charge["Id"] = h[k][8];
                viewData.salesinvoicecharges.push(charge);
            }
            //ledger
            var tableledgers = _$ledgerTable.DataTable();
            var form_dataledger = tableledgers.rows().data();
            var x = form_dataledger;

            for (var y = 0; x.length > y; y++) {
                var debit = parseFloat(x[y][2]);
                var credit = parseFloat(x[y][3]);
                ledger = {};
                ledger["TransactionTypeId"] = "0";
                ledger["TransactionId"] = formdata.Id;
                ledger["TransactionCode"] = formdata.Code;
                ledger["TransactionTime"] = formdata.TransactionTime;
                ledger["AccountId"] = x[y][5];
                ledger["Debit"] = x[y][2];
                ledger["Credit"] = x[y][3];
                if (debit > 0)
                {
                    ledger["BaseTypeId"] = "1";
                }
                else
                {
                    ledger["BaseTypeId"] = "2";
                }

                ledger["Description"] = "";
                ledger["CenterTypeId"] = "1";
                ledger["PartyId"] = x[y][6];
                ledger["ProjectId"] = "0";
                var partyid = x[y][6];
                if (partyid > 0) {
                    ledger["PartyName"] = x[y][4];
                    ledger["PartyCode"] = "105";
                }
                else {
                    ledger["PartyName"] = "";
                    ledger["PartyCode"] = "0";
                }
                ledger["CompanyId"] = formdata.CompanyId;
                viewData.generalledger.push(ledger);
            }

            abp.message.confirm(
                'New sales order will be created.',
                'Are you sure?',
                function (isConfirmed) {
                    if (isConfirmed) {
                        abp.ui.setBusy(_$form);
                        _salesInvoiceService.updateSalesInvoice(viewData).done(function (result) {
                            if (result === null || result === "0") { return; }
                            abp.message.success('Sales invoice updated', 'Success');
                            getinvoice();
                        }).always(function () {
                            abp.ui.clearBusy(_$form);
                        });
                    }
                }
            );
        }

        $('#SaveButton').click(function (e) {
            e.preventDefault();
            save();
        });

        _$itemsTable.on('click', 'a.delete-item', function (e) {
            e.preventDefault();
            $this = $(this);
            var dtRow = $this.parents('tr');
            var table = _$itemsTable.DataTable();
            table.row(dtRow[0].rowIndex - 1).remove().draw(false);
            computeTotal();
        });
        // Delete product unit record

        $('#TaxTypes').on('change', function (e) {
            computeTotal();
        });
        $('#AddItemButton').click(function (e) {
            e.preventDefault();
            addnewitem();
        });
        //Datatable Add
    });
})();



