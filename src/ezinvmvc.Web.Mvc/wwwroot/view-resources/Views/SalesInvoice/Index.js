﻿$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
var $month = (new Date().getMonth() + 1);
var mdayone = ($month.toString().length > 1 ? $month : "0" + $month) + "/01/" + new Date().getFullYear();
$('#DateFrom').val(mdayone);

$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

$('select').selectpicker();

(function () {
    $(function () {

        var _$Table = $('#ListTable');
        var _salesInvoiceService = abp.services.app.salesInvoiceService;

        var _permissions = {
            create: abp.auth.hasPermission('Page.Sales.Invoice.Create'),
            edit: abp.auth.hasPermission('Page.Sales.Invoice.Edit'),
            'delete': abp.auth.hasPermission('Page.Sales.Invoice.Delete')
        };

        var dataTable = _$Table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _salesInvoiceService.getSalesInvoices,
                inputFilter: function () {
                    var $id = $('#SearchFilter').val();
                    var $client = 'null';
                    var $statusid = $('#StatusTypes').val();
                    var $datefrom = $('#DateFrom').val();
                    var $dateto = $('#DateTo').val();
                    if ($id === '') {
                        $id = 'null';
                    }
                    return {
                        filter: $id + '|' + $client + '|' + $statusid + '|' + $datefrom + '|' + $dateto
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    targets: 1,
                    data: "code"
                },
                {
                    targets: 2,
                    data: "client"
                }
                ,
                {
                    targets: 3,
                    "data": "transactionTime",
                    "render": function (data) {
                        var tt = new Date(data);
                        return getFormattedDate(tt);
                    }
                },
                {
                    targets: 4,
                    data: { status: "status", statusId: "statusId" },
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-primary">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                },
                {
                    targets: 5,
                    data: "netTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    targets: 6,
                    data: "tax",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    targets: 7,
                    data: "subTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    targets: 8,
                    data: "otherCharges",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    targets: 9,
                    data: "grandTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    orderable: false,
                    targets: 10,
                    class: "text-center",
                    data: { id: "id", code: "code" },
                    "render": function (data) {
                        return '<a id="edit-order" title="edit" href="#" class="edit-order" data-order-id="' + data.id + '"><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-order" title="delete" href="#" class="delete-order" data-order-id="' + data.id + '" data-order-code="' + data.code + '"><i class="fa fa-trash"></i></a>';
                    }
                }
            ]
        });

        // Edit record
        _$Table.on('click', 'a.edit-order', function (e) {
            e.preventDefault();
            var orderId = $(this).attr("data-order-id");
            window.location.href = abp.appPath + 'SalesInvoice/Edit?id=' + orderId;
        });

        // Delete record

        function getDataList() {
            dataTable.ajax.reload();
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#ExportCompanyToExcelButton').click(function () {
            _companyService
                .getProductsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getDataList();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getDataList();
        });

        abp.event.on('app.createOrEditProductModalSaved', function () {
            getDataList();
        });

        $('#ProductTableFilter').focus();

        $("#lbBrands").change(function () {
            getSalesOrders();
        });

        $("#lbCategories").change(function () {
            getSalesOrders();
        });

    });
})();
