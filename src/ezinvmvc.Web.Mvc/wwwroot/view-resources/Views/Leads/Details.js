﻿function decimalOnly(txt) {
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode === 46) {
        var txtbx = document.getElementById(txt);
        var amount = document.getElementById(txt).value;
        var present = 0;
        var count = 0;

        //if (amount.indexOf(".", present) || amount.indexOf(".", present + 1));
        //{}
        do {
            present = amount.indexOf(".", present);
            if (present !== -1) {
                count++;
                present++;
            }
        }
        while (present !== -1);
        if (present === -1 && amount.length === 0 && event.keyCode === 46) {
            event.keyCode = 0;
            return false;
        }

        if (count >= 1 && event.keyCode === 46) {

            event.keyCode = 0;
            return false;
        }
        if (count === 1) {
            var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
            if (lastdigits.length >= 2) {
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    else {
        event.keyCode = 0;
        return false;
    }
}

//Save Button
(function ($) {
    $(".date-picker").datepicker("update", new Date());
    $('.date-picker').datepicker({
        locale: abp.localization.currentLanguage.name,
        format: 'L'
    });
    $('.datetime-picker').datepicker({
        locale: abp.localization.currentLanguage.name,
        format: 'L LT'
    });
    $('#datetimepicker1').datetimepicker({
        focusOnShow: true
    });
    $('#datetimepicker2').datetimepicker({
        format: 'L',
        focusOnShow: true
    });

    var _leadService = abp.services.app.leadService;
    var _updatesService = abp.services.app.leadUpdateService;
    var _companyService = abp.services.app.companyService;
    var _commonService = abp.services.app.commonService;
    var _clientService = abp.services.app.clientService;
    var _contactPersonService = abp.services.app.contactPersonService;
    var _employeeService = abp.services.app.employeeService;
    var _pricingTypeService = abp.services.app.pricingTypeService;
    var _rfqService = abp.services.app.rFQService;
    var _quotationService = abp.services.app.quotationService;
    var _salesOrderService = abp.services.app.salesOrderService;


    var _$form = $('form[name=LeadForm]');
    var _$updatesTable = $('#UpdatesTable');
    var _$rfqform = $('form[name=RfqForm]');
    var _$rfqItemsTable = $('#RFQItemsTable');
    var _$qtform = $('form[name=QuotationForm]');
    var _$qtItemsTable = $('#QTItemsTable');
    var _$soform = $('form[name=SalesOrderForm]');
    var _$soItemsTable = $('#SOItemsTable');

    $('#CreateUpdatesButton').click(function (e) {
        $id = $("#LeadId").val();

        e.preventDefault();
        $.ajax({
            url: abp.appPath + 'Leads/CreateSingleLeadUpdateModal?id=' + $id,
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#LeadUpdateCreateModal').modal('show');
                $('#LeadUpdateCreateModal div.modal-content').html(content);
            },
            error: function (e) { }
        });
    });

    var updatesDataTable = _$updatesTable.DataTable({
        paging: true,
        serverSide: true,
        processing: true,
        searching: false,
        listAction: {
            ajaxFunction: _updatesService.getLeadUpdatesByLeadId,
            inputFilter: function () {
                var $s = $('#LeadId').val();
                return {
                    filter: $s
                };
            }
        },
        columnDefs: [
            {
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                targets: 1,
                data: "leadUpdateDate",
                render: function (data) {
                    var tt = new Date(data);
                    return getFormattedDate(tt);
                }
            },
            {
                targets: 2,
                data: "leadTask"
            }
            ,
            {
                targets: 3,
                data: "notes"
            },
            {
                targets: 4,
                data: "assignedTo"
            },
            {
                targets: 5,
                data: "nextContactDateTime",
                render: function (data) {
                    var tt = new Date(data);
                    return getFormattedDateTime(tt);
                }
            }
        ]
    });

    function updateGetAll() {
        updatesDataTable.ajax.reload();
    }

    $("#LeadUpdateCreateModal").on('hidden.bs.modal', function () {
        updateGetAll();
    });

    function getrfq() {
        var $id = $('#RFQId').val();
        abp.ui.setBusy(_$rfqform);
        _rfqService.getRFQ({ id: $id }).done(function (result) {
            $('#RFQPrefix').val(result.prefix);
            $('#RFQCode').val(result.code);
            var rtransactiontime = new Date(result.transactionTime);
            var tt = getFormattedDate(rtransactiontime);
            $('#RFQTransactionTime').val(tt);
            $('#RFQRevisionNo').val(result.revisionNo);
            $('#RFQRefNo').val(result.code);
            $('#RFQSeries').val(result.seriesTypeId);
            $('#RFQCompanyId').val(result.companyId);
            $("#RFQType").val(result.type);
            $("#RFQLeadId").val(result.leadId);
            $("#RFQLeadCode").val(result.lead);
            $("#RFQLead").val(result.lead + ' - ' + result.client + ' - ' + result.projectName);
            $('#RFQClientId').val(result.clientId);
            $('#RFQClientName').val(result.client);
            $('#RFQProjectName').val(result.projectName);
            $('#RFQContactPersonId').val(result.contactPersonId);
            $('#RFQContactPerson').val(result.contactPerson);

            $('#RFQTelno').val(result.telNo);
            $('#RFQPhone').val(result.phone);
            $('#RFQEmail').val(result.email);
            $('#RFQAddress').val(result.address);
            $('#RFQDeliveryAddress').val(result.deliveryAddress);
            $('#RFQDiscount').val(result.discount);
            $('#RFQVat').val(result.vat);
            $('#RFQStatusId').val(result.statusId);
            $('#RFQStatusBadge').text(result.status);

            switch (result.statusId) {
                case 1:
                    $('#RFQStatusBadge').addClass('badge badge-secondary');


                    //if ($('#UpdateRfqButton').length) {
                    //    $('#UpdateRfqButton').removeAttr('hidden');
                    //}
                    //if ($('#SubmitButton').length) {
                    //    $('#SubmitButton').removeAttr('hidden');
                    //}
                    break;
                case 2:
                    $('#RFQStatusBadge').addClass('badge badge-success');
                    //if ($('#ReviseButton').length) {
                    //    $('#ReviseButton').removeAttr('hidden');
                    //}
                    break;
                case 3:
                    $('#RFQStatusBadge').addClass('badge badge-danger');
                    //if ($('#SubmitButton').length) {
                    //    $('#SubmitButton').removeAttr('hidden');
                    //}
                    break;
                case 4:
                    $('#RFQStatusBadge').addClass('badge badge-primary');
                    break;
                default:
                    $('#RFQStatusBadge').addClass('badge badge-secondary');
            }
            
            var compid = result.companyId;
            var comp = $('#RFQCompany');
            getcompany(compid, comp);

            getrfqdetails($id);
            getRFQRevisionNos();
            abp.ui.clearBusy(_$rfqform);
        });


    };

    function getRFQRevisionNos() {
        var code = $('#RFQCode').val();
        var $revnos = $('#sRFQRevisionNo');
        var revno = $('#RFQRevisionNo').val();
        var id = $('#Id').val();
        $revnos.empty();
        _rfqService.getRFQRevisions({ filter: code, sorting: "revisionno asc" }).done(function (result) {
            for (var i = 0; i < result.length; i++) {
                //alert(id + "===" + result[i].id + "|" + revno + "===" + result[i].revisionNo);
                if (revno.trim() === (result[i].revisionNo + "").trim()) {
                    //alert("equal");
                    $revnos.append('<option value=' + result[i].id + ' selected>' + result[i].revisionNo + '</option>');
                }
                else {
                    $revnos.append('<option value=' + result[i].id + '>' + result[i].revisionNo + '</option>');
                }
            }
            $revnos.selectpicker('refresh');
        });
    }

    $('#sRFQRevisionNo').change(function () {
        var id = $(this).children("option:selected").val();
        $('#RFQId').val(id);
        getrfq();
    });

    function getrfqdetails(id) {
        _$rfqItemsTable.DataTable().rows().remove().draw(false);
        _rfqService.getRfqDetailsByParentId({ id: id }).done(function (result) {

            for (var i = 0; i < result.items.length; i++) {
                var $soiid = result.items[i].id;
                var $soiproductid = result.items[i].productId;
                var $soiproductcode = result.items[i].productCode;
                var $soiproductname = result.items[i].description;
                var $soiunitid = result.items[i].unitId;
                var $soiunit = result.items[i].unit;
                var $soiquantity = result.items[i].qty;
                var $soiprice = result.items[i].unitPrice;

                var $soidisc1 = 0;
                var $soidisc2 = 0;
                var $soidisc3 = 0;
                var $soidtype1 = "0";
                var $soidtype2 = "0";
                var $soidtype3 = "0";
                var $soiperdescription = result.items[i].description;

                var soiprice = parseFloat($soiprice);
                var soiquantity = parseFloat($soiquantity);

                var soidisc1 = 0;
                var soidisc2 = 0;
                var soidisc3 = 0;
                if ($soidisc1 !== "") {
                    soidisc1 = parseFloat($soidisc1);
                }
                if ($soidisc2 !== "") {
                    soidisc2 = parseFloat($soidisc2);
                }
                if ($soidisc3 !== "") {
                    soidisc3 = parseFloat($soidisc3);
                }

                var soidiscount = priceDiscount(soiprice, soidisc1, parseInt($soidtype1), soidisc2, parseInt($soidtype2), soidisc3, parseInt($soidtype3));
                var soitotaldiscount = soidiscount * soiquantity;
                var soilessprice = soiprice - soidiscount;
                var soitotal = soilessprice * soiquantity;
                var soidatacount = rfqDataTable.rows().count();
                var soiitemno = soidatacount + 1;

                rfqDataTable.row.add([soiitemno,
                    '<a href="#" class="btn-link">' + $soiproductcode + '</a><br /><small><label class="text-muted">' + $soiperdescription + '</label></small>',
                    '<label class="text-muted">' + $soiquantity + '</label>|<label class="text-muted">' + $soiunit + '</label>',
                    '',
                    $soiproductid, $soiperdescription, $soiquantity, $soiunitid, $soiid
                ]).draw();
            }
        });
    };

    var rfqDataTable = _$rfqItemsTable.DataTable({
        responsive: true,
        paging: false,
        "bInfo": false,
        searching: false,
        columnDefs: [{
            "visible": false,
            //targets: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
            //MARC --WALA KA NA NITO SA VIEW
            //targets: [3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
            targets: [4, 5, 6, 7]
        },
        {
            orderable: false,
            targets: [0, 1, 2, 3]
        },
        {
            className: 'text-center',
            targets: [2]
        },
        {
            visible: false,
            data: null,
            className: "text-center",
            "render": function () {
                return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
            },
            targets: [3]
        }
        ]
    });

    function getcompany(id, comp) {
        _companyService.getCompanies().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    comp.val(result.items[i].name);
                }
            }
        });
    }

    function getquotation() {
        var $id = $('#QuotationId').val();
        abp.ui.setBusy(_$qtform);
        _quotationService.getQuotation({ id: $id }).done(function (result) {
            $('#QTPrefix').val(result.prefix);
            $('#QuotationCode').val(result.code);
            $('#QTRevisionNo').val(result.revisionNo);
            $('#QTCode').val(result.code);
            $('#QTRefNo').val(result.code);
            $('#QTSeries').val(result.seriesTypeId);
            $('#QTCompanies').val(result.companyId);
            $('#QTClientId').val(result.clientId);
            //$('#ClientName').val(result.client);
            var qtransactiontime = new Date(result.transactionTime);
            $('#QTTransactionTime').val(getFormattedDate(qtransactiontime));
            $('#QTOrderTypes').val(result.orderTypeId);
            $('#QTPricingTypes').val(result.pricingTypeId);
            $('#QTSalesAgent').val(result.agent);
            $('#QTSalesAgentId').val(result.salesAgentId);
            $('#QTStatusId').val(result.statusId);
            $('#QTRequestId').val(result.requestId);
            $('#QTPrevRequestId').val(result.requestId);
            $('#QTRequestCode').val(result.requestCode);
            $('#QTTaxTypes').val(result.taxTypeId);
            $('#QTNotes').val(result.notes);
            var sonettotal = currencyFormat(result.netTotal);
            var sotax = currencyFormat(result.tax);
            var sototal = currencyFormat(result.grandTotal);
            $('#QTSubTotal').val(sonettotal);
            $('#QTTax').val(sotax);
            $('#QTTotal').val(sototal);
            $('#QTStatusBadge').text(result.status);

            switch (result.statusId) {
                case 1:
                    $('#QTStatusBadge').addClass('badge badge-secondary');


                    //if ($('#SaveButton').length) {
                    //    $('#SaveButton').removeAttr('hidden');
                    //}
                    //if ($('#SubmitButton').length) {
                    //    $('#SubmitButton').removeAttr('hidden');
                    //}
                    break;
                case 2:
                    $('#QTStatusBadge').addClass('badge badge-success');
                    //if ($('#ReviseButton').length) {
                    //    $('#ReviseButton').removeAttr('hidden');
                    //}
                    break;
                case 3:
                    $('#QTStatusBadge').addClass('badge badge-danger');
                    //if ($('#SubmitButton').length) {
                    //    $('#SubmitButton').removeAttr('hidden');
                    //}
                    break;
                case 4:
                    $('#QTStatusBadge').addClass('badge badge-primary');
                    break;
                case 5:
                    $('#QTStatusBadge').addClass('badge badge-info');
                    break;
                default:
                    $('#QTStatusBadge').addClass('badge badge-secondary');
            }

            getqtcompanies(result.companyId);
            getqtordertype(result.orderTypeId);
            getqttaxtype(result.taxTypeId);
            getqtpricingtype(result.pricingTypeId);
            getqtrfq();
            getqtagent();
            getqtclient();
            getquotationitems($id);
            getqtRevisionNos();
            //getcontactpersons(result.contactPersonId);
            abp.ui.clearBusy(_$qtform);
        });
    };

    function getqtRevisionNos() {
        var code = $('#QuotationCode').val();
        var $revnos = $('#sQTRevisionNo');
        var revno = $('#QTRevisionNo').val();
        var id = $('#QuotationId').val();
        $revnos.empty();
        _quotationService.getQuotationRevisions({ filter: code, sorting: "revisionno asc" }).done(function (result) {
            for (var i = 0; i < result.length; i++) {
                //alert(id + "===" + result[i].id + "|" + revno + "===" + result[i].revisionNo);
                if (revno.trim() === (result[i].revisionNo + "").trim()) {
                    //alert("equal");
                    $revnos.append('<option value=' + result[i].id + ' selected>' + result[i].revisionNo + '</option>');
                }
                else {
                    $revnos.append('<option value=' + result[i].id + '>' + result[i].revisionNo + '</option>');
                }
            }
            $revnos.selectpicker('refresh');
        });
    }

    $('#sQTRevisionNo').change(function () {
        var id = $(this).children("option:selected").val();
        $('#QuotationId').val(id);
        getquotation();
    });

    function getquotationitems(id) {
        _$qtItemsTable.DataTable().rows().remove().draw(false);
        _quotationService.getQuotationItemsByParentId({ id: id }).done(function (result) {

            for (var i = 0; i < result.items.length; i++) {
                var $sqiid = result.items[i].id;
                var $sqiproductid = result.items[i].productId;
                var $sqiproductcode = result.items[i].productCode;
                var $sqiproductname = result.items[i].productName;
                var $sqiproductdescription = result.items[i].productDescription;
                var $sqiunitid = result.items[i].unitId;
                var $sqiunit = result.items[i].unit;
                var $sqiquantity = result.items[i].orderQty;
                var $sqiprice = result.items[i].unitPrice;
                var $sqiimagename = result.items[i].imageName;

                var $sqidisc1 = result.items[i].disc1;
                var $sqidisc2 = result.items[i].disc2;
                var $sqidisc3 = result.items[i].disc3;
                var $sqidtype1 = result.items[i].discType1;
                var $sqidtype2 = result.items[i].discType2;
                var $sqidtype3 = result.items[i].discType3;
                var $sqiperdescription = result.items[i].description;

                var sqiprice = parseFloat($sqiprice);
                var sqiquantity = parseFloat($sqiquantity);

                var sqidisc1 = 0;
                var sqidisc2 = 0;
                var sqidisc3 = 0;
                if ($sqidisc1 !== "") {
                    sqidisc1 = parseFloat($sqidisc1);
                }
                if ($sqidisc2 !== "") {
                    sqidisc2 = parseFloat($sqidisc2);
                }
                if ($sqidisc3 !== "") {
                    sqidisc3 = parseFloat($sqidisc3);
                }

                var sqidiscount = priceDiscount(sqiprice, sqidisc1, parseInt($sqidtype1), sqidisc2, parseInt($sqidtype2), sqidisc3, parseInt($sqidtype3));
                var sqitotaldiscount = sqidiscount * sqiquantity;
                var sqilessprice = sqiprice - sqidiscount;
                var sqitotal = sqilessprice * sqiquantity;
                var sqidatacount = qtDataTable.rows().count();
                var sqiitemno = sqidatacount + 1;

                qtDataTable.row.add([sqiitemno,
                    '<a href="#" class="btn-link">' + $sqiproductcode + '</a><br /><small><label class="text-muted">' + $sqiproductname + '</label></small>',
                    '<label class="text-muted">' + $sqiquantity + '</label>|<label class="text-muted">' + $sqiunit + '</label>',
                    sqiprice,
                    sqitotaldiscount,
                    sqitotal,
                    '',
                    $sqiproductid, $sqiperdescription, $sqiquantity, $sqiunitid, sqidisc1, parseInt($sqidtype1), sqidisc2, parseInt($sqidtype2), sqidisc3, parseInt($sqidtype3), $sqiid
                ]).draw();
            }
        });
    };

    function getqtcompanies(id) {
        var companies = $('#QTCompanies');
        companies.empty();
        _companyService.getCompanies().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    companies.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            companies.selectpicker('refresh');
        });
    }
    function getqtordertype(id) {

        var ordertypes = $('#QTOrderTypes');
        ordertypes.empty();
        _commonService.getOrderTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    ordertypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    ordertypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            ordertypes.selectpicker('refresh');
        });
    }
    function getqtpricingtype(id) {

        var pricingtypes = $('#QTPricingTypes');
        pricingtypes.empty();
        _pricingTypeService.getPricingTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    pricingtypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    pricingtypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            pricingtypes.selectpicker('refresh');
        });
    }
    function getqttaxtype(id) {
        var taxtypes = $('#QTTaxTypes');
        taxtypes.empty();
        _commonService.getTaxTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                }

            }
            taxtypes.selectpicker('refresh');
        });
    }
    function getqtcontactpersons(id) {
        var $clientid = $('#QTClientId').val();

        var contactpersons = $('#QTContactPersons');
        contactpersons.empty();
        _contactPersonService.getContactPersonsFiltered({ id: 0, reference: 'Client', referenceId: $clientid }).done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    contactpersons.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].fullName + '</option>');
                }
                else {
                    contactpersons.append('<option value=' + result.items[i].id + '>' + result.items[i].fullName + '</option>');
                }
            }
            contactpersons.selectpicker('refresh');
        });
    };
    function getqtrfq() {
        var $rfqid = $('#QTRequestId').val();
        _rfqService.getRFQ({ id: $rfqid }).done(function (result) {
            $("#QTRequest").val(result.code + ' - ' + result.client + ' - ' + result.projectName);
            $("#QTRequestCode").val(result.code);
            $('#QTClientId').val(result.clientId);
            $('#QTClientName').val(result.client);
            $('#QTProject').val(result.projectName);
            $('#QTContactPersonId').val(result.contactPersonId);
            $('#QTContactPerson').val(result.contactPerson);
        });
    };
    function getqtclient() {
        var $clientid = $('#QTClientId').val();
        _clientService.getClient({ id: $clientid }).done(function (result) {
            $('#QTClientAddress').val(result.address);
            $('#QTClientEmail').val(result.email);
            $('#QTClientTelephone').val(result.telNo);
        });
    };
    function getqtagent() {
        var $salesagentid = $('#QTSalesAgentId').val();
        _employeeService.getEmployee({ id: $salesagentid }).done(function (result) {
            $('#QTSalesAgentMobile').val(result.cellNo);
            $('#QTSalesAgentEmail').val(result.email);
        });
    };
    //Datatable Add
    var qtDataTable = _$qtItemsTable.DataTable({
        responsive: true,
        paging: false,
        "bInfo": false,
        searching: false,
        columnDefs: [{
            "visible": false,
            targets: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        },
        {
            orderable: false,
            targets: [0, 1, 2, 3, 4, 5, 6]
        },
        {
            render: $.fn.dataTable.render.number(',', '.', 2),
            className: 'text-right',
            targets: [3, 4, 5]
        },
        {
            className: 'text-center',
            targets: [2]
        },
        {
            visible: false,
            data: null,
            className: "text-center",
            "render": function () {
                return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
            },
            targets: [6]
        }
        ]
    });

    function getsalesorder() {
        var $id = $('#SalesOrderId').val();
        _salesOrderService.getSalesOrder({ id: $id }).done(function (result) {
            //$('#Id').val(result.id);
            $('#SOPrefix').val(result.prefix);
            $('#SalesOrderCode').val(result.code);
            $('#SOSeries').val(result.seriesTypeId);
            $('#SOCompanies').val(result.companyId);
            $('#SOClientId').val(result.clientId);
            $('#SOClientName').val(result.client);
            var sotransactiontime = new Date(result.transactionTime);
            var sodeliverytime = new Date(result.deliveryTime);
            $('#SOTransactionTime').val(getFormattedDate(sotransactiontime));
            $('#SODeliveryTime').val(getFormattedDate(sodeliverytime));
            $('#SOOrderTypes').val(result.orderTypeId);
            $('#SOPricingTypes').val(result.pricingTypeId);
            $('#SOPaymentTerms').val(result.termId);
            $('#SOSalesAgentId').val(result.salesAgentId);
            $('#SOQuotationId').val(result.quotationId);
            $('#SOQuotationCode').val(result.quotationCode);
            $('#SOClientOrderNo').val(result.clientOrderNo);
            $('#SOTaxTypes').val(result.taxTypeId);
            $('#SONotes').val(result.notes);
            var sonettotal = currencyFormat(result.netTotal);
            var sotax = currencyFormat(result.tax);
            var sototal = currencyFormat(result.grandTotal);
            $('#SOSubTotal').val(sonettotal);
            $('#SOTax').val(sotax);
            $('#SOTotal').val(sototal);
            //getseriestype(result.seriesTypeId);
            getsocompanies(result.companyId);
            getsoordertype(result.orderTypeId);
            getsotaxtype(result.taxTypeId);
            getsopricingtype(result.pricingTypeId);
            getsopaymentterm(result.termId);
            getsoclient();
            getsalesorderitems($id);
        });
    };
    function getsalesorderitems(id) {
        _salesOrderService.getSalesOrderItemsByParentId({ id: id }).done(function (result) {

            for (var i = 0; i < result.items.length; i++) {
                var $soiid = result.items[i].id;
                var $soiproductid = result.items[i].productId;
                var $soiproductcode = result.items[i].productCode;
                var $soiproductname = result.items[i].description;
                var $soiunitid = result.items[i].unitId;
                var $soiunit = result.items[i].unit;
                var $soiquantity = result.items[i].orderQty;
                var $soiprice = result.items[i].unitPrice;

                var $soidisc1 = result.items[i].disc1;
                var $soidisc2 = result.items[i].disc2;
                var $soidisc3 = result.items[i].disc3;
                var $soidtype1 = result.items[i].discType1;
                var $soidtype2 = result.items[i].discType2;
                var $soidtype3 = result.items[i].discType3;
                var $soiperdescription = result.items[i].description;

                var soiprice = parseFloat($soiprice);
                var soiquantity = parseFloat($soiquantity);

                var soidisc1 = 0;
                var soidisc2 = 0;
                var soidisc3 = 0;
                if ($soidisc1 !== "") {
                    soidisc1 = parseFloat($soidisc1);
                }
                if ($soidisc2 !== "") {
                    soidisc2 = parseFloat($soidisc2);
                }
                if ($soidisc3 !== "") {
                    soidisc3 = parseFloat($soidisc3);
                }

                var soidiscount = priceDiscount(soiprice, soidisc1, parseInt($soidtype1), soidisc2, parseInt($soidtype2), soidisc3, parseInt($soidtype3));
                var soitotaldiscount = soidiscount * soiquantity;
                var soilessprice = soiprice - soidiscount;
                var soitotal = soilessprice * soiquantity;
                var soidatacount = soDataTable.rows().count();
                var soiitemno = soidatacount + 1;

                soDataTable.row.add([soiitemno,
                    '<a href="#" class="btn-link">' + $soiproductcode + '</a><br /><small><label class="text-muted">' + $soiperdescription + '</label></small>',
                    '<label class="text-muted">' + $soiquantity + '</label>|<label class="text-muted">' + $soiunit + '</label>',
                    soiprice,
                    soitotaldiscount,
                    soitotal,
                    '',
                    $soiproductid, $soiperdescription, $soiquantity, $soiunitid, soidisc1, parseInt($soidtype1), soidisc2, parseInt($soidtype2), soidisc3, parseInt($soidtype3), $soiid
                ]).draw();
            }
        });
    };
    function getsocompanies(id) {

        var companies = $('#SOCompanies');
        companies.empty();
        _companyService.getCompanies().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    companies.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            companies.selectpicker('refresh');
        });
    }
    function getsoordertype(id) {

        var ordertypes = $('#SOOrderTypes');
        ordertypes.empty();
        _commonService.getOrderTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    ordertypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    ordertypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            ordertypes.selectpicker('refresh');
        });
    }
    function getsopricingtype(id) {

        var pricingtypes = $('#SOPricingTypes');
        pricingtypes.empty();
        _pricingTypeService.getPricingTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    pricingtypes.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    pricingtypes.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            pricingtypes.selectpicker('refresh');
        });
    }
    function getsopaymentterm(id) {

        var paymentterms = $('#SOPaymentTerms');
        paymentterms.empty();
        _commonService.getPaymentTerms().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    paymentterms.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    paymentterms.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
            }
            paymentterms.selectpicker('refresh');
        });
    }
    function getsotaxtype(id) {
        var taxtypes = $('#SOTaxTypes');
        taxtypes.empty();
        _commonService.getTaxTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                if (id === result.items[i].id) {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + ' selected>' + result.items[i].name + '</option>');
                }
                else {
                    taxtypes.append('<option value=' + result.items[i].id + ' data-rate=' + result.items[i].rate + ' data-code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                }

            }
            taxtypes.selectpicker('refresh');
        });
    }
    function getsoclient() {
        var $clientid = $('#ClientId').val();
        _clientService.getClient({ id: $clientid }).done(function (result) {
            $('#SOClientAddress').val(result.address);
            $('#SOClientEmail').val(result.email);
        });
    };
    //Datatable Add
    var soDataTable = _$soItemsTable.DataTable({
        responsive: true,
        paging: false,
        "bInfo": false,
        searching: false,
        columnDefs: [{
            "visible": false,
            targets: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        },
        {
            orderable: false,
            targets: [0, 1, 2, 3, 4, 5, 6]
        },
        {
            render: $.fn.dataTable.render.number(',', '.', 2),
            className: 'text-right',
            targets: [3, 4, 5]
        },
        {
            className: 'text-center',
            targets: [2]
        },
        {
            data: null,
            className: "text-center",
            "render": function () {
                return '<a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
            },
            targets: [6]
        }
        ]
    });

    function loadPage() {
        var rfqId = $("#RFQId").val();
        var qtId = $("#QuotationId").val();
        var soId = $("#SalesOrderId").val();
        updateGetAll();

        if (rfqId > 0) {
            $('#liRFQ').show();
            getrfq();
        }
        else {
            $('#liRFQ').hide();
        }

        if (qtId > 0) {
            $('#liQuotation').show();
            getquotation();
        }
        else {
            $('#liQuotation').hide();
        }

        if (soId > 0) {
            $('#liSalesOrder').show();
            getsalesorder();
        }
        else {
            $('#liSalesOrder').hide();
        }
    }

    loadPage();
})(jQuery);




