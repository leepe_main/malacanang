﻿$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
var $month = (new Date().getMonth() + 1);
var mdayone = ($month.toString().length > 1 ? $month : "0" + $month) + "/01/" + new Date().getFullYear();
$('#DateFrom').val(mdayone);

$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

$('select').selectpicker();

(function () {
    $(function () {

        var _$Table = $('#ListTable');
        var _collectionService = abp.services.app.collectionService;

        var _permissions = {
            create: abp.auth.hasPermission('Page.Sales.Invoice.Create'),
            edit: abp.auth.hasPermission('Page.Sales.Invoice.Edit'),
            'delete': abp.auth.hasPermission('Page.Sales.Invoice.Delete')
        };

        var dataTable = _$Table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _collectionService.getCollections,
                inputFilter: function () {
                    var $id = $('#SearchFilter').val();
                    var $client =  $('#Client').val();
                    var $status = 'null';
                    var $datefrom = $('#DateFrom').val();
                    var $dateto = $('#DateTo').val();
                    var $clientid = 'null';
                    if ($id === '') {
                        $id = 'null';
                    }
                    if ($client === '') {
                        $id = 'null';
                    }
                    return {
                        filter: $id + '|' + $client + '|' + $status + '|' + $datefrom + '|' + $dateto + '|' + $clientid
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    targets: 1,
                    data: "code"
                },
                {
                    targets: 2,
                    data: "client"
                }
                ,
                {
                    targets: 3,
                    "data": "transactionTime",
                    "render": function (data) {
                        var tt = new Date(data);
                        return getFormattedDate(tt);
                    }
                },
                {
                    targets: 4,
                    data: { status: "status", statusId: "statusId" },
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-primary">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                }
                ,
                {
                    targets: 5,
                    data: "paymentMode"
                },
                {
                    targets: 6,
                    data: "grandTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    orderable: false,
                    targets: 7,
                    class: "text-center",
                    data: { id: "id", code: "code" },
                    "render": function (data) {
                        return '<a id="edit-order" title="edit" href="#" class="edit-order" data-collection-id="' + data.id + '"><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-order" title="delete" href="#" class="delete-order" data-collection-id="' + data.id + '" data-collection-code="' + data.code + '"><i class="fa fa-trash"></i></a>';
                    }
                }
            ]
        });

        // Edit record
        _$Table.on('click', 'a.edit-order', function (e) {
            e.preventDefault();
            var orderId = $(this).attr("data-collection-id");
            window.location.href = abp.appPath + 'Collections/Edit?id=' + orderId;
        });

        // Delete record

        function getDataList() {
            dataTable.ajax.reload();
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#ExportCompanyToExcelButton').click(function () {
            _companyService
                .getProductsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getDataList();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getDataList();
        });

        abp.event.on('app.createOrEditProductModalSaved', function () {
            getDataList();
        });

        $('#ProductTableFilter').focus();

        $("#lbBrands").change(function () {
            getSalesOrders();
        });

        $("#lbCategories").change(function () {
            getSalesOrders();
        });

    });
})();
