﻿
$(".date-picker").datepicker();
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

$('select').selectpicker();
document.getElementById("EmployeeList").disabled = true;
document.getElementById("FlexiTime").disabled = true;
document.getElementById("StrickOvertime").disabled = true;

(function () {
    $(function () {
        var _employeesservice = abp.services.app.employeeService;
        var _bioAttendanceService = abp.services.app.bioAttendanceService;
        var _employeeSalariesService = abp.services.app.employeeSalariesService;
        var _employeeOTRateService = abp.services.app.employeeOTRateService;
        var _empAttRecordService = abp.services.app.empAttRecordService;
        var _$Employeestable = $('#EmployeesTable');
        var _$AttendanceTable = $('#AttendanceTable');
        var _$itemsexceltable = $('#exceltable');
        var _$Attntable = $('#Attntable');

        var _$FormAtt = $('form[name=FormAtt]');
        var _$FormAttEmp = $('form[name=FormAttEmp]');
        var _$FormAttid = $('form[name=Attid]');
        var _$FormAttidDetails = $('form[name=AttidDetails]');

        //Employee
        var dataTableatt = _$Employeestable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _employeesservice.getAttEmployees,
                inputFilter: function () {
                    var $p = $('#EmployeeTableFilter').val();
                    var $c = $('#SearchBy').val();
                    var $e = $('#attid').val();
                    if ($p === '') {
                        $p = 'null';
                    }
                    if ($e === '') {
                        $e = 'null';
                    }
                    return {
                        filter: $c + '|' + $p + '|' + $e
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',

                    visible: false,
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    targets: 1,
                    data: "employeeCode"
                },
                {
                    targets: 2,
                    data: "completeName"
                },
                {
                    targets: 3,
                    data: "cellNo"
                },
                {
                    targets: 4,
                    data: "dept"
                },
                {
                    visible: false,
                    targets: 5,
                    data: "post"
                },
                {
                    visible: false,
                    targets: 6,
                    data: "status"
                },
                {
                    orderable: false,
                    targets: 7,
                    class: "text-center",
                    data: { id: "id", completeName: "completeName", employeeCode: "employeeCode" },
                    "render": function (data) {
                        return '<a id="edit-Employee" title="Select Employee" href="#" class="edit-Employee" data-Employee-id="' + data.id + '" data-Employee-completeName="' + data.completeName + '" data-Employee-employeeCode="' + data.employeeCode + '"><i class="fa fa-lg fa-pencil-square-o"></i></a>';
                    }
                }
            ]
        });
        function GetEmployees() {
            dataTableatt.ajax.reload();
        }

        $('#EmployeesTable').on('click', 'a.edit-Employee', function (e) {
            e.preventDefault();
            $('#EmpId').val("");
            $('#completeName').val("");
            $('#EmpCode').val("");

            var employeeId = $(this).attr("data-Employee-id");
            var completeName = $(this).attr("data-Employee-completeName");
            var employeeCode = $(this).attr("data-Employee-employeeCode");

            $('#EmpId').val(employeeId);
            $('#completeName').val(completeName);
            $('#EmpCode').val(employeeCode);

            GetAttendanceRecord();
            GetAttendanceEmpRecord();
            GetEmployeeSalary(employeeId);
            GetEmployeeOTRalesSalary(employeeId);
            
        });
        $('#BtnLoadRecord').click(function (e) {
            e.preventDefault();
            dataTableEmp.ajax.reload();
        });
        //Attendance
        var dataTable = _$AttendanceTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _bioAttendanceService.getAllAttendance

            },
            columnDefs: [

                {
                    targets: 0,
                    data: "dateT",
                    "render": function (data) {
                        var dt = new Date(data);
                        return getFormattedDate(dt);
                    }
                },
                {
                    targets: 1,
                    data: "attendanceId"
                },
                {
                    targets: 2,
                    data: "company"
                },

                {
                    orderable: false,
                    targets: 3,
                    class: "text-center",
                    data: { attendanceId: "attendanceId", company: "company" },
                    "render": function (data) {
                        return '<a id="edit-attendanceId" title="Select AttendanceId" href="#" class="edit-attendanceId" data-attendanceId-id="' + data.attendanceId + '" data-attendanceId-company="' + data.company + '"><i class="fa fa-lg fa-pencil-square-o"></i></a>';
                    }
                }
            ]
        });

        var dataTableAtt = _$itemsexceltable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _bioAttendanceService.getAllAttendanceByCode,
                inputFilter: function () {
                    var $d = $('#EmpCode').val();
                    var $c = $('#attid').val();
                    if ($d === '') {
                        $d = 0;
                    }
                    if ($c === '') {
                        $c = 0;
                    }
                    return {
                        filter: $d + '|' + $c 
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: true,
                    targets: 1,
                    data: "company"
                },
                {
                    visible: true,
                    targets: 2,
                    data: "name"
                },
                {
                    visible: true,
                    targets: 3,
                    data: "no"
                },
                {
                    targets: 4,
                    data: "date",
                    "render": function (data) {
                        var dt = new Date(data);
                        return getFormattedDateTime(dt);
                    }
                },
                {
                    visible: false,
                    targets: 5,
                    data: "locId"
                },
                {
                    visible: false,
                    targets: 6,
                    data: "idNumber"
                },
                {
                    visible: false,
                    targets: 7,
                    data: "verifyCode"
                },
                {
                    visible: false,
                    targets: 8,
                    data: "cardNo"
                },
                {
                    visible: true,
                    targets: 9,
                    data: "status"
                },
                {
                    orderable: false,
                    targets: 10,
                    class: "text-center",
                    data: { id: "id", company: "company" },
                    "render": function (data) {
                        return '<a id="edit-attendanceId" title="Select AttendanceId" href="#" class="edit-attendanceId" data-attendanceId-id="' + data.attendanceId + '" data-attendanceId-company="' + data.company + '"><i class="fa fa-lg fa-pencil-square-o"></i></a>';
                    }
                }
            ]
        });

        function GetAttendanceTable() {
            dataTable.ajax.reload();
        }
        ////baba
        var dataTableEmp = _$Attntable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _bioAttendanceService.getAttendanceVersion2,
                inputFilter: function () {
                    var $d = $('#EmpCode').val();
                    var $c = $('#attid').val();
                    var $e = $('#DateRecStart').val();
                    var $f = $('#DateRecEnd').val();
                    if ($d === '') {
                        $d = 0;
                    }
                    if ($c === '') {
                        $c = 0;
                    }
                    if ($e === '') {
                        $e = '1900-01-01';
                    }
                    if ($f === '') {
                        $f = '1900-01-01';
                    }
                    return {
                        filter: $d + '|' + $c + '|' + $e + '|' + $f
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    visible: false,
                    targets: 1,

                     data: {empId: "empId" },
                    "render": function (data) {
                        var $empId = data.empId;
                        if ($empId == 0)
                        {
                            var empId = document.getElementById("EmpId").value;
                        }
                        else
                        {
                            var empId = $empId;
                        }
                        return empId;
                    }
                },
                {
                    visible: true,
                    targets: 2,
                    data: { No: "no" },
                    "render": function (data) {
                        var $No = data.No;
                        if ($No !== 0) {
                            var EmpNo = document.getElementById("EmpCode").value;
                        }
                        else {
                            var EmpNo = $No;
                        }
                        return EmpNo;
                    }
                },
                {
                    visible: false,
                    targets: 3,
                    data: { attRecId: "AttRecId"},
                    "render": function (data) {
                        if (data.attRecId == "")
                        {
                            return document.getElementById("attid").value;
                        }
                    }
                },
                {
                    visible: true,
                    targets: 4,
                    data: "attendanceId"
                },
                {
                    visible: true,
                    targets: 5,
                    data: "days"
                },
                {
                    visible: true,
                    targets: 6,
                    data: "date",
                    "render": function (data) {
                        var dt = new Date(data);
                        return getFormattedDate(dt);
                    }
                },
                {
                    visible: true,
                    targets: 7,
                    data: { dateIn1: "dateIn", no1: "no" },
                    "render": function (data) {
                        var $No = data.no;
                        if ($No === 0)
                        {
                            return "";
                        }
                        else
                        {
                            var dt = new Date(data.dateIn);
                            return getFormattedDate(dt);
                        }
                    }
                },
                {
                    visible: true,
                    targets: 8,
                    data: "workIn"
                },
                {
                    visible: true,
                    targets: 9,
                    data: "timeIn"
                },
                {
                    visible: true,
                    targets: 10,
                    data: "lateIn"
                },
                {
                    visible: true,
                    targets: 11,
                    data: { hrsPerDay: "hrsPerDay", hrsWorked: "hrsWorked", payrollRatePerHour: "payrollRatePerHour", otPerHourPercent: "otPerHourPercent", otHour: "otHour", holRates: "holRates", lRates: "lRates", underTimeHour: "underTimeHour", attendanceId: "attendanceId", payrollRatePerDay: "payrollRatePerDay", dateIn1: "dateIn", lateIn: "lateIn", minLates: "minLates", deductionLate: "deductionLate", holiday: "holiday", holidayDescription: "holidayDescription" },
                    "render": function (data) {
                        var minlate = data.minLates;
                        var $FlexiTime = document.getElementById("FlexiTime").value;
                        if ($FlexiTime == "false")
                        {
                            var mn = minlate;
                        }
                        if ($FlexiTime == "true")
                        {
                            var mn = 0;
                        }

                        return $minLates = mn;
                    }
                },
                {
                    visible: true,
                    targets: 12,
                    data: "workOut"
                },
                {
                    visible: true,
                    targets: 13,
                    data: "timeOut"
                },
                {
                    visible: true,
                    targets: 14,
                    data: "hrsWorked"
                },
                {
                    visible: true,
                    targets: 15,
                    data: { hrsPerDay: "hrsPerDay", hrsWorked: "hrsWorked", otHour: "otHour" },
                    "render": function (data) {
                        var $hrsWorked = data.hrsWorked;
                        var $hrsPerDay = data.hrsPerDay;
                        var otHour = data.otHour;
                        var $StrickOvertime = document.getElementById("StrickOvertime").value;
                        var $FlexiTime = document.getElementById("FlexiTime").value;
                        if ($StrickOvertime == "true")
                        {
                            if ($FlexiTime == "false")
                            {
                                if ($hrsWorked > $hrsPerDay)
                                {
                                    var $Ot = otHour;
                                }
                                else
                                {
                                    var $Ot = 0;
                                }

                            }
                            if ($FlexiTime == "true")
                            {
                                if ($hrsWorked > $hrsPerDay)
                                {
                                    var $Ot = $hrsWorked - $hrsPerDay;
                                }
                                else
                                {
                                    var $Ot = 0;
                                }
                            }
                            
                        }
                        if ($StrickOvertime == "false")
                        {
                            var $Ot = 0;
                        }
                        return $otHour = $Ot;

                    }


                },
                {
                    visible: true,
                    targets: 16,
                    data: { underTimeHour: "underTimeHour", hrsPerDay: "hrsPerDay", hrsWorked: "hrsWorked", hrsPerDay: "hrsPerDay" },
                    "render": function (data)
                    {
                        var $hrsPerDay = data.hrsPerDay;
                        var $hrsWorked = data.hrsWorked;
                        var underTimeHour = data.underTimeHour;
                        var $FlexiTime = document.getElementById("FlexiTime").value;
                        if ($FlexiTime == "true")
                        {
                            if ($hrsWorked != 0 || $hrsWorked != null)
                            {
                                if ($hrsWorked < $hrsPerDay)
                                {
                                    var $UT =  $hrsPerDay - $hrsWorked;
                                }
                                else
                                {
                                    var $UT = 0;
                                }
                            }
                            if ($hrsWorked == 0)
                            {
                                var $UT = $hrsPerDay;
                            }
                        }
                        if ($FlexiTime == "false")
                        {
                            if ($hrsWorked != 0)
                            {
                                if ($hrsWorked > $hrsPerDay)
                                {
                                    var $UT = 0;
                                }
                                else
                                {
                                    var $UT = underTimeHour;
                                }
                            }
                            if ($hrsWorked == 0)
                            {
                                var $UT = $hrsPerDay;
                            }
                        }
                        return $underTimeHour = $UT;
                    }
                },
                {
                    visible: true,
                    targets: 17,
                    data: "holidayDescription"
                },
                {
                    visible: true,
                    targets: 18,
                    data: "otPerHourPercent"
                },
                {
                    visible: false,
                    targets: 19,
                    data: "holiday"
                },
                {
                    visible: false,
                    targets: 20,
                    data: "holRates"
                },
                {
                    visible: true,
                    targets: 21,
                    data: "enTitlement"
                },
                {
                    visible: true,
                    targets: 22,
                    data: "lRates"
                },
                {
                    visible: false,
                    targets: 23,
                    data: "hrsPerDay"
                },
                {
                    visible: false,
                    targets: 24,
                    data: "payrollRatePerHour"
                },
                {
                    visible: false,
                    targets: 25,
                    data: "deductionLate"
                },
                {
                    visible: true,
                    targets: 26,
                    data: "payrollRatePerDay"
                },
                {
                    visible: true,
                    targets: 27,
                    data: { hrsPerDay: "hrsPerDay", hrsWorked: "hrsWorked", payrollRatePerHour: "payrollRatePerHour", otPerHourPercent: "otPerHourPercent", otHour: "otHour", holRates: "holRates", lRates: "lRates", underTimeHour: "underTimeHour", attendanceId: "attendanceId", payrollRatePerDay: "payrollRatePerDay", dateIn1: "dateIn", lateIn: "lateIn", minLates: "minLates", deductionLate: "deductionLate", holiday: "holiday", holidayDescription:"holidayDescription" },
                    "render": function (data) {
                        //var $lRates = "";
                        var $hrsWorked = data.hrsWorked;
                        var $hrsPerDay = data.hrsPerDay;
                        var $payrollRatePerHour = data.payrollRatePerHour;
                        var $otPerHourPercent = data.otPerHourPercent;
                        var $otHour = data.otHour;
                        var $holRates = data.holRates;
                        var $lRates = data.lRates;
                        var $payrollRatePerDay = document.getElementById("PayPerDay").value;
                        var $FlexiTime = document.getElementById("FlexiTime").value;
                        var $StrickOvertime = document.getElementById("StrickOvertime").value;
                        //MINLATE
                        var lateInstrDate = data.lateIn;
                        if (lateInstrDate == null) { lateInstrDate = "00:00"; }
                        var lateInarr = lateInstrDate.split(':');
                        var lateInMin = parseInt(lateInarr[1]);
                        
                        if (data.minLates > 0 && data.minLates >= lateInMin) {
                            var MinLatetime = (data.minLates / lateInMin) * data.deductionLate;
                        }
                        else
                        {
                            var MinLatetime = 0;
                        }
                        //MINLATE

                        if (data.attendanceId !== "Restday" && data.attendanceId !== "Absent" && data.attendanceId !== "Leave" && data.attendanceId !== "Holiday") {
                            if (data.hrsWorked > 0 || data.hrsWorked !== null)
                            {
                                if (data.holidayDescription != "0")
                                {
                                    if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1 }
                                    if ($lRates !== null) { var $lRates = $lRates; } else { var $lRates = 0 }
                                    if ($underTimeHour !== null) { var $underTimeHour = data.underTimeHour * $payrollRatePerHour } else { var $underTimeHour = 1 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            if ($hrsWorked > $hrsPerDay) {
                                                var $OtHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour) + OTRate;
                                            }
                                            if ($hrsWorked < $hrsPerDay) {
                                                var $OtHour2 = 0;

                                                var $underTimeHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                            }
                                        }
                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime);
                                        }
                                        if ($FlexiTime == "true")  
                                        {
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour;
                                        }

                                    }
                                }
                                else
                                {
                                    if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1 }
                                    if ($lRates !== null) { var $lRates = $lRates; } else { var $lRates = 0 }
                                    if ($underTimeHour !== null) { var $underTimeHour = data.underTimeHour * $payrollRatePerHour } else { var $underTimeHour = 1 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            if ($hrsWorked > $hrsPerDay)
                                            {
                                                var $OtHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour) + OTRate;
                                            }
                                            if ($hrsWorked <= $hrsPerDay)
                                            {
                                                var $OtHour2 = 0;

                                                var $underTimeHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                            }
                                        }
                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime);
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = ($holRates + $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour;
                                        }

                                    }
                                }
                            }
                            if (data.hrsWorked == 0 || data.hrsWorked == null)
                            {
                                var rate = "0.00";
                            }
                            var $Rate2 = rate;
                        }

                        if (data.attendanceId == "Restday")
                        {
                            if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1 }
                            if ($lRates !== null) { var $lRates = $lRates; } else { var $lRates = 1 }
                            
                            if (data.hrsWorked > 0 || data.hrsWorked !== null)
                            {
                                if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1  }
                                if (data.holidayDescription != "0")
                                {
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour = (data.underTimeHour + Math.floor("2")) * $payrollRatePerHour } else { var $underTimeHour = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour - MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            if ($hrsWorked > $hrsPerDay)
                                            {
                                                var $OtHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour) + OTRate;
                                            }
                                            if ($hrsWorked <= $hrsPerDay) {
                                                var $OtHour2 = 0;
                                                var $underTimeHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour) + OTRate;
                                            }
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour - MinLatetime;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour;
                                        }

                                    }
                                }
                                else
                                {   
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour2 = (data.underTimeHour + Math.floor("1")) * $payrollRatePerHour } else { var $underTimeHour2 = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour2 + MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            if ($hrsWorked > $hrsPerDay)
                                            {
                                                var $OtHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                            }
                                            if ($hrsWorked <= $hrsPerDay)
                                            {
                                                var $OtHour2 = 0;
                                                var $underTimeHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * $OtHour2;
                                                var rate = (($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                            }
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour2 + MinLatetime);
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2;
                                        }

                                    }

                                }
                            }
                            if (data.hrsWorked == 0 || data.hrsWorked == null)
                            {
                                var rate = "0.00";
                            }
                            var $Rate2 = rate;
                        }

                        if (data.attendanceId == "Leave")
                        {
                            if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1 }
                            if ($lRates !== null) { var $lRates = $lRates; } else { var $lRates = 1 }

                            if (data.hrsWorked > 0 || data.hrsWorked !== null) {
                                if ($holRates !== null) { var $holRates = $holRates + 1; } else { var $holRates = 1 }
                                if (data.holidayDescription != "0") {
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour = (data.underTimeHour + Math.floor("2")) * $payrollRatePerHour } else { var $underTimeHour = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour - MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour) + OTRate;
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour - MinLatetime;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour;
                                        }

                                    }
                                }
                                else 
                                {
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour2 = (data.underTimeHour + Math.floor("1")) * $payrollRatePerHour } else { var $underTimeHour2 = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour2 - MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour2) + OTRate;
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false") {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour2 - MinLatetime;
                                        }
                                        if ($FlexiTime == "true") {
                                            var rate = $holRates * $lRates * $payrollRatePerHour * $hrsPerDay - $underTimeHour2;
                                        }

                                    }

                                }
                            }
                            if (data.hrsWorked == 0 || data.hrsWorked == null)
                            {
                                var rate = $payrollRatePerDay;
                            }
                            var $Rate2 = rate;

                        }

                        if (data.attendanceId == "Holiday") {

                            if (data.hrsWorked > 0 || data.hrsWorked !== null)
                            {

                                if ($lRates !== null) { var $lRates = $lRates; } else { var $lRates = 1 }
                                if ($holRates !== null) { var $holRates = $holRates; } else { var $holRates = 1 }
                                if (data.holidayDescription != "0")
                                {
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour = (data.underTimeHour + Math.floor("2")) * $payrollRatePerHour } else { var $underTimeHour = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = ($otPerHourPercent * $payrollRatePerHour) * $otHour;
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            if ($hrsWorked > $hrsPerDay)
                                            {
                                                var otHour2 = $hrsWorked - $hrsPerDay;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * otHour2;
                                                var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay) - $underTimeHour + OTRate;
                                            }
                                            if ($hrsWorked < $hrsPerDay) 
                                            {
                                                var otHour2 = 0;
                                                var OTRate = $otPerHourPercent * $payrollRatePerHour * otHour2;
                                                var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay) - $underTimeHour + OTRate;
                                            }
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay) - ($underTimeHour + MinLatetime);
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = ($holRates * $lRates * $payrollRatePerHour * $hrsPerDay) - $underTimeHour;
                                        }

                                    }
                                }
                                else
                                {
                                    if (data.underTimeHour !== null && data.underTimeHour !== 0) { var $underTimeHour2 = (data.underTimeHour + Math.floor("1")) * $payrollRatePerHour } else { var $underTimeHour2 = 0 }

                                    if ($StrickOvertime == "true")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour2 + MinLatetime) + OTRate;
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var OTRate = $otPerHourPercent * $payrollRatePerHour * $otHour;
                                            var rate = (($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                        }

                                    }
                                    if ($StrickOvertime == "false")
                                    {
                                        if ($FlexiTime == "false")
                                        {
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - ($underTimeHour2 + MinLatetime);
                                        }
                                        if ($FlexiTime == "true")
                                        {
                                            var rate = ($holRates * $lRates) * ($payrollRatePerHour * $hrsPerDay) - $underTimeHour2;
                                        }

                                    }

                                }
                            }
                            if (data.hrsWorked == 0 || data.hrsWorked == null)
                            {
                                var rate = $payrollRatePerDay;
                            }
                            var $Rate2 = rate;
                        }

                        if (data.attendanceId == "Absent") {
                            var $Rate2 = "0.00"
                        }

                        var RateAmount = parseInt($Rate2);
                        if (RateAmount <= 0)
                        {
                            return $Rate2 = "0.00";
                        }
                        else
                        {
                            return (Math.round($Rate2 * 100) / 100).toFixed(2);
                        }
                    }
                },
                
                {
                    visible: false,
                    orderable: false,
                    targets: 28,
                    class: "text-center",
                    data: { id: "id", company: "company" },
                    "render": function (data) {
                        return '<a id="edit-attendanceId" title="Select AttendanceId" href="#" class="edit-attendanceId" data-attendanceId-id="' + data.attendanceId + '" data-attendanceId-company="' + data.company + '"><i class="fa fa-lg fa-pencil-square-o"></i></a>';
                    }
                }
                
            ]
        });

        function GetAttendanceEmpRecord() {
            document.getElementById("EmployeeList").disabled = false;
            GetEmployees();
        }

        function GetAttendanceRecord() {
            dataTableAtt.ajax.reload();
        }

        $('#AttendanceTable').on('click', 'a.edit-attendanceId', function (e) {
            e.preventDefault();
            $('#attid').val("");
            var Id = $(this).attr("data-attendanceId-id");
            $('#attid').val(Id);
            GetAscAttendanceRecord(Id);
            GetDescAttendanceRecord(Id);
            GetAttendanceRecord();
            GetAttendanceEmpRecord();
        });
        function GetAscAttendanceRecord(Id) {
            _bioAttendanceService.getTop1AscOvertimeRate({ attendanceId: Id }).done(function (result) {
                abp.ui.setBusy(_$FormAtt);
                var StartDate = new Date(result.date);
                $('#DateRecStart').val(getFormattedDate(StartDate));

                GetAttendanceEmpRecord();
                abp.ui.clearBusy(_$FormAtt);
            });

        }

        function GetDescAttendanceRecord(Id) {
            _bioAttendanceService.getTop1DescOvertimeRate({ attendanceId: Id }).done(function (result) {
                abp.ui.setBusy(_$FormAttEmp);
                var EndDate = new Date(result.date);
                $('#DateRecEnd').val(getFormattedDate(EndDate));
                GetAttendanceEmpRecord();

                abp.ui.clearBusy(_$FormAttEmp);
            });
           
        }

        function GetEmployeeSalary(employeeId) {
            _employeeSalariesService.getTop1EmployeeSalary({ empId: employeeId, }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {

                    var TimeIn2 = new Date(result.items[i].timeIn);
                    var TimeIOut2 = new Date(result.items[i].timeIOut);
                    var MinLate2 = new Date(result.items[i].minLate);
                    var payrollRatePerDay2 = result.items[i].payrollRatePerDay;

                    var currentTimeIn = new Date(TimeIn2),
                        hoursin = currentTimeIn.getHours(),
                        minutesin = currentTimeIn.getMinutes();

                    if (minutesin < 10) {
                        minutesin = "0" + minutesin;
                    }

                    var currentTimeOut = new Date(TimeIOut2),
                        hoursOut = currentTimeOut.getHours(),
                        minutesOut = currentTimeOut.getMinutes();

                    if (minutesOut < 10) {
                        minutesOut = "0" + minutesOut;
                    }

                    var currentTimeLate = new Date(MinLate2),
                        hoursLate = currentTimeLate.getHours(),
                        minutesLate = currentTimeLate.getMinutes();

                    if (minutesLate < 10) {
                        minutesLate = "0" + minutesLate;
                    }

                    $('#PayPerDay').val(payrollRatePerDay2)
                    $('#TimeIn').val(hoursin + ":" + minutesin);
                    $('#TimeIOut').val(hoursOut + ":" + minutesOut);
                    $('#MinLate').val(hoursLate + ":" + minutesLate);
                    $('#NoHours').val(result.items[i].dayCount)
                    var selectoptions = $('#StrickOvertime');
                    if (result.items[i].strickOverTime === true) {
                        selectoptions.append('<option value=' + result.items[i].strickOverTime + ' selected>Yes</option>');
                    }
                    else {
                        selectoptions.append('<option value=' + result.items[i].strickOverTime + ' selected>No</option>');
                    }
                    selectoptions.selectpicker('refresh');

                    var selectFlexiTime = $('#FlexiTime');
                    if (result.items[i].flexiTime === true) {
                        selectFlexiTime.append('<option value=' + result.items[i].flexiTime + ' selected>Yes</option>');
                    }
                    else {
                        selectFlexiTime.append('<option value=' + result.items[i].flexiTime + ' selected>No</option>');
                    }
                    selectFlexiTime.selectpicker('refresh');
                }
            })
        };

        function GetEmployeeOTRalesSalary(employeeId) {
            $('#OTNoHours').val(0);
            _employeeOTRateService.getTop1EmployeeOTRates({ empId: employeeId, }).done(function (result) {
                if (result == null)
                {
                    $('#OTNoHours').val(0)
                }
                else
                {
                    $('#OTNoHours').val(result.regularDayHours);
                }
            });
        }

        $('#SaveAttbutton').click(function (e) {
            e.preventDefault();
            SaveAtt();
        });

        function SaveAtt() {
            if (!_$FormAttid.valid()) {
                return;
            }
            var today = new Date();
            var date = (today.getMonth() + 1) + '-' + today.getDate() + '-' +  today.getFullYear();

            var disabled = _$FormAttid.find(':input:disabled').removeAttr('disabled');
            var formdata = _$FormAttid.serializeFormToObject();

            var viewData = {
                empAttInputs: {

                    "empId": formdata.EmpId,
                    "empCode": formdata.EmpCode,
                    "attId": formdata.attid,
                    "dateRecStart": formdata.DateRecStart,
                    "dateRecEnd": formdata.DateRecEnd,
                    "timeIn": formdata.TimeIn,
                    "minLate": formdata.MinLate,
                    "timeIOut": formdata.TimeIOut,
                    "noHours": formdata.NoHours,
                    "strickOvertime": $("#StrickOvertime").val(),
                    "flexiTime": $("#FlexiTime").val(),
                    "oTNoHours": formdata.OTNoHours,
                    "status": "Active",
                    "date": date,
                    "description1": "",
                    "description2": ""
                },
                empAttDetailsInputs: []
            };
            
            disabled.attr('disabled', 'disabled');

            var table2 = $('#Attntable').DataTable();
            var form_data = table2.rows().data();
            var f = form_data;

            for (var i = 0; f.length > i; i++) {
                var $EmpId = f[i]["empId"];
                var $No = f[i]["no"];
                var $AttRecId = f[i]["attRecId"];
                var $AttendanceId = f[i]["attendanceId"];
                var $Days = f[i]["days"];
                var $Date = f[i]["date"];
                var $DateIn = f[i]["dateIn"];
                var $WorkIn = f[i]["workIn"];
                var $TimeIn = f[i]["timeIn"];
                var $LateIn = f[i]["lateIn"];
                var $MinLates = f[i]["minLates"];
                var $WorkOut = f[i]["workOut"];
                var $TimeOut = f[i]["timeOut"];
                var $HrsWorked = f[i]["hrsWorked"];
                var $OtHour = f[i]["otHour"];
                var $UnderTimeHour = f[i]["underTimeHour"];
                var $HolidayDescription = f[i]["holidayDescription"];
                var $OTPerHourPercent = f[i]["otPerHourPercent"];
                var $Holiday = f[i]["holiday"];
                var $HolRates = f[i]["holRates"];
                var $EnTitlement = f[i]["enTitlement"];
                var $LRates = f[i]["lRates"];
                var $HrsPerDay = f[i]["hrsPerDay"];
                var $PayrollRatePerHour = f[i]["payrollRatePerHour"];
                var $DeductionLate = f[i]["deductionLate"];
                var $PayrollRatePerDay = f[i]["payrollRatePerDay"];
                var $Rates = f[i]["rates"];
                var $Description1 = "";
                var $Description2 = "";
                var $Status = "Active";                


                //$EmpId
                if ($EmpId == 0)
                {
                    $EmpId = document.getElementById("EmpId").value;
                }
                //No
                if ($No == 0) {
                    var $No = document.getElementById("EmpCode").value;
                }
                //AttendanceId
                if ($AttRecId == "")
                {
                    $AttRecId = document.getElementById("attid").value;
                }
                //date
                var dt = new Date($Date);
                var $Date = getFormattedDate(dt)
                //date
                if ($DateIn == null) {
                    var DateIn = "";
                }
                else
                {
                    var dt2 = new Date($DateIn);
                    var DateIn = getFormattedDate(dt2)
                }

                var $FlexiTime = document.getElementById("FlexiTime").value;
                var $StrickOvertime = document.getElementById("StrickOvertime").value;
                var $payrollRatePerDay = document.getElementById("PayPerDay").value;

                if ($FlexiTime == "true")
                {
                    if ($HrsWorked != 0)
                    {
                        if ($HrsWorked < $HrsPerDay)
                        {
                            var $UT = $HrsPerDay - $HrsWorked;
                        }
                        else
                        {
                            var $UT = 0;
                        }
                    }
                    if ($HrsWorked == 0)
                    {
                        var $UT = $HrsPerDay;
                    }
                }
                if ($FlexiTime == "false")
                {
                    if ($HrsWorked != 0)
                    {
                        if ($HrsWorked > $HrsPerDay)
                        {
                            var $UT = 0;
                        }
                        else
                        {
                            var $UT = $UnderTimeHour;
                        }
                    }
                    if ($HrsWorked == 0)
                    {
                        var $UT = $HrsPerDay;
                    }
                }
                var $UnderTimeHour1 = $UT;
                //rate;
                
                //Minrate;
                if ($FlexiTime == "false")
                {
                    var FlexMinLates = $MinLates;
                }
                if ($FlexiTime == "true")
                {
                    var FlexMinLates = 0;
                }
                //Minrate;

                //OT;
                if ($StrickOvertime == "true")
                {
                    var $Ot = 0;
                    if ($FlexiTime == "true")
                    {
                        if ($HrsWorked > $HrsPerDay)
                        {
                            var $count = $HrsWorked - $HrsPerDay;
                            var $Ot = $count;
                        }
                        if ($HrsWorked < $HrsPerDay)
                        {
                            var $count = 0;
                            var $Ot = $count;
                        }
                    }
                    if ($FlexiTime == "false")
                    {
                        if ($HrsWorked > $HrsPerDay)
                        {
                            var $Ot = $OtHour;
                        }
                        else
                        {
                            var $Ot = 0;
                        }
                    }

                }
                if ($StrickOvertime == "false")
                {
                    var $Ot = 0;
                }
                //OT;

                ////MINLATE
                var lateInstrDate = $LateIn;
                if (lateInstrDate == null) { lateInstrDate = "00:00"; }
                var lateInarr = lateInstrDate.split(':');
                var lateInMin = parseInt(lateInarr[1]);      
                if ($MinLates > 0 && $MinLates >= lateInMin)
                {
                    var MinLatetime = ($MinLates / lateInMin) * $DeductionLate;
                }
                else
                {
                    var MinLatetime = 0;
                }
                //MINLATE
                if ($AttendanceId !== "Restday" && $AttendanceId !== "Absent" && $AttendanceId !== "Leave" && $AttendanceId !== "Holiday") {
                    if ($HrsWorked > 0 || $HrsWorked !== null)
                    {
                        if ($HolidayDescription != "0")
                        {
                            if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                            if ($LRates !== null) { var $LRates = $LRates; } else { var $LRates = 0 }
                            if ($UnderTimeHour !== null) { var UnderTimeHour = $UnderTimeHour * $PayrollRatePerHour } else { var UnderTimeHour = 1 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    if ($HrsWorked > $HrsPerDay)
                                    {
                                        var $OtHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - UnderTimeHour + OTRate;
                                    }
                                    if ($HrsWorked <= $HrsPerDay)
                                    {
                                        var $OtHour2 = 0;

                                        var $underTimeHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2 + OTRate;
                                    } 
                                }
                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime);
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - UnderTimeHour;
                                }

                            }
                        }
                        else
                        {
                            if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                            if ($LRates !== null) { var $LRates = $LRates; } else { var $LRates = 0 }
                            if ($UnderTimeHour !== null) { var UnderTimeHour = $UnderTimeHour * $PayrollRatePerHour } else { var UnderTimeHour = 1 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    if ($HrsWorked > $HrsPerDay) {
                                        var $OtHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - UnderTimeHour) + OTRate;
                                    }
                                    if ($HrsWorked <= $HrsPerDay) {
                                        var $OtHour2 = 0;
                                        var $underTimeHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2) + OTRate;
                                    }                                    
                                }
                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime);
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = ($HolRates + $LRates) * ($PayrollRatePerHour * $HrsPerDay) - UnderTimeHour;
                                }

                            }
                        }
                    }
                    if ($HrsWorked == 0 || $HrsWorked == null)
                    {
                        var rate = "0.00";
                    }
                    var $Rate2 = rate;
                }

                if ($AttendanceId == "Restday") {
                    if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                    if ($LRates !== null) { var $LRates = $LRates; } else { var $LRates = 1 }

                    if ($HrsWorked > 0 || $HrsWorked !== null) {
                        if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                        if ($HolidayDescription != "0") {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var UnderTimeHour = ($UnderTimeHour + Math.floor("2")) * $PayrollRatePerHour } else { var UnderTimeHour = 0 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour - MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    if ($HrsWorked > $HrsPerDay)
                                    {
                                        var $OtHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour) + OTRate;
                                    }
                                    if ($HrsWorked <= $HrsPerDay)
                                    {
                                        var $OtHour2 = 0;
                                        var $underTimeHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates * $LRates) * ($PayrollRatePerHour * $hrsPerDay) - $underTimeHour2) + OTRate;
                                    }
                                }
                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour - MinLatetime;
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour;
                                }

                            }
                        }
                        else
                        {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var $underTimeHour2 = ($UnderTimeHour + Math.floor("1")) * $PayrollRatePerHour } else { var $underTimeHour2 = 0 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - ($underTimeHour2 + MinLatetime) + OTRate;
                                }

                                if ($FlexiTime == "true") {
                                    if ($HrsWorked > $HrsPerDay)
                                    {
                                        var $OtHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2) + OTRate;
                                    }
                                    if ($HrsWorked <= $HrsPerDay)
                                    {
                                        var $OtHour2 = 0;
                                        var $underTimeHour3 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour2;
                                        var rate = (($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour3) + OTRate;
                                    }
                                }
                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - ($underTimeHour2 + MinLatetime);
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2;
                                }

                            }

                        }
                    }
                    if ($HrsWorked == 0 || $HrsWorked == null)
                    {
                        var rate = "0.00";
                    }
                    var $Rate2 = rate;
                }

                if ($AttendanceId == "Leave") {
                    if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                    if ($LRates !== null) { var $LRates = $LRates; } else { var $LRates = 1 }

                    if ($HrsWorked > 0 || $HrsWorked !== null) {
                        if ($HolRates !== null) { var $HolRates = $HolRates + 1; } else { var $HolRates = 1 }
                        if ($HolidayDescription != "0") {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var UnderTimeHour = ($UnderTimeHour + Math.floor("2")) * $PayrollRatePerHour } else { var UnderTimeHour = 0 }

                            if ($StrickOvertime == "true") {
                                if ($FlexiTime == "false") {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour - MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true") {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour) + OTRate;
                                }

                            }
                            if ($StrickOvertime == "false") {
                                if ($FlexiTime == "false") {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour - MinLatetime;
                                }
                                if ($FlexiTime == "true") {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - UnderTimeHour;
                                }

                            }
                        }
                        else
                        {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var $underTimeHour2 = ($UnderTimeHour + Math.floor("1")) * $PayrollRatePerHour } else { var $underTimeHour2 = 0 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - $underTimeHour2 - MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - $underTimeHour2) + OTRate;
                                }

                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - $underTimeHour2 - MinLatetime;
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = $HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay - $underTimeHour2;
                                }

                            }

                        }
                    }
                    if ($HrsWorked == 0 || $HrsWorked == null)
                    {
                        var rate = $payrollRatePerDay;
                    }
                    var $Rate2 = rate;

                }

                if ($AttendanceId == "Holiday")
                {
                    if ($HrsWorked > 0 || $HrsWorked !== null) {

                        if ($LRates !== null) { var $LRates = $LRates; } else { var $LRates = 1 }
                        if ($HolRates !== null) { var $HolRates = $HolRates; } else { var $HolRates = 1 }
                        if ($HolidayDescription != "0")
                        {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var UnderTimeHour = ($UnderTimeHour + Math.floor("2")) * $PayrollRatePerHour } else { var UnderTimeHour = 0 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = ($OTPerHourPercent * $PayrollRatePerHour) * $OtHour;
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    if ($HrsWorked > $HrsPerDay)
                                    {
                                        var otHour2 = $HrsWorked - $HrsPerDay;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * otHour2;
                                        var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay) - UnderTimeHour + OTRate;
                                    }
                                    if ($HrsWorked < $HrsPerDay)
                                    {
                                        var otHour2 = 0;
                                        var OTRate = $OTPerHourPercent * $PayrollRatePerHour * otHour2;
                                        var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay) - UnderTimeHour + OTRate;
                                    }
                                }
                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay) - (UnderTimeHour + MinLatetime);
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = ($HolRates * $LRates * $PayrollRatePerHour * $HrsPerDay) - UnderTimeHour;
                                }

                            }
                        }
                        else
                        {
                            if ($UnderTimeHour !== null && $UnderTimeHour !== 0) { var $underTimeHour2 = ($UnderTimeHour + Math.floor("1")) * $PayrollRatePerHour } else { var $underTimeHour2 = 0 }

                            if ($StrickOvertime == "true")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - ($underTimeHour2 + MinLatetime) + OTRate;
                                }
                                if ($FlexiTime == "true")
                                {
                                    var OTRate = $OTPerHourPercent * $PayrollRatePerHour * $OtHour;
                                    var rate = (($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2) + OTRate;
                                }

                            }
                            if ($StrickOvertime == "false")
                            {
                                if ($FlexiTime == "false")
                                {
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - ($underTimeHour2 + MinLatetime);
                                }
                                if ($FlexiTime == "true")
                                {
                                    var rate = ($HolRates * $LRates) * ($PayrollRatePerHour * $HrsPerDay) - $underTimeHour2;
                                }
                            }
                        }
                    }
                    if ($HrsWorked == 0 || $HrsWorked == null)
                    {
                        var rate = $payrollRatePerDay;
                    }
                    var $Rate2 = rate;
                }

                if ($AttendanceId == "Absent") {
                    var $Rate2 = "0.00"
                }

                var RateAmount = parseInt($Rate2);
                if (RateAmount <= 0)
                {
                    $rateR = "0.00";
                }
                else
                {
                    $rateR = (Math.round($Rate2 * 100) / 100).toFixed(2);
                }
                //rate

                item = {};
                item["EmpId"] = $EmpId;
                item["No"] = $No;
                item["AttRecId"] = $AttRecId;
                item["AttendanceId"] = $AttendanceId;
                item["Days"] = $Days;
                item["Date"] = $Date;
                item["DateIn"] = DateIn;
                item["WorkIn"] = $WorkIn;
                item["TimeIn"] = $TimeIn;
                item["LateIn"] = $LateIn;
                item["MinLates"] = FlexMinLates;
                item["WorkOut"] = $WorkOut;
                item["TimeOut"] = $TimeOut;
                item["HrsWorked"] = $HrsWorked;
                item["OtHour"] = $Ot;
                item["UnderTimeHour"] = $UnderTimeHour1;
                item["HolidayDescription"] = $HolidayDescription;
                item["OTPerHourPercent"] = $OTPerHourPercent;
                item["Holiday"] = $Holiday;
                item["HolRates"] = $HolRates;
                item["EnTitlement"] = $EnTitlement;
                item["LRates"] = $LRates;
                item["HrsPerDay"] = $HrsPerDay;
                item["PayrollRatePerHour"] = $PayrollRatePerHour;
                item["DeductionLate"] = $DeductionLate;
                item["PayrollRatePerDay"] = $PayrollRatePerDay;
                item["Rates"] = $rateR;
                item["Description1"] = $Description1;
                item["Description2"] = $Description2;
                item["Status"] = $Status;

                viewData.empAttDetailsInputs.push(item);
            }

            abp.message.confirm(
                'Employee Attendance be created.',
                'Are you sure?',
                function (isConfirmed) {
                    if (isConfirmed) {
                        abp.ui.setBusy(_$FormAttidDetails);
                        _empAttRecordService.createEmpAtt(viewData).done(function (result) {
                            if (result === null || result === "0") { return; }
                            abp.notify.success('Employee Attendace Save', 'Success');
                            //window.location.href = abp.appPath + 'SalesOrders/Edit?id=' + result;
                        }).always(function () {
                            GetEmployees();
                            document.getElementById("completeName").value = "";
                            document.getElementById("EmpCode").value = "";
                            document.getElementById("TimeIn").value = "";
                            document.getElementById("TimeIOut").value = "";
                            //document.getElementById("attid").value = "";
                            abp.ui.clearBusy(_$FormAttidDetails);
                        });
                    }
                }
            );
        }
    });
})(jQuery);