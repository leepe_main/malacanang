﻿$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
var $month = (new Date().getMonth() + 1);
var mdayone = ($month.length > 1 ? $month : $month) + "/01/" + new Date().getFullYear();
$('#DateFrom').val(mdayone);
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});


$('select').selectpicker();

(function () {
    $(function () {

        var _$quotationsTable = $('#QuotationsTable');
        var _quotationService = abp.services.app.quotationService;

        var _permissions = {
            create: abp.auth.hasPermission('Page.Quotations.Create'),
            edit: abp.auth.hasPermission('Page.Quotations.Edit'),
            'delete': abp.auth.hasPermission('Page.Quotations.Delete')
        };

        var dataTable = _$quotationsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _quotationService.getQuotations,
                inputFilter: function () {
                    var $id = $('#SearchFilter').val();
                    var $client = 'null';
                    var $statusid = $('#StatusTypes').val();
                    var $datefrom = $('#DateFrom').val();
                    var $dateto = $('#DateTo').val();
                    var $clientid = 'null';
                    var $accountexecutive = 'null';
                    if (!abp.auth.isGranted("Pages.Quotations.AllAccounts")) {
                        var empid = $('#h1').val(); //getUserEmployee(abp.session.userId);
                        console.log(empid);
                        $accountexecutive = empid;
                    }
                    if ($id === '') {
                        $id = 'null';
                    }
                    return {
                        filter: $id + '|' + $client + '|' + $statusid + '|' + $datefrom + '|' + $dateto + '|' + $clientid + '|' + $accountexecutive   
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    orderData: 8,
                    targets: 1,
                    data: { id: "id", code: "code", revisionNo: "revisionNo" },
                    "render": function (data) {
                        return '<a id="view-quotation" title="view" href="#" class="view-quotation" data-quotation-id="' + data.id + '" data-quotation-code="' + data.code + '-' + data.revisionNo + '">' + data.code + '-' + data.revisionNo + '</i></a>';
                    }
                },
                {
                    targets: 2,
                    data: "client"
                }
                ,
                {
                    targets: 3,
                    "data": "transactionTime",
                    "render": function (data) {
                        var tt = new Date(data);
                        return getFormattedDate(tt);
                    }
                },
                {
                    targets: 4,
                    data: "agent"
                },
                {
                    orderData: 9,
                    targets: 5,
                    data: { status: "status", statusId : "statusId"},
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-danger">' + data.status + '</span>';
                        }
                        else if (data.statusId === 4) {
                            return '<span class="badge badge-primary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 5) {
                            return '<span class="badge badge-info">' + data.status + '</span>';
                        }
                        else if (data.statusId === 6) {
                            return '<span class="badge badge-warning">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                },
                {
                    targets: 6,
                    data: "grandTotal",
                    render: $.fn.dataTable.render.number(',', '.', 2),
                    className: 'text-right'
                },
                {
                    orderable: false,
                    targets: 7,
                    class: "text-center",
                    data: { id: "id", code: "code" },
                    "render": function (data) {
                        return '<a id="edit-order" title="edit" href="#" class="edit-order" data-order-id="' + data.id + '"><i class="fa fa-pencil-square-o"></i></a>|<a id="delete-order" title="delete" href="#" class="delete-order" data-order-id="' + data.id + '" data-order-code="' + data.code + '"><i class="fa fa-trash"></i></a>';
                    }
                },
                {
                    visible: false,
                    targets: 8,
                    data: "code"
                },
                {
                    visible: false,
                    targets: 9,
                    data: "statusId"
                }
            ]
        });

        // View record
        _$quotationsTable.on('click', 'a.view-quotation', function (e) {
            e.preventDefault();
            var clientId = $(this).attr("data-quotation-id");
            window.location.href = abp.appPath + 'Quotations/Details?id=' + clientId;
        });

        // Edit record
        _$quotationsTable.on('click', 'a.edit-order', function (e) {
            e.preventDefault();
            var orderId = $(this).attr("data-order-id");
            window.location.href = abp.appPath + 'Quotations/Edit?id=' + orderId;
        });

        // Delete record
        _$quotationsTable.on('click', 'a.delete-product', function (e) {
            var productId = $(this).attr("data-product-id");
            var productName = $(this).attr("data-product-name");
            var productCode = $(this).attr("data-product-code");

            e.preventDefault();
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('DeleteProductConfirmation', 'ezinvmvc'), productName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _productService.deleteProduct({
                            id: productId
                        }).done(function () {

                            $.ajax({
                                url: abp.appPath + 'Products/RemoveFile?code=' + productCode,
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                success: function () { },
                                error: function (e) { }
                            });

                            getQuotations();
                        });
                    }
                }
            );
        });

        function getQuotations() {
            dataTable.ajax.reload();
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#ExportCompanyToExcelButton').click(function () {
            _companyService
                .getProductsToExcel({})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getQuotations();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getQuotations();
        });

        $('#ProductTableFilter').focus();

        $("#StatusTypes").change(function () {
            getQuotations();
        });

    });
})();
