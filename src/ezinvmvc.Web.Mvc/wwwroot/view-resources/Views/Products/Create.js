﻿function decimalOnly(txt) {
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode === 46) {
        var txtbx = document.getElementById(txt);
        var amount = document.getElementById(txt).value;
        var present = 0;
        var count = 0;

        //if (amount.indexOf(".", present) || amount.indexOf(".", present + 1));
        //{}
        do {
            present = amount.indexOf(".", present);
            if (present !== -1) {
                count++;
                present++;
            }
        }
        while (present !== -1);
        if (present === -1 && amount.length === 0 && event.keyCode === 46) {
            event.keyCode = 0;
            return false;
        }

        if (count >= 1 && event.keyCode === 46) {

            event.keyCode = 0;
            return false;
        }
        if (count === 1) {
            var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
            if (lastdigits.length >= 2) {
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    else {
        event.keyCode = 0;
        return false;
    }
}

//Image Upload

$('.custom-file-input').on('change', function () {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#filepreview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#fileinput").change(function () {
    readURL(this);
});

//Save Button
(function ($) {

    var _productService = abp.services.app.productService;
    var _categoryService = abp.services.app.categoryService;
    var _brandService = abp.services.app.brandService;
    var _unitService = abp.services.app.unitService;
    var _costingTypeService = abp.services.app.costingTypeService;
    var _vendorService = abp.services.app.vendorService;

    var _$form = $('form[name=ProductForm]');

    function getcategories() {
        var selectoptions = $('#lbCategories');
        selectoptions.empty();
        _categoryService.getCategories().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                selectoptions.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
            }
            selectoptions.selectpicker('refresh');
        });
    }
    function getbrands() {
        var selectoptions = $('#lbBrands');
        selectoptions.empty();
        _brandService.getBrands().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                selectoptions.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
            }
            selectoptions.selectpicker('refresh');
        });
    }
    function getunits() {
        var priceunits = $('#UnitId');
        priceunits.empty();
        _unitService.getUnits().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                priceunits.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
            }
            priceunits.selectpicker('refresh');
        });
    }
    function getcostingtypes() {
        var selectoptions = $('#CostingTypeId');
        selectoptions.empty();
        _costingTypeService.getCostingTypes().done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                selectoptions.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
            }
            selectoptions.selectpicker('refresh');
        });
    }
    function getvendors() {
        var selectoptions = $('#Vendors');
        selectoptions.empty();
        _vendorService.getVendors({ filter: ''}).done(function (result) {
            for (var i = 0; i < result.items.length; i++) {
                selectoptions.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
            }
            selectoptions.selectpicker('refresh');
        });
    }
    getcategories();
    getbrands();
    getunits();
    getcostingtypes();
    getvendors();

    function saveProduct() {
        if (!_$form.valid()) {
            return;
        }
        var formData = new FormData();
        formData.append('file', $('#fileinput')[0].files[0]);

        var product = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        if ($('#fileinput')[0].files.length !== 0)
        {
            product.imageName = $('#fileinput')[0].files[0].name;
        }

        var $c = product.Code;

        abp.message.confirm(
            'New product will be added.',
            'Are you sure?',
            function (isConfirmed) {
                if (isConfirmed) {
                    abp.ui.setBusy(_$form);
                    _productService.createProduct(product).done(function (result) {
                        $.ajax({
                            url: abp.appPath + 'Products/UploadFile?code=' + result,
                            type: 'POST',
                            data: formData,
                            processData: false, 
                            contentType: false,  
                            success: function () {
                                //$("#ProductForm")[0].reset();
                                //$("#fileinput").val('');
                                //$("#filepreview").attr("src", "../images/default.png");
                                //$("#fileinputlabel").html("Choose file...");
                                abp.message.success('New product added successfully', 'Success');
                                window.location.href = abp.appPath + 'Products/Edit?id=' + result;

                            },
                            error: function (e) { }
                        });
                    }).always(function () {
                        abp.ui.clearBusy(_$form);
                    });
                }
            }
        );
    }

    //Handle save button click
    $('#SaveProductButton').click(function (e) {
        e.preventDefault();
        saveProduct();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            saveProduct();
        }
    });

    $('#CreateCategoryButton').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: abp.appPath + 'Products/CreateCategoryModal',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#CategoryCreateModal div.modal-content').html(content);
            },
            error: function (e) { }
        });
    });

    $('#CreateBrandButton').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: abp.appPath + 'Products/CreateBrandModal',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#BrandCreateModal div.modal-content').html(content);
            },
            error: function (e) { }
        });
    });

    $('#CreateUnitButton').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: abp.appPath + 'Products/CreateUnitModal',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                getunits();
                $('#UnitCreateModal div.modal-content').html(content);
            },
            error: function (e) { }
        });
    });

})(jQuery);




