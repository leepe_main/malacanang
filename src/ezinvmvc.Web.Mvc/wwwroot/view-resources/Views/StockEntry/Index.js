﻿$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
var $month = (new Date().getMonth() + 1);
var mdayone = ($month.length > 1 ? $month : $month) + "/01/" + new Date().getFullYear();
$('#DateFrom').val(mdayone);
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});


$('select').selectpicker();

abp.ui.block();

(function () {
    $(function () {

        var _commonService = abp.services.app.commonService;
        var _stockEntryService = abp.services.app.stockEntryService;
        var _warehouseService = abp.services.app.warehouseService;
        var _$Table = $('#ListTable');

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Stock.Entry.Create'),
            edit: abp.auth.hasPermission('Pages.Stock.Entry.Edit'),
            'delete': abp.auth.hasPermission('Pages.Stock.Entry.Delete')
        };


        function getentrytype() {
            var selectoptions = $('#EntryTypes');
            selectoptions.empty();
            _commonService.getEntryTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    selectoptions.append('<option value=' + result.items[i].id + ' data - code=' + result.items[i].code + '>' + result.items[i].name + '</option>');
                }
                selectoptions.selectpicker('refresh');
            });
        }
        getentrytype();
        function getwarehouses() {
            var selectoptionsources = $('#DefaultSources');
            var selectoptiondistinations = $('#DefaultDestinations');
            selectoptionsources.empty();
            selectoptiondistinations.empty();
            _warehouseService.getWarehouses().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    selectoptionsources.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    selectoptiondistinations.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                selectoptionsources.selectpicker('refresh');
                selectoptiondistinations.selectpicker('refresh');
                abp.ui.unblock();
            });
        }
        getwarehouses();

        var dataTable = _$Table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            searching: false,
            listAction: {
                ajaxFunction: _stockEntryService.getStockEntries,
                inputFilter: function () {
                    var $id = $('#SearchFilter').val();
                    var $entrytypeid = $('#EntryTypes').val();
                    var $statusid = $('#StatusTypes').val();
                    var $datefrom = $('#DateFrom').val();
                    var $dateto = $('#DateTo').val();
                    var $source = $('#DefaultSources').val();
                    var $destination = $('#DefaultDestinations').val();
                    if ($id === '') {
                        $id = 'null';
                    }
                    return {
                        filter: $id + '|' + $entrytypeid + '|' + $statusid + '|' + $datefrom + '|' + $dateto + '|' + $source + '|' + $destination
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    targets: 1,
                    data: "entryType"
                },
                {
                    orderData: 7,
                    targets: 2,
                    data: { status: "status", statusId: "statusId" },
                    "render": function (data) {
                        if (data.statusId === 1) {
                            return '<span class="badge badge-warning">' + data.status + '</span>';
                        }
                        else if (data.statusId === 2) {
                            return '<span class="badge badge-success">' + data.status + '</span>';
                        }
                        else if (data.statusId === 3) {
                            return '<span class="badge badge-primary">' + data.status + '</span>';
                        }
                        else if (data.statusId === 4) {
                            return '<span class="badge badge-danger">' + data.status + '</span>';
                        }
                        else {
                            return '<span class="badge badge-secondary">' + data.status + '</span>';
                        }
                    }
                },
                {
                    orderData: 8,
                    targets: 3,
                    data: { defaultSource: "defaultSource", entryTypeId: "entryTypeId",defaultDestination: "defaultDestination" },
                    "render": function (data) {
                        if (data.entryTypeId === 1) {
                            return '<span class="text-muted">' + data.defaultSource + '</span>';
                        }
                        else if (data.entryTypeId === 2) {
                            return '<span class="text-muted"> ' + data.defaultSource + '</span>';
                        }
                        else if (data.entryTypeId === 3) {
                            return '<span class="text-muted"> ' + data.defaultSource + '&nbsp;>&nbsp;' + data.defaultDestination + '</span>';
                        }
                        else {
                            return '<span class="text-muted">' + data.defaultSource + '</span>';
                        }
                    }
                },
                {
                    targets: 4,
                    data: "code"
                },
                {
                    targets: 5,
                    "data": "transactionTime",
                    "render": function (data) {
                        var tt = new Date(data);
                        return getFormattedDate(tt);
                    }
                },
                {
                    orderable: false,
                    targets: 6,
                    class: "text-center",
                    data: { id: "id", code: "code" },
                    "render": function (data) {
                        return '<a id="edit" title="details" href="#" class="edit" data-id="' + data.id + '"><i class="fa fa-lg fa-pencil-square-o"></i></a>';
                    }
                },
                {
                    visible: false,
                    targets: 7,
                    data: "status"
                },
                {
                    visible: false,
                    targets: 8,
                    data: "defaultSource"
                }
            ]
        });

        // View record
        _$Table.on('click', 'a.view-quotation', function (e) {
            e.preventDefault();
            var clientId = $(this).attr("data-quotation-id");
            window.location.href = abp.appPath + 'Quotations/Details?id=' + clientId;
        });

        // Edit record
        _$Table.on('click', 'a.edit', function (e) {
            e.preventDefault();
            var orderId = $(this).attr("data-id");
            window.location.href = abp.appPath + 'StockEntry/Edit?id=' + orderId;
        });

        // Delete record
        //_$Table.on('click', 'a.delete-product', function (e) {
        //    var productId = $(this).attr("data-product-id");
        //    var productName = $(this).attr("data-product-name");
        //    var productCode = $(this).attr("data-product-code");

        //    e.preventDefault();
        //    abp.message.confirm(
        //        abp.utils.formatString(abp.localization.localize('DeleteProductConfirmation', 'ezinvmvc'), productName),
        //        function (isConfirmed) {
        //            if (isConfirmed) {
        //                _productService.deleteProduct({
        //                    id: productId
        //                }).done(function () {

        //                    $.ajax({
        //                        url: abp.appPath + 'Products/RemoveFile?code=' + productCode,
        //                        type: 'POST',
        //                        processData: false,
        //                        contentType: false,
        //                        success: function () { },
        //                        error: function (e) { }
        //                    });

        //                    getQuotations();
        //                });
        //            }
        //        }
        //    );
        //});

        function getDataList() {
            dataTable.ajax.reload();
        }

        $('#SearchButton').click(function (e) {
            e.preventDefault();
            getDataList();
        });

        $('#SearchFilter').on('keydown', function (e) {
            if (e.keyCode !== 13) {
                return;
            }
            e.preventDefault();
            getDataList();
        });

        $('#SearchFilter').focus();

        $("#StatusTypes").change(function () {
            getDataList();
        });

        $("#DefaultSources").change(function () {
            getDataList();
        });

        $("#DefaultDestinations").change(function () {
            getDataList();
        });

        $("#EntryTypes").change(function () {
            getDataList();
        });

    });
})();
