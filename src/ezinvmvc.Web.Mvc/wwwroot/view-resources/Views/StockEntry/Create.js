﻿abp.ui.block();

function decimalOnly(txt) {
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode === 46) {
        var txtbx = document.getElementById(txt);
        var amount = document.getElementById(txt).value;
        var present = 0;
        var count = 0;

        do {
            present = amount.indexOf(".", present);
            if (present !== -1) {
                count++;
                present++;
            }
        }
        while (present !== -1);
        if (present === -1 && amount.length === 0 && event.keyCode === 46) {
            event.keyCode = 0;
            return false;
        }

        if (count >= 1 && event.keyCode === 46) {

            event.keyCode = 0;
            return false;
        }
        if (count === 1) {
            var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
            if (lastdigits.length >= 2) {
                event.keyCode = 0;
                return false;
            }
        }
        return true;
    }
    else {
        event.keyCode = 0;
        return false;
    }
}
$(".date-picker").datepicker("update", new Date());
$('.date-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L'
});
$('.datetime-picker').datepicker({
    locale: abp.localization.currentLanguage.name,
    format: 'L LT'
});

(function () {
    $(function () {
        var _productPriceService = abp.services.app.productPriceService;
        var _productService = abp.services.app.productService;
        var _companyService = abp.services.app.companyService;
        var _commonService = abp.services.app.commonService;
        var _stockEntryService = abp.services.app.stockEntryService;
        var _warehouseService = abp.services.app.warehouseService;
        var _$form = $('form[name=StockEntryForm]');
        var _$itemsTable = $('#ItemsTable');
        $("#ProductImage").hide();
        $("#EProductImage").hide();

        function getcompanies() {
            var companies = $('#Companies');
            companies.empty();
            _companyService.getCompanies().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (result.items[i].isDefault === true) {
                        companies.append('<option value=' + result.items[i].id + ' selected>' + result.items[i].name + '</option>');
                        getseriestype(result.items[i].id);
                    }
                    else {
                        companies.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    }
                }
                companies.selectpicker('refresh');
            });
        }
        getcompanies();
        function getseriestype(companyid) {
            var series = $('#Series');
            series.empty();
            _commonService.getSeriesTypesFiltered({ id: 0, transactionCode: 107, companyId: companyid }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    series.append('<option value=' + result.items[i].id + '>' + result.items[i].prefix + '</option>');
                }
                series.selectpicker('refresh');
            });
        }
        function getentrytype() {
            var selectoptions = $('#EntryTypes');
            selectoptions.empty();
            selectoptions.append('<option value=0 >-- Select --</option>');
            _commonService.getEntryTypes().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    selectoptions.append('<option value=' + result.items[i].id + ' data - code=' + result.items[i].code+ '>' + result.items[i].name + '</option>');
                }
                selectoptions.selectpicker('refresh');
            });
        }
        getentrytype();

        function getwarehouses() {
            var selectoptionsources = $('#DefaultSources');
            var selectoptiondistinations = $('#DefaultDestinations');
            selectoptionsources.empty();
            selectoptiondistinations.empty();

            selectoptionsources.append('<option value=0>-- Select --</option>');
            selectoptiondistinations.append('<option value=0>-- Select --</option>');

            _warehouseService.getWarehouses().done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    selectoptionsources.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                    selectoptiondistinations.append('<option value=' + result.items[i].id + '>' + result.items[i].name + '</option>');
                }
                selectoptionsources.selectpicker('refresh');
                selectoptiondistinations.selectpicker('refresh');
                abp.ui.unblock();
            });
        }
        getwarehouses();
        //abp.ui.unblock();
 
        //Item Autocomplete
        var getproducts = function (request, response) {
            _productService.getProductByName({ filter: request.term }).done(function (result) {
                response($.map(result.items, function (el) {
                    return {
                        label: el.name + ' ' + el.code,
                        value: el.id
                    };
                }));
            });
        };
        function getproduct() {
            var $productid = $('#ProductId').val();
            _productService.getProduct({ id: $productid }).done(function (result) {
                $('#ProductCode').val(result.code);
                $("#ProductName").val(result.name);
                $('#PerDescription').val(result.description);
                if (result.imageName !== null && result.imageName !== '') {
                    $("#ProductImage").attr("src", abp.appPath + "products/" + result.id + "/" + result.imageName);
                    $("#ProductImage").show();
                }
                else {
                    $("#ProductImage").hide();
                }
            });
        }
        function getproductunits() {
            var units = $('#Units');
            var $productid = $('#ProductId').val();
            units.empty();
            _productService.getProductUnits({
                id: $productid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    units.append('<option value=' + result.items[i].unitId + '>' + result.items[i].unit + '</option>');
                }
                units.selectpicker('refresh');
            });
        }
        function getproductprice() {
            $("#Price").val("0.00");
            var $unitid = $('#Units').val();
            var $pricingtypeid = $('#PricingTypes').val();
            var $productid = $('#ProductId').val();
            if ($unitid === null) {
                $unitid = 0;
            }
            if ($pricingtypeid === null) {
                $pricingtypeid = 0;
            }
            _productPriceService.getProductPrices({
                productId: $productid, pricingTypeId: $pricingtypeid, unitId: $unitid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var price = currencyFormat(result.items[i].unitPrice);
                    $("#Price").val(result.items[i].unitPrice ? price : "0.00");
                }
            });
        }
        function getproductstock() {
            $("#Stocks").val("0");
            var warehouseid = $('#DefaultSources').val(); 
            var $productid = $('#ProductId').val();
            _stockEntryService.getStockSummary({ filter: warehouseid + '|' + $productid + '||'}).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var stocks = currencyFormat(result.items[i].qty);
                    $("#Stocks").val(result.items[i].qty ? stocks : "0");
                }
            });
        }
        var selectproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item ? ui.item.value : "");
            //$("#ProductName").val(ui.item ? ui.item.label : "");
            getproduct();
            getproductunits();
            getproductprice();
            getproductstock();
            return false;
        };
        var focusproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item.value);
            //$("#ProductName").val(ui.item.label);
            //$("#PerDescription").val(ui.item.label);
            getproduct();
            getproductunits();
            getproductprice();
            getproductstock();
        };
        var changeproduct = function (event, ui) {
            event.preventDefault();
            $("#ProductId").val(ui.item ? ui.item.value : "");
            //$("#ProductName").val(ui.item ? ui.item.label : "");
            if (ui.item === null) {
                $("#Stocks").val("");
                $("#ProductCode").val("");
                $("#ProductName").val("");
                $("#Quantity").val("");
                $("#Price").val("");
                $("#PerDescription").val("");
                var units = $('#Units');
                units.empty();
                units.selectpicker('refresh');
                $("#ProductImage").hide();
            }
        };
        $("#ProductName").autocomplete({
            source: getproducts,
            select: selectproduct,
            focus: focusproduct,
            minLength: 2,
            delay: 100,
            change: changeproduct
        });
        $('#Units').on('change', function (e) {
            getproductprice();
        });
        //Item Autocomplete

        //Edit Item Autocomplete
        var editgetproducts = function (request, response) {
            _productService.getProductByName({ filter: request.term }).done(function (result) {
                response($.map(result.items, function (el) {
                    return {
                        label: el.name + ' ' + el.code,
                        value: el.id
                    };
                }));
            });
        };
        function editgetproduct() {
            var $productid = $('#EProductId').val();
            _productService.getProduct({ id: $productid }).done(function (result) {
                $('#EProductCode').val(result.code);
                $('#EProductName').val(result.name);
                $('#EPerDescription').val(result.description);
                if (result.imageName !== null && result.imageName !== '') {
                    $("#EProductImage").attr("src", abp.appPath + "products/" + result.id + "/" + result.imageName);
                    $("#EProductImage").show();
                }
                else {
                    $("#EProductImage").hide();
                }
            });
        }
        function editgetproductunits(unitid) {
            var units = $('#EUnits');
            var $productid = $('#EProductId').val();
            units.empty();
            _productService.getProductUnits({
                id: $productid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    if (unitid === result.items[i].id) {
                        units.append('<option value=' + result.items[i].unitId + ' selected>' + result.items[i].unit + '</option>');
                    }
                    else {
                        units.append('<option value=' + result.items[i].unitId + '>' + result.items[i].unit + '</option>');
                    }
                }
                units.selectpicker('refresh');
            });
        }
        function editgetproductprice() {
            $("#EPrice").val("0.00");
            var $unitid = $('#EUnits').val();
            var $pricingtypeid = $('#EPricingTypes').val();
            var $productid = $('#EProductId').val();
            if ($unitid === null) {
                $unitid = 0;
            }
            if ($pricingtypeid === null) {
                $pricingtypeid = 0;
            }
            _productPriceService.getProductPrices({
                productId: $productid, pricingTypeId: $pricingtypeid, unitId: $unitid
            }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var price = currencyFormat(result.items[i].unitPrice);
                    $("#EPrice").val(result.items[i].unitPrice ? price : "");
                }
            });
        }
        function editgetproductstock() {
            var warehouseid = $('#DefaultSources').val();
            $("#EStocks").val("0");
            var $productid = $('#EProductId').val();
            _stockEntryService.getStockSummary({ filter: warehouseid + '|' + $productid + '||' }).done(function (result) {
                for (var i = 0; i < result.items.length; i++) {
                    var stocks = currencyFormat(result.items[i].qty);
                    $("#EStocks").val(result.items[i].qty ? stocks : "0");
                }
            });
        }
        var editselectproduct = function (event, ui) {
            event.preventDefault();
            $("#EProductId").val(ui.item ? ui.item.value : "");
            //$("#EProductName").val(ui.item ? ui.item.label : "");
            editgetproduct();
            editgetproductunits(0);
            editgetproductprice();
            editgetproductstock();
            return false;
        };
        var editfocusproduct = function (event, ui) {
            event.preventDefault();
            $("#EProductId").val(ui.item.value);
            ////$("#EProductName").val(ui.item.label);
            //$("#PerDescription").val(ui.item.label);
            editgetproduct();
            editgetproductunits(0);
            editgetproductprice();
            editgetproductstock();
        };
        var editchangeproduct = function (event, ui) {
            event.preventDefault();
            $("#EProductId").val(ui.item ? ui.item.value : "");
            //$("#EProductName").val(ui.item ? ui.item.label : "");
            if (ui.item === null) {
                $("#EStocks").val("");
                $("#EProductName").val("");
                $("#EProductCode").val("");
                $("#EQuantity").val("");
                $("#EPrice").val("");
                $("#EPerDescription").val("");
                var units = $('#Units');
                units.empty();
                units.selectpicker('refresh');
                $("#EProductImage").hide();
            }
        };
        $("#EProductName").autocomplete({
            source: editgetproducts,
            select: editselectproduct,
            focus: editfocusproduct,
            minLength: 2,
            delay: 100,
            change: editchangeproduct
        });
        $('#EUnits').on('change', function (e) {
            editgetproductprice();
        });
        //Edit Item Autocomplete


        //Other Charges

        //Datatable Add
        var dataTable = _$itemsTable.DataTable({
            responsive: true,
            paging: false,
            "bInfo": false,
            searching: false,
            columnDefs: [{
                "visible": false,
                targets: [3, 4, 6, 7, 8, 9]
            },
            {
                orderable: false,
                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
            {
                render: $.fn.dataTable.render.number(',', '.', 2),
                className: 'text-right',
                targets: [3, 4]
            },
            {
                className: 'text-right',
                targets: [2, 5]
            },
            {
                className: 'text-center',
                targets: [0]
            }
            ]
        });
        function addnewitem() {

            var $stocks = $('#Stocks').val();
            var $productid = $('#ProductId').val();
            var $productcode = $('#ProductCode').val();
            var $productname = $('#ProductName').val();
            var $unitid = $('#Units').val();
            var $unit = $("#Units option:selected").html();
            var $quantity = $('#Quantity').val();
            var $price = $('#Price').val();
            var $perdescription = $('#PerDescription').val();
            
            if ($('#DefaultSources').val() === '0'){
                abp.notify.error('Select your Source Warehouse.', 'Warning');
                return;
            }
            if ($productid === '' || $productcode === '' || $productname === '' || $quantity === '' || $price === '' || $stocks ==='') { return; }

            var stocks = parseFloat($stocks);
            var quantity = parseFloat($quantity);
            var quantityadded = getAddQuantity($productid, -1);
            if ($('#EntryTypes').val() === '1' || $('#EntryTypes').val() === '3') {
                if (quantity + quantityadded > stocks) {
                    abp.notify.error('Not enough stocks.', 'Warning');
                    return;
                }
            }
            var price = parseFloat($price.replace(',', ''));

            var total = price * quantity;
            var datacount = dataTable.rows().count();
            var itemno = datacount + 1;
            dataTable.row.add([itemno,
                '<a href="#" class="btn-link">' + $productcode + '</a><br /><small><span class="text-muted">' + $productname + '</span></small>',
                '<span class="text-muted">' + $quantity + '</span>|<span class="text-muted">' + $unit + '</span>',
                price,
                total,
                '<a id="edit-item" class="edit-item" title="edit" href="#" data-toggle="modal" data-target="#ItemEditModal" data-itemno="'+itemno+'"  data-id="' + $productid + '" data-unitid="' + $unitid + '" data-perdesc="' + $perdescription + '" data-qty="' + $quantity + '" data-price="' + price + '" ><i class="fa fa-edit"></i></a> | <a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>',
                $productid, $perdescription, $quantity, $unitid
            ]).draw();
            computeTotal();
            $('#ProductId').val("");
            $('#ProductCode').val("");
            $('#ProductName').val("");
            $('#Units').empty();
            $('#Units').selectpicker('refresh');
            $('#Quantity').val("");
            $('#Price').val("");
            $('#PerDescription').val("");
            $("#ProductImage").hide();
            $('#Stocks').val("");
            abp.notify.success('Item #' + itemno + ' added!', 'Success');
        }
        function computeTotal() {
            var grandtotal = 0;
            dataTable.column(8).data()
                .each(function (value, index) {
                    var $grandtotal = parseFloat(value);
                    grandtotal = grandtotal + $grandtotal;
                });
            $('#GrandTotal').val(currencyFormat(grandtotal));
        }
        function currencyFormat(num) {
            return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
      
        function save() {
            if (!_$form.valid()) {
                return;
            }


            var disabled = _$form.find(':input:disabled').removeAttr('disabled');
            var formdata = _$form.serializeFormToObject();


            if (formdata.EntryTypeId === '0') {
                abp.notify.warn('Select your Entry Type.', 'Warning');
                return;
            }
            if (formdata.DefaultSourceId === '0') {
                abp.notify.warn('Select your Source Warehouse.', 'Warning');
                return;
            }

            if (formdata.EntryTypeId === '1' || formdata.EntryTypeId === '2') {
                formdata.DefaultDestinationId = formdata.DefaultSourceId;
            }
            if (formdata.EntryTypeId === '3') {
                if (formdata.DefaultDestinationId === '0') {
                    abp.notify.warn('Select your Destination Warehouse.', 'Warning');
                    return;
                }
                if (formdata.DefaultDestinationId === formdata.DefaultSourceId ) {
                    abp.notify.warn('Select different Destination Warehouse.', 'Warning');
                    return;
                }
            }
          
            
            //items
            var table = _$itemsTable.DataTable();
            var form_data = table.rows().data();
            var f = form_data;
            if (f.length === 0) {
                abp.notify.warn('Add your item first.', 'Warning');
                    return;
            }
            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var viewData = {
                stockentry: {
                    "companyId": formdata.CompanyId,
                    "seriesTypeId": formdata.SeriesTypeId,
                    "prefix": $("#Series option:selected").html(),
                    "code": "0",
                    "transactionTime": formdata.TransactionTime + ' ' + time,
                    "entryTypeId": formdata.EntryTypeId,
                    "defaultSourceId": formdata.DefaultSourceId,
                    "defaultDestinationId": formdata.DefaultDestinationId,
                    "notes": formdata.Notes,
                    "statusId": "1"
                },
                stockentryitem: []
            };
            disabled.attr('disabled', 'disabled');

            for (var i = 0; f.length > i; i++) {
                item = {};
                item["StockEntryId"] = "0";
                item["ProductId"] = f[i][6];
                item["Description"] = f[i][7];
                item["QtyRel"] = f[i][8];
                item["Qty"] = f[i][8];
                item["UnitId"] = f[i][9];
                item["UnitPrice"] = f[i][3];
                item["Total"] = f[i][4];
                viewData.stockentryitem.push(item);
            }

            abp.message.confirm(
                'New stock entry will be created.',
                'Are you sure?',
                function (isConfirmed) {
                    if (isConfirmed) {
                        abp.ui.setBusy(_$form);
                        _stockEntryService.createStockEntry(viewData).done(function (result) {
                            abp.message.success('Stock Entry created', 'Success');
                            window.location.href = abp.appPath + 'StockEntry/Edit?id=' + result;
                        }).always(function () {
                            abp.ui.clearBusy(_$form);
                        });
                    }
                }
            );
        }

        $('#EntryTypes').on('change', function (e) {
            //e.preventDefault();
            //_$itemsTable.dataTable().clear();

            $("#Stocks").val("");
            $("#ProductCode").val("");
            $("#ProductName").val("");
            $("#Quantity").val("");
            $("#Price").val("");
            $("#PerDescription").val("");
            var units = $('#Units');
            units.empty();
            units.selectpicker('refresh');
            $("#ProductImage").hide();

            $("#EStocks").val("");
            $("#EProductCode").val("");
            $("#EProductName").val("");
            $("#EQuantity").val("");
            $("#EPrice").val("");
            $("#EPerDescription").val("");
            var eunits = $('#EUnits');
            eunits.empty();
            eunits.selectpicker('refresh');
            $("#EProductImage").hide();

            _$itemsTable.dataTable().fnClearTable();
            _$itemsTable.dataTable().fnDraw();
            computeTotal();
            var $entrytypeid = $('#EntryTypes').val();
            if ($entrytypeid === '1') {
                $('#SourceWarehouseLabel').text("Target Warehouse");
                $("#divDestinationWarehouse").hide();
            }
            else if ($entrytypeid === '2') {
                $('#SourceWarehouseLabel').text("Target Warehouse");
                $("#divDestinationWarehouse").hide();
            }
            else if ($entrytypeid === '3') {
                $('#SourceWarehouseLabel').text("Source Warehouse");
                $("#divDestinationWarehouse").show();
            }
            else {
                $('#SourceWarehouseLabel').text("Source Warehouse");
                $("#divDestinationWarehouse").show();
            }

        });
        $('#DefaultSources').on('change', function (e) {
            //e.preventDefault();
            $("#Stocks").val("");
            $("#ProductCode").val("");
            $("#ProductName").val("");
            $("#Quantity").val("");
            $("#Price").val("");
            $("#PerDescription").val("");
            var units = $('#Units');
            units.empty();
            units.selectpicker('refresh');
            $("#ProductImage").hide();
            $("#EStocks").val("");
            $("#EProductCode").val("");
            $("#EProductName").val("");
            $("#EQuantity").val("");
            $("#EPrice").val("");
            $("#EPerDescription").val("");
            var eunits = $('#EUnits');
            eunits.empty();
            eunits.selectpicker('refresh');
            $("#EProductImage").hide();
            _$itemsTable.dataTable().fnClearTable();
            _$itemsTable.dataTable().fnDraw();
            computeTotal();
        });
        $('#SaveButton').click(function (e) {
            e.preventDefault();
            save();
        });

        _$itemsTable.on('click', 'a.delete-item', function (e) {
            e.preventDefault();
            $this = $(this);
            var dtRow = $this.parents('tr');
            var table = _$itemsTable.DataTable();
            table.row(dtRow[0].rowIndex - 1).remove().draw(false);
            rearrange();
            computeTotal();
            abp.notify.warn('Item #' + dtRow[0].rowIndex  + ' deleted!', 'Warning');

        });
        _$itemsTable.on('click', 'a.edit-item', function (e) {
            e.preventDefault();
            var $itemno = $(this).attr("data-itemno");
            var $productid = $(this).attr("data-id");
            var $qty = $(this).attr("data-qty");
            var $unitid = $(this).attr("data-unitid");
            var $perdescription = $(this).attr("data-perdesc");
            var $price = $(this).attr("data-price");
            $('#EProductId').val($productid);
            $('#EIndexNo').text($itemno);
            editgetproduct();
            editgetproductunits($unitid);
            editgetproductstock();
            $('#EQuantity').val($qty);
            $('#EPrice').val($price);
            $('#EPerDescription').val($perdescription);
        });
        function getAddQuantity(productId,indexNo) {
            var table = _$itemsTable.DataTable();
            var form_data = table.rows().data();
            var f = form_data;
            var retval = 0;
            for (var i = 0; f.length > i; i++) {
                var itemid = f[i][6];
                var itemqty = parseFloat(f[i][8]);
                if (productId === itemid && indexNo !== i) {
                    retval = retval + itemqty;
                }
            }
            return retval;
        }
        // Delete product unit record
        $('#AddItemButton').click(function (e) {
            e.preventDefault();
            addnewitem();
        });
        $('#UpdatetemButton').click(function (e) {
            e.preventDefault();
            var $stocks = $('#EStocks').val();
            var $indexno = parseInt($('#EIndexNo').text()) - 1;
            var $productid = $('#EProductId').val();
            var $productcode = $('#EProductCode').val();
            var $productname = $('#EProductName').val();
            var $unitid = $('#EUnits').val();
            var $unit = $("#EUnits option:selected").html();
            var $quantity = $('#EQuantity').val();
            var $price = $('#EPrice').val();
            var $perdescription = $('#EPerDescription').val();

            if ($('#EntryTypes').val() === '0') {
                abp.notify.error('Select your Transaction Type.', 'Warning');
                return;
            }
            if ($('#DefaultSources').val() === '0') {
                abp.notify.error('Select your Source Warehouse.', 'Warning');
                return;
            }

            if ($productid === '' || $productcode === '' || $productname === '' || $quantity === '' || $price === '' || $stocks === '') { $('#ItemEditModal').modal('hide'); return; }

            var stocks = parseFloat($stocks);
            var quantity = parseFloat($quantity);
            var quantityadded = getAddQuantity($productid, $indexno);
            if ($('#EntryTypes').val() === '1' || $('#EntryTypes').val() === '3') {
                if (quantity + quantityadded > stocks) {
                    abp.notify.error('Not enough stocks.', 'Warning');
                    return;
                }
            }
            var price = parseFloat($price.replace(',', ''));
            var total = price * quantity;

            var table = _$itemsTable.DataTable();
            var temp = table.row($indexno).data();
            temp[1] = '<a href="#" class="btn-link">' + $productcode + '</a><br /><small><span class="text-muted">' + $productname + '</span></small>';
            temp[2] = '<span class="text-muted">' + $quantity + '</span>|<span class="text-muted">' + $unit + '</span>';
            temp[3] = price;
            temp[4] = total;
            temp[5] = '<a id="edit-item" class="edit-item" title="edit" href="#" data-toggle="modal" data-target="#ItemEditModal" data-itemno="' + $('#EIndexNo').text() + '"  data-id="' + $productid + '" data-unitid="' + $unitid + '" data-perdesc="' + $perdescription + '" data-qty="' + $quantity + '" data-price="' + price + '" ><i class="fa fa-edit"></i></a> | <a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
            temp[6] = $productid;
            temp[7] = $perdescription;
            temp[8] = $quantity;
            temp[9] = $unitid;
            $('#ItemsTable').dataTable().fnUpdate(temp, $indexno, undefined, false);
            $('#ItemEditModal').modal('hide');
            computeTotal();
            abp.notify.info('Item #' + $('#EIndexNo').text() + ' updated!', 'Info');
        });
        //Datatable Add

        function rearrange() {
            var table = _$itemsTable.DataTable();
            var form_data = table.rows().data();
            var f = form_data;
            for (var i = 0; f.length > i; i++) {
                var temp = table.row(i).data();
                var itemno = i + 1;

                var $productid = f[i][6];
                var $unitid = f[i][9];
                var $perdescription = f[i][7];
                var $quantity = f[i][8];
                var $price = f[i][3];

                temp[0] = itemno;
                temp[5] = '<a id="edit-item" class="edit-item" title="edit" href="#" data-toggle="modal" data-target="#ItemEditModal" data-itemno="' + itemno + '"  data-id="' + $productid + '" data-unitid="' + $unitid + '" data-perdesc="' + $perdescription + '" data-qty="' + $quantity + '" data-price="' + $price + '" ><i class="fa fa-edit"></i></a> | <a id="delete-item" class="delete-item" title="delete" href="#" ><i class="fa fa-trash"></i></a>';
                $('#ItemsTable').dataTable().fnUpdate(temp, i, undefined, false);
            }
        }
    });
})();
