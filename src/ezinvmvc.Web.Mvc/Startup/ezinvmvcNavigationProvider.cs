﻿using Abp.Application.Navigation;
using Abp.Localization;
using ezinvmvc.Authorization;

namespace ezinvmvc.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class ezinvmvcNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem
                (
                    new MenuItemDefinition(
                        "Home",
                        new FixedLocalizableString("Home"),
                        icon: "home",
                        url: "Home")
                        
                )
                .AddItem
                (
                    new MenuItemDefinition
                    (
                        "FolderUsers",
                        new FixedLocalizableString("Folder"),
                        icon: "folder-o",
                        url: "FolderUsers",
                        requiredPermissionName: PermissionNames.Pages_Folder_Users
                    )
                ).AddItem
                (
                    new MenuItemDefinition
                    (
                        "SharedUser",
                        new FixedLocalizableString("User Shared"),
                        icon: "share-alt-square",
                        url: "SharedUsers",
                        requiredPermissionName: PermissionNames.Pages_Shared_Users
                    )
                ).AddItem
                (
                    new MenuItemDefinition
                    (
                        "SharedRole",
                        new FixedLocalizableString("Role Shared"),
                        icon: "users",
                        url: "SharedRole",
                        requiredPermissionName: PermissionNames.Pages_Shared_Roles
                    )
                )
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Purchases",
                ////        L("Purchases"),
                ////        icon: "cart-plus")
                ////          .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        "Orders",
                ////        new FixedLocalizableString("Orders"),
                ////        url: "Orders",
                ////        icon: "fa-truck",
                ////        requiredPermissionName: PermissionNames.Pages_Purchase_Orders)
                ////    )
                ////)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "CRM",
                ////        new FixedLocalizableString("CRM"),
                ////        icon: "users")
                ////        .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Leads,
                ////        L("Leads"),
                ////        url: "Leads",
                ////        icon: "users",
                ////        requiredPermissionName: PermissionNames.CRM_Leads)
                ////    )
                ////     .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.RFQ,
                ////        new FixedLocalizableString("RFQ"),
                ////        url: "RFQ",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Rfq
                ////        )
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Quotations"),
                ////        url: "Quotations",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Quotations)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Assignment,
                ////        new FixedLocalizableString("Assignment List"),
                ////        url: "Assignment",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Assignment
                ////        )
                ////    )
                ////)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Sales",
                ////        L("Sales"),
                ////        icon: "tags")
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Sales_Orders,
                ////        new FixedLocalizableString("Orders"),
                ////        url: "SalesOrders",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Sales_Orders)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Sales_Invoice,
                ////        new FixedLocalizableString("Invoices"),
                ////        url: "SalesInvoice",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Sales_Invoice)
                ////    )

                ////)
                //// .AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Accounting",
                ////         new FixedLocalizableString("Accounting"),
                ////        icon: "money")
                ////         .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Collections,
                ////        new FixedLocalizableString("Collections"),
                ////        url: "Collections",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Collections)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Journal Entry"),
                ////        url: "JournalEntry",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Journal_Entry)
                ////    ).AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Accounts_Receivable,
                ////        new FixedLocalizableString("Receivables"),
                ////        url: "AccountsReceivable",
                ////        icon: "book",
                ////        requiredPermissionName: PermissionNames.Pages_Accounts_Receivable)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.General_Ledger,
                ////        new FixedLocalizableString("General Ledger"),
                ////        url: "GeneralLedger",
                ////        icon: "book",
                ////        requiredPermissionName: PermissionNames.Pages_General_Ledger)
                ////    )
                ////     .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Request For Payment"),
                ////        url: "RFP",
                ////        icon: "file-text-o",
                ////        requiredPermissionName: PermissionNames.Pages_Rfp)
                ////    )

                ////)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Stocks",
                ////        L("Stocks"),
                ////        icon: "cubes")
                ////    .AddItem
                ////    (
                ////            new MenuItemDefinition(
                ////            PageNames.Roles,
                ////            new FixedLocalizableString("Stock Entry"),
                ////            url: "StockEntry",
                ////            icon: "paper-plane-o",
                ////            requiredPermissionName: PermissionNames.Pages_Stock_Entry)
                ////    )
                ////     .AddItem
                ////    (
                ////            new MenuItemDefinition(
                ////            PageNames.Stock_Card,
                ////            new FixedLocalizableString("Stock Card"),
                ////            url: "StockCard",
                ////            icon: "bar-chart-o",
                ////            requiredPermissionName: PermissionNames.Pages_Stock_Card)
                ////    )
                ////      .AddItem
                ////    (
                ////            new MenuItemDefinition(
                ////            PageNames.Stock_Summary,
                ////            new FixedLocalizableString("Stock Summary"),
                ////            url: "StockSummary",
                ////            icon: "bar-chart-o",
                ////            requiredPermissionName: PermissionNames.Pages_Stock_Summary)
                ////    )
                ////)
                //.AddItem
                //(
                //    new MenuItemDefinition("Recruitment", new FixedLocalizableString("Recruitment"), icon: "male")
                //    .AddItem
                //    (
                //        new MenuItemDefinition(
                //        PageNames.Roles,
                //        new FixedLocalizableString("Job Description"),
                //        url: "JobDescription",
                //        icon: "male",
                //        requiredPermissionName: PermissionNames.Pages_JobDescription)
                //    )
                //    .AddItem
                //    (
                //        new MenuItemDefinition(
                //        PageNames.Roles,
                //        new FixedLocalizableString("Interview"),
                //        url: "Recruitment",
                //        icon: "male",
                //        requiredPermissionName: PermissionNames.Pages_EmployeeApp)
                //    )
                //    .AddItem
                //    (
                //        new MenuItemDefinition(
                //        PageNames.Roles,
                //        new FixedLocalizableString("Examination"),
                //        url: "Recruitment",
                //        icon: "male",
                //        requiredPermissionName: PermissionNames.Pages_EmployeeApp)
                //    )
                //    .AddItem
                //    (
                //        new MenuItemDefinition(
                //        PageNames.Roles,
                //        new FixedLocalizableString("Applicant Tracker"),
                //        url: "Recruitment",
                //        icon: "male",
                //        requiredPermissionName: PermissionNames.Pages_EmployeeApp)
                //    )
                //    .AddItem
                //    (
                //        new MenuItemDefinition(
                //        PageNames.Roles,
                //        new FixedLocalizableString("Evaluation"),
                //        url: "Recruitment",
                //        icon: "male",
                //        requiredPermissionName: PermissionNames.Pages_EmployeeApp)
                //    )
                //)

                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Human Resource",
                ////        new FixedLocalizableString("Human Resource"),
                ////        icon: "user")
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Employees"),
                ////        url: "Employees",
                ////        icon: "fa-puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Master_Employees)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Accounts Ability"),
                ////        url: "EmployeesAccountsAbility",
                ////        icon: "fa-puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Accounts_Ability)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Employee Records"),
                ////        url: "EmployeeRecords",
                ////        icon: "fa-puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Employee_Records)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Employee Rates"),
                ////        url: "EmployeePayrollRate",
                ////        icon: "fa-puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Employee_Salary_Rates)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Allowances"),
                ////        url: "EmployeeAllowance",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Employee_Allowance)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("OT Rates"),
                ////        url: "EmployeeOTRates",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Employee_OTRates)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Employee Loan"),
                ////        url: "EmployeeLoans",
                ////        icon: "fa fa-puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Employee_Loan)
                ////    )
                ////)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Timekeeping",
                ////        new FixedLocalizableString("Timekeeping"),
                ////        icon: "clock-o")
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Attendance"),
                ////        url: "Attendance",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Attendance)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Emp Attendance"),
                ////        url: "EmployeeBioAtt",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_EmpAttendance)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Att Approver"),
                ////        url: "EmpAttApprover",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_AttendanceApprover)
                ////    )
                ////)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Payroll",
                ////        new FixedLocalizableString("Payroll"),
                ////        icon: "calendar-o")
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Payroll"),
                ////        url: "Payroll",
                ////        icon: "puzzle-piece",
                ////        requiredPermissionName: PermissionNames.Pages_Payroll)
                ////    )
                ////)
                //.AddItem
                //(
                //    new MenuItemDefinition(
                //        "Reports",
                //        L("Reports"),
                //        icon: "bar-chart-o")
                //)
                ////.AddItem
                ////(
                ////    new MenuItemDefinition(
                ////        "Master Data",
                ////        L("MasterData"),
                ////        icon: "cogs")
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        L("Users"),
                ////        url: "Users",
                ////        icon: "users",
                ////        requiredPermissionName: PermissionNames.Pages_Users)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        L("Roles"),
                ////        url: "Roles",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Master_Roles)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Warehouses,
                ////        new FixedLocalizableString("Warehouse"),
                ////        url: "Warehouse",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Master_Warehouse)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Clients,
                ////        new FixedLocalizableString("Clients"),
                ////        url: "Clients",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Master_Clients)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Vendors,
                ////        new FixedLocalizableString("Vendors"),
                ////        url: "Vendors",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Master_Vendors)
                ////    )
                ////     .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Products"),
                ////        url: "Products",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Master_Products)
                ////    )
                ////    .AddItem
                ////    (
                ////        new MenuItemDefinition(
                ////        PageNames.Roles,
                ////        new FixedLocalizableString("Overtime Table"),
                ////        url: "OTRate",
                ////        icon: "user-circle-o",
                ////        requiredPermissionName: PermissionNames.Pages_Overtime_Rates)
                ////    )

                ////)
                .AddItem
                (
                    new MenuItemDefinition(
                        PageNames.Users,
                        L("Users"),
                        url: "Users",
                        icon: "user-plus",
                        requiredPermissionName: PermissionNames.Pages_Users)

                )

                 .AddItem
                (
                   new MenuItemDefinition(
                        PageNames.Roles,
                        L("Roles"),
                        url: "Roles",
                        icon: "user-circle-o",
                        requiredPermissionName: PermissionNames.Master_Roles)

                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ezinvmvcConsts.LocalizationSourceName);
        }
    }
}
