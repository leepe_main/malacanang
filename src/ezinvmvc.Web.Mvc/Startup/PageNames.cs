﻿namespace ezinvmvc.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Users = "Users";
        public const string Roles = "Roles"; 
        public const string Warehouses = "Warehouses";

        public const string Clients = "Clients";
        public const string Clients_Create = "Clients.Create";
        public const string Clients_Edit = "Clients.Edit";
        public const string Clients_Details = "Clients.Details";

        public const string Leads = "Leads";
        public const string Leads_Create = "Leads.Create";
        public const string Leads_Edit = "Leads.Edit";
        public const string Leads_Details = "Leads.Details";

        public const string Vendors = "Vendors";
        public const string Products = "Products";
        public const string Products_Create = "Products.Create";
        public const string Products_Edit = "Products.Edit";

        public const string Purchase_Orders = "Orders";
        
        public const string Sales = "Sales";
        public const string Sales_Orders = "Sales.Orders";
        public const string Sales_Orders_Create = "Sales.Order.Create";
        public const string Sales_Orders_Edit = "Sales.Order.Edit";

        public const string Sales_Invoice = "Sales.Invoice";
        public const string Sales_Invoice_Create = "Sales.Invoice.Create";
        public const string Sales_Invoice_Edit = "Sales.Invoice.Edit";

        public const string Journal = "Journal";
        public const string Journal_Entry = "Journal.Entry";
        public const string Journal_Entry_Create = "Journal.Entry.Create";
        public const string Journal_Entry_Edit = "Journal.Entry.Edit";
        public const string Journal_Entry_Details = "Journal.Entry.Details";


        public const string RFQ = "RFQ";
        public const string RFQ_Create = "RFQ.Create";
        public const string RFQ_Edit = "RFQ.Edit";
        public const string RFQ_Details = "RFQ.Details";

        public const string Assignment = "Assignment";
        public const string Assignment_Create = "Assignment.Create";
        public const string Assignment_Edit = "Assignment.Edit";
        public const string Assignment_Details = "Assignment.Details";

        public const string JobDescription = "JobDescription";

        public const string EmployeeApp = "EmployeeApp";

        public const string Employees = "Employees";
        public const string Employees_Edit = "Employees.Edit";
        public const string Employees_Details = "Employees.Details";
        public const string Accounts_Ability = "Accounts.Ability";
        public const string Employees_Records = "Employees.Records";
        public const string Employee_Salary_Rates = "Employee.Salary.Rates";
        public const string Employee_Allowance = "Employee.Allowance";

        public const string Employee_OTRates = "Employee.OTRate";
        public const string Overtime_Rates = "Overtime.Rates";
        public const string Employee_Loans = "Employee.Loans";
        public const string Attendance = "Attendance";
        public const string EmployeeAttendance = "Employee.Attendance";
        public const string AttendanceApprover = "AttendanceApprover";
        public const string Payroll = "Payroll";

        public const string Quotations = "Quotations";
        public const string Quotations_Create = "Quotations.Create";
        public const string Quotations_Edit = "Quotations.Edit";
        public const string Quotations_Details = "Quotations.Details";

        public const string Stock_Entry = "Stock.Entry";
        public const string Stock_Entry_Create = "Stock.Entry.Create";
        public const string Stock_Entry_Edit = "Stock.Entry.Edit";
        public const string Stock_Entry_Items = "Stock.Entry.Items";

        public const string Stock_Card = "Stock.Card";
        public const string Stock_Summary = "Stock.Summary";

        public const string General_Ledger = "General.Ledger";
        public const string Accounts_Receivable = "Accounts.Receivable";

        public const string Collections = "Collections";
        public const string Collections_Create = "Collections.Create";
        public const string Collections_Edit = "Collections.Edit";
        public const string Collections_Items = "Collections.Items";
        
        public const string RFP = "RFP";
        public const string RFP_Create = "RFP.Create";
        public const string RFP_Edit = "RFP.Edit";
        public const string RFP_Details = "RFP.Details";

        public const string FolderUsers = "FolderUsers";
        public const string SharedUser = "SharedUser";
        public const string SharedRole = "SharedRole";
        public const string RegisterAccount = "RegisterAccount";
    }
}